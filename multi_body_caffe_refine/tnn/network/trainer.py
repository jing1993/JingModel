from __future__ import print_function

import os
import datetime
import cv2
import numpy as np
from collections import OrderedDict
import shutil

import torch
import torch.nn as nn

from tnn.utils.timer import Timer
from tnn.utils.path import mkdir
import tnn.utils.meter as meter_utils
import tnn.network.net_utils as net_utils
import tnn.network.visualization as visualization
from tnn.datasets.gta_data.visualization import visualization as img_visualization
from torch.optim.lr_scheduler import ReduceLROnPlateau

try:
    import pycrayon
    from tnn.utils.crayon import CrayonClient
except ImportError:
    CrayonClient = None


def get_learning_rates(optimizer):
    lrs = [pg['lr'] for pg in optimizer.param_groups]
    lrs = np.asarray(lrs, dtype=np.float)
    return lrs


def adjust_learning_rates(optimizer, learning_rates):
    print('adjust learning rate from {} to {}'.format(get_learning_rates(optimizer), learning_rates))

    if len(learning_rates) != len(optimizer.param_groups):
        raise ValueError("expected {} lrs, got {}".format(len(optimizer.param_groups), len(learning_rates)))

    for i, param_group in enumerate(optimizer.param_groups):
        param_group['lr'] = learning_rates[i]


class Trainer(object):

    class TrainParams(object):
        task = None
        exp_name = 'Exp_name'
        # init_lr = 0.01
        lr_decay_epoch = {5, 10, 20}
        lr_decay = 0.1
        use_auto_lr = None

        batch_size = 32
        max_epoch = 30
        optimizer = None
        optimizers = None
        multitask_weights = None
        gpus = [0]
        save_dir = None
        ckpt = None
        zero_epoch = False
        ignore_opt_state = False
        re_init = False

        val_nbatch = 2
        val_nbatch_epoch = None

        save_freq = None
        save_epoch_freq = 1

        print_freq = 20
        tensorboard_freq = 500
        tensorboard_hostname = None

        do_vis = False
        vis_params = {
            'server': 'http://localhost',
            'port': 8097,
            'env': 'main'
        }

        vis_freq = 100
        do_dist = False
        do_cum = False
        do_info = True
        do_mean_weights = False

        do_net_graph = False

        # hooks
        on_start_epoch_hooks = []
        on_end_epoch_hooks = []

    def __init__(self, model, train_params, batch_processor, train_data,
                 val_data=None, use_auto_lr=True):
        assert isinstance(train_params, self.TrainParams)
        self.params = train_params
        self.train_data = train_data
        self.val_data = val_data
        self.val_stream = self.val_data.get_stream() if self.val_data is not None else None

        self.batch_processor = batch_processor
        self.batch_per_epoch = len(train_data)

        # set CUDA_VISIBLE_DEVICES=gpus
        gpus = ','.join([str(x) for x in self.params.gpus])
        print(('Set CUDA_VISIBLE_DEVICES to {}, please do not run .cuda(params.gpus[0]) before this line'.format(gpus)))
        os.environ['CUDA_VISIBLE_DEVICES'] = gpus
        self.params.gpus = tuple(range(len(self.params.gpus)))

        self.last_epoch = 0

        assert self.params.optimizers is None or self.params.optimizer is None, \
            'set params.optimizers and params.optimizer at the same time'

        self.multitask = self.params.optimizers is not None
        self.optimizers = self.params.optimizers if self.multitask else [self.params.optimizer]
        self.multitask_weights = self.params.multitask_weights

        assert self.optimizers is not None and self.optimizers[0] is not None, 'optimizers is not determined'

        self.use_auto_lr = use_auto_lr if self.params.use_auto_lr is None else self.params.use_auto_lr
        self.auto_lr_schedulers = [ReduceLROnPlateau(opt, 'min', factor=0.8, patience=10,
                                                     verbose=True, cooldown=5.) for opt in self.optimizers]

        self.log_values = OrderedDict()
        self.visualizer = visualization.Visualize(model, **self.params.vis_params)
        self.vis_win_names = set()
        self.batch_timer = Timer()
        self.data_timer = Timer()

        mkdir(self.params.save_dir)

        # load model
        self.model = model
        ckpt = self.params.ckpt
        if ckpt is None:
            ckpts = [fname for fname in os.listdir(self.params.save_dir) if os.path.splitext(fname)[-1] == '.h5']
            ckpt = os.path.join(
                self.params.save_dir, sorted(ckpts, key=lambda name: int(os.path.splitext(name)[0].split('_')[-1]))[-1]
            ) if len(ckpts) > 0 else None
        if ckpt is None and self.params.re_init:

            self.model = nn.DataParallel(self.model, device_ids=self.params.gpus)
            self.model = self.model.cuda(device_id=self.params.gpus[0])

            pretrained_dict = torch.load('ckpt_caffe')
            new_state_dict = OrderedDict()
            total_pre_keys = pretrained_dict.keys()


            for key in total_pre_keys:
                if key in self.model.state_dict().keys():
                    new_state_dict[key] = pretrained_dict[key]


            state = self.model.state_dict()
            state.update(new_state_dict)
            self.model.load_state_dict(state)
            print("re-init model weight")

        elif ckpt is not None:
            self._load_ckpt(ckpt)

        self.model = nn.DataParallel(self.model, device_ids=self.params.gpus)
        self.model = self.model.cuda(device_id=self.params.gpus[0])
        self.model.train()

        self.graph_plotted = False
        # tensorboard
        self.tf_exp = None
        if self.params.tensorboard_hostname is not None and CrayonClient is not None:
            cc = CrayonClient(hostname=self.params.tensorboard_hostname)
            exp_name = self.params.exp_name
            try:
                if self.last_epoch == 0:
                    cc.remove_experiment(exp_name)
                    exp = cc.create_experiment(exp_name)
                else:
                    exp = cc.open_experiment(exp_name)
            except ValueError:
                exp = cc.create_experiment(exp_name)
            self.tf_exp = exp

    def train(self):
        best_loss = np.inf
        for epoch in range(self.last_epoch, self.params.max_epoch):
            for fun in self.params.on_start_epoch_hooks:
                fun(self)

            train_loss = self._train_epoch()

            for fun in self.params.on_end_epoch_hooks:
                fun(self)

            # save model
            if (self.last_epoch % self.params.save_epoch_freq == 0) or (self.last_epoch == self.params.max_epoch - 1):
                save_name = 'ckpt_{}.h5'.format(self.last_epoch)
                save_to = os.path.join(self.params.save_dir, save_name)
                self._save_ckpt(save_to)

                # find best model
                if self.params.val_nbatch_epoch is not None and self.params.val_nbatch_epoch > 0\
                        and self.val_stream is not None:
                    val_loss = self._val_epoch(self.params.val_nbatch_epoch)
                    if val_loss < best_loss:
                        best_loss = val_loss
                        best_file = os.path.join(self.params.save_dir, 'ckpt_{}_{:.4f}.h5.best'.format(self.last_epoch, best_loss))
                        shutil.copyfile(save_to, best_file)
                        print('copy to {}'.format(best_file))

                    # if use auto tune learning rate
                    if self.use_auto_lr:
                        for lrs in self.auto_lr_schedulers:
                            lrs.step(val_loss)

            if not self.use_auto_lr and self.last_epoch in self.params.lr_decay_epoch:
                # else use step decay learning rate
                for i, opts in enumerate(self.optimizers):
                    learning_rate = get_learning_rates(opts)
                    learning_rate *= self.params.lr_decay
                    adjust_learning_rates(opts, learning_rate)

    def _save_ckpt(self, save_to):
        model = self.model.module if isinstance(self.model, nn.DataParallel) else self.model
        net_utils.save_net(save_to, model, epoch=self.last_epoch, optimizers=self.optimizers, rm_prev_opt=True)
        print('save to {}'.format(save_to))

    def _load_ckpt(self, ckpt):
        epoch, state_dicts = net_utils.load_net(ckpt, self.model, load_state_dict=True)
        if not self.params.ignore_opt_state and not self.params.zero_epoch and epoch >= 0:
            self.last_epoch = epoch
            print('set last epoch to {}'.format(self.last_epoch))
            if state_dicts is not None:
                # set gpu
                if len(self.optimizers) != len(state_dicts):
                    print('Number of optimizers ({}) is different from checkpoint ({}), won\'t load optimizer state!'.format(len(self.optimizers), len(state_dicts)))
                for i, opt in enumerate(self.optimizers):
                    opt.load_state_dict(state_dicts[i])
                    net_utils.set_optimizer_state_devices(opt.state, self.params.gpus[0])
                print('load optimizer state from checkpoint, new learning rate: {}'.format(get_learning_rates(self.optimizers[0])))

    def _process_log(self, src_dict, dest_dict):
        for k, v in src_dict.items():
            if isinstance(v, np.ndarray):
                assert v.ndim == 2 or (v.ndim == 3 and (v.shape[2] == 3 or v.shape[2] == 1)), \
                    '{}:, expected images of HxW or HxWx3, but got {}'.format(k, v.shape)
                dest_dict[k] = np.expand_dims(v, 0) if v.ndim == 2 else np.transpose(v, [2, 0, 1])

            elif hasattr(v, '__iter__'):
                # a set of scale
                for i, x in enumerate(v):
                    sub_k = '{}_{}'.format(k, i)
                    dest_dict.setdefault(sub_k, meter_utils.AverageValueMeter())
                    try:
                        dest_dict[sub_k].add(float(x))
                    except TypeError:
                        print(sub_k)
                        raise TypeError
            else:
                dest_dict.setdefault(k, meter_utils.AverageValueMeter())
                dest_dict[k].add(float(v))

    def _print_log(self, step, log_values, title=None):
        text_file = open("loss_record.txt", "a")
        
        if title is None:
            print('epoch {}[{}/{}]'.format(self.last_epoch, step, self.batch_per_epoch), end='')
            text_file.write('epoch {}[{}/{}]'.format(self.last_epoch, step, self.batch_per_epoch))
        else:
            print(title, end='')
            text_file.write(title)
        i = 0
        global_step = step + (self.last_epoch-1) * self.batch_per_epoch
        for k, v in log_values.items():
            if isinstance(v, meter_utils.AverageValueMeter):
                if i % 1 == 0 and i > 0:
                    print('\n\t\t', end='')
                    text_file.write('\n\t\t')
                else:
                    print(', ', end='')
                    text_file.write(', ')
                mean, std = v.value()
                print('{}: {:.4f}'.format(k, mean), end='')
                text_file.write('{}: {:.4f}'.format(k, mean))
                i += 1

                if self.params.do_vis:
                    win = '{}_{}'.format(title, k)
                    update = 'append' if win in self.vis_win_names else None
                    self.visualizer.vis.line(np.array([mean], dtype=np.float),
                                             np.array([global_step], dtype=int),
                                             win=win, update=update)
                    self.vis_win_names.add(win)

            elif isinstance(v, np.ndarray) and self.params.do_vis:
                win = '{}_{}'.format(title, k)
                if v.shape[0] == 1:
                    # plot as heatmap
                    ht = np.flipud(v[0])
                    self.visualizer.vis.heatmap(ht, win=win, opts={'colormap': 'Jet'})
                elif v.shape[0] == 3:
                    self.visualizer.vis.image(v, win=win)

        if title is None:
            # print time
            data_time = self.data_timer.duration
            batch_time = self.batch_timer.duration
            print(' ({:.2f}/{:.2f}s, fps:{:.1f}, rest: {})'.format(data_time, batch_time, self.params.batch_size/batch_time,
                                        str(datetime.timedelta(seconds=int((self.batch_per_epoch - step) * batch_time)))))
            text_file.write(' ({:.2f}/{:.2f}s, fps:{:.1f}, rest: {})\n'.format(data_time, batch_time, self.params.batch_size/batch_time,
                                        str(datetime.timedelta(seconds=int((self.batch_per_epoch - step) * batch_time)))))
            self.batch_timer.clear()
            self.data_timer.clear()
        else:
            print()
            text_file.write('\n')

        text_file.close()

    def _tensorboard_log(self, step, log_values, postfix=''):
        if self.tf_exp is None:
            return
        step = step + (self.last_epoch - 1) * self.batch_per_epoch
        for k, v in log_values.items():
            if isinstance(v, meter_utils.AverageValueMeter):
                self.tf_exp.add_scalar_value(k + postfix, v.value()[0], step=step)

    def _reset_log(self, log_values):
        for k, v in log_values.items():
            if isinstance(v, meter_utils.AverageValueMeter):
                v.reset()

    def _train_epoch(self):
        self.last_epoch += 1
        print('start epoch: {}'.format(self.last_epoch))

        self.batch_timer.clear()
        self.data_timer.clear()
        self.batch_timer.tic()
        self.data_timer.tic()
        total_loss = meter_utils.AverageValueMeter()

        for step, batch in enumerate(self.train_data):

            inputs, gts, _ = self.batch_processor(self, batch)

            self.data_timer.toc()

            # forward
            output, saved_for_loss = self.model(*inputs)

            if self.multitask:
                loss, saved_for_log =  \
                    self.model.module.build_loss(
                        saved_for_loss,
                        multitask=True,
                        multitask_weights=self.multitask_weights, *gts)
            else:
                loss, saved_for_log =  \
                    self.model.module.build_loss(saved_for_loss, *gts)

            # plot network arch
            if not self.graph_plotted and self.params.do_net_graph:
                graph_fname = os.path.join(self.params.save_dir,
                                           'network_graph.dot')
                graph_fname = net_utils.plot_graph(loss, graph_fname)

                if self.params.do_vis:
                    graph = cv2.imread(graph_fname)
                    self.visualizer.vis.image(np.transpose(graph, (2, 0, 1)),
                                              win='network_graph')
                self.graph_plotted = True

            # save log
            self._process_log(saved_for_log, self.log_values)

            # backward
            for optimizer in self.optimizers:
                optimizer.zero_grad()

            loss.backward()
            total_loss.add(loss.data[0])
            for optimizer in self.optimizers:
                optimizer.step()

            self.batch_timer.toc()

            # print log
            reset = False
            if step % self.params.print_freq == 0:
                self.params.task = 'body'
                #if step % 300 == 0:
                img_visualization(batch, output, self.last_epoch, step, task=self.params.task)
                self._print_log(step, self.log_values)
                reset = True

            if self.tf_exp is not None and step % self.params.tensorboard_freq == 0:
                self._tensorboard_log(step, self.log_values)
                reset = True

            #visualization
            if self.params.do_vis and step % self.params.vis_freq == 0:
                    self.visualizer.full_plot(
                        plot_stats=self.params.do_info,
                        plot_dist=self.params.do_dist,
                        plot_cumu=self.params.do_cum,
                        plot_mw=self.params.do_mean_weights,
                        lr=get_learning_rates(self.optimizers[0]))

            # validation
            if step % self.params.tensorboard_freq == 0:
                val_logs = self._val_nbatch(self.params.val_nbatch, step=step)
                self._tensorboard_log(step, val_logs, postfix='_val')

            if self.params.save_freq is not None and step % self.params.save_freq == 0 and step > 0:
                save_to = os.path.join(self.params.save_dir,
                                       'ckpt_{}.h5.ckpt'.format((self.last_epoch - 1) * self.batch_per_epoch + step))
                self._save_ckpt(save_to)

            if reset:
                self._reset_log(self.log_values)

            self.data_timer.tic()
            self.batch_timer.tic()

        total_loss, std = total_loss.value()
        return total_loss

    def _val_nbatch(self, n_batch, step=0):
        if self.val_stream is None:
            return

        self.model.eval()
        logs = OrderedDict()
        for i in range(self.params.val_nbatch):
            batch = next(self.val_stream)
            inputs, gts, _ = self.batch_processor(self, batch)

            output, saved_for_loss = self.model(*inputs)
            if self.multitask:
                loss, saved_for_log =  \
                    self.model.module.build_loss(
                        saved_for_loss,
                        multitask=True,
                        multitask_weights=self.multitask_weights, *gts)
            else:
                loss, saved_for_log =  \
                    self.model.module.build_loss(saved_for_loss, *gts)

            self._process_log(saved_for_log, logs)
        self._print_log(step, logs, title='Validation')

        self.model.train()
        return logs

    def _val_epoch(self, n_batch):
        self.model.eval()
        sum_loss = meter_utils.AverageValueMeter()
        print('val on validation set...')
        for step, batch in enumerate(self.val_data):
            if step > n_batch:
                break
            if step % self.params.print_freq == 0:
                print('[{}/{}]'.format(step, min(n_batch, len(self.val_data))))
            inputs, gts, _ = self.batch_processor(self, batch)
            output, saved_for_loss = self.model(*inputs)
            if self.multitask:
                loss, saved_for_log =  \
                    self.model.module.build_loss(
                        saved_for_loss,
                        multitask=True,
                        multitask_weights=self.multitask_weights, *gts)
            else:
                loss, saved_for_log =  \
                    self.model.module.build_loss(saved_for_loss, *gts)

            sum_loss.add(loss.data[0])

        mean, std = sum_loss.value()
        print('val: mean: {}, std: {}'.format(mean, std))
        self.model.train()
        return mean
