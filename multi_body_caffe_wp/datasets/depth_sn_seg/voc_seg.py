import os
import cv2
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
import utils.depth as depth_utils
import datasets.data_utils as data_utils


class VOCSeg(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=4, max_pts=6000, training=True):

        assert name in ('train', 'val'), name
        assert os.path.isdir(root), root

        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.max_pts = max_pts
        self.training = training
        self.seg_root = os.path.join(root, 'segmentations')
        self.im_root = os.path.join(root, 'images')

        self.im_names = os.listdir(self.im_root)
        num_samples = len(self.im_names)
        if name == 'train':
            self.index_list = range(0, 9 * num_samples // 10)
        else:
            self.index_list = range(9 * num_samples // 10, num_samples)

    def __len__(self):
        return len(self.index_list)

    def read_images(self, i):
        idx = self.index_list[i]
        im_name = self.im_names[idx]

        im = cv2.imread(os.path.join(self.im_root, im_name))
        assert im is not None, os.path.join(self.im_root, im_name)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        img_idx = im_name[:-4]

        seg = cv2.imread(os.path.join(self.seg_root, img_idx + '.png'), 0)
        assert seg is not None, os.path.join(self.seg_root, img_idx + '.png')
        seg = seg.astype(np.float32)
        seg[seg > 0] = 1

        seg_mask = np.ones(seg.shape, dtype=np.float32)

        return im, seg, seg_mask

    def __getitem__(self, i):

        im, seg, seg_mask = self.read_images(i)
        additions = (seg,)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 0.8
        max_scale = target_scale * 1.2
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 35,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': False
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        seg_mask = im_transform.apply_affine(seg_mask, *trans_params, pad_val=1)
        seg = additions[0]
        # image recolor
        im, seg = im_transform.imcv2_recolor(im, a=0.05, noise_scale=0.01, blur_ksize=1, blur_sigma=1, mask=seg)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # =========== end data augmentation ==============

        seg = cv2.resize(seg, tuple(self.out_size))
        seg_mask = cv2.resize(seg_mask, tuple(self.out_size))
        seg_mask[seg_mask > 0.99] = 1
        seg_mask[seg_mask <= 0.99] = 0

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        im_data = torch.from_numpy(im_data)
        seg_data = torch.from_numpy(seg.astype(np.float32))
        seg_mask = torch.from_numpy(seg_mask)

        # fake dwt data
        dwt_label = np.zeros_like(seg, dtype=np.int64)
        dwt_mask = np.zeros_like(seg, dtype=np.float32)
        dwt_label = torch.from_numpy(dwt_label)
        dwt_mask = torch.from_numpy(dwt_mask)

        # fake data
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        num_pts = 1
        ow, oh = self.out_size
        pts[0] = depth_utils.fake_pts(self.out_size)
        pts = torch.from_numpy(pts)

        depth_data = torch.zeros(oh, ow)
        sn_data = torch.zeros(3, oh, ow)
        sn_mask = torch.zeros(1, oh, ow)

        return im_data, depth_data, sn_data, seg_data, dwt_label, seg_mask, sn_mask, dwt_mask, pts, num_pts
        #
        # gts = torch.stack((depth_data, seg_data, seg_mask), 0) # 3xHxW
        # sn_gts = torch.cat((sn_data, sn_mask), 0)   # 4xHxW
        # gts = torch.cat((gts, sn_gts), 0) # 7xHxW
        # return im_data, gts, pts, num_pts