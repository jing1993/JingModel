import os

import cv2
from scipy.misc import imread
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
import utils.depth as depth_utils
import datasets.data_utils as data_utils

"""
List of cityscapes labels:

                     name |  id | trainId |       category | categoryId | hasInstances | ignoreInEval
    --------------------------------------------------------------------------------------------------
                unlabeled |   0 |     255 |           void |          0 |            0 |            1
              ego vehicle |   1 |     255 |           void |          0 |            0 |            1
     rectification border |   2 |     255 |           void |          0 |            0 |            1
               out of roi |   3 |     255 |           void |          0 |            0 |            1
                   static |   4 |     255 |           void |          0 |            0 |            1
                  dynamic |   5 |     255 |           void |          0 |            0 |            1
                   ground |   6 |     255 |           void |          0 |            0 |            1
                     road |   7 |       0 |           flat |          1 |            0 |            0
                 sidewalk |   8 |       1 |           flat |          1 |            0 |            0
                  parking |   9 |     255 |           flat |          1 |            0 |            1
               rail track |  10 |     255 |           flat |          1 |            0 |            1
                 building |  11 |       2 |   construction |          2 |            0 |            0
                     wall |  12 |       3 |   construction |          2 |            0 |            0
                    fence |  13 |       4 |   construction |          2 |            0 |            0
               guard rail |  14 |     255 |   construction |          2 |            0 |            1
                   bridge |  15 |     255 |   construction |          2 |            0 |            1
                   tunnel |  16 |     255 |   construction |          2 |            0 |            1
                     pole |  17 |       5 |         object |          3 |            0 |            0
                polegroup |  18 |     255 |         object |          3 |            0 |            1
            traffic light |  19 |       6 |         object |          3 |            0 |            0
             traffic sign |  20 |       7 |         object |          3 |            0 |            0
               vegetation |  21 |       8 |         nature |          4 |            0 |            0
                  terrain |  22 |       9 |         nature |          4 |            0 |            0
                      sky |  23 |      10 |            sky |          5 |            0 |            0
                   person |  24 |      11 |          human |          6 |            1 |            0
                    rider |  25 |      12 |          human |          6 |            1 |            0
                      car |  26 |      13 |        vehicle |          7 |            1 |            0
                    truck |  27 |      14 |        vehicle |          7 |            1 |            0
                      bus |  28 |      15 |        vehicle |          7 |            1 |            0
                  caravan |  29 |     255 |        vehicle |          7 |            1 |            1
                  trailer |  30 |     255 |        vehicle |          7 |            1 |            1
                    train |  31 |      16 |        vehicle |          7 |            1 |            0
               motorcycle |  32 |      17 |        vehicle |          7 |            1 |            0
                  bicycle |  33 |      18 |        vehicle |          7 |            1 |            0
            license plate |  -1 |      -1 |        vehicle |          7 |            0 |            1
"""


class CityScapes(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, thresh1=0.1, thresh2=0.1, training=True, random_seed=None):
        self.root = root
        self.name = name
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training
        self.max_pts = max_pts
        self.thresh1 = thresh1
        self.thresh2 = thresh2
        self.feat_stride = feat_stride

        self.im_root = os.path.join(root, 'leftImg8bit', name)
        self.gt_root = os.path.join(root, 'gtFine', name)
        self.depth_root = os.path.join(root, 'disparity_trainvaltest/disparity', name)

        self.im_names = []
        for im_root, dirs, filenames in os.walk(self.im_root):
            for filename in filenames:
                if os.path.splitext(filename)[-1] == '.png':
                    self.im_names.append((os.path.split(im_root)[-1], filename))

        self.random_seed = random_seed

    def __len__(self):
        return len(self.im_names)

    def read_images(self, i):
        dir_name, im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, dir_name, im_name)
        gt_file = os.path.join(self.gt_root, dir_name, im_name.replace('leftImg8bit', 'gtFine_labelTrainIds'))
        depth_file = os.path.join(self.depth_root, dir_name, im_name.replace('leftImg8bit', 'disparity'))

        # image
        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        # depth
        depth = imread(depth_file)
        # depth mask
        depth_mask = np.zeros_like(depth, dtype=np.float32)
        depth_mask[depth > 0] = 1
        depth_mask[0:100, :] = 0
        depth = -(depth.astype(np.float32) - 1.) / 256.

        # label
        label = imread(gt_file, )
        label_mask = (label < 255) & (label >= 0)
        label_mask = label_mask.astype(np.float32)
        label[np.logical_not(label_mask)] = 19

        return im, depth, label, depth_mask, label_mask

    def __getitem__(self, i):
        im, depth, label, depth_mask, label_mask = self.read_images(i)
        additions = (depth, label, depth_mask, label_mask)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / min(im.shape[0:2])
        min_scale = target_scale * 0.8
        max_scale = target_scale * 1.2
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 25,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': True
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        depth, label, depth_mask, label_mask = additions
        # image recolor
        im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.015, blur_ksize=1, blur_sigma=1)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # scale depth
        scale = trans_params[0]
        depth = depth / scale

        # =========== end data augmentation ==============

        # resize to output
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        depth_mask = cv2.resize(depth_mask, tuple(self.out_size))
        label_mask = cv2.resize(label_mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        label = cv2.resize(label, tuple(self.out_size), interpolation=cv2.INTER_NEAREST)

        depth_mask[depth_mask > 0.9] = 1
        depth_mask[depth_mask <= 0.9] = 0

        label_mask[label_mask > 0.9] = 1
        label_mask[label_mask <= 0.9] = 0
        label[np.logical_not(label_mask)] = 19

        # normalize depth
        depth_t = depth[depth_mask > 0]
        dmin, dmax = depth_t.min(), depth_t.max()
        depth_n = (depth - dmin) / (dmax - dmin + 1e-5)
        depth_n = depth_n.astype(np.float32)

        # get surface normal from depth
        sn = depth_utils.depth_to_normal(depth_n, im_order=False).astype(np.float32)
        sn_mask = depth_mask[2:, 1:-1] * depth_mask[0:-2, 1:-1] * depth_mask[1:-1, 2:] * depth_mask[1:-1, 0:-2]
        sn_mask = np.pad(sn_mask, ((1, 1), (1, 1)), mode='constant')[np.newaxis, :, :]

        # to tensor
        im_data = torch.from_numpy(im_data)
        depth_data = torch.from_numpy(depth_n)
        label_data = torch.from_numpy(label)
        label_mask = torch.from_numpy(label_mask)
        sn_data = torch.from_numpy(sn)
        sn_mask = torch.from_numpy(sn_mask)

        # =============== sample points =============
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        sampled_pts = depth_utils.sample_pts(depth_n, depth_mask, max_pts=self.max_pts, dis_scale=60/self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts = len(sampled_pts)
        pts[:num_pts, :] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)

        return im_data, depth_data, sn_data, label_data, label_mask, sn_mask, pts, num_pts
