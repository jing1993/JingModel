#include "real_gemm_common.h"

int gemm_blas(BLASLONG bm, BLASLONG bn, BLASLONG bk, FLOAT alpha, FLOAT *ba, BLASLONG lda, FLOAT *bb, BLASLONG ldb, FLOAT *C, BLASLONG ldc) {
    BLASLONG i, j, k;
    FLOAT *C0, *C1, *ptrba, *ptrbb;
    FLOAT *a_;
    FLOAT res0, res1, res2, res3, load0, load1, load2, load3, load4, load5, load6, load7;
    for (j = 0; j < bn / 2; j += 1) {
        C0 = C;
        C1 = C0 + ldc;
        a_ = ba;
        for (i = 0; i < bm / 2; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            res2 = 0;
            res3 = 0;
            for (k = 0; k < bk / 4; k += 1) {
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + load0 * load1;

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + load2 * load1;

                load3 = ptrbb[0 + 1 * ldb];
                res2 = res2 + load0 * load3;
                res3 = res3 + load2 * load3;

                load4 = ptrba[lda * 1 + 0];
                load5 = ptrbb[1 + 0];
                res0 = res0 + load4 * load5;

                load6 = ptrba[lda * 1 + 1];
                res1 = res1 + load6 * load5;

                load7 = ptrbb[1 + 1 * ldb];
                res2 = res2 + load4 * load7;
                res3 = res3 + load6 * load7;

                load0 = ptrba[lda * 2 + 0];
                load1 = ptrbb[2 + 0];
                res0 = res0 + load0 * load1;

                load2 = ptrba[lda * 2 + 1];
                res1 = res1 + load2 * load1;

                load3 = ptrbb[2 + 1 * ldb];
                res2 = res2 + load0 * load3;
                res3 = res3 + load2 * load3;

                load4 = ptrba[lda * 3 + 0];
                load5 = ptrbb[3 + 0];
                res0 = res0 + load4 * load5;

                load6 = ptrba[lda * 3 + 1];
                res1 = res1 + load6 * load5;

                load7 = ptrbb[3 + 1 * ldb];
                res2 = res2 + load4 * load7;
                res3 = res3 + load6 * load7;

                ptrba = ptrba + 4 * lda;
                ptrbb = ptrbb + 4;
            }
            for (k = 0; k < (bk & 3); k += 1) {
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + load0 * load1;

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + load2 * load1;

                load3 = ptrbb[0 + 1 * ldb];
                res2 = res2 + load0 * load3;
                res3 = res3 + load2 * load3;

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }
            res0 = res0 * alpha;
            C0[0] = C0[0] + res0;
            res1 = res1 * alpha;
            C0[1] = C0[1] + res1;

            res2 = res2 * alpha;
            C1[0] = C1[0] + res2;
            res3 = res3 * alpha;
            C1[1] = C1[1] + res3;

            C0 = C0 + 2;
            C1 = C1 + 2;
            a_ = a_ + 2;
        }

        for (i = 0; i < (bm & 1); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0 * ldb];
                res0 = res0 + load0 * load1;

                load2 = ptrbb[0 + 1 * ldb];
                res1 = res1 + load0 * load2;

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }
            res0 = res0 * alpha;
            C0[0] = C0[0] + res0;
            res1 = res1 * alpha;
            C1[0] = C1[0] + res1;
            C0 = C0 + 1;
            C1 = C1 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 1);
        bb = bb + k;
        i = (ldc << 1);
        C = C + i;
    }
    for (j = 0; j < (bn & 1); j += 1) {
        C0 = C;
        a_ = ba;
        for (i = 0; i < bm / 2; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + load0 * load1;

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + load2 * load1;
                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }
            res0 = res0 * alpha;
            C0[0] = C0[0] + res0;
            res1 = res1 * alpha;
            C0[1] = C0[1] + res1;
            C0 = C0 + 2;
            a_ = a_ + 2;
        }

        for (i = 0; i < (bm & 1); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + load0 * load1;
                ptrba = ptrba + 1;
                ptrbb = ptrbb + 1;
            }
            res0 = res0 * alpha;
            C0[0] = C0[0] + res0;
            C0 = C0 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 0);
        bb = bb + k;
        C = C + ldc;
    }
    return 0;
}
