import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.functional import _pair
from torch.nn import Parameter
from torch.autograd import Function
from torch.autograd import Variable

from _ext import real_conv


class RealConv2dFunction(Function):

    def __init__(self, kernel_size, stride, padding,columns):
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        self.columns = columns

    def forward(self, input, weight):
        assert not input.is_cuda

        columns = self.columns
        output = torch.FloatTensor()
        real_conv.RealSpatialConvolutionMM_updateOutput(
            input, output, weight, columns, self.kernel_size[0], self.kernel_size[1],
            self.stride[0], self.stride[1], self.padding[0], self.padding[1]
        )

        return output

    def backward(self, grad_top):
        assert False


class RealConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding):
        super(RealConv2d, self).__init__()
        self.ksize = _pair(kernel_size)
        self.stride = _pair(stride)
        self.padding = _pair(padding)

        self.weight = Parameter(torch.FloatTensor(out_channels, in_channels, *self.ksize))
        self.register_parameter('bias', None)

        # buffers of columns
        self.finput = torch.Tensor()

    def forward(self, input):
        output = RealConv2dFunction(self.ksize, self.stride, self.padding, self.finput)(input, self.weight)

        return output

