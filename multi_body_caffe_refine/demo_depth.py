try:
    from utils import depth_cloud
except:
    depth_cloud = None

import os
import cv2
import numpy as np
from numpy import ma
import torch
from torch.autograd import Variable

from network.depth_small38 import Model
from paf_to_pose import paf_to_pose
from tnn.datasets.single_imagefolder import get_loader
from tnn.network import net_utils
from tnn.utils.im_transform import crop_with_factor
from tnn.utils.path import mkdir
from tnn.utils.timer import Timer


thre_params = {'thre1':0.2, 'thre2':0.05, 'thre3':0.8}
exp_name = 'exp_caffe_v1'
save_dir = 'models/training/{}'.format(exp_name)

ckpt = os.path.join(save_dir, 'ckpt_27.h5')

model = Model(stages=5, have_bn= False, have_bias = True)

im_dir = None
video_dir = None
cam_id = 0

inp_size = 368
skip_frame = 1
win_name = 'main'
global_max = 3
global_min = -3
person_only = True
single_image = False
scale = 1
choice = 0

gpus = [0]

output_dir = os.path.join('output', exp_name)
mkdir(output_dir)

net_utils.load_net(ckpt, model)
"""
model = torch.nn.DataParallel(model).cuda()
print('model saved as pickle')
torch.save(model.state_dict(), 'ckpt_45')
"""
print('load ckpt from: {}'.format(ckpt))
if gpus is not None:
    model = model.cuda(gpus[0])
model.eval()
net_timer = Timer()


def nothing(x):
    pass

cv2.namedWindow(win_name)
cv2.createTrackbar('min', win_name, 0, 1000, nothing)
cv2.createTrackbar('max', win_name, 1000, 1000, nothing)
cv2.createTrackbar('seg', win_name, 50, 100, nothing)


def network_forward(image):
    # resize and padding
    real_inp_size = inp_size
    im_pad, im_scale, real_shape = crop_with_factor(image, real_inp_size, factor=8, pad_val=255)

    # preprocess image
    frame_resized = im_pad.astype(np.float32)
    im_croped = cv2.cvtColor(im_pad, cv2.COLOR_BGR2RGB)
    
    im_croped = im_croped.astype(np.float32) / 255. - 0.5
    #print im_croped.shape
    # forward
    im_data = torch.from_numpy(im_croped).permute(2, 0, 1)
    im_data = im_data.unsqueeze(0)
    im_var = Variable(im_data, volatile=True)
    if gpus is not None:
        im_var = im_var.cuda(gpus[0])

    net_timer.tic()
    output, _ = model(im_var)
    net_timer.toc()
    # end forward
    
    PAFs, heatmaps= output[0][0].data.cpu().numpy(), output[1][0].data.cpu().numpy()

    #seg_pred = heatmaps[-1]
    seg_pred = output[2][0][0].data.cpu().numpy()
    
    heatmaps_to_plot = heatmaps[5]

    heatmaps_to_plot[heatmaps_to_plot < 0] = 0
    
    for i in range(len(heatmaps) - 1):
        heatmaps_to_plot = np.maximum(heatmaps_to_plot, heatmaps[i])
    
    heatmaps_to_plot = (heatmaps_to_plot * 255).astype(np.uint8)

    
    pafs_to_plot = np.zeros((46, 62))
    #for j in range(10, 12, 2):
    for j in range(0, len(PAFs), 2):
        temp = np.sqrt(np.power(PAFs[j], 2) + np.power(PAFs[j+1], 2))
        pafs_to_plot = np.maximum(pafs_to_plot, temp)

    pafs_to_plot = (pafs_to_plot * 255).astype(np.uint8)    
    

    PAFs = np.transpose(PAFs, (1, 2, 0))
    heatmaps = np.transpose(heatmaps, (1, 2, 0))
    


    to_plot, canvas, _, _ = paf_to_pose(frame_resized, thre_params, heatmaps, PAFs)
    #ht_img = None
    #paf_img = None

    # crop & remove padding
    h, w = im_pad.shape[:2]
    
    ht_img = cv2.resize(heatmaps_to_plot, dsize=(w, h))
    ht_img = ht_img[:real_shape[0], :real_shape[1]]

    paf_img = cv2.resize(pafs_to_plot, dsize=(w, h))
    paf_img = paf_img[:real_shape[0], :real_shape[1]]
    
    canvas = cv2.resize(canvas, dsize=(w, h))
    canvas = canvas[:real_shape[0], :real_shape[1]]

    seg_pred = cv2.resize(seg_pred, dsize=(w, h))
    hr, wr = real_shape[:2]

    im_croped = im_pad[:real_shape[0], :real_shape[1]]
    seg = seg_pred[:real_shape[0], :real_shape[1]]

    # 256*341(*3)
    #canvas = None
    return im_croped, seg, canvas, ht_img, paf_img


def process_frame(im_orig, frame_ind=0, auto_pause=False):
    def cvkeys_callback(key, im_input, depth_output, ind, paused, person_only, single_image):
        key %= 128
        if key == ord(' '):
            if depth_cloud is not None:
                depth_cloud.plot_depth(im_input, depth_output, min_size=320)
        elif key == ord('p'):
            paused = not paused
        elif key == ord('n'):
            person_only = not person_only
        elif key == ord('m'):
            single_image = not single_image
        elif key == ord('q'):
            exit(0)
        elif key == ord('s'):
            save_name = os.path.join(output_dir, '{:04d}.jpg'.format(ind))
            cv2.imwrite(save_name, im2show)

            # save heatmap
            cv2.imwrite(os.path.join(output_dir, '{:04d}_im.jpg'.format(ind)), im_input)
            np.save(os.path.join(output_dir, '{:04d}_depth.npy'.format(ind)), depth_output)
            depth_cloud.save_ply(os.path.join(output_dir, '{:04d}_points.ply'.format(ind)),
                                 im_input, depth_output, min_size=640)
        if single_image:
            global choice
            if key == ord('j'):
                choice = (choice + 1) % 4

        return paused, person_only, single_image

    im_croped, seg, canvas, ht_img, paf_img = network_forward(im_orig)
    
    nht_img = np.repeat(ht_img[:,:,np.newaxis], 3, axis=2)
    npaf_img = np.repeat(paf_img[:,:,np.newaxis], 3, axis=2)
    
    paused = True
    global person_only, single_image

    while paused:
        if not auto_pause:
            paused = False
            auto_pause = True

        image = im_croped

        # seg
        seg_thr = cv2.getTrackbarPos('seg', win_name) / 100.
        seg_mask = np.zeros_like(seg) + 0.2
        seg_mask[seg > seg_thr] = 1
        seg_im = (im_croped.astype(np.float) * np.expand_dims(seg_mask, -1))

        # blur
        min_d = cv2.getTrackbarPos('min', win_name)
        max_d = cv2.getTrackbarPos('max', win_name)

        if min_d > 0.1 or max_d < 999.9:
            dd = depth * 1000.
            mask = np.logical_and(dd >= min_d, dd <= max_d)
            mask = np.expand_dims(mask, -1).astype(np.int)
            im_blur = cv2.GaussianBlur(im_croped, (31, 31), 0)
            image = mask * im_croped + (1 - mask) * im_blur
            image = image


        if not single_image:
            im2show2 = np.hstack((seg_im, nht_img))
            #im2show = np.hstack((canvas, canvas, canvas))
            im2show = np.hstack((canvas, npaf_img))
            im2show = np.vstack((im2show, im2show2))
        else:
            if choice == 0:
                im2show = cv2.resize(image, dsize=(0, 0), fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
            elif choice == 1:
                im2show = cv2.resize(depth_im, dsize=(0, 0), fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
            elif choice == 2:
                im2show = cv2.resize(seg_im, dsize=(0, 0), fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
            elif choice == 3:
                im2show = cv2.resize(sn_im, dsize=(0, 0), fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
            im2show = im2show.clip(0, 255).astype(np.uint8)

        # resize
        max_size = 1000.
        if max(im2show.shape[:2]) > max_size:
            s = max_size / max(im2show.shape[:2])
            im2show = cv2.resize(im2show, None, fx=s, fy=s)

        cv2.imshow(win_name, im2show.astype(np.uint8))
        key = cv2.waitKey(1)
        #paused, person_only, single_image = cvkeys_callback(key, im_croped, depth, frame_ind, paused, person_only, single_image)


def main():

    timer = Timer()
    if video_dir is not None or cam_id is not None:
        device = cam_id if cam_id is not None else video_dir
        print(device)
        cap = cv2.VideoCapture(device)
        i = 0
        j = 0
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break
            if i % skip_frame == 0:
                timer.tic()
                process_frame(frame, j)
                timer.toc()
                j += 1
            if i % 20 == 0:
                duration = timer.duration + 1e-6
                fps = 1. / duration
                print('net fps: {}, fps: {}'.format(1. / (net_timer.duration + 1e-6), fps))
                timer.clear()
                net_timer.clear()
            i += 1
        cap.release()
    else:
        dataset = get_loader(im_dir, ext=('.png', '.jpg'), batch_size=1)
        for i, batch in enumerate(dataset):
            im_orig = batch

            timer.tic()
            process_frame(im_orig, i, auto_pause=True)
            timer.toc()

            if i % 10 == 0:
                duration = timer.duration
                fps = 1. / duration
                print('net fps: {}'.format(1./net_timer.duration))
                print('fps: {}'.format(fps))
                timer.clear()
                net_timer.clear()


if __name__ == '__main__':
    main()
