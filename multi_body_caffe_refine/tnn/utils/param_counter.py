"""Simple Parameter Counter for Pytorch models Based on code from @Shuang Liu

__author__     = Alberto Sadde
__copyright__  = Copyright 2017, AiFi Inc.
__maintainer__ = Alberto Sadde
__email__      = alberto@aifi.io
__status__     = Experimental
"""

import torch.nn as nn


def param_counter(model, conv=False):
    """Count the number of layers, parameters, trainable parameters in all
    layers and all convolutional layers (optional)
    :param model: the pytorch model
    :param conv: bool, True if counting parameters for Conv2D layers
    :return : [ints], All the counts
    """
    param_cnt = 0
    conv_param_cnt = 0
    conv_layers_cnt = 0
    trainable_param_cnt = 0
    trainable_conv_param_cnt = 0
    layer_cnt = 0
    trainable_param_cnt = 0

    for module in model.modules():
        layer_cnt += 1
        params = module.parameters()
        if conv and isinstance(module, nn.Conv2d):
            conv_layers_cnt += 1
            conv_params, conv_train_params = _param_counter(params)
            conv_param_cnt += conv_params
            trainable_conv_param_cnt += conv_train_params
        params, trainable_params = _param_counter(params)
        param_cnt += params
        trainable_param_cnt += trainable_params

    return (layer_cnt, param_cnt, trainable_param_cnt, conv_layers_cnt,
            conv_param_cnt, trainable_conv_param_cnt)


def pretty_param_counter(model, conv=False, size=False):
    """ Prints All the counts
    :param model: the Pytorch model
    :param conv: bool, True if counting parameters for Conv2D layers
    """
    counts = param_counter(model, conv)
    print(len(counts))
    assert len(counts) == 6
    print("Total number of Layers = {}".format(counts[0]))
    print("Total number of parameters = {}".format(counts[1]))
    print("Total number of trainable params = {}".format(counts[2]))

    if conv:
        print("Total number of Conv Layers = {}".format(counts[3]))
        print("Total number of Conv paramters = {}".format(counts[4]))
        print("Total number of trainable params = {}".format(counts[5]))
    if size:
        print("Total size of model = {} MBs".format(get_model_size(model)))


def _param_counter(params):
    """Counts the number of parameters in a module
    :param params: An iterator over Pytorch parameters
    :return : tuple, the count of parameters and count of trainable parameters
    """
    param_cnt = 0
    trainable_param_cnt = 0
    for param in params:
        param_cnt += param.numel()
        if param.requires_grad:
            trainable_param_cnt += param.numel()
    return param_cnt, trainable_param_cnt


def get_model_size(model):
    """
    Returns value for storage size of the model
    """
    state_dict = model.state_dict()
    total_size = 0.0
    for key, value in state_dict.iteritems():
        if 'weight' in key or 'bias' in key:
            curr = value.cpu().numpy().nbytes
            total_size += curr

    return total_size / (1024**2)
