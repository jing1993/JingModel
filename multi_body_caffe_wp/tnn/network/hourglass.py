import torch
import torch.nn as nn


class ResidualBlock(nn.Module):
    def __init__(self, conv_block, in_channels, out_channels, kernel_size, stride=1, same_padding=False):
        super(ResidualBlock, self).__init__()
        padding = int((kernel_size - 1) / 2) if same_padding else 0

        assert out_channels % 4 == 0, 'wrong out_channels: {}'.format(out_channels)
        out1 = out_channels // 2
        out2 = out3 = out_channels // 4

        self.conv1 = conv_block(in_channels, out1, kernel_size, stride, padding)
        self.conv2 = conv_block(out1, out2, kernel_size, 1, padding)
        self.conv3 = conv_block(out3, out3, kernel_size, 1, padding)

        self.conv0 = conv_block(in_channels, out_channels, 1, stride,
                                    0) if in_channels != out_channels else None

    def forward(self, x):
        outputs = []
        outputs.append(self.conv1(x))
        outputs.append(self.conv2(outputs[-1]))
        outputs.append(self.conv3(outputs[-1]))

        output = torch.cat(outputs, 1)
        if self.conv0 is not None:
            x = self.conv0(x)
        output += x

        return output


class Hourglass(nn.Module):
    def __init__(self, conv_block, n_channels, kernel_size, n_maxpool):
        super(Hourglass, self).__init__()

        # input --------------------- block0 ---------------------->
        #   - maxpool - block - sub_hourglass - block -> upsample -> + -> output

        self.block0 = ResidualBlock(conv_block, n_channels, n_channels, kernel_size, stride=1, same_padding=True)

        self.block1 = nn.Sequential(
            nn.MaxPool2d(kernel_size=2, stride=2),
            ResidualBlock(conv_block, n_channels, n_channels, kernel_size, stride=1, same_padding=True),

            Hourglass(conv_block, n_channels, kernel_size, n_maxpool - 1)
                if n_maxpool > 1 else ResidualBlock(conv_block, n_channels, n_channels, kernel_size, stride=1, same_padding=True),

            ResidualBlock(conv_block, n_channels, n_channels, kernel_size, stride=1, same_padding=True),
            # UpSampling(2)
            nn.UpsamplingNearest2d(scale_factor=2)
            # nn.UpsamplingBilinear2d(scale_factor=2)
        )

    def forward(self, x):
        x0 = self.block0(x)
        x1 = self.block1(x)
        return x0 + x1
