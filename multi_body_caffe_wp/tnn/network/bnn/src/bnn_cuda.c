#include <THC/THC.h>
#include <stdio.h>
#include "bnn_cuda_kernel.h"

#define THCUNN_assertSameGPU( ... ) THAssertMsg( THCudaTensor_checkGPU( __VA_ARGS__ ), \
						 "Some of weight/gradient/input tensors are located on different GPUs. Please move them to a single one." )

extern THCState *state;

void BinarySpatialConvolution_forward(
	THCudaTensor *input,
	THCudaTensor *output,
	THCudaIntTensor *weight,
	THCudaIntTensor *columns,
	THCudaTensor *alphas,
	THCudaIntTensor *columns_binary,
	THCudaTensor *running_mean,
	THCudaTensor * gamma_sign,
	int kW, int kH,
	int dW, int dH,
	int padW, int padH )
{
	THCUNN_assertSameGPU( state, 5, input, output, weight, columns, columns_binary );

	/* Params: */
	int	nInputPlane	= weight->nDimension == 2 ? weight->size[1] / (kH * kW) * 32 : weight->size[1];
	int	nOutputPlane	= weight->size[0];

	input = THCudaTensor_newContiguous( state, input );
	int batch = 1;
	if ( input->nDimension == 3 )
	{
		/* Force batch */
		batch = 0;
		THCudaTensor_resize4d( state, input, 1, input->size[0], input->size[1], input->size[2] );
	}

	long	inputWidth	= input->size[3];
	long	inputHeight	= input->size[2];
	long	outputWidth	= (inputWidth + 2 * padW - kW) / dW + 1;
	long	outputHeight	= (inputHeight + 2 * padH - kH) / dH + 1;

	/* Batch size + input planes */
	long batchSize = input->size[0];

	/* Resize output */
	THCudaTensor_resize4d( state, output, batchSize, nOutputPlane, outputHeight, outputWidth );

	/* Resize temporary columns */
	THCudaIntTensor_resize2d( state, columns, nInputPlane * kW * kH, outputHeight * outputWidth );

	/* Resize the weight/columns buffers */
	if ( columns_binary->nDimension != 2 || columns_binary->size[0] * columns_binary->size[1] < outputHeight * outputWidth * weight->size[1] )
	{
		THCudaIntTensor_resize2d( state, columns_binary, weight->size[1], outputHeight * outputWidth );
	}

	/* Helpers */
	THCudaTensor	*input_n	= THCudaTensor_new( state );
	THCudaTensor	*output_n	= THCudaTensor_new( state );

	/* For each elt in batch, do: */
	int elt;
	for ( elt = 0; elt < batchSize; elt++ )
	{
		/* Matrix mulitply per output: */
		THCudaTensor_select( state, input_n, input, 0, elt );
		THCudaTensor_select( state, output_n, output, 0, elt );

		/* Extract columns: */
		im2col(
			THCudaTensor_data( state, input_n ), THCudaTensor_data( state, running_mean ), THCudaTensor_data( state, gamma_sign ),
			nInputPlane, inputHeight, inputWidth, kH, kW, padH, padW, dH, dW,
			1, 1, THCudaIntTensor_data( state, columns ),
			THCState_getCurrentStream( state )
			);

		/* Encode cols */
		encode_cols_ongpu(
			THCudaIntTensor_data( state, columns ),
			(unsigned int *) THCudaIntTensor_data( state, columns_binary ),
			columns->size[0], columns->size[1],
			THCState_getCurrentStream( state )
			);


		/*
		 * M,N,K are dims of matrix A and B
		 * (see http://docs.nvidia.com/cuda/cublas/#cublas-lt-t-gt-gemm)
		 * row-major to column-major change
		 */
		int	m	= weight->size[0];
		int	n	= weight->size[1];
		int	k	= columns->size[1];
/*
 *        printf("%d\n", n);
 * Do here the binary_gemm call - popcount(XOR) is called here
 */
		binary_gemm_ongpu(
			(unsigned int *) THCudaIntTensor_data( state, weight ),
			(unsigned int *) THCudaIntTensor_data( state, columns_binary ),
			THCudaTensor_data( state, output_n ), m, n, k, THCudaTensor_data( state, alphas ),
			THCState_getCurrentStream( state ) );
	}

	/* Free */
	THCudaTensor_free( state, input_n );
	THCudaTensor_free( state, output_n );

	if ( batch == 0 )
	{
		THCudaTensor_resize3d( state, output, nOutputPlane, outputHeight, outputWidth );
		THCudaTensor_resize3d( state, input, nInputPlane, inputHeight, inputWidth );
	}

	THCudaTensor_free( state, input );
}


void encode_rows( THCudaTensor* input, THCudaIntTensor* output )
{
	THCUNN_assertSameGPU( state, 2, input, output );

	int count = THCudaIntTensor_nElement( state, output );
	encode_rows_ongpu(
		THCudaTensor_data( state, input ),
		(unsigned int *) THCudaIntTensor_data( state, output ),
		count,
		THCState_getCurrentStream( state )
		);
}


void decode_rows( THCudaIntTensor* input, THCudaTensor* output )
{
	THCUNN_assertSameGPU( state, 2, input, output );

	int count = THCudaIntTensor_nElement( state, input );
	decode_rows_ongpu(
		(unsigned int *) THCudaIntTensor_data( state, input ),
		THCudaTensor_data( state, output ),
		count,
		THCState_getCurrentStream( state )
		);
}