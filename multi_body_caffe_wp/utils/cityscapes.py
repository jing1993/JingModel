import cv2
import numpy as np

color_map = {
    0: (128, 64, 128),
    1: (244, 35, 232),
    2: (70, 70, 70),
    3: (102, 102, 156),
    4: (190, 153, 153),
    5: (153, 153, 153),
    6: (250, 170, 30),
    7: (220, 220, 0),
    8: (107, 142, 35),
    9: (152, 251, 152),
    10: (70, 130, 180),
    11: (220, 20, 60),
    12: (255, 0, 0),
    13: (0, 0, 142),
    14: (0, 0, 70),
    15: (0, 60, 100),
    16: (0, 80, 100),
    17: (0, 0, 230),
    18: (119, 11, 32),
    19: (0, 0, 0),
}


def pred2im(pred):
    h, w = pred.shape[0:2]
    im = np.zeros((h, w, 3), dtype=np.uint8)
    for k, v in color_map.items():
        im[pred == k] = v
    return im


def train2test(pred):
    maps = {
        0: 7,
        1: 8,
        2: 11,
        3: 12,
        4: 13,
        5: 17,
        6: 19,
        7: 20,
        8: 21,
        9: 22,
        10: 23,
        11: 24,
        12: 25,
        13: 26,
        14: 27,
        15: 28,
        16: 31,
        17: 32,
        18: 33,
        19: 0
    }

    pred2 = np.zeros_like(pred, dtype=np.uint8)
    for k, v in maps.items():
        pred2[pred==k] = v
    return pred2