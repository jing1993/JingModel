import numpy as np
import numpy.random as npr

from tnn.utils.cython_bbox import bbox_overlaps, bbox_intersections
from tnn.network.rpn.generate_anchors import generate_anchors
from tnn.network.rpn.bbox_transform import bbox_transform_inv
from tnn.utils.bbox import clip_boxes
from tnn.utils.nms_wrapper import nms


def anchor_layer(rpn_cls_prob, rpn_bbox_pred, inp_size, out_size, anchor_scales, anchor_aspects):
    """
    Outputs object detection proposals by applying estimated bounding-box
    transformations to a set of regular boxes (called "anchors").
    
    :param rpn_cls_prob: (1, A*2, H, W), outputs of RPN, (W, H) = out_size
    :param rpn_bbox_pred: (1, A*4, H, W)
    :param inp_size: original image size (width, height)
    :param out_size: output image size
    :param anchor_scales: the scales to the basic_anchor (basic anchor is [16, 16])
    :param anchor_aspects: aspect ratio
    :return: proposals: (1, H, W, A, 4)
             scores: (1, H, W, A, 1)
    """

    _anchors = generate_anchors(base_size=16, ratios=np.array(anchor_aspects), scales=np.array(anchor_scales))
    _num_anchors = _anchors.shape[0]

    scores = rpn_cls_prob[:, _num_anchors:, :, :]
    bbox_deltas = rpn_bbox_pred

    # 1. Generate proposals from bbox deltas and shifted anchors
    height, width = scores.shape[-2:]
    inp_width, inp_height = inp_size

    _feat_stride = inp_width // width

    # Enumerate all shifts
    shift_x = np.arange(0, width) * _feat_stride
    shift_y = np.arange(0, height) * _feat_stride
    shift_x, shift_y = np.meshgrid(shift_x, shift_y)
    shifts = np.vstack((shift_x.ravel(), shift_y.ravel(),
                        shift_x.ravel(), shift_y.ravel())).transpose()

    # Enumerate all shifted anchors:
    #
    # add A anchors (1, A, 4) to
    # cell K shifts (K, 1, 4) to get
    # shift anchors (K, A, 4)
    # reshape to (K*A, 4) shifted anchors
    A = _num_anchors
    K = shifts.shape[0]
    anchors = _anchors.reshape((1, A, 4)) + \
              shifts.reshape((1, K, 4)).transpose((1, 0, 2))
    anchors = anchors.reshape((K * A, 4))

    # Transpose and reshape predicted bbox transformations to get them
    # into the same order as the anchors:
    #
    # bbox deltas will be (1, 4 * A, H, W) format
    # transpose to (1, H, W, 4 * A)
    # reshape to (1 * H * W * A, 4) where rows are ordered by (h, w, a)
    # in slowest to fastest order
    bbox_deltas = bbox_deltas.transpose((0, 2, 3, 1)).reshape((-1, 4))

    # Same story for the scores:
    #
    # scores are (1, A, H, W) format
    # transpose to (1, H, W, A)
    # reshape to (1,  H * W, A, 1) where rows are ordered by (h, w, a)
    scores = scores.transpose((0, 2, 3, 1)).reshape((1, height, width, A, 1))

    # Convert anchors into proposals via bbox transformations
    proposals = bbox_transform_inv(anchors, bbox_deltas)
    # 2. clip predicted boxes to image
    proposals = clip_boxes(proposals, [inp_height, inp_width])

    proposals = proposals.reshape((1, height, width, A, 4))

    return proposals, scores


def _filter_boxes(boxes, min_size):
    """Remove all boxes with any side smaller than min_size."""
    ws = boxes[:, 2] - boxes[:, 0] + 1
    hs = boxes[:, 3] - boxes[:, 1] + 1
    keep = np.where((ws >= min_size) & (hs >= min_size))[0]
    return keep
