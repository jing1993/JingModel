import numpy as np
from scipy.optimize import linear_sum_assignment
import cv2
import os
from numba import jit
from tnn.utils.bbox import clip_boxes

from sklearn.metrics.pairwise import cosine_similarity
from scipy import spatial


def ious(box1, boxes, is_iou=True):
    boxes = np.asarray(boxes, dtype=np.float32)
    # tb = np.minimum(box1[0] + box1[2], boxes[:, 0] + boxes[:, 2]) - np.maximum(box1[0], boxes[:, 0])
    # lr = np.minimum(box1[1] + box1[3], boxes[:, 1] + boxes[:, 3]) - np.maximum(box1[1], boxes[:, 1])
    tb = np.minimum(box1[0] + 0.5 * box1[2], boxes[:, 0] + 0.5 * boxes[:, 2]) - np.maximum(box1[0] - 0.5 * box1[2], boxes[:, 0] - 0.5 * boxes[:, 2])
    lr = np.minimum(box1[1] + 0.5 * box1[3], boxes[:, 1] + 0.5 * boxes[:, 3]) - np.maximum(box1[1] - 0.5 * box1[3], boxes[:, 1] - 0.5 * boxes[:, 3])
    intersection = tb * lr
    intersection[tb < 0] = 0
    intersection[lr < 0] = 0

    if is_iou:
        _ious = intersection / (box1[2] * box1[3] + boxes[:, 2] * boxes[:, 3] - intersection)
    else:
        _ious = intersection / np.minimum(box1[2] * box1[3], boxes[:, 2] * boxes[:, 3])

    return _ious


def iou(box1, box2, centered=True, iou=True):
    if centered:
        tb = min(box1[0] + 0.5 * box1[2], box2[0] + 0.5 * box2[2]) - max(box1[0] - 0.5 * box1[2], box2[0] - 0.5 * box2[2])
        lr = min(box1[1] + 0.5 * box1[3], box2[1] + 0.5 * box2[3]) - max(box1[1] - 0.5 * box1[3], box2[1] - 0.5 * box2[3])
    else:
        tb = float(min(box1[0] + box1[2], box2[0] + box2[2]) - max(box1[0], box2[0]))
        lr = float(min(box1[1] + box1[3], box2[1] + box2[3]) - max(box1[1], box2[1]))

    if tb < 0 or lr < 0:
        intersection = 0
    else:
        intersection = tb * lr
    if iou:
        return intersection / (box1[2] * box1[3] + box2[2] * box2[3] - intersection)
    else:
        return intersection / min(box1[2]*box1[3], box2[2]*box2[3])


def rmse(box1, box2):
    return np.sqrt(np.sum((np.power(box1 - box2, 2))))


def similarity(box1, box2):
    cx1, cy1, w1, h1 = box1
    cx2, cy2, w2, h2 = box2
    s = (np.abs(cx1-cx2) + np.abs(w1-w2)) / (w1 + w2) + (np.abs(cy1-cy2) + np.abs(h1-h2)) / (h1 + h2)
    return np.exp(-0.5 * s)


def associate(cost, nms=False):
    if nms:
        return np.argmin(cost, axis=1)
    else:
        row_ind, col_ind = linear_sum_assignment(cost)
        return row_ind, col_ind


def clip_boxes_mask(boxes, im_shape, aspect_range, only_aspect=False):
    if boxes.shape[0] == 0:
        return boxes

    aspects = (boxes[:, 2] - boxes[:, 0]) / (boxes[:, 3] - boxes[:, 1])
    mask = (aspects > aspect_range[1]) | (aspects < aspect_range[0])
    if not only_aspect:
        mask = np.any(boxes < 0, axis=1) | np.any(boxes[:, 0::2] > im_shape[1], axis=1) |\
               np.any(boxes[:, 1::2] > im_shape[0]) | mask
    return mask


@jit
def extract_depth_cls(model, bboxes, depth, seg, seg_threshold=0.5):
    zs = np.zeros([len(bboxes), 1], dtype=np.float)

    if len(bboxes) == 0:
        return zs, np.zeros([len(bboxes), 1], dtype=np.float)

    bboxes = np.round(bboxes).astype(np.int)
    bboxes = clip_boxes(bboxes, depth.shape)
    seg_mask = seg > seg_threshold

    for i, bbox in enumerate(bboxes):
        x1, y1, x2, y2 = bbox
        seg_mask_i = seg_mask[y1:y2 + 1, x1:x2 + 1]
        depth_i = depth[y1:y2 + 1, x1:x2 + 1]

        depth_values = depth_i[seg_mask_i] if seg_mask_i.sum() > 10 else depth_i
        zs[i] = np.mean(depth_values)

    cls_scores = model.get_cls_score_numpy(model.cls_feat, bboxes)

    return zs, cls_scores


@jit
def sampling_rois(bboxes, n_sample=20):
    if len(bboxes) == 0:
        return np.empty([0, 4]), np.empty([0])
    bboxes = bboxes.repeat(n_sample, 0)
    new_bboxes = np.zeros_like(bboxes)

    cxs, cys = np.mean(bboxes[:, 0::2], axis=1), np.mean(bboxes[:, 1::2], axis=1)
    hs, ws = (bboxes[:, 3] - bboxes[:, 1]) * 0.5, (bboxes[:, 2] - bboxes[:, 0]) * 0.5

    dist_x = np.random.standard_normal(len(cxs)) * ws * 0.2
    dist_y = np.random.standard_normal(len(cxs)) * ws * 0.2

    cxs += dist_x
    cys += dist_y
    hs += np.random.standard_normal(len(hs)) * ws * 0.06
    ws += np.random.standard_normal(len(ws)) * ws * 0.06

    new_bboxes[:, 0] = cxs - ws
    new_bboxes[:, 1] = cys - hs
    new_bboxes[:, 2] = cxs + ws
    new_bboxes[:, 3] = cys + hs

    # dists = np.abs(dist_x) + np.abs(dist_y)

    return new_bboxes


def plot_tracking(image, bboxes, obj_ids, scores=None, frame_id=0, fps=0):
    im = np.copy(image)
    im_h, im_w = im.shape[:2]

    top_view = np.zeros([im_w, im_w, 3], dtype=np.uint8) + 255

    text_scale = max(1, image.shape[1] / 800.)
    text_thickness = 2 if text_scale > 1.1 else 1
    line_thickness = max(1, int(image.shape[1] / 250.))

    radius = max(5, int(im_w/140.))
    cv2.putText(im, 'frame: %d fps: %.2f num: %d' % (frame_id, fps, len(bboxes)),
                (0, int(15 * text_scale)), cv2.FONT_HERSHEY_PLAIN, text_scale, (0, 0, 255), thickness=2)

    # scores = np.clip(np.asarray(scores, dtype=np.float) * 255, 0, 255).astype(np.uint8)
    # colors = cv2.applyColorMap(scores, cv2.COLORMAP_JET)

    for i, bbox in enumerate(bboxes):
        intbox = tuple([int(x) for x in bbox[:4]])
        z = im_w - int(bbox[4])
        cx = int(np.mean(bbox[0:2:4]))

        obj_id = obj_ids[i]
        _line_thickness = 1 if obj_id < 0 else line_thickness
        color = ((37 * obj_id) % 255, (17 * obj_id) % 255, (29 * obj_id) % 255)

        cv2.rectangle(im, intbox[0:2], intbox[2:4], color, _line_thickness)
        cv2.putText(im, '{}'.format(int(obj_id)), (intbox[0], intbox[1] + 30), cv2.FONT_HERSHEY_PLAIN, text_scale, (0, 0, 255),
                    thickness=text_thickness)

        cv2.circle(top_view, (cx, z), radius, color, thickness=_line_thickness)
        cv2.putText(top_view, '{}'.format(int(obj_id)), (cx - 8, z - 15), cv2.FONT_HERSHEY_PLAIN, text_scale, (255, 0, 0),
                    thickness=text_thickness)

    return im, top_view


def plot_detections(image, dets, color=(255, 0, 0), ids=None):
    im = np.copy(image)
    text_scale = max(1, image.shape[1] / 800.)
    thickness = 2 if text_scale > 1.3 else 1
    for i, det in enumerate(dets):
        x1, y1, x2, y2 = np.asarray(det[:4], dtype=np.int)
        if len(det) >= 7:
            if ids is not None:
                text = '{:.2f}: {:d}'.format(det[6], ids[i])
            else:
                text = '{:.2f}'.format(det[6])
            cv2.putText(im, text, (x1, y1 + 30), cv2.FONT_HERSHEY_PLAIN, text_scale, (0, 255, 0), thickness=thickness)
        cv2.rectangle(im, (x1, y1), (x2, y2), color, 2)

    return im


'''
labels={'ped', ...			% 1
	'person_on_vhcl', ...	% 2
	'car', ...				% 3
	'bicycle', ...			% 4
	'mbike', ...			% 5
	'non_mot_vhcl', ...		% 6
	'static_person', ...	% 7
	'distractor', ...		% 8
	'occluder', ...			% 9
	'occluder_on_grnd', ...		%10
	'occluder_full', ...		% 11
	'reflection', ...		% 12
	'crowd' ...			% 13
	};
'''


def read_mot_results(filename, is_gt=False):
    labels = {1, 7, -1}
    targets = dict()
    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            for line in f.readlines():
                linelist = line.split(',')
                if len(linelist) < 7:
                    continue
                fid = int(linelist[0])
                targets.setdefault(fid, list())

                if is_gt:
                    label = int(float(linelist[-2])) if len(linelist) > 7 else -1
                    if label not in labels:
                        continue
                target = tuple(float(x) for x in linelist[2:7])
                target_id = int(linelist[1])

                targets[fid].append((target, target_id))

    return targets

