import numpy as np

matched_parts = ((0, 1), (0, 2), (1, 3), (2, 4), (0, 17), (17, 5), (17, 6), (5, 7), (6, 8), (7, 9),
                 (8, 10), (17, 11), (17, 12), (11, 13), (12, 14), (13, 15), (14, 16))


def limb_heatmap(location, size, sigma):
    PAF = np.zeros([2, size[0], size[1]], dtype=np.float32)
    n_map = np.zeros([size[0], size[1]])
    mp = np.mgrid[0:size[0], 0:size[1]]
    limb_vec = location[1] - location[0]
    limb_len = np.linalg.norm(limb_vec)
    if limb_len == 0:
        return PAF, n_map
    limb_width = 1
    limb_para = limb_vec / limb_len
    limb_perp = np.asarray([limb_para[1], -limb_para[0]])
    mp = mp.reshape(2, -1).T
    vec = mp - location[0]
    vec_para = vec.dot(limb_para)
    vec_perp = vec.dot(limb_perp)
    vec_para = vec_para.reshape(size[0], size[1]).T
    vec_perp = vec_perp.reshape(size[0], size[1]).T
    cond1 = vec_para >= 0
    cond2 = vec_para <= limb_len
    cond3 = np.abs(vec_perp) <= limb_width
    cond = np.logical_and.reduce((cond1, cond2, cond3))
    PAF[0][cond] = limb_para[0]
    PAF[1][cond] = limb_para[1]
    n_map[cond] = 1
    return PAF, n_map


def limbmap(locations, size):
    maps = []
    n_maps = []
    for location in locations:
        if location[0] is not 0:
            mp, n_map = limb_heatmap(location[1], size, sigma=2)
        else:
            mp = np.zeros([2, size[0], size[1]], dtype=np.float32)
            n_map = np.zeros([size[0], size[1]])
        maps.append(mp)
        n_maps.append(n_map)
    maps = np.sum(maps, 0)
    n_maps = np.sum(n_maps, 0)
    maps[0][n_maps > 0] /= n_maps[n_maps > 0]
    maps[1][n_maps > 0] /= n_maps[n_maps > 0]
    return maps


def PAFmap_generator(anns, input_size, feat_stride):
    PAFmap_size = input_size / feat_stride
    PAFmaps = []
    for limb in matched_parts:
        limb_locations = []
        for person in anns:
            limb_locations.append([person[1][limb[0]] * person[1][limb[1]], np.asarray(
                [np.asarray(person[2][limb[0]], dtype=int) / feat_stride,
                 np.asarray(person[2][limb[1]], dtype=int) / feat_stride], dtype=np.float32)])
        limb_map = limbmap(limb_locations, PAFmap_size)
        PAFmaps.append(limb_map)
    return np.concatenate(PAFmaps, 0)
