'''
__author__     = Alberto Sadde
__copyright__  = Copyright 2017, AiFi Inc.
__license__    =
__version__    = 0.0
__maintainer__ = Alberto Sadde
__email__      = alberto@aifi.io
__status__     = Experimental
'''

from __future__ import print_function
import os
import os.path as path
import json
import sys
import multiprocessing
from joblib import Parallel, delayed
import itertools

""" Data Integrity Tests for Unity-generated Data

    Usage: Just set the root_path variable to the root path
           of the data to check and run the script
"""

root_path = '/data/synthetic/hands_v0.1'

if not path.exists(root_path):
    print("Error: WRONG DATA PATH")

json_path = path.join(root_path, "_joints")
if not path.exists(json_path):
    print("Error: WRONG JSON PATH")

print("Checking Data Length")
print("======================================================================")
"""
    Simple test to ensure that there are enough frames generated for each
    type of ground truth
"""
json_files = os.listdir(json_path)
print("There are {} frames with valid joints data".format(len(json_files)))

ground_truths = os.listdir(root_path)
no_error = True
for path in ground_truths:
    data_path = os.path.join(root_path, path)
    if not os.path.exists(data_path):
        print("Error : {} ground_truths not available".format(path))
        no_error = False
    elif len(os.listdir(data_path)) != len(json_files):
        print("ERROR: {} data size doesn't match".format(path))
        no_error = False

if (no_error):
    print("Data Length is consistent!")
else:
    print("==================================")
    print("ERROR: Data Length is inconsistent")
    print("==================================")
print("======================================================================")

print("Checking Data Integrity")
print("======================================================================")
"""
    Get frame information from JSON metadata.
    Test whether there are people in the image
    Test whether frame contains all corresponding ground truths
"""


def test_integrity(json_path, json_files, files_per_proc, index):
    start = index * files_per_proc
    end = min(start + files_per_proc, len(json_files))
    json_files = json_files[start:end]
    errors = []
    for json_file in json_files:
        jf = os.path.join(json_path, json_file)
        with open(jf) as f:
            data = json.load(f)
            filename = data['image_name'] + "_" + data['frame_id']
            # Ensure Humans in all frames
            if data['num_of_people'] < 1:
                errors.append(("no humans", filename))

            # Ensure there is data for each human in the frame
            if data['num_of_people'] != len(data['data']):
                errors.append(("Joint info not consistent with num_of_people", filename))

            # Check Integrity of Data
            for datum in data['data']:
                # Joints should be consistent
                if datum['joint_count'] != len(datum['joints']):
                    errors.append(("Inconsistent joints for {}".format(datum['sk_id']), filename))
                # Check that visibility information is correct
                for joints in datum['joints']:
                    position = joints['xyzv']
                    if (len(position) != 4):
                        errors.append(("Inconsistent joint {}".format(joints['name']), filename))
                        continue
                    visible = True
                    for index in range(0, 3):
                        if position[index] == -1.0:
                            visible = False
                        break
                    if not visible and position[3] != 2.0:
                        errors.append(("Wrong visibility info {}".format(joints['name']), filename))

            # Check that each frame has all the corresponding Ground Truths
            for gt in data['ground_truths']:
                gt_file = os.path.join(os.path.join(root_path, gt), filename + gt + ".png")
                if not os.path.exists(gt_file):
                    errors.append(("no {}".format(gt), filename))

    return errors


def parallel_integrity(index):
    return test_integrity(json_path, json_files, files_per_proc, index)


num_cores = multiprocessing.cpu_count()
# Ensure always
files_per_proc = (len(json_files) + num_cores - 1) / num_cores
inputs = range(0, num_cores)

"""Runs in the above integrity test in parallel!"""
errors = Parallel(n_jobs=num_cores)(delayed(parallel_integrity)(i) for i in inputs)
errors = list(itertools.chain.from_iterable(errors))

if (len(errors) < 1):
    print("Data is consistent!")
else:
    print("ERROR: Data is NOT consistent!")
    for (msg, filename) in errors:
        print("Frame {} has error: {}".format(filename, msg))
