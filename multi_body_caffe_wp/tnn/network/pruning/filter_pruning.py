'''
__author__     = Alberto Sadde
__copyright__  = Copyright 2017, AiFi Inc.
__license__    =
__version__    = 0.0
__maintainer__ = Alberto Sadde
__email__      = alberto@aifi.io
__status__     = Experimental
'''

from collections import OrderedDict

import numpy as np
import torch
import torch.nn as nn

from tnn.network.trainer import Trainer
from tnn.network.net_utils import load_net
from tnn.utils.param_counter import param_counter, get_model_size


class PruneFilters(object):
    """
    A general pipeline for pruning filters.
    This implementation follows ideas from the paper
    "Pruning Filtes for Efficient Convnets"(https://arxiv.org/abs/1608.08710)
    The main idea is to rank the filters in each layer by the L1 distance
    of the sum of the weights and prune the k smaller filters where k is
    a hyperparameter to set (prune_amount).

    This class enables the user to prune a filters from Conv and BatchNorm
    layers in any model. The user needs to input a list of OrderedDicts
    specifying the layers to prune. We need at least two layers to prune
    since the in_channels of the upper layer need to change accordingly so
    the connection in the computation graph doesn't break.

    Currently only one-shot retraining is supported as described on the paper
    aforementioned. Iterative retraining might yield better results but
    will take longer time.
    """
    def __init__(self, model, train_params, batch_p, train_d, val_d, prune_p,
                 eval_func):
        self.model = model
        self.prune_amount = prune_p
        self.train_params = train_params
        self.batch_p = batch_p
        self.train_d = train_d
        self.val_d = val_d
        self.eval_func = eval_func

    @staticmethod
    def _rank_filters(filters):
        """Rank the filters in a layer by the sum of the absolute value
        of its weights.
        The weight tensor is 4D (out_channels, in_channels, k, k)
        :param filters: 4D torch tensor
        :return ranked: list of tuples (i, v) where i is the index of the
        filter and v its value used for ranking
        """
        if filters.ndim != 4:
            raise ValueError("Expected tensor of 4"
                             "dimensions but got {}".format(filters.ndim))

        ranked = []
        for i in range(filters.shape[0]):
            # curr_filter is 3D tensor of in_channels x k x k
            curr_filter = np.abs(filters[i, :, :, :])
            ranked.append((i, np.sum(curr_filter)))

        # Sort (in place) keys in descending order by the snd element of tuple
        ranked.sort(key=lambda tup: tup[1], reverse=False)
        return ranked

    @staticmethod
    def _slice_filters(filters, ranked, out=True):
        """Prune a single layer of model
        :param filters: numpy array, the filter tensor of the current layer
        :param ranked: list of tuples, [(index, rank)]
        :param out: bool, True if pruning output channels
        """
        if out:
            axis = 0
        else:
            axis = 1

        keep = [k[0] for k in ranked]
        tensors = np.delete(filters, keep, axis=axis)
        return tensors

    def _prune_bn(self, layer, ranked):
        """Prune a BatchNorm2d Layer
        :param layer: a BatchNorm2d layer
        :param ranked: list of tuples [(index, rank)]
        :return: the unmodified ranked list
        """
        if not isinstance(layer, nn.BatchNorm2d):
            raise ValuError("Layer is not BatchNorm2d")

        # Get all data
        print("Original shape: {}".format(layer.weight.data.size()))
        weight = layer.weight.data.numpy()
        bias = layer.bias.data.numpy()
        run_mean = layer.running_mean.numpy()
        run_var = layer.running_var.numpy()

        # Prune filters
        indeces = [r[0] for r in ranked]
        new_weight = np.delete(weight, indeces)
        new_bias = np.delete(bias, indeces)
        new_run_mean = np.delete(run_mean, indeces)
        new_run_var = np.delete(run_var, indeces)

        # Update layer
        layer.weight.data = torch.from_numpy(new_weight).float()
        layer.bias.data = torch.from_numpy(new_bias).float()
        layer.running_mean = torch.from_numpy(new_run_mean).float()
        layer.running_var = torch.from_numpy(new_run_var).float()
        layer.register_buffer('out_pruned', torch.ones(1))  # Mark as pruned
        print("Pruned shape: {}".format(layer.weight.data.size()))

        return ranked

    def _prune_layer(self, layer, prev_rank, out=True, inp=True):
        """Prune a single layer
        :param layer: an nn.BatchNorm2d or nn.Conv2d layer
        :param prev_rank: list of [(index, rank)] of the previous layer
        :param out: bool, True if pruning the output channels
        :param inp: bool, True if pruning the input channels
        :return prev_rank: list of [(index, rank)] of the current layer
        """

        if isinstance(layer, nn.BatchNorm2d):  # Prune BatchNorm Layer
            return self._prune_bn(layer, prev_rank)

        if not isinstance(layer, nn.Conv2d):
            raise ValuError("Expected an nn.Conv2d Layer")

        print("Original shape: {}".format(layer.weight.data.size()))

        if inp and not hasattr(layer, 'in_pruned'):  # Slice in_channels
            filters = layer.weight.data.cpu().numpy()
            filters = self._slice_filters(filters, prev_rank, out=False)

            # Update layer with new tensor
            layer.weight.data = torch.from_numpy(filters).float()
            layer.in_channels = filters.shape[1]
            layer.register_buffer('in_pruned', torch.ones(1))  # Mark as pruned

        if out and not hasattr(layer, 'out_pruned'):  # Slice out channels
            filters = layer.weight.data.cpu().numpy()
            ranked = self._rank_filters(filters)
            threshold = int(filters.shape[0] * self.prune_amount)
            ranked = ranked[threshold:]  # Indeces to prune
            new_filters = self._slice_filters(filters, ranked)
            prev_rank = ranked

            # Update layer with new tensor
            layer.weight.data = torch.from_numpy(new_filters).float()
            layer.out_channels = new_filters.shape[0]
            layer.register_buffer('out_pruned', torch.ones(1))  # Mark pruned

            if layer.bias is not None:  # Update bias
                bias = layer.bias.data.cpu().numpy()
                indeces = [r[0] for r in ranked]
                new_bias = np.delete(bias, indeces)
                layer.bias.data = torch.from_numpy(new_bias).float()

                assert layer.bias.data.numpy().shape[0] == \
                    layer.weight.data.numpy().shape[0]

        print("Pruned shape: {}".format(layer.weight.data.size()))

        return prev_rank

    def _prune_block(self, block):
        """Prune filters for the given layers
        This function assumes that all layers in a single list are in
        sequential order, i.e. layers[0] -> layers[1] -> ... -> layers[n] are
        connected in the graph

        :param block: OrderedDict of layers
        :return prev_rank: list of [(index, rank)] of the current layer
        """
        if len(block) < 2:
            raise ValueError("Need at least two layers to prune")

        (name, layer) = block.items()[0]

        self._print_size(msg="Before")
        print("Pruning First Layer {}".format(name))

        # Slice only out_channels of first layer
        prev_rank = self._prune_layer(layer, [], out=True, inp=False)

        # Prune remaining layers
        if len(block.items()) > 1:
            end = len(block.items()) - 1
            for index, (name, layer) in enumerate(block.items()[1:end], 1):
                print("Pruning Layer {}".format(name))
                # Slice in and out channels for all intermediate layers
                prev_rank = self._prune_layer(layer, prev_rank,
                                              out=True, inp=True)

            # Slice only in channels of last layer
            (name, layer) = block.items()[-1]
            print("Pruning Last Layer {}".format(name))
            prev_rank = self._prune_layer(layer, prev_rank,
                                          out=False, inp=True)

        self._print_size(msg="After")
        return prev_rank

    def prune_layers(self, layers):
        for block in layers:
            self._prune_block(block)

    def sensitivity(self, layers, concat=False):
        """Measure the layers' sensitivity to pruning.
        This is done by pruning each layer and evaluating the model on the
        validation set.

        :param layers: list of OrderedDicts of layers in sequential order
        :param concat: bool, False is layers contain no concatenation
        :return results: list of evaluation results
        """
        results = []
        for i in range(len(layers)):
            block = layers[i]
            block_size = len(block.keys())
            if block_size != 2 and block_size != 3:
                raise ValueError("Need exactly two or three layers per block")

            # Save original values
            orig_weights = []
            for val in block.values():
                orig_weights.append(val.weight.data)

            bn_layer = block.values()[1]
            if block_size == 3 and isinstance(bn_layer, nn.BatchNorm2d):
                running_mean = bn_layer.running_mean
                running_var = bn_layer.running_mean
                bn_bias = bn_layer.bias.data

            biases = []
            for val in block.values():
                if val.bias is not None:
                    biases.append(val.bias.data)

            # Prune the block
            if concat:
                last = len(block.items()) - 1
                new_block = OrderedDict(block.items()[:last])
                prev_rank = self._prune_block(block=new_block)
                (_, layer) = block.items()[last]
                self._prune_layer(layer=layer, prev_rank=prev_rank, out=False)

            else:
                self._prune_block(block=block)

            # Evaluate pruned model
            results.append(self.eval_func(self.model.cuda()))
            self.model.cpu()

            # Restore original weights
            for i in range(len(block)):
                layer = block.values()[i]
                layer.weight.data = orig_weights[i]

                if not isinstance(layer, nn.BatchNorm2d):
                    in_c = int(orig_weights[i].size().__getitem__(1))
                    layer.in_channels = in_c

                out_c = int(orig_weights[i].size().__getitem__(0))
                layer.out_channels = out_c

                if isinstance(layer, nn.BatchNorm2d):
                    layer.running_mean = running_mean
                    layer.running_var = running_var
                    layer.bias.data = bn_bias

                if layer.bias is not None:
                    layer.bias.data = biases[i]

                # Remove buffers
                if i == 0:
                    del layer._buffers['out_pruned']
                elif i != 0 and i != len(block) - 1 and not concat:
                    del layer._buffers['out_pruned']
                    if not isinstance(layer, nn.BatchNorm2d):
                        del layer._buffers['in_pruned']
                elif i == len(block) - 1 or concat:
                    del layer._buffers['in_pruned']

        return results

    def _make_trainer(self, model):
        """Utility to initialise a trainer with set parameters"""
        return Trainer(model=model,
                       train_params=self.train_params,
                       batch_processor=self.batch_p,
                       train_data=self.train_d,
                       use_auto_lr=True,
                       val_data=self.val_d)

    def prune_and_retrain(self, layers, evaluate=False):
        """Prune and retrain function
        :param layers: a list of lists of layers to prune
        :param evaluate: bool, True if validation after training
        """

        num_params = param_counter(self.model)
        size = get_model_size(self.model)
        print("Original size = {} MB".format(size))
        print("Original params = {} ".format(num_params[0]))

        if evaluate:
            print("Will Evaluate model after every epoch")
            self.train_params.on_end_epoch_hooks.append(self.eval_func)

        print("Pruning model...")
        self.prune_layers(layers)

        num_params = param_counter(self.model)
        size = get_model_size(self.model)
        print("Size after pruning = {} MB".format(size))
        print("Params after pruning = {} ".format(num_params[0]))

        print("Starting training...")
        trainer = self._make_trainer(self.model)
        trainer.train()

    def eval(self):
        """Eval the model"""
        try:
            if self.eval_func is not None:
                self.eval_func(self.model)
            else:
                raise TypeError
        except TypeError:
            print("ERROR: Eval function not set")

    def prune_and_eval(self, layers, ckpt):
        """Prune and evaluate the model
        :param layers: list of OrderedDicts, the layers to prune
        :param ckpt: string, path to ckpt to load the weights from
        """
        self.prune_layers(layers)
        load_net(ckpt, self.model)
        self.model.eval()
        self.model.cuda()
        self.eval()

    def _print_size(self, msg):
        print("Size {} pruning  = {}".format(msg, get_model_size(self.model)))
        print("Params {} pruning = {}".format(msg, param_counter(self.model)[0]))
