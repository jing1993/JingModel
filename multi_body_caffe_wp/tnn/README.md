## Train Neural Networks in PyTorch

This is a small framework for training and evaluating neural networks in a
simple way. We always need to copy the `training loop` from one training
script to another to train different models. I made this `training loop` as a
standalone class in order to avoid duplication in different training scripts
and to print log information during training automatically.

## Source codes Layout
```
tnn/
├── datasets                  Basic Dataloaders
│   ├── dataloader.py         sDataLoader inherited from `DataLoader` with an
|   |                         extra `get_stream` method which returns an 
│   │                         unlimited generator.
│   └── ...
├── network                   Anything about pytorch and neural networks
│   ├── bnn                   Binarized convolutional layer
│   ├── trainer.py            Standalone training loop
│   ├── tester.py             Standalone testing loop
│   ├── net_utils.py          Some tools for neural networks
│   └── ...
├── utils                     Tools can be used for different projects
│   ├── timer.py              Timing using `tic` and `toc`
│   ├── im_transform.py       Data augmentation
│   ├── nms_wrapper.py        NMS for boxes
│   └── ...
└── make.sh                   Build bnn and some utils in Cython
```

## Usage
1. Add `tnn` as a submodule in git
    ```bash
    cd Project_Home/
    git submodule add git@gitlab.com:aifi-ml/tnn.git
    ```

2. (Optional) Build bnn layer and Cython tools (only if you need bnn layer, nms
    and methods for bbox_iou)
    ```bash
    cd tnn
    sh make.sh
    ```

3. Implement module class with a `foward(*inputs)` and an extra static method
    named `build_loss(saved_for_loss, *gts)`.

    The `foward(*inputs)` method should return two values: `output` and 
    `saved_for_loss`. `saved_for_loss` is a list of `Variable`s and will be
    passed to `build_loss` automatically.
    See more details in [tnn/network/base_model.py](
    https://gitlab.com/aifi-ml/tnn/blob/master/network/base_model.py).

    The `build_loss(saved_for_loss, *gts)` returns two values: `loss` and
    `saved_for_log`. The second one is an `OrderedDict`. You can save 
    `key-float` pairs such as `max_output` and `loss_value` in the 
    `saved_for_log` `OrderedDict` and `Trainer` will print them periodically.
     ```python
     saved_for_log = OrderedDict()
     saved_for_log['max_out'] = torch.max(predicts[-1].data)
     saved_for_log['loss'] = sum(log_losses)
     saved_for_log['stacked'] = log_losses
     ```

    Note: In order to support multi-gpu training using `nn.DataParallel`,
    `saved_for_loss` and `saved_for_log` are passed explicitly but not served
    as class members of the module because `nn.DataParallel` replicates
    the module for multiple gpus and it's not easy to access members in them.

4. Set training parameters such as `max_epoch`, `init_lr`, `batch_size` using
    `Trainer.TrainParams`.

    See [tnn/network/trainer.py](
    https://gitlab.com/aifi-ml/tnn/blob/master/network/trainer.py) for more
    details.
    ```python
    from tnn.network.trainer import Trainer

    params = Trainer.TrainParams()
    params.exp_name = 'pose_real2_small6'
    params.save_dir = 'models/training/{}'.format(params.exp_name)
    params.ckpt = None # checkpoint file to load

    params.max_epoch = 50
    params.lr_decay_epoch = {5, 10, 20}
    params.init_lr = 0.01
    params.lr_decay = 0.1
    ...
    ```

5. Write a `batch_processor(state, batch)` to wrap input tensor and labels
    loaded from dataloader to Variables. A `batch_processor` should return
    three lists:

    ```python
    def batch_processor(state, batch):
        # State is the instance of `Trainer`.
        # You can access training params from `state.params` and module from
        # `state.model`.
        gpus = state.params.gpus
        im_orig, im_data, heatmap_data, visible, objs = batch

        volatile = not state.model.training
        im_var = Variable(im_data, volatile=volatile).cuda(device_id=gpus[0])
        heatmap_var = Variable(heatmap_data).cuda(device_id=gpus[0])
        visible_var = Variable(visible).cuda(device_id=gpus[0])

        inputs = [im_var]                  # passed to `forward`
        gts = [heatmap_var, visible_var]   # sent to `build_loss`
        saved_for_eval = [objs]            # used for `Tester`

    return inputs, gts, saved_for_eval
    ```

6. Set an optimizer and train. Trainer will save a checkpoint at the end of
    every epoch or after some steps if you specified `params.save_freq`. Note
    that I save network parameters as an HDF5 file using `h5py` instead of 
    `torch.save` to gain more flexibility when loading parameters from `ckpt`
    (See details of [save_net and load_net](
    https://gitlab.com/aifi-ml/tnn/blob/master/network/net_utils.py#L298)).
    ```python
    trainable_vars = [
        param for param in model.parameters() if param.requires_grad]
    params.optimizer = torch.optim.Adam(
        trainable_vars, lr=params.init_lr, weight_decay=weight_decay)

    # train_data and valid_data are Dataloaders
    trainer = Trainer(model, params, batch_processor, train_data, valid_data)
    trainer.train()
    ```

## Visualization
We use Visdom to visualize model information and training results.
+ Install Visdom: `pip install visdom`
+ Run server: `python -m visdom.server`

Visdom parameters can be set like:
```python
params.do_vis = True
params.vis_freq = 100
params.vis_params = {
    'server': 'http://127.0.0.1',
    'port': 8097,
    'env': params.exp_name
}
params.do_dist = False
params.do_cum = False
params.do_mean_weights = True
```

If `params.do_vis` is set to `True`, each float value in `saved_for_log` will
be plotted as a line in Visdom. Each `np.ndarray` with shape `(H, W, 3)` will
be plotted as an image. Each `np.ndarray` with shape `(H, W)` or `(H, W, 1)`
will be plotted as a heatmap.
```python
def build_loss(...):
    ...
    # Visualize images.
    im = np.transpose(
        images.data[0].cpu().numpy(), (1, 2, 0))  # (c, h, w) -> (h, w, c)
    gt = gts.data[0].cpu().squeeze().numpy()
    pred = predict.data[0].cpu().squeeze().numpy()
    saved_for_log['image'] = im
    saved_for_log['gt'] = gt
    saved_for_log['pred'] = pred

    return loss, saved_for_log
```

## Example
+ Dataloader example: 
    https://gitlab.com/aifi-ml/longc/blob/master/datasets/mpii.py
+ Module example: 
    https://gitlab.com/aifi-ml/longc/blob/master/network/pose_model.py
+ Training with `tnn.network.Trainer`: 
    https://gitlab.com/aifi-ml/longc/blob/master/pose_train.py
+ Evaluating with `tnn.network.Tester`:
    https://gitlab.com/aifi-ml/longc/blob/master/pose_test.py

