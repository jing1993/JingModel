# coding=utf-8
import random
import cv2
import numpy as np
from pycocotools.coco import COCO


def aug_scale(img, seg, mask, params_transform):

    dice = random.random()
    scale = (params_transform['scale_max'] - params_transform['scale_min']) * dice + params_transform['scale_min']

    img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
    seg = cv2.resize(seg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
    mask = cv2.resize(mask, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
    return img, seg, mask


def aug_croppad(img, seg, mask, params_transform):
    dice_x = random.random()
    dice_y = random.random()
    random_color = (np.random.random(3)*255).astype(np.uint8)
    crop_x = int(params_transform['crop_size_x'])
    crop_y = int(params_transform['crop_size_y'])
    x_offset = int((dice_x - 0.5) * 2 *
                   params_transform['center_perterb_max'])
    y_offset = int((dice_y - 0.5) * 2 *
                   params_transform['center_perterb_max'])

    center = np.array([img.shape[1]/2, img.shape[0]/2]) + np.array([x_offset, y_offset])
    center = center.astype(int)

    # pad up and down

    pad_v = np.ones((crop_y, mask.shape[1], 3), dtype=np.uint8) * random_color
    pad_v_seg = np.zeros((crop_y, seg.shape[1]), dtype=np.uint8) * 255
    pad_v_mask = np.ones((crop_y, mask.shape[1]), dtype=np.uint8) * 255

    img = np.concatenate((pad_v, img, pad_v), axis=0)
    seg = np.concatenate((pad_v_seg, seg, pad_v_seg), axis=0)
    mask = np.concatenate((pad_v_mask, mask, pad_v_mask), axis=0)

    # pad right and left
    pad_h = np.ones([img.shape[0], crop_x, 3], dtype = np.uint8) * random_color
    pad_h_seg = np.zeros((seg.shape[0], crop_x), dtype=np.uint8) * 255
    pad_h_mask = np.ones((mask.shape[0], crop_x), dtype=np.uint8) * 255

    img = np.concatenate((pad_h, img, pad_h), axis=1)
    seg = np.concatenate((pad_h_seg, seg, pad_h_seg), axis=1)
    mask = np.concatenate((pad_h_mask, mask, pad_h_mask), axis=1)

    img = img[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x, :]
    seg = seg[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x]
    mask = mask[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x]


    return img, seg, mask


def aug_flip(img, seg, mask, params_transform):
    dice = random.random()
    doflip = dice <= params_transform['flip_prob']

    if doflip:
        img = cv2.flip(img, 1)
        seg = cv2.flip(seg, 1)
        mask = cv2.flip(mask, 1)
    
    return img, seg, mask

def rotate_bound(image, angle, bordervalue):
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_CONSTANT,
                          borderValue=bordervalue)


def aug_rotate(img, seg, mask, params_transform, type="random", input=0, fillType="nearest", constant=0):
    dice = random.random()
    degree = (dice - 0.5) * 2 * params_transform['max_rotate_degree']  # degree [-40,40]
    random_color = (np.random.random(3)*255).astype(int)
    img_rot = rotate_bound(img, np.copy(degree), random_color)
    seg_rot = rotate_bound(seg, np.copy(degree), (0))
    mask_rot = rotate_bound(mask, np.copy(degree), (255))
    return img_rot, seg_rot, mask_rot

def aug_blur(img, seg, k_std = 1, xy_std = 1):
    ksize = np.abs(np.random.normal(loc = 0, scale = k_std, size = 2).astype(int))
    xy = np.abs(np.random.normal(loc = 0, scale = xy_std, size = 2).astype(int))
    return  cv2.GaussianBlur(img, ksize = (2*ksize[0]+1, 2*ksize[1]+1), sigmaX = xy[0], sigmaY = xy[1]), cv2.GaussianBlur(seg, ksize = (2*ksize[0]+1, 2*ksize[1]+1), sigmaX = xy[0], sigmaY = xy[1])

def aug_noise(img, std = 5):
    noise_matrix = np.random.normal(loc = 0, scale = std, size = img.shape)
    img = img+noise_matrix
    img[img<0] = 0
    img[img>255] = 255
    return img.astype(np.uint8)

def aug_tint(img, scale = 0.1):
    tint_scale = 1+scale*np.random.random([1,1,3])-scale/2
    img = img*tint_scale
    img[img<0] = 0
    img[img>255] = 255
    return img.astype(np.uint8)

def ImageAugmentation(img, seg, mask, params):
    img, seg, mask = aug_scale(img, seg, mask, params)
    img, seg, mask = aug_rotate(img, seg, mask, params)
    img, seg, mask = aug_croppad(img, seg, mask, params)
    img, seg, mask = aug_flip(img, seg, mask, params)
    img, seg = aug_blur(img, seg)
    img = aug_tint(img)
    img = aug_noise(img)
    return img, seg, mask


if __name__ == '__main__':
    params_transform = dict()
    params_transform['scale_min'] = 0.5
    params_transform['scale_max'] = 1.1
    params_transform['max_rotate_degree'] = 40
    params_transform['center_perterb_max'] = 40
    params_transform['crop_size_x'] = 368
    params_transform['crop_size_y'] = 368
    params_transform['flip_prob'] = 0.5


    dataDir='/data/coco'
    dataType='train2014'
    annFile='{}/coco/annotations/person_keypoints_{}.json'.format(dataDir,dataType)
    coco=COCO(annFile)

    catIds = coco.getCatIds(catNms=['person']);
    imgIds = coco.getImgIds(catIds=catIds);

    find = False
    while not find:
        img = coco.loadImgs(imgIds[np.random.randint(0,len(imgIds))])[0]
        annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds)
        anns = coco.loadAnns(annIds)
        for ann in anns:
            if ann['iscrowd']:
                find = True
                break

    image = cv2.imread('{}/images/{}/{}'.format(dataDir, dataType, img['file_name']))
    seg = cv2.imread('{}/mask2014_semantic/{}_mask_semantic_{}.png'.format(dataDir, dataType, img['file_name'][-16:-4]), 0)
    mask = cv2.imread('{}/mask2014/{}_mask_miss_{}.png'.format(dataDir, dataType, img['file_name'][-16:-4]), 0)
    

    image, seg, mask = ImageAugmentation(image, seg, mask, params_transform)

    cv2.imwrite('img.png', image)
    cv2.imwrite('seg.png', seg)
    cv2.imwrite('mask.png', mask)
