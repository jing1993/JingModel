import torch
import torch.utils.data as data
import numpy as np
import cv2
import os
import cPickle

from utils.mot import read_mot_results
from tnn.datasets.dataloader import sDataLoader


class MOTSeq(data.Dataset):
    def __init__(self, root, seq_name):
        self.root = root
        self.seq_name = seq_name

        self.im_root = os.path.join(self.root, self.seq_name, 'img1')
        self.im_names = sorted([name for name in os.listdir(self.im_root) if os.path.splitext(name)[-1] == '.jpg'])

        self.det_file = os.path.join(self.root, self.seq_name, 'det', 'det.txt')
        # self.det_file = os.path.join('/data/2DMOT2015/sort_det', self.seq_name.split('/')[-1], 'det.txt')
        self.dets = read_mot_results(self.det_file, is_gt=False)

        self.gt_file = os.path.join(self.root, self.seq_name, 'gt', 'gt.txt')
        if os.path.isfile(self.gt_file):
            self.gts = read_mot_results(self.gt_file, is_gt=True)
        else:
            self.gts = None

    def __len__(self):
        return len(self.im_names)

    def __getitem__(self, i):
        im_name = os.path.join(self.im_root, self.im_names[i])
        im = cv2.imread(im_name)

        frame = i + 1
        dets = self.dets.get(frame, [])
        det_targets, _ = zip(*self.dets[frame]) if len(dets) > 0 else (np.empty([0, 5]), np.empty([0, 1]))

        if self.gts is not None:
            gts = self.gts.get(frame, [])
            gt_targets, gt_ids = zip(*self.gts[frame]) if len(gts) > 0 else (np.empty([0, 5]), np.empty([0, 1]))
        else:
            gt_targets, gt_ids = None, None

        return im, det_targets, gt_targets, gt_ids


def collate_fn(data):
    return data[0]


def get_loader(root, name, num_workers=3):
    dataset = MOTSeq(root, name)

    data_loader = sDataLoader(dataset, 1, False, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/data/2DMOT2015/',
                                'train/ADL-Rundle-6', num_workers=3)
        for i, batch in enumerate(train_data):
            im, det_targets, gt_targets, gt_ids = batch

            for (x, y, w, h, s) in gt_targets:
                bbox = tuple(int(round(x)) for x in [x, y, x+w, y+h])
                cv2.rectangle(im, bbox[0:2], bbox[2:4], (255, 0, 0), 2)

            cv2.imshow('test', im)

            cv2.waitKey(1)

            # print len(im_data)
            # print type(im_data), im_data.size()
            # # print heatmap_data, visible
            # print type(heatmap_data), heatmap_data.size()
            # print visible.shape
            #
            # cv2.imwrite('origin.jpg', im_orig[0])
            # cv2.imwrite('croped.jpg', im_data.numpy()[0])
            # # cv2.imshow('orgin', im_orig[0])
            # # cv2.imshow('croped', im_data.numpy()[0])
            # for i in range(len(heatmap_data.numpy()[0])):
            #     cv2.imwrite('ht_{}.jpg'.format(i), heatmap_data.numpy()[0][i] * 255)
            #     # cv2.waitKey(0)
            # assert False
    main()


