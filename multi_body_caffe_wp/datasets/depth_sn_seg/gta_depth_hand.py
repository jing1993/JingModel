import os
import collections
import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
import utils.depth as depth_utils
import datasets.data_utils as data_utils


class GTADepth(data.Dataset):
    def __init__(self, root, inp_size=256, feat_stride=4, max_pts=6000, thresh1=0.1, thresh2=0.01, subset=None, training=True):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride[-1] if isinstance(feat_stride, collections.Iterable) else feat_stride
        self.all_scales = self.feat_stride / np.asarray(feat_stride, dtype=np.float) if isinstance(feat_stride, collections.Iterable) else (1,)
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.thresh1 = thresh1
        self.thresh2 = thresh2
        self.max_pts = max_pts

        self.im_root = os.path.join(self.root, 'origin')
        self.depth_root = os.path.join(self.root, 'depth')
        self.label_root = os.path.join(self.root, 'label')
        self.snormal_root = os.path.join(self.root, 'surfaceNorm')
        if not os.path.isdir(self.snormal_root):
            self.snormal_root = None

        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.png']
        if subset is not None:
            self.im_names = self.im_names[subset[0]:subset[1]]

    def __len__(self):
        return len(self.im_names)

    def read_images(self, i):
        im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, im_name)
        name = im_name.split('_')[0]
        depth_file = os.path.join(self.depth_root, '{}_depth.h5'.format(name))
        label_file = os.path.join(self.label_root, '{}_label.png'.format(name))

        im = cv2.imread(im_file)
        if im.shape[0] < 600 or im.shape[1] < 600:
            print('W0: {}, imshape: {}'.format(name, im.shape))
            next_i = (i + 1) % self.__len__()
            return self.__getitem__(next_i)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        d5 = h5py.File(depth_file, 'r')
        gta_depth = np.asarray(d5['depth'])
        seg_label = cv2.imread(label_file, cv2.IMREAD_GRAYSCALE)

        if self.snormal_root is not None:
            snormal_file = os.path.join(self.snormal_root, '{}_surfaceNorm.png'.format(name))
            sn = cv2.imread(snormal_file)
        else:
            sn = np.zeros_like(im)

        seg_mask = data_utils.mask_generation(seg_label)
        seg_label = seg_label.astype(np.float32)
        seg_label[seg_label > 0] = 1
        seg_mask = seg_mask.astype(np.float32)
        seg_mask[seg_mask > 0] = 1

        sn_mask = np.ones_like(gta_depth, dtype=np.float32)

        mask_sky = np.zeros_like(gta_depth, dtype=np.float32)
        mask_sky[gta_depth > 0] = 1

        return im, gta_depth, seg_label, sn, seg_mask, sn_mask, mask_sky

    def __getitem__(self, i):
        # read images
        im, gta_depth, seg_label, sn, mask, sn_mask, mask_sky = self.read_images(i)

        # for gta
        depth = np.exp(-20. * gta_depth) * 4000
        abs_depth = 0.1 / (np.exp(1 * (gta_depth + 1e-5)) - 1)
        additions = (depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 1.0
        max_scale = target_scale * 1.3
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 25,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': True
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn = additions
        # image recolor
        im, seg_label = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.015, blur_ksize=2, blur_sigma=2,
                                                   mask=seg_label)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # scale depth
        scale = trans_params[0]
        depth = depth / scale
        abs_depth = abs_depth / scale
        # degree = trans_params[1][-1]
        # radius = degree * np.pi / 180.
        # rotate_matrix = np.asarray(
        #     [[np.cos(radius), 0, np.sin(radius)], [0, 1., 0], [-np.sin(radius), 0, np.cos(radius)]])
        # sn = ((sn / 127.5 - 1).dot(rotate_matrix) * 127.5 + 127.5).astype(np.uint8)

        # =========== end data augmentation ==============

        # resize to output
        additions = (depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn)
        additions = [cv2.resize(v, tuple(self.out_size)) for v in additions]
        depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn = additions

        mask[mask > 0.9] = 1
        mask[mask <= 0.9] = 0
        mask_sky[mask_sky > 0.9] = 1
        mask_sky[mask_sky <= 0.1] = 0

        # normalize depth
        depth_t = depth[mask > 0]
        if depth_t.size > 0:
            dmin, dmax = depth_t.min(), depth_t.max()
            depth_n = (depth - dmin) / (dmax - dmin + 1e-5)
            depth_n = depth_n.astype(np.float32)
        else:
            depth_n = depth

        # get surface normal from depth
        # sn = depth_utils.depth_to_normal(depth_n, im_order=False).astype(np.float32)
        sn = depth_utils.depth_to_normal(abs_depth, z_scale=0.008, im_order=False).astype(np.float32)
        # sn = sn.astype(np.float32).transpose([2, 0, 1]) / 127.5 - 1

        sn_mask = sn_mask * mask_sky
        sn_mask[0, :] = 0
        sn_mask[-1, :] = 0
        sn_mask[:, 0] = 0
        sn_mask[:, -1] = 0
        sn_mask[np.sum(np.abs(sn), axis=0) < 0.001] = 0
        sn_mask = sn_mask[np.newaxis, :, :]

        # to tensor
        im_data = torch.from_numpy(im_data)
        depth_data = torch.from_numpy(depth_n)
        seg_data = torch.from_numpy(seg_label)
        seg_mask = torch.from_numpy(np.zeros_like(mask))
        sn_data = torch.from_numpy(sn)
        sn_mask = torch.from_numpy(sn_mask)

        # fake dwt data
        dwt_label = np.zeros_like(seg_label, dtype=np.int64)
        dwt_mask = np.zeros_like(seg_label, dtype=np.float32)
        dwt_label = torch.from_numpy(dwt_label)
        dwt_mask = torch.from_numpy(dwt_mask)

        # =============== sample points =============
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        num_pts = 0

        # part one: regular
        max_pts = int(self.max_pts * 0.5)
        sampled_pts = depth_utils.sample_pts(depth_n, mask, max_pts=max_pts, dis_scale=50./self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts += len(sampled_pts)
        pts[:num_pts, :] = sampled_pts

        # part two: regular sampling on human
        max_pts = int((self.max_pts - num_pts) * 0.4)
        sampled_pts = depth_utils.sample_pts(depth_n, seg_label, max_pts=max_pts, dis_scale=10./self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts:num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part three: human and floor
        floor_mask = np.zeros_like(mask, dtype=np.float32)
        h = floor_mask.shape[0]
        floor_mask[0:int(h * 0.3), :] = 1
        floor_mask[h - int(h * 0.2):, :] = 1
        floor_mask = floor_mask * mask
        max_pts = int((self.max_pts - num_pts) * 0.1)
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, floor_mask, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part four: human and background
        max_pts = int((self.max_pts - num_pts) * 0.3)
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, mask, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part five: human and human
        max_pts = self.max_pts - num_pts
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, seg_label, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # random permutation
        rinds = np.random.permutation(num_pts)
        pts[:num_pts, :] = pts[rinds]
        pts = torch.from_numpy(pts).type(torch.LongTensor)
        # =============== end sample pts ===============

        return im_data, depth_data, sn_data, seg_data, dwt_label, seg_mask, sn_mask, dwt_mask, pts, num_pts
        #
        # gts = torch.stack((depth_data, seg_data, seg_mask), 0) # 3xHxW
        # sn_gts = torch.cat((sn_data, sn_mask), 0)   # 4xHxW
        # gts = torch.cat((gts, sn_gts), 0) # 7xHxW
        # return im_data, gts, pts, num_pts

