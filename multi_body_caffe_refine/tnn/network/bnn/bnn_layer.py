import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init
from torch.nn.functional import _pair
from torch.nn import Parameter
from torch.autograd import Function
from torch.autograd import Variable

from _ext import bnn_layer


class BinaryConv2dFunction(Function):

    def __init__(self, kernel_size, stride, padding, weight, alphas, columns, columns_binary, running_mean, gamma_sign):
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        self.weight = weight
        self.alphas = alphas

        self.columns = columns
        self.columns_binary = columns_binary

        self.running_mean = running_mean
        self.gamma_sign = gamma_sign

    def forward(self, input):

        # buffers of columns
        columns = self.columns
        columns_binary = self.columns_binary

        output = torch.FloatTensor()
        if input.is_cuda:
            output = output.cuda(input.get_device())

            # self.columns.is_cuda, self.columns_binary.is_cuda
            bnn_layer.BinarySpatialConvolution_forward(
                input, output, self.weight, columns, self.alphas, columns_binary, self.running_mean, self.gamma_sign,
                self.kernel_size[0], self.kernel_size[1],
                self.stride[0], self.stride[1], self.padding[0], self.padding[1]
            )
        else:
            # print self.alphas.cpu().numpy().reshape(-1, 2)
            # one_alphas = torch.ones(*self.alphas.size())
            bnn_layer.BinarySpatialConvolution_forward_cpu(
                input, output, self.weight, columns, self.alphas, columns_binary, self.running_mean, self.gamma_sign,
                self.kernel_size[0], self.kernel_size[1],
                self.stride[0], self.stride[1], self.padding[0], self.padding[1]
            )
            # output *= self.alphas.expand_as(output)

        return output

    def backward(self, grad_top):
        assert False


class BinaryActiveFunction(Function):
    def forward(self, input):
        self.input = input
        return input.sign()

    def backward(self, grad_output):
        input = self.input.cuda(grad_output.get_device()) if grad_output.is_cuda else self.input

        grad_output[input.ge(1)] = 0
        grad_output[input.le(-1)] = 0

        return grad_output

        # grad_input = torch.FloatTensor(grad_output.size())
        # if grad_output.is_cuda:
        #     grad_input = grad_input.cuda(grad_output.get_device())
        # grad_input.copy_(grad_output)
        #
        # grad_input[input.ge(1)] = 0
        # grad_input[input.le(-1)] = 0
        # # grad_output.copy_(grad_input)
        # # return grad_output
        # return grad_input


class BinaryConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, bn_buffer):
        super(BinaryConv2d, self).__init__()
        self.ksize = _pair(kernel_size)
        self.stride = _pair(stride)
        self.padding = _pair(padding)

        self.in_channels = in_channels
        self.kernel_size = kernel_size if isinstance(kernel_size, int) else kernel_size

        self.real_weight = Parameter(torch.FloatTensor(out_channels, in_channels, *self.ksize))

        self.register_buffer('weight', torch.IntTensor(out_channels, in_channels//32*self.ksize[0]*self.ksize[1]))
        self.register_buffer('alphas', torch.Tensor(1, out_channels, 1, 1))

        # buffers of columns
        self.binary_buffers = {'columns': torch.IntTensor(), 'columns_binary': torch.IntTensor()}
        self.bn_buffer = bn_buffer

        self.register_backward_hook(self.backward_hook)

        # init
        self.reset_parameters()

    def reset_parameters(self):
        n = self.in_channels
        for k in self.ksize:
            n *= k
        stdv = 1. / math.sqrt(n)
        self.real_weight.data.uniform_(-stdv, stdv)

    def forward2(self, input):
        # self.decode_binary_weight()
        if self.training:
            # output = BinaryConv2dFunction_train(self.ksize, self.stride, self.padding)(input, self.real_weight)

            self.meancenter_weight(self.real_weight)
            self.real_weight.data = torch.clamp(self.real_weight.data, -1, 1)

            self.real_weight_bk = torch.Tensor(self.real_weight.data.size())
            self.real_weight_bk.copy_(self.real_weight.data)

            bin_weight, self._alphas = BinaryConv2d.get_bin_weight(self.real_weight.data)
            self.real_weight.data.copy_(bin_weight)
            input_ = F.pad(input, (self.padding[0], self.padding[0], self.padding[1], self.padding[1]), value=-1)

            output = F.conv2d(input_, self.real_weight, None, self.stride, 0)

            self.real_weight.data.copy_(self.real_weight_bk)
        else:
            output = BinaryConv2dFunction(self.ksize, self.stride, self.padding, self.weight, self.alphas,
                                          self.binary_buffers['columns'], self.binary_buffers['columns_binary'],
                                          self.bn_buffer['bn_mean'], self.bn_buffer['gamma_sign'])(input)

        return output

    def forward(self, input):
        # output = BinaryConv2dFunction_train(self.ksize, self.stride, self.padding)(input, self.real_weight)

        self.meancenter_weight(self.real_weight)
        self.real_weight.data = torch.clamp(self.real_weight.data, -1, 1)

        self.real_weight_bk = torch.Tensor(self.real_weight.data.size())
        self.real_weight_bk.copy_(self.real_weight.data)

        bin_weight, self._alphas = BinaryConv2d.get_bin_weight(self.real_weight.data)
        self.real_weight.data.copy_(bin_weight)
        input_ = F.pad(input, (self.padding[0], self.padding[0], self.padding[1], self.padding[1]), value=-1)

        output = F.conv2d(input_, self.real_weight, None, self.stride, 0)

        self.real_weight.data.copy_(self.real_weight_bk)

        return output

    @staticmethod
    def backward_hook(module, grad_input, grad_output):
        grad_data, grad_weight, _ = grad_input

        # module.real_weight.data.copy_(module.real_weight_bk)

        n = module.real_weight[0].numel()
        alphas = module._alphas

        alphas[module.real_weight.data.le(-1)] = 0
        alphas[module.real_weight.data.gt(1)] = 0
        alphas = (1. / n + alphas) * (1 - 1. / module.real_weight.size(1))
        # if sgd then alphas *= n ?
        alphas *= n

        # grad_weight_ = grad_weight * Variable(alphas)
        # return grad_data, grad_weight_,

        grad_weight_ = grad_weight.data * alphas
        grad_weight.data.copy_(grad_weight_)
        return None

    @staticmethod
    def get_bin_weight(real_weight):
        n = real_weight[0].numel()
        alphas = real_weight.norm(1, 3).sum(2).sum(1) / n
        alphas = alphas.expand_as(real_weight)

        bin_weight = real_weight.gt(0).type_as(real_weight)
        bin_weight[bin_weight.lt(1)] = -1
        bin_weight *= alphas

        return bin_weight, alphas

    def decode_binary_weight(self):
        bnn_layer.decode_rows(self.weight, self.real_weight.data)
        self.real_weight.data[self.real_weight.data.lt(1)] = -1
        self.real_weight.data *= self.alphas.expand_as(self.real_weight.data)

    def encode_binary_weight(self):
        n = self.real_weight[0].numel()
        new_alphas = self.real_weight.data.norm(1, 3).sum(2).sum(1) / n
        self.alphas.copy_(new_alphas)
        bnn_layer.encode_rows(self.real_weight.data, self.weight)

    @staticmethod
    def meancenter_weight(real_weight):
        mean = real_weight.data.mean(1)
        real_weight.data -= mean.expand_as(real_weight.data)

    def _apply(self, fn):
        for module in self.children():
            module._apply(fn)
        for param in self._parameters.values():
            if param is not None:
                # Variables stored in modules are graph leaves, and we don't
                # want to create copy nodes, so we have to unpack the data.
                param.data = fn(param.data)
                if param._grad is not None:
                    param._grad.data = fn(param._grad.data)
        for key, buf in self._buffers.items():
            if buf is not None:
                self._buffers[key] = fn(buf)
        for key, buf in self.binary_buffers.items():
            if buf is not None:
                self.binary_buffers[key] = fn(buf)
        return self


class BNBinaryConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding):
        super(BNBinaryConv2d, self).__init__()

        self.bn = nn.BatchNorm2d(in_channels)

        self.register_buffer('bn_mean', torch.FloatTensor(in_channels))
        self.register_buffer('gamma_sign', torch.FloatTensor(in_channels))

        self.conv = BinaryConv2d(in_channels, out_channels, kernel_size, stride, padding,
                                 self._buffers)

    def forward(self, x):

        # if self.training:
        #     x = self.bn(x)
        #     x = BinaryActiveFunction()(x)
        x = self.bn(x)
        x = BinaryActiveFunction()(x)

        x = self.conv(x)

        return x

    def encode_bn_mean(self):
        bn = self.bn
        # print bn.running_mean
        sign = torch.sign(bn.weight.data)
        self.gamma_sign.copy_(sign)
        self.bn_mean.copy_(
            -bn.bias.data * (torch.sqrt(bn.running_var+1e-5)) / bn.weight.data + bn.running_mean)
        # self.gamma_sign = self.gamma_sign.view(1, self.gamma_sign.size(0), 1, 1)


class BNRealConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding):
        super(BNRealConv2d, self).__init__()

        self.bn = nn.BatchNorm2d(in_channels)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, bias=True)

    def forward(self, x):
        x = self.bn(x)

        x = F.relu(x, inplace=True)
        x = self.conv(x)

        return x


class RealConv2dBN(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding):
        super(RealConv2dBN, self).__init__()

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, bias=True)
        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = F.relu(x, inplace=True)

        return x


def encode_all_binary_weight(model):
    for m in model.modules():
        if isinstance(m, BinaryConv2d):
            m.encode_binary_weight()


def decode_all_binary_weight(model):
    for m in model.modules():
        if isinstance(m, BinaryConv2d):
            m.decode_binary_weight()


def encode_all_bn_mean(model):
    for m in model.modules():
        if isinstance(m, BNBinaryConv2d):
            m.encode_bn_mean()
