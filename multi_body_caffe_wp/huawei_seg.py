import os

import cv2
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable

#from network.rtpose_vgg import get_model, use_mobilenet, use_vgg19
from paf_to_pose import paf_to_pose
from tnn.network.tester import Tester

from network.depth_small38 import Model
from tnn.network import net_utils
from tnn.utils.im_transform import crop_with_factor
from tnn.utils.path import mkdir
from tnn.utils.timer import Timer
# Set Testing parameters
params = Tester.TestParams()
thre_params = {'thre1':0.3, 'thre2':0.05, 'thre3':0.5}
# ===================================================#
# ========change the following parameters============#
# ===================================================#
data_dir = '/data/coco/'
json_path = '/data/coco/COCO.json'

extractor = 'inception_v3'  # select the feature_extractor
# get test data
testdata_dir = 'huawei'
testresult_dir = 'testresult_high_seg'
params.gpus = [0]

gpus = [0]

params.save_dir = '/data/'
exp_name = 'exp_caffe_v1'
save_dir = 'models/training/{}'.format(exp_name)

params.ckpt = os.path.join(save_dir, 'ckpt_27.h5')
#params.ckpt = 'ckpt_92.h5'  # checkpoint file to load, no need to change this

inp_size = 368
inp_size = 736

# ===================================================#
# ===================================================#
# ===================================================#

model = Model(stages=5, have_bn= False, have_bias = True)
net_utils.load_net(params.ckpt, model)


#grabcut algorithm
def post_process(img, seg, mode = 'final'):
    bg_threshold = 0.1
    fg_threshold = 0.9
    pr_threshold = 0.5
    threshold = 0.3

    raw_mask = np.zeros_like(seg)
    if mode == 'raw':
        raw_mask[seg < threshold] = 0
        raw_mask[seg >= threshold] = 1
        return raw_mask

    raw_mask[seg < pr_threshold] = 2
    raw_mask[seg >= pr_threshold] = 3
    raw_mask[seg < bg_threshold] = 0
    raw_mask[seg > fg_threshold] = 1
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)

    #cv2.imwrite('image.png', img)
    #cv2.imwrite('seg.png', raw_mask*85)
    refined_mask, bgdModel, fgdModel = cv2.grabCut(img,np.uint8(raw_mask),None,bgdModel,fgdModel,1,cv2.GC_INIT_WITH_MASK)
    if mode == 'refined':
        refined_mask[refined_mask == 2] = 0 
        refined_mask[refined_mask == 3] = 1
        return refined_mask
    elif mode == 'final':
        refined_mask[(raw_mask == 3) & (refined_mask == 3)] = 1
        refined_mask[refined_mask != 1] = 0
        return refined_mask
    else:
        assert False, 'invalid mode.'


def run_test(model, testdata_dir, testresult_dir, params):

    #model = nn.DataParallel(model, device_ids=params.gpus)
    
    model = model.cuda(device_id=params.gpus[0])
    model.eval()


    if not os.path.isdir(params.save_dir):
        os.mkdir(params.save_dir)

    if not os.path.isdir(testdata_dir):
        assert False, 'Please download test data from https://drive.google.com/drive/u/0/folders/0B5L6mkx2k-2lQS1JcHV4SjdHdGc.'

    if not os.path.isdir(testresult_dir):
        os.mkdir(testresult_dir)

    img_list = os.listdir(testdata_dir)

    for img_name in img_list:
        print 'Processing image: ' + img_name
        image = cv2.imread(os.path.join(testdata_dir, img_name))#.astype(np.float32)
        origin_h, origin_w = image.shape[:2]

        real_inp_size = inp_size
        im_pad, im_scale, real_shape = crop_with_factor(image, real_inp_size, factor=8, pad_val=255)

        # preprocess image
        frame_resized = im_pad.astype(np.float32)
        im_croped = cv2.cvtColor(im_pad, cv2.COLOR_BGR2RGB)
        
        im_croped = im_croped.astype(np.float32) / 255. - 0.5
        #print im_croped.shape
        # forward
        im_data = torch.from_numpy(im_croped).permute(2, 0, 1)
        im_data = im_data.unsqueeze(0)
        im_var = Variable(im_data, volatile=True)
        if gpus is not None:
            im_var = im_var.cuda(gpus[0])

        #net_timer.tic()
        output, _ = model(im_var)
        #net_timer.toc()
        # end forward
        
        PAFs, heatmaps= output[0][0].data.cpu().numpy(), output[1][0].data.cpu().numpy()
        PAFs = np.transpose(PAFs, (1, 2, 0))
        heatmaps = np.transpose(heatmaps, (1, 2, 0))
                                    
        seg_pred = output[2][0][0].data.cpu().numpy()
        #seg_pred = cv2.resize(seg_pred, dsize = (origin_w, origin_h))
        
        h, w = im_pad.shape[:2]
        seg_pred = cv2.resize(seg_pred, dsize=(w, h))

        seg = seg_pred[:real_shape[0], :real_shape[1]]
        im_croped = im_pad[:real_shape[0], :real_shape[1]]


        seg_raw = post_process(im_croped, seg, mode = 'raw')
        seg_refined = post_process(im_croped, seg, mode = 'refined')


        heatmap_max = np.max(heatmaps[:, :, :18], 2)
        PAF_max = np.max(PAFs[:, :, :], 2)
        
        to_plot, canvas, _, _ = paf_to_pose(frame_resized, thre_params, heatmaps, PAFs)
        
        
        #seg_mask = np.zeros_like(seg) + 0.2
        #seg_mask[seg > 0.2] = 1
        #seg_im = (im_croped.astype(np.float) * np.expand_dims(seg_mask, -1))
        
        #seg[seg > 0.01] = 1
        #seg_im = (im_croped.astype(np.float) * np.expand_dims(seg, -1))
        alpha = (im_croped.astype(np.float) * np.expand_dims(seg, -1))
        raw_output = (im_croped.astype(np.float) * np.expand_dims(seg_raw, -1))
        refined_output = (im_croped.astype(np.float) * np.expand_dims(seg_refined, -1))

        compiled = np.concatenate([alpha, raw_output, refined_output], axis=1)

        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_compare.png'), compiled)

        cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_raw.png'), alpha)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_raw.png'), seg_pred * 255)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_grab.png'), seg * 255)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_seg.png'), seg_im)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_1heatmap.png'),heatmap_max*255)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_2PAF.png'),PAF_max*255)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_3keypoints.png'),to_plot)
        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_4associations.png'),canvas)
    print 'completed...'


run_test(model, testdata_dir, testresult_dir, params)