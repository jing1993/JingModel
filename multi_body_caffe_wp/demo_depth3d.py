import os
import sys
import time
from multiprocessing.queues import Queue, Empty, Full

import cv2
import numpy as np
import torch
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QTimer, Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QApplication, QSlider
from torch.autograd import Variable
from vispy import app as vispy_app
from vispy import scene
from vispy.scene import cameras

from tnn.network import net_utils
from tnn.utils.im_transform import crop_with_factor
from tnn.utils.path import mkdir
from tnn.utils.timer import Timer
from utils import depth_cloud

from network.depth_small38 import Model


exp_name = 'depth_inception_v3_stage5_small38'
save_dir = 'models/training/{}'.format(exp_name)

ckpt = 'ckpt_90.h5'

model = Model(stages=5, extractor='inception_v3', transform_input=False)


video_dir = 0

inp_size = 320
global_max = 3
global_min = 0

gpus = [0]

output_dir = os.path.join('output', exp_name)
mkdir(output_dir)

net_utils.load_net(ckpt, model)
print('load ckpt from: {}'.format(ckpt))
if gpus is not None:
    model = model.cuda(gpus[0])
model.eval()
net_timer = Timer()


def im_preprocess(image):
    # resize and padding
    real_inp_size = inp_size
    im_pad, im_scale, real_shape = crop_with_factor(image, real_inp_size, factor=8, pad_val=255)

    # preprocess image
    im_croped = cv2.cvtColor(im_pad, cv2.COLOR_BGR2RGB)
    im_croped = im_croped.astype(np.float32) / 255. - 0.5

    return im_croped, im_pad, real_shape


def network_forward(image):
    im_croped, im_pad, real_shape = image if isinstance(image, tuple) else im_preprocess(image)

    # forward
    im_data = torch.from_numpy(im_croped).permute(2, 0, 1)
    im_data = im_data.unsqueeze(0)
    im_var = Variable(im_data, volatile=True)
    if gpus is not None:
        im_var = im_var.cuda(gpus[0])

    net_timer.tic()
    output, _ = model(im_var)
    net_timer.toc()
    # end forward

    depth_pred = output[0].data.squeeze(0).cpu().numpy()[0]
    sn_pred = output[1].data.cpu().numpy()[0]
    sn_pred = np.transpose(sn_pred, [1, 2, 0])
    if len(output) >= 3:
        seg_pred = output[2].data.cpu().numpy()[0, 0]
    else:
        seg_pred = np.zeros_like(depth_pred, dtype=np.float32)

    # crop & remove padding
    h, w = im_pad.shape[:2]
    depth_pred = cv2.resize(depth_pred, dsize=(w, h))
    sn_pred = cv2.resize(sn_pred, dsize=(w, h))
    seg_pred = cv2.resize(seg_pred, dsize=(w, h))
    # wt_pred = cv2.resize(wt_pred, dsize=(w, h))
    hr, wr = real_shape[:2]

    im_croped = im_pad[:real_shape[0], :real_shape[1]]
    depth = depth_pred[:real_shape[0], :real_shape[1]]
    sn = sn_pred[:real_shape[0], :real_shape[1]]
    seg = seg_pred[:real_shape[0], :real_shape[1]]

    # smooth depth
    global global_max, global_min
    momentum = 0.00001
    lmax, lmin = np.max(depth), np.min(depth)
    if global_min is None:
        global_min = lmin
        global_max = lmax
    global_max = max(global_max, lmax) * (1 - momentum) + lmax * momentum
    global_min = min(global_min, lmin) * (1 - momentum) + lmin * momentum

    depth_normalized = (depth - global_min) / max(global_max - global_min, 2)

    return im_croped, depth, depth_normalized, sn, seg


depth_buffer = Queue(maxsize=3)


class DepthThread(QThread):

    def __init__(self, model, device=0):
        super(DepthThread, self).__init__()
        self.model = model

        print(device)
        self.cap = cv2.VideoCapture(device)
        assert self.cap.isOpened(), device

        self.frame_id = 0
        self._stop = False

        self.min_size = 180
        self.scale = 2

        self.person_only = False
        self.seg_threshold = 0.5
        self.cmap_choice = 0 # image, depth, sn

    def change_cmap(self):
        self.cmap_choice = (self.cmap_choice + 1) % 3

    def stop(self):
        self._stop = True

    def run(self):
        while True:
            if self._stop:
                self._stop = False
                break
            if depth_buffer.full():
                time.sleep(0.1)
                continue

            ret, frame = self.cap.read()
            if not ret:
                break

            im_croped, depth_ori, depth, sn, seg = network_forward(frame)

            if self.cmap_choice == 1:
                cimage = depth[:, :, np.newaxis]
            elif self.cmap_choice == 2:
                cimage = np.clip((sn + 1.) * 127.5, 0, 255) / 255.
            else:
                cimage = cv2.cvtColor(im_croped, cv2.COLOR_BGR2RGB) / 255.

            if self.person_only:
                cimage[seg < self.seg_threshold] = 1

            rtns = depth_cloud.depth_to_points(depth_ori / 7., cimage, self.min_size, self.scale)

            self.frame_id += 1

            try:
                depth_buffer.put(rtns, timeout=0.1)
            except Full:
                # print 'Full'
                pass


class Window(QWidget):

    def __init__(self, model, device=0):
        super(Window, self).__init__()

        self.mthread = DepthThread(model, device)

        self.initUI()

        self.timer = QTimer()
        self.timer.timeout.connect(self.on_update)
        self.timer.start(30)

        self.frame_clk = Timer()
        self.frame_cnt = 0
        self.pause = False

        self.mthread.start()

    def initUI(self):
        self.canvas = scene.SceneCanvas(bgcolor='w')
        self.view = self.canvas.central_widget.add_view()
        self.view.camera = cameras.ArcballCamera()
        # view.camera = cameras.TurntableCamera()
        self.view.camera.fov = 60
        self.view.camera.distance = 600
        self.p1 = None

        self.canvas.events.mouse_double_click.connect(self.on_double_click)
        self.canvas.events.key_press.connect(self.keyPressEvent)

        def get_sld(parent, title, min_value, max_value, init_value, slot):
            box = QtWidgets.QHBoxLayout(parent)

            text = QtWidgets.QLabel(title)
            v_text = QtWidgets.QLCDNumber(3)
            v_text.display(init_value)
            sld = QtWidgets.QSlider(Qt.Horizontal)
            sld.setMinimum(min_value)
            sld.setMaximum(max_value)
            sld.setValue(init_value)
            sld.valueChanged.connect(slot)
            sld.valueChanged.connect(v_text.display)

            box.addWidget(text)
            box.addWidget(v_text)
            box.addWidget(sld)
            return box

        # fov
        fov_sld = get_sld(None, 'fov', 30, 120, self.view.camera.fov, self.on_update_fov)
        dis_sld = get_sld(None, 'dis', 200, 800, self.view.camera.distance, self.on_update_distance)

        # vbox.addWidget(fov_sld)
        main_layout = QVBoxLayout()
        main_layout.addLayout(fov_sld)
        main_layout.addLayout(dis_sld)
        main_layout.addWidget(self.canvas.native)

        self.setLayout(main_layout)

        self.setWindowTitle('Depth Cloud')
        self.show()

    @pyqtSlot(int)
    def on_update_fov(self, value):
        self.view.camera.fov = value

    @pyqtSlot(int)
    def on_update_distance(self, value):
        self.view.camera.distance = value

    @pyqtSlot()
    def on_update(self):
        self.frame_clk.tic()

        try:
            rtns = depth_buffer.get_nowait()
            x, y, z, cmap = rtns
            if self.p1 is None:
                self.p1 = scene.visuals.GridMesh(x, z, -y, cmap, shading=None)
                self.view.add(self.p1)
            else:
                self.p1.set_data(x, z, -y, cmap)

            self.frame_clk.toc()
            if self.frame_cnt % 10 == 0:
                print('frame {}, duration: {:.2f}ms, fps: {:.2f}'.format(self.frame_cnt, self.frame_clk.duration * 1000.,
                                                                         1. / self.frame_clk.duration))
                self.frame_clk.clear()

            self.frame_cnt += 1
        except Empty:
            # print 'Empty'
            pass

    @pyqtSlot()
    def on_exit(self):
        self.timer.stop()
        self.mthread.stop()
        # self.mthread.quit()
        # self.mthread.wait()
        exit(0)

    def on_double_click(self, e):
        self.mthread.change_cmap()

    def keyPressEvent(self, e):
        if isinstance(e, QtGui.QKeyEvent):
            e.key = e.text()

        if e.key == 'q':
            self.on_exit()
        elif e.key == 'n':
            self.mthread.person_only = not self.mthread.person_only
        elif e.key == 'm':
            self.mthread.change_cmap()
        elif e.key == 'p':
            self.pause = not self.pause
            self.timer.blockSignals(self.pause)


if __name__ == '__main__':
    app = QApplication([])
    win = Window(model, device=video_dir)

    win.mthread.finished.connect(app.exit)
    app.aboutToQuit.connect(win.on_exit)
    app.aboutToQuit.connect(win.deleteLater)

    sys.exit(app.exec_())
