import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class GTASeg(data.Dataset):
    def __init__(self, root, inp_size=256, feat_stride=4, subset=None, training=True):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training

        self.im_root = os.path.join(self.root, 'origin')
        self.depth_root = os.path.join(self.root, 'depth')
        self.label_root = os.path.join(self.root, 'label')

        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.png']
        if subset is not None:
            self.im_names = self.im_names[subset[0]:subset[1]]

    def __len__(self):
        return len(self.im_names)

    def __getitem__(self, i):
        im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, im_name)
        name = im_name.split('_')[0]
        depth_file = os.path.join(self.depth_root, '{}_depth.h5'.format(name))
        label_file = os.path.join(self.label_root, '{}_label.png'.format(name))

        im = cv2.imread(im_file)
        if im.shape[0] < 600 or im.shape[1] < 600:
            print('W0: {}, imshape: {}'.format(name, im.shape))
            next_i = (i+1) % self.__len__()
            return self.__getitem__(next_i)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        seg_label = cv2.imread(label_file, cv2.IMREAD_GRAYSCALE)

        mask = np.ones_like(seg_label, dtype=np.float32)

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=25, min_scale=0.5, max_scale=1.1)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.018, blur_ksize=2, blur_sigma=3)
            mask = im_transform.apply_affine(mask, *trans_params)
            seg_label = im_transform.apply_affine(seg_label, *trans_params)
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            seg_label = seg_label[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        seg_label = cv2.resize(seg_label, tuple(self.out_size))

        mask2 = np.zeros_like(seg_label, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        label2 = np.zeros_like(seg_label, dtype=np.float32)
        label2[seg_label > 0] = 1
        seg_label = label2

        # mask = ins_map * mask
        im_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(mask)
        seg_data = torch.from_numpy(seg_label)

        return im_data, seg_data, mask_data


def get_loader(root, inp_size=256, feat_stride=4, subset=None, training=True,
               batch_size=32, shuffle=True, num_workers=3):
    dataset = GTASeg(root, inp_size, feat_stride, subset, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
