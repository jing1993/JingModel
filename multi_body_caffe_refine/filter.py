import os
import collections
import cv2

import numpy as np
import torch
import torch.utils.data as data


try:
    import ujson as json
except ImportError:
    import json


def mean_depth(coors):
    total = 0.0
    nums = 0
    for joint in range(len(coors)):
        x = coors[joint, 0]
        y = coors[joint, 1]
        if x >= 0 and y >= 0:
            nums += 1
            total += coors[joint, 2]

    return total/nums


colors = [[255, 0, 0], [255, 0, 170], [85, 255, 0], [0, 85, 255], [255, 255, 0], [0, 255, 0], [170, 0, 255], [0, 255, 255],
            [0, 170, 255], [0, 0, 255], [85, 0, 255], [255, 0, 255],  [255, 0, 85], [255, 0, 0], [255, 0, 0], [255, 0, 170], 
            [85, 255, 0], [0, 85, 255], [255, 255, 0], [0, 255, 0], [170, 0, 255], [0, 255, 255],
            [0, 170, 255], [0, 0, 255], [85, 0, 255], [255, 0, 255], [255, 0, 85], [255, 0, 0]]

label_list = [9, 17, 25, 33, 41, 57, 129, 137, 145, 153, 161, 169, 177, 185]

root = '/data/new_GTA/1'

im_root = os.path.join(root, 'origin')
label_root = os.path.join(root, 'stencil')
instance_root = os.path.join(root, 'instance')
instancevis_root = os.path.join(root, 'instance_vis')
keypoint_root = os.path.join(root, 'keypoint', 'kpts.json')

with open(keypoint_root) as data_file:
    kpts_data = json.load(data_file)
        
im_names = [filename for filename in os.listdir(im_root) if os.path.splitext(filename)[-1] == '.png']

for index in range(len(im_names)):
    """
    if index != 82:
        continue
    """
    im_name = im_names[index]
    im_file = os.path.join(im_root, im_name)
    name = im_name.split('_')[0]
    print name
    if name != 'frame299725':
        continue

    coors = kpts_data[name]

    

    label_file = os.path.join(label_root, '{}_stencil.png'.format(name))
    instance_file = os.path.join(instance_root, '{}_instance.png'.format(name))
    instance_vis = os.path.join(instancevis_root, '{}_instance_vis.png'.format(name))
    instance_v = cv2.imread(instance_vis)
    #print instance_v.shape

    im = cv2.imread(im_file)
    if im.shape[0] < 600 or im.shape[1] < 600:
        continue
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)



    seg_label = cv2.imread(label_file, cv2.IMREAD_GRAYSCALE)
    instance = cv2.imread(instance_file, cv2.IMREAD_GRAYSCALE)



    for num in label_list:
        seg_label[seg_label == num] = 1
    seg_label[seg_label != 1] = 0
    seg_label = seg_label * 255


    isthrow = False
    bbox = []
    for person in coors.keys():
        cur_coors = np.reshape(coors[person], (98, 3))
        x1, x2, y1, y2 = -1, -1, -1, -1
        for j in range(98):
            x = cur_coors[j][0]
            y = cur_coors[j][1]
            if x >= 0 and y >= 0:
                if x1 == -1:
                    x1 = x
                    x2 = x1
                    y1 = y
                    y2 = y1
                else:
                    x1 = min(x1, x)
                    x2 = max(x2, x)
                    y1 = min(y1, y)
                    y2 = max(y2, y)
        if x1 != -1:
            box = [x1, x2, y1, y2, cur_coors]
            bbox.append(box)

    if len(bbox) >= 2:
        for j in range(len(bbox)-1):
            p1 = bbox[j]
            for k in range(j+1, len(bbox)):
                p2 = bbox[k]
                if p2[0] > p1[0] and p2[1] < p1[1] and p2[2] > p1[2] and p2[3] < p1[3]:
                    coors_p2 = p2[-1]
                    coors_p1 = p1[-1]
                    depth_p2 = mean_depth(coors_p2)
                    depth_p1 = mean_depth(coors_p1)
                    if depth_p2 > depth_p1:
                        isthrow = True
                        break
                    
                if p1[0] > p2[0] and p1[1] < p2[1] and p1[2] > p2[2] and p1[3] < p2[3]:
                    coors_p2 = p2[-1]
                    coors_p1 = p1[-1]
                    depth_p2 = mean_depth(coors_p2)
                    depth_p1 = mean_depth(coors_p1)
                    if depth_p1 > depth_p2:
                        isthrow = True
                        break
            if isthrow: break


    if not isthrow:
        person_we_care = []
        for person in coors.keys():
            cur_coors = np.reshape(coors[person], (98, 3))
            total_joints = 0
            outside = 0.0
            for j in range(98):
                x = cur_coors[j][0]
                y = cur_coors[j][1]
                if x >= 0 and y >= 0:
                    total_joints += 1
                    x = int(x)
                    y = int(y)
                    if seg_label[y, x] == 0:
                        outside += 1
            #print total_joints, ', ', outside
            if total_joints != 0 and (outside / total_joints) < 0.5:
                person_we_care.append(person)


        instance[instance > 100] -= 128
        cv2.imwrite('/data/instance.png', (instance * 51).astype(np.uint8))
        unique_numbers = np.unique(instance)
        

        dtype = [('left', int), ('upper', int), ('label', int)]
        left_upper_0 = []
        for it in range(1, len(unique_numbers)):
            cur = (1000, 1000, unique_numbers[it])
            left_upper_0.append(cur)

        left_upper = np.array(left_upper_0, dtype = dtype)


        for row in range(len(instance)):
            for col  in range(len(instance[0])):
                label = instance[row, col]
                if label != 0:
                    for it in range(len(left_upper)):
                        if label == left_upper[it][2]:
                            left = col
                            upper = row

                            left_upper[it][0] = min(left_upper[it][0], left)
                            left_upper[it][1] = min(left_upper[it][1], upper)

        left_upper = np.sort(left_upper, order = ['left', 'upper'])
        

        person_after_filter = []

        dtype = [('x', int), ('y', int), ('person', 'S15')]
        all_keypoints = []
        for p in range(len(person_we_care)):
            this_person = person_we_care[p]
            current_coors = coors[this_person]
            current_coors = np.reshape(current_coors, (98, 3))
            for joint in range(98):
                x = current_coors[joint][0]
                y = current_coors[joint][1]
                if x >= 0 and y >= 0:
                    this = (int(x), int(y), this_person)
                    all_keypoints.append(this)

        all_keypoints = np.array(all_keypoints, dtype = dtype)
        all_keypoints = np.sort(all_keypoints, order = ['x', 'y'])



        color_for_this = []
        pointer = 0
        
        for point in all_keypoints:

            if pointer >= len(left_upper):
                break
            x = point[0]
            y = point[1]

            if instance[y, x] == 0:
                continue
            if instance[y, x] != 255 and point[2] not in person_after_filter:

                label = left_upper[pointer][2]
                indices = np.argwhere(instance == label)
                r = indices[0][0]
                c = indices[0][1]
                cl = instance_v[r][c]
                cl = cl.astype(int)
                
                instance[instance == label] = 255

                pointer += 1
                person_after_filter.append(point[2])

                color_for_this.append(cl)

        if pointer < len(left_upper):
            isthrow = True

        if not isthrow:
            p = 0
            im_c = im.copy()
            for person in coors.keys():
                
                for joint in range(98):
                    x = coors[person][joint * 3]
                    y = coors[person][joint * 3 + 1]
                    if x >= 0 and y >= 0:
                        cv2.circle(im_c, (int(x), int(y)), 4, colors[p], -1)
                p += 1
            cv2.imwrite('/data/result/keep/{}_before.png'.format(index), im_c)
            
            p = 0
            im_copy = im.copy()
            for person in person_after_filter:
                
                for joint in range(98):
                    x = coors[person][joint * 3]
                    y = coors[person][joint * 3 + 1]
                    if x >= 0 and y >= 0:
                        cv2.circle(im_copy, (int(x), int(y)), 4, color_for_this[p], -1)
                p += 1

            cv2.imwrite('/data/result/keep/{}_filter.png'.format(index), im_copy)
            
            #cv2.imwrite('/data/result/keep/{}_seg.png'.format(index), seg_label)
            
            cv2.imwrite('/data/result/keep/{}_instance_vis.png'.format(index), instance_v)





    if isthrow:

        p = 0
        im_copy = im.copy()
        for person in coors.keys():
            for joint in range(98):
                x = coors[person][joint * 3]
                y = coors[person][joint * 3 + 1]
                if x >= 0 and y >= 0:
                    cv2.circle(im_copy, (int(x), int(y)), 4, colors[p], -1)
            p += 1
        cv2.imwrite('/data/result/{}_im.png'.format(index), im_copy)
            






        
                    
                

