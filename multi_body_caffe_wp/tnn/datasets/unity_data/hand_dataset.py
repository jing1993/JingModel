import os
import json

import cv2
import numpy as np

import torch
from torch.utils.data import Dataset
from collections import Counter


class HandDataset(Dataset):
    DATASET_DIR = "/media/data/hands/hands_v0.2/"

    JOINTS_DIR = "_joints"
    BASE_IMAGE_NAME = "hands_v0.2_HANDS_%s%s.png"

    IMG_NAME = "_img"
    DEPTH_NAME = "_depth"
    ID_NAME = "_id"
    INSTANCE_NAME = "_instance"
    LAYERS_NAME = "_layers"
    MOTION_NAME = "_motion"
    NORMALS_NAME = "_normals"
    PARTS_NAME = "_parts"

    HANDS_IDS = list(range(1,19)) + list(range(20, 30)) + list(range(31,41)) + [42, 43]

    def __init__(self, training=True, dataset_dir=None, validation_percentage=0.1):
        if dataset_dir is None:
            self.data_dir = self.DATASET_DIR
        else:
            self.data_dir = dataset_dir

        joints_dir = os.path.join(self.data_dir, self.JOINTS_DIR)
        joints_files = os.listdir(joints_dir)

        # divide dataset into training/validation
        total_size = len(joints_files)
        middle = total_size - int(total_size * validation_percentage)
        if training:
            self.joints_files = joints_files[:middle]
        else:
            self.joints_files = joints_files[middle:]
        self.numSample = len(self.joints_files)

        self.resolution = None

    def get_json_data(self, json_filename):
        file_path = os.path.join(self.data_dir, self.JOINTS_DIR, json_filename)
        hands = {}
        frame_id = None

        with open(file_path, "r") as f:
            data = json.load(f)
            frame_id = data["frame_id"]

            for person in range(data["num_of_people"]):
                joints = data['data'][person]['joints']
                is_visible_left = False
                is_visible_right = False

                left_hand = []
                right_hand = []

                # Find all parts of hands and check whether at least one part is visible.
                for id_ in self.HANDS_IDS:
                    joint = joints[id_-1]
                    if joint['name'].startswith('r'):
                        right_hand += [joint]
                        if joint['xyzv'][0] >= 0.0:
                            is_visible_right = True
                    elif joint['name'].startswith('l'):
                        left_hand += [joint]
                        if joint['xyzv'][0] >= 0.0:
                            is_visible_left = True

                hands[person] = {'left_hand':[is_visible_left, left_hand],
                                 'right_hand':[is_visible_right, right_hand]}

        return hands, frame_id

    def get_file_path(self, part, frame_id):
        return os.path.join(self.data_dir, part, self.BASE_IMAGE_NAME % (frame_id, part))

    def read_image(self, part, frame_id):
        f = self.get_file_path(part, frame_id)
        img = cv2.imread(self.get_file_path(part, frame_id))
        if self.resolution:
            img = cv2.resize(img, self.resolution)
        if img.shape[2] > 2:
            return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        else:
            return img

    def get_image(self, frame_id):
        return self.read_image(self.IMG_NAME, frame_id)

    def get_segmentation(self, frame_id):
        img = self.read_image(self.PARTS_NAME, frame_id)
        # Extract classes
        img = img[:, :, 1]
        # Find pixels that belong to hand
        where = np.isin(img, self.HANDS_IDS)
        # Change all other pixels to background - 0 value
        img = np.where(where, img, 0)

        # Prepare dict with mapping and add channel for background
        mapping = dict([reversed(x) for x in enumerate(self.HANDS_IDS)])
        mapping[0] = len(self.HANDS_IDS)
        keys = sorted(mapping.keys())
        values = np.array(map(lambda x: mapping[x], keys))
        # prepare index for mapping
        index = np.digitize(img.ravel(), keys, right=True)

        # map values
        parts = values[index].reshape(img.shape)

        values = np.array(map(lambda x: 0. if x == 0 else 1., keys))
        mask = values[index].reshape(img.shape)

        return parts, mask

    def get_normals(self, frame_id):
        return self.read_image(self.NORMALS_NAME, frame_id)

    def get_depth(self, frame_id):
        return self.read_image(self.DEPTH_NAME, frame_id)

    def get_instance(self, frame_id):
        return self.read_image(self.INSTANCE_NAME, frame_id)

    def set_resolution(self, resolution):
        self.resolution = resolution

    def __getitem__(self, index):
        json_filename = self.joints_files[index]
        hands, frame_id = self.get_json_data(json_filename)

        image = self.get_image(frame_id)
        parts, mask = self.get_segmentation(frame_id)
        normals = self.get_normals(frame_id)
        depth = self.get_depth(frame_id)

        image = image.astype(np.float32) / 255.
        mask = mask.astype(np.float32)
        normals = normals.astype(np.float32) / 255.
        depth = depth.astype(np.float32) / 255.

        image = torch.from_numpy(image.transpose((2, 0 ,1)))

        mask = np.expand_dims(mask, 0)
        normals = normals.transpose((2, 0, 1))
        depth = np.expand_dims(depth[:, :, 0], 0)

        return image, parts, normals, depth, mask

    def __len__(self):
        return self.numSample
