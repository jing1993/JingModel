# --------------------------------------------------------
# Faster R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick and Sean Bell
# --------------------------------------------------------

import numpy as np
import numpy.random as npr

from tnn.utils.cython_bbox import bbox_overlaps, bbox_intersections
from tnn.network.rpn.generate_anchors import generate_anchors
from tnn.network.rpn.bbox_transform import bbox_transform

DEBUG = False


def anchor_target_layer(gt_boxes, inp_size, out_size, anchor_scales, anchor_aspects):
    """
    Assign anchors to ground-truth targets. Produces anchor classification
    labels and bounding-box regression targets.
    :param gt_boxes: (G, 5) vstack of [x1, y1, x2, y2, class] relative to original image (inp_size)
    :param inp_size: original image size (width, height)
    :param out_size: output image size
    :param anchor_scales: the scales to the basic_anchor (basic anchor is [16, 16])
    :param anchor_aspects: aspect ratio
    :return: labels, (A, H, W)
             bbox_targets, (A*4, H, W)
             bbox_mask, (A*4, H, W)
    """

    _anchors = generate_anchors(base_size=16, ratios=np.array(anchor_aspects), scales=np.array(anchor_scales))
    _num_anchors = _anchors.shape[0]

    # allow boxes to sit over the edge by a small amount
    _allowed_border = 0

    # map of shape (..., H, W)
    inp_width, inp_height = inp_size
    width, height = out_size

    _feat_stride = inp_width // width

    # 1. Generate proposals from bbox deltas and shifted anchors
    shift_x = np.arange(0, width) * _feat_stride
    shift_y = np.arange(0, height) * _feat_stride
    shift_x, shift_y = np.meshgrid(shift_x, shift_y)  # in W H order
    # K is H x W
    shifts = np.vstack((shift_x.ravel(), shift_y.ravel(),
                        shift_x.ravel(), shift_y.ravel())).transpose()
    # add A anchors (1, A, 4) to
    # cell K shifts (K, 1, 4) to get
    # shift anchors (K, A, 4)
    # reshape to (K*A, 4) shifted anchors
    A = _num_anchors
    K = shifts.shape[0]
    all_anchors = (_anchors.reshape((1, A, 4)) +
                   shifts.reshape((1, K, 4)).transpose((1, 0, 2)))
    all_anchors = all_anchors.reshape((K * A, 4))
    total_anchors = int(K * A)

    # only keep anchors inside the image
    inds_inside = np.where(
        (all_anchors[:, 0] >= -_allowed_border) &
        (all_anchors[:, 1] >= -_allowed_border) &
        (all_anchors[:, 2] < inp_width + _allowed_border) &  # width
        (all_anchors[:, 3] < inp_height + _allowed_border)  # height
    )[0]
    # keep only inside anchors
    anchors = all_anchors[inds_inside, :]

    # label: 1 is positive, 0 is negative, -1 is dont care
    # (A)
    labels = np.empty((len(inds_inside),), dtype=np.float32)
    labels.fill(-1)

    # overlaps between the anchors and the gt boxes
    # overlaps (ex, gt), shape is A x G
    overlaps = bbox_overlaps(
        np.ascontiguousarray(anchors, dtype=np.float),
        np.ascontiguousarray(gt_boxes, dtype=np.float))
    argmax_overlaps = overlaps.argmax(axis=1)  # (A)
    max_overlaps = overlaps[np.arange(len(inds_inside)), argmax_overlaps]
    gt_argmax_overlaps = overlaps.argmax(axis=0)  # G
    gt_max_overlaps = overlaps[gt_argmax_overlaps,
                               np.arange(overlaps.shape[1])]
    gt_argmax_overlaps = np.where(overlaps == gt_max_overlaps)[0]

    # assign bg labels first so that positive labels can clobber them
    labels[max_overlaps < 0.3] = 0

    # fg label: for each gt, anchor with highest overlap
    labels[gt_argmax_overlaps] = 1
    # fg label: above threshold IOU
    labels[max_overlaps >= 0.7] = 1

    # subsample positive and negative labels ?

    # bbox_targets = np.zeros((len(inds_inside), 4), dtype=np.float32)
    bbox_targets = _compute_targets(anchors, gt_boxes[argmax_overlaps, :])

    bbox_mask = np.zeros((len(inds_inside), 4), dtype=np.float32)
    bbox_mask[labels == 1, :] = np.ones((1, 4))

    # map up to original set of anchors
    labels = _unmap(labels, total_anchors, inds_inside, fill=-1)
    bbox_targets = _unmap(bbox_targets, total_anchors, inds_inside, fill=0)
    bbox_mask = _unmap(bbox_mask, total_anchors, inds_inside, fill=0)

    # labels
    labels = labels.reshape((height, width, A)).transpose(2, 0, 1)

    # bbox_targets
    bbox_targets = bbox_targets.reshape((height, width, A * 4)).transpose(2, 0, 1)
    bbox_mask = bbox_mask.reshape((height, width, A * 4)).transpose(2, 0, 1)

    return labels, bbox_targets, bbox_mask


def _unmap(data, count, inds, fill=0):
    """ Unmap a subset of item (data) back to the original set of items (of
    size count) """
    if len(data.shape) == 1:
        ret = np.empty((count,), dtype=np.float32)
        ret.fill(fill)
        ret[inds] = data
    else:
        ret = np.empty((count,) + data.shape[1:], dtype=np.float32)
        ret.fill(fill)
        ret[inds, :] = data
    return ret


def _compute_targets(ex_rois, gt_rois):
    """Compute bounding-box regression targets for an image."""

    assert ex_rois.shape[0] == gt_rois.shape[0]
    assert ex_rois.shape[1] == 4
    assert gt_rois.shape[1] >= 4

    targets = bbox_transform(ex_rois, gt_rois[:, :4]).astype(np.float32, copy=False)

    return targets
