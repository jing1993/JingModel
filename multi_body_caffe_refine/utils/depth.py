import os
import cv2
import numpy as np
#import skimage.morphology
import h5py

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.autograd.function import Function


def eval_WKDR(pred, pts, num_pts, threshes):

    bsize = pred.size(0)
    eq_correct_counts = np.zeros(len(threshes), dtype=np.float)
    not_eq_correct_counts = np.zeros(len(threshes), dtype=np.float)
    eq_count = 0
    not_eq_count = 0
    for i in range(bsize):
        pt1_x, pt1_y = pts[i, :num_pts[i], 0], pts[i, :num_pts[i], 1]
        pt2_x, pt2_y = pts[i, :num_pts[i], 2], pts[i, :num_pts[i], 3]
        gt = pts[i, :num_pts[i], 4]

        x1 = torch.index_select(pred[i], 1, pt1_x).gather(0, pt1_y.contiguous().view(1, -1)).view(-1)
        x2 = torch.index_select(pred[i], 1, pt2_x).gather(0, pt2_y.contiguous().view(1, -1)).view(-1)
        mask = torch.abs(gt)

        not_eq_count += mask.sum()
        eq_count += torch.sum(1 - mask)

        d = x1 - x2
        for j, thresh in enumerate(threshes):
            rlt = torch.sign(d).type_as(mask) * (torch.abs(d).gt(thresh)).type_as(mask)
            corrects = rlt.eq(gt).type_as(mask)
            not_eq_correct_counts[j] += torch.sum(corrects * mask)
            eq_correct_counts[j] += torch.sum(corrects * (1 - mask))

    # print eq_count, not_eq_count, eq_correct_count, not_eq_correct_count

    return eq_correct_counts, not_eq_correct_counts, eq_count, not_eq_count


def error_metrics(pred, gt, mask=None, ims=None):
    assert pred.ndim == 3 and gt.ndim == 3
    if pred.shape != gt.shape:
        if pred.ndim == 2:
            dsize = (gt.shape[1], gt.shape[0])
            pred = cv2.resize(pred, dsize)
        else:
            new_pred = []
            dsize = (gt[0].shape[1], gt[0].shape[0])
            for p in pred:
                p = cv2.resize(p, dsize)
                new_pred.append(p)
            pred = np.asarray(new_pred, dtype=np.float)

    im2show = None
    if ims is not None:
        im = np.transpose(ims[0], [1, 2, 0])
        im = cv2.cvtColor((im * 255).astype(np.uint8), cv2.COLOR_RGB2BGR)
        im2show = plot_result(im, pred[0], gt[0])

    if mask is None:
        # mask = np.ones(gt.shape, dtype=np.int)
        mask = gt > 0
    n_pxls = np.sum(mask, axis=(1, 2))

    mask = ~mask.astype(np.bool)
    # Mean Absolute Relative Error
    rel = np.abs(gt - pred) / (gt + 1e-8)
    rel[mask] = 0
    rel = np.sum(rel, axis=(1, 2)) / n_pxls

    # Root Mean Squared Error
    rms = (gt - pred) ** 2
    rms[mask] = 0
    rms = np.sqrt(np.sum(rms, axis=(1, 2)) / n_pxls)

    # LOG10 Error
    pred[pred <= 0] = 1e-8
    gt[gt <= 0] = 1e-8
    lg10 = np.abs(np.log10(gt) - np.log10(pred))
    lg10[mask] = 0
    lg10 = np.sum(lg10, axis=(1, 2)) / n_pxls

    return rel, rms, lg10, im2show


def _depth_to_normal(depth, im_order=True):
    expand = False
    if depth.ndim == 2:
        depth = np.expand_dims(depth, 0)
        expand = True

    dzdx = (depth[:, 2:, 1:-1] - depth[:, 0:-2, 1:-1]) / 2.
    dzdy = (depth[:, 1:-1, 2:] - depth[:, 1:-1, 0:-2]) / 2.

    sn = np.stack((-dzdx, dzdy, np.ones_like(dzdx) * 0.0007), axis=3)
    # sn = np.stack((-dzdx, -np.ones_like(dzdx) * 0.0007, dzdy), axis=3)
    sn = sn / np.linalg.norm(sn, axis=-1, keepdims=True)
    # cv2.normalize()
    sn = np.pad(sn, ((0, 0), (1, 1), (1, 1), (0, 0)), mode='constant')

    if not im_order:
        sn = np.transpose(sn, (0, 3, 1, 2))

    if expand:
        sn = sn[0]
    return sn


def depth_to_normal(depth, z_scale=0.001, im_order=True):
    expand = False
    h, w = depth.shape
    if depth.ndim == 2:
        depth = depth.reshape(1, 1, h, w)
        expand = True

    wx = np.array([[1, 0, -1],
                   [2, 0, -2],
                   [1, 0, -1]], dtype=np.float32) / 8.
    wy = np.array([[1, 2, 1],
                   [0, 0, 0],
                   [-1, -2, -1]], dtype=np.float32) / 8.
    wx_var = Variable(torch.from_numpy(wx)).view(1, 1, 3, 3)
    wy_var = Variable(torch.from_numpy(wy)).view(1, 1, 3, 3)

    depth_var = Variable(torch.from_numpy(depth.astype(np.float32)))
    dzdx = F.conv2d(depth_var, wx_var, padding=1, groups=depth_var.size(1)).data.numpy()
    dzdy = F.conv2d(depth_var, wy_var, padding=1, groups=depth_var.size(1)).data.numpy()

    # sn = np.stack((-dzdx, dzdy, np.ones_like(dzdx) * 0.0007), axis=3)
    sn = np.concatenate((dzdy, -dzdx, np.ones_like(dzdx) * z_scale), axis=1)
    sn = sn / np.linalg.norm(sn, axis=1, keepdims=True)

    if im_order:
        sn = sn.transpose((0, 2, 3, 1))

    if expand:
        sn = sn[0]

    return sn


class Depth2Normal(nn.Module):
    def __init__(self, in_channels, bias=False, groups=None, require_grad=False):
        super(Depth2Normal, self).__init__()

        self.in_channels = in_channels
        self.groups = in_channels if groups is None else groups
        self.conv = nn.Conv2d(in_channels, 2, kernel_size=3, groups=self.groups, bias=bias, padding=1)

        self.conv.weight.requires_grad = require_grad
        self.reset_parameters()

    def reset_parameters(self):
        wx = np.array([[1, 0, -1],
                       [2, 0, -2],
                       [1, 0, -1]], dtype=np.float32) / 8.
        wy = np.array([[1, 2, 1],
                       [0, 0, 0],
                       [-1, -2, -1]], dtype=np.float32) / 8.
        ws = np.expand_dims(np.stack((wx, wy)), 1).repeat(self.in_channels // self.groups, axis=1)
        self.conv.weight.data.copy_(torch.from_numpy(ws))

        if self.conv.bias is not None:
            self.conv.bias.data.zero_()

    def forward(self, x):
        x = self.conv(x)
        return x


def depth_to_normal_torch(depth):
    expand = False
    if depth.dim() == 2:
        depth = depth.unsqueeze(0)
        expand = True

    dzdx = (depth[:, 2:, 1:-1] - depth[:, 0:-2, 1:-1]) / 2.
    dzdy = (depth[:, 1:-1, 2:] - depth[:, 1:-1, 0:-2]) / 2.

    is_var = isinstance(depth, Variable)
    ones = dzdx.clone() if not is_var else dzdx.data.clone()
    ones.zero_()
    ones = ones + 0.0007
    if is_var:
        ones = Variable(ones)

    sn = torch.stack((-dzdx, ones, dzdy), dim=1)
    sn = sn / torch.norm(sn, dim=1).unsqueeze(1)

    sn = F.pad(sn, (1, 1, 1, 1))
    if expand:
        sn = sn[0]
    return sn


def plot_result(im, pred, gt=None, mask=None, instance=None, pts=None, colormap=cv2.COLORMAP_JET, hstack=True):
    im = im.copy()
    if pred.dtype.name != 'uint8':
        pred = (
            ((pred - np.min(pred)) / max(np.max(pred) - np.min(pred), 0.001))
            * 255).astype(np.uint8)

    sampled_pts = None
    if pts is not None:
        sampled_pts = np.copy(pts[np.random.choice(len(pts), min(200, len(pts)), replace=False)])
        sampled_pts = sampled_pts.astype(np.float)
        sampled_pts[:, 0:4:2] *= float(im.shape[1]) / pred.shape[1]
        sampled_pts[:, 1:4:2] *= float(im.shape[0]) / pred.shape[0]
        sampled_pts = sampled_pts.astype(np.int)

    dsize = (im.shape[1], im.shape[0])
    if im.shape[0:2] != pred.shape[0:2]:
        pred = cv2.resize(pred, dsize)
    if colormap is not None:
        pred_ht = cv2.applyColorMap(pred, colormap)
    else:
        pred_ht = cv2.cvtColor(pred, cv2.COLOR_GRAY2BGR)

    if hstack is None:
        return pred_ht

    stack_fun = np.hstack if hstack else np.vstack
    im2show = stack_fun((im, pred_ht))

    if gt is not None:
        gt = (((gt - np.min(gt)) / max(np.max(gt) - np.min(gt), 0.001)) * 255).astype(np.uint8)
        gt = cv2.resize(gt, dsize)
        gt_ht = cv2.applyColorMap(gt, colormap)

        if sampled_pts is not None:
            for pt in sampled_pts:
                pt1 = tuple(pt[0:2])
                pt2 = tuple(pt[2:4])
                color1 = (0, 0, 255) if pt[4] <= 0 else (255, 0, 0)
                color2 = (0, 0, 255) if pt[4] >= 0 else (255, 0, 0)
                cv2.circle(gt_ht, pt1, 2, color1, thickness=2)
                cv2.circle(gt_ht, pt2, 2, color2, thickness=2)
                cv2.line(gt_ht, pt1, pt2, (0, 255, 0), thickness=1)

        im2show = stack_fun((im2show, gt_ht))

    if mask is not None:
        if mask.ndim == 2:
            mask = ((mask) * 255.).astype(np.uint8)
            mask = cv2.resize(mask, dsize)
            # mask = cv2.applyColorMap(mask, cv2.COLORMAP_JET)
            mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        else:
            mask = cv2.resize(mask, dsize)
        im2show = stack_fun((im2show, mask))

    if instance is not None:
        if instance.dtype != np.uint8:
            instance = ((instance - np.min(instance)) / max(np.max(instance) - np.min(instance), 0.001) * 255).astype(np.uint8)
            instance = cv2.resize(instance, dsize)
            instance = cv2.applyColorMap(instance, cv2.	COLORMAP_RAINBOW)
        else:
            instance = cv2.resize(instance, dsize)
        im2show = stack_fun((im2show, instance))

    return im2show


class BerhuLoss(Function):
    def __init__(self, q=80):
        self.q = q
        self.c = None

    def forward(self, x):
        x_abs = torch.abs(x)
        c = np.percentile(x_abs.cpu().numpy(), self.q) + 1e-6
        self.c = c
        loss = x_abs

        idx = x_abs > c
        loss2 = (x ** 2 + c ** 2) / (2. * c)
        loss[idx] = loss2[idx]
        self.save_for_backward(x)

        return loss

    def backward(self, grad_output):
        x = self.saved_tensors[0]
        c = self.c

        idx = torch.abs(x) > c
        grad_input = grad_output.clone()
        grad_input = grad_input * torch.sign(x)

        grad_input[idx] = grad_output[idx] * x[idx] / c

        return grad_input


class DepthLoss(nn.Module):
    def __init__(self, num_grad=0):
        self.num_grad = num_grad
        super(DepthLoss, self).__init__()

    def forward(self, pred, gt, mask):

        bsize = pred.size(0)
        pred_vec = pred.view(bsize, -1)
        gt_vec = gt.view(bsize, -1)
        mask_vec = mask.view(bsize, -1)

        p = pred_vec * mask_vec
        t = gt_vec * mask_vec
        d = p - t

        nvalid_pix = torch.sum(mask_vec, 1)
        depth_error = (torch.sum(nvalid_pix * torch.sum(d**2, 1)) - 0.5 * torch.sum(torch.sum(d, 1)**2))\
                      / max(torch.sum(nvalid_pix.data**2), 1)
        # depth_error = torch.sum(BerhuLoss()(pred - gt) * mask) / (torch.sum(mask) + 1.)

        pred = torch.squeeze(pred, dim=1)
        gt = torch.squeeze(gt, dim=1)
        mask = torch.squeeze(mask, dim=1)

        def cal_grad_cost(h=1):
            p_di = (pred[:, h:, :] - pred[:, :-h, :]) / float(h)
            p_dj = (pred[:, :, h:] - pred[:, :, :-h]) / float(h)

            t_di = (gt[:, h:, :] - gt[:, :-h, :]) / float(h)
            t_dj = (gt[:, :, h:] - gt[:, :, :-h]) / float(h)

            m_di = mask[:, h:, :] * mask[:, :-h, :]
            m_dj = mask[:, :, h:] * mask[:, :, :-h]
            grad_cost = torch.sum(m_di * (p_di - t_di) ** 2) / torch.sum(m_di) + torch.sum(m_dj * (p_dj - t_dj) ** 2) / torch.sum(m_dj)

            return grad_cost

        grad_cost = 0
        for i in range(1, self.num_grad + 1):
            grad_cost = grad_cost + cal_grad_cost(i)

        return depth_error, grad_cost


class RelativeLoss(nn.Module):
    def __init__(self, ohnm=-1, size_average=True, only_sign=False, ignore_eq=False, num_scale=1):
        super(RelativeLoss, self).__init__()
        self.ohnm = ohnm
        self.size_average = size_average
        self.only_sign = only_sign
        self.ignore_eq = ignore_eq
        self.num_scale = num_scale

    # def rlt_loss(self, pt1_x, pt1_y, pt2_x, pt2_y, gt, depth_map):
    #     x1 = torch.index_select(depth_map, 1, pt1_x).gather(0, pt1_y.contiguous().view(1, -1))
    #     x2 = torch.index_select(depth_map, 1, pt2_x).gather(0, pt2_y.contiguous().view(1, -1))
    #     mask = torch.abs(gt)
    #
    #     d = x1 - x2
    #     if self.only_sign:
    #         val2 = torch.abs(d.sign() - gt)
    #     else:
    #         val2 = torch.log(1 + torch.exp(-d * gt))
    #     depth_loss = mask * val2
    #
    #     if not self.ignore_eq:
    #         val1 = d ** 2
    #         depth_loss = depth_loss + (1 - mask) * val1
    #     return depth_loss

    def _rlt_loss1(self, d1, d2, gt):
        mask = torch.abs(gt)

        d = d1 - d2
        if self.only_sign:
            val2 = torch.abs(d.sign() - gt)
        else:
            val2 = torch.log(1 + torch.exp(-d * gt))
        loss = mask * val2

        if not self.ignore_eq:
            val1 = d ** 2
            loss = loss + (1 - mask) * val1

        return loss

    def rlt_loss2(self, pts, num_pt, depth_map):
        # x1, y1, x2, y2, x3, y3
        pt1_x, pt1_y = pts[0:num_pt, 0], pts[0:num_pt, 1]
        pt2_x, pt2_y = pts[0:num_pt, 2], pts[0:num_pt, 3]
        pt3_x, pt3_y = pts[0:num_pt, 4], pts[0:num_pt, 5]

        # gt1, gt2, gt3
        gt1 = pts[0:num_pt, 6].type_as(depth_map)
        gt2 = pts[0:num_pt, 7].type_as(depth_map)
        gt3 = pts[0:num_pt, 8].type_as(depth_map)

        d1 = torch.index_select(depth_map, 1, pt1_x).gather(0, pt1_y.contiguous().view(1, -1))
        d2 = torch.index_select(depth_map, 1, pt2_x).gather(0, pt2_y.contiguous().view(1, -1))
        d3 = torch.index_select(depth_map, 1, pt3_x).gather(0, pt3_y.contiguous().view(1, -1))

        # 1st loss
        loss1 = self._rlt_loss1(d1, d2, gt1)
        loss2 = self._rlt_loss1(d1, d3, gt2)
        loss3 = self._rlt_loss1(d2, d3, gt3)

        # 2nd loss
        # dgt1, dgt2, dgt3
        dgt1 = pts[0:num_pt, 9].type_as(depth_map)
        dgt2 = pts[0:num_pt, 10].type_as(depth_map)
        dgt3 = pts[0:num_pt, 11].type_as(depth_map)
        dd1 = torch.abs(d1 - d2)
        dd2 = torch.abs(d1 - d3)
        dd3 = torch.abs(d2 - d3)
        dloss1 = self._rlt_loss1(dd1, dd2, dgt1)
        dloss2 = self._rlt_loss1(dd1, dd3, dgt2)
        dloss3 = self._rlt_loss1(dd2, dd3, dgt3)

        total_loss = loss1 + loss2 + loss3 + dloss1 + dloss2 + dloss3
        return total_loss

    def forward(self, pred, pts, num_pts):
        """
        
        :param pred: [bsize, 1, H, W]
        :param pts: [bsize, MAX_PTS, 5]
        :param num_pts: [bsize]
        :return: 
        """
        depth_pred = pred[:, 0, :, :]
        bsize = depth_pred.size(0)
        num_pts = num_pts.data

        # print num_pts
        n_point_total = 0

        total_loss = 0
        for i in range(bsize):
            num_pt = max(1, int(num_pts[i] * self.num_scale))
            depth_map = depth_pred[i]

            depth_loss = self.rlt_loss2(pts[i], num_pt, depth_map)

            if self.ohnm > 0:
                k = int(self.ohnm * num_pt) + 1
                k = min(k, num_pt)
                depth_loss, inds = torch.topk(depth_loss, k, largest=True)

            total_loss += torch.sum(depth_loss)
            n_point_total += depth_loss.size()[-1]
        if self.size_average:
            total_loss = total_loss / n_point_total

        return total_loss, n_point_total


def fake_pts(im_shape=None):
    h, w = (0, 0) if im_shape is None else im_shape[0:2]
    fake_data = np.asarray((w // 2, h // 2, w // 2, h // 2, w // 2, h // 2, 0, 0, 0, 0, 0, 0), dtype=np.int64)
    return fake_data


def sample_from_two_mask(gt, mask1, mask2, max_pts=6000, thresh1=0.02, thresh2=0.02):
    h, w = gt.shape[:2]
    max_gt = np.max(gt)

    valid_idxes1 = np.where(mask1 > 0)
    valid_idxes2 = np.where(mask2 > 0)
    valid_idxes3 = np.where((mask1 > 0) | (mask2 > 0))
    if len(valid_idxes1[0]) == 0 or len(valid_idxes2[0]) == 0:
        return fake_pts(gt.shape)
    inds1 = np.random.choice(len(valid_idxes1[0]), size=max_pts)
    y1s, x1s = valid_idxes1[0][inds1], valid_idxes1[1][inds1]

    inds2 = np.random.choice(len(valid_idxes2[0]), size=max_pts)
    y2s, x2s = valid_idxes2[0][inds2], valid_idxes2[1][inds2]

    inds3 = np.random.choice(len(valid_idxes3[0]), size=max_pts)
    y3s, x3s = valid_idxes3[0][inds3], valid_idxes3[1][inds3]

    # depth diff
    gt_norm = (gt - gt.min()) / (gt.max() - gt.min() + 1e-5)
    d1 = gt_norm[y1s, x1s]
    d2 = gt_norm[y2s, x2s]
    d3 = gt_norm[y3s, x3s]

    dd1 = np.abs(d1 - d2)
    dd2 = np.abs(d1 - d3)
    dd3 = np.abs(d2 - d3)

    def get_sign(d1, d2, thresh):
        ds = (d1 - d2) / (np.minimum(d1, d2) + 1)
        ds_sign = np.sign(ds)
        ds_sign[np.abs(ds) < thresh] = 0
        return ds_sign

    pts = zip(
        x1s, y1s, x2s, y2s, x3s, y3s,
        get_sign(d1, d2, thresh1), get_sign(d1, d3, thresh1), get_sign(d2, d3, thresh1),
        get_sign(dd1, dd2, thresh2), get_sign(dd1, dd3, thresh2), get_sign(dd2, dd3, thresh2)
    )

    pts = np.asarray(pts, dtype=np.int64)
    return pts


def sample_pts(gt, mask, max_pts=6000, dis_scale=40, thresh1=0.02, thresh2=0.02):
    h, w = gt.shape[:2]

    max_gt = np.max(gt)
    valid_idxes = np.where(mask > 0)
    if len(valid_idxes[0]) == 0:
        return fake_pts(gt.shape)

    inds = np.random.choice(len(valid_idxes[0]), size=max_pts)
    y1s, x1s = valid_idxes[0][inds], valid_idxes[1][inds]

    # random angle and r
    n_idxes = len(x1s)
    rs = np.random.uniform(4, dis_scale, (n_idxes, 2))
    ags = np.random.uniform(0, 2 * np.pi, (n_idxes, 2))
    dxs, dys = rs * np.sin(ags), rs * np.cos(ags)
    xs, ys = np.round(x1s.reshape(-1, 1) + dxs).astype(np.int), np.round(y1s.reshape(-1, 1) + dys).astype(np.int)
    x2s = xs[:, 0]
    y2s = ys[:, 0]
    x3s = xs[:, 1]
    y3s = ys[:, 1]

    # filter out pts
    keep = (x2s >= 0) & (x2s < w) & (y2s >= 0) & (y2s < h) & (x3s >= 0) & (x3s < w) & (y3s >= 0) & (y3s < h)
    x1s = x1s[keep]
    y1s = y1s[keep]
    x2s = x2s[keep]
    y2s = y2s[keep]
    x3s = x3s[keep]
    y3s = y3s[keep]
    if len(x1s) == 0:
        return fake_pts(gt.shape)
    keep = (mask[y2s, x2s] > 0) & (mask[y3s, x3s] > 0)
    x1s = x1s[keep]
    y1s = y1s[keep]
    x2s = x2s[keep]
    y2s = y2s[keep]
    x3s = x3s[keep]
    y3s = y3s[keep]
    if len(x1s) == 0:
        return fake_pts(gt.shape)

    # depth diff
    gt_norm = (gt - gt.min()) / (gt.max() - gt.min() + 1e-5)
    d1 = gt_norm[y1s, x1s]
    d2 = gt_norm[y2s, x2s]
    d3 = gt_norm[y3s, x3s]

    dd1 = np.abs(d1 - d2)
    dd2 = np.abs(d1 - d3)
    dd3 = np.abs(d2 - d3)

    def get_sign(d1, d2, thresh):
        ds = (d1 - d2) / (np.minimum(d1, d2) + 1)
        ds_sign = np.sign(ds)
        ds_sign[np.abs(ds) < thresh] = 0
        return ds_sign

    pts = zip(
        x1s, y1s, x2s, y2s, x3s, y3s,
        get_sign(d1, d2, thresh1), get_sign(d1, d3, thresh1), get_sign(d2, d3, thresh1),
        get_sign(dd1, dd2, thresh2), get_sign(dd1, dd3, thresh2), get_sign(dd2, dd3, thresh2)
    )
    pts = np.asarray(pts, dtype=np.int64)
    return pts


def watershed_cut(watershed, seg, wt_threshold=1, min_size=20):
    """

    :param watershed: watershed prediction [HxW]
    :param seg: seg mask [HxW]
    :param wt_threshold:
    :param min_size:
    :return:
    """
    seg = seg.astype(np.int32)
    result_im = np.zeros_like(seg)

    seg2 = seg.astype(np.float)
    watershed = watershed * seg
    seg2[watershed > 2] = 0.5

    seg2_im = cv2.applyColorMap((seg2*255).astype(np.uint8), cv2.COLORMAP_JET)
    cv2.imshow('seg2', seg2_im)

    ins_im = ((watershed > wt_threshold)).astype(bool)
    ins_im = skimage.morphology.remove_small_objects(ins_im, min_size=min_size)
    ins_im = skimage.morphology.remove_small_holes(ins_im)
    ins_labels = skimage.morphology.label(ins_im)

    selem = np.ones((3, 3), dtype=bool) if wt_threshold == 1 else np.ones((5, 5), dtype=bool)
    ins_ids = np.unique(ins_labels)[1:]
    for i, ins_id in enumerate(ins_ids):
        ins_mask = (ins_labels == ins_id)
        ins_mask = skimage.morphology.binary_dilation(ins_mask, selem)
        result_im[ins_mask] = i + 1

    return result_im



if __name__ == '__main__':
    def main1():
        import matplotlib.pyplot as plt
        im = cv2.imread('/data/GTA/examples/frame0_origin.png')
        df5 = h5py.File('/data/GTA/examples/frame0_depth.h5', mode='r')
        sn_gt_im = cv2.imread('/data/GTA/examples/frame0_surfaceNorm.png')
        fx = 0.5
        im = cv2.resize(im, None, fx=fx, fy=fx)
        sn_gt_im = cv2.resize(sn_gt_im, None, fx=fx, fy=fx)
        sn_gt = sn_gt_im / 127.5 - 1

        depth = np.asarray(df5['depth'])
        depth = cv2.resize(depth, None, fx=fx, fy=fx)
        # depth = np.exp(-3 * depth)
        depth2 = 0.1 / (np.exp(1 * (depth + 1e-5)) - 1)
        depth = (depth2 - depth2.min()) / (depth2.max() - depth2.min())
        plt.imshow(depth2, cmap='hot')
        plt.show()

        sn2 = _depth_to_normal(depth)
        sn = depth_to_normal(depth2)

        sn_im = ((sn + 1) * 127.5).astype(np.uint8)
        sn_im2 = ((sn2 + 1) * 127.5).astype(np.uint8)
        d_im = depth

        cv2.imshow('depth', d_im)
        cv2.imshow('sn', sn_im)
        cv2.imshow('sn2', sn_im2)
        cv2.imshow('sn_gt', sn_gt_im)

        cv2.waitKey(0)
    main1()

    # def test_sample():
    #     gt = np.random.randn(240, 320).astype(np.float32)
    #     mask = np.random.randn(240, 320)
    #     mask = (mask > 0).astype(np.float32)
    #     sample_pts(gt, mask, max_pts=6000)
    #
    # test_sample()