import cPickle
import os

import cv2
import numpy as np
import torch
import torch.utils.data as data

import utils.heatmaps as heatmap_utils
import utils.pose as pose_utils
from tnn.datasets.dataloader import sDataLoader


class MPIIDataset(data.Dataset):
    def __init__(self, root, anno_pk, sub_set=None, inp_size=256, feat_stride=4, training=True):
        self.root = root
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.training = training

        self.im_root = os.path.join(self.root, 'images')
        self.heatmap_size = self.inp_size // self.feat_stride

        with open(os.path.join(root, anno_pk), 'r') as f:
            self.annotations = cPickle.load(f)
        if sub_set is not None:
            self.annotations = self.annotations[sub_set[0]:sub_set[1]]

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, i):
        obj = self.annotations[i]

        im_name = os.path.join(self.im_root, obj['image'])
        im_orig = cv2.imread(im_name)
        im = cv2.cvtColor(im_orig, cv2.COLOR_BGR2RGB)

        # transform
        center, scale = self.data_transform(obj['center'], obj['scale'], im_orig.shape) \
            if self.training else (obj['center'], obj['scale'])
        # center, scale = obj['center'], obj['scale']

        src_size = [200*scale] * 2
        dst_size = self.inp_size
        im_croped = pose_utils.crop(im, center, src_size, dst_size)
        if im_croped is None:
            return self.__getitem__(i)
        im_croped = im_croped.astype(np.float32) / 255.

        # heatmap_data, visible = None, None
        # if self.training:
        src_pts = obj['points']
        dst_pts = pose_utils.transform(src_pts, center, src_size, self.heatmap_size, invert=False)

        # random noise
        std = obj['headSize'] * 1.0
        pre_pts = self.random_pts(src_pts, std)
        pre_pts = pose_utils.transform(pre_pts, center, src_size, self.inp_size, invert=False)

        visible = obj['visible']
        heatmaps = []
        pre_heatmap = np.zeros(self.inp_size, dtype=np.float)
        for i, pt in enumerate(dst_pts):
            x, y = pt
            if visible[i] <= 0 or x < 0 or y < 0 or x >= self.heatmap_size[0] or y >= self.heatmap_size[1]:
                visible[i] = 0
                heatmaps.append(np.zeros(self.heatmap_size, dtype=np.float))
                continue
            heatmaps.append(heatmap_utils.make_joint_heatmap(self.heatmap_size[0], self.heatmap_size[1],
                                                             x, y, kernel_size=5, kernel_sigma=2))
            pre_x, pre_y = pre_pts[i]
            if 0 <= pre_x < self.inp_size[0] and 0 <= pre_y < self.inp_size[1]:
                pre_heatmap += heatmap_utils.make_joint_heatmap(self.inp_size[0], self.inp_size[1],
                                                                pre_x, pre_y, kernel_size=30, kernel_sigma=10)

        heatmaps = np.asarray(heatmaps, dtype=np.float32)
        heatmap_data = torch.from_numpy(heatmaps)

        # pre_heatmap = torch.from_numpy(pre_heatmap)
        pre_heatmap = np.expand_dims(pre_heatmap, axis=2).astype(np.float32)
        im_data = np.dstack([im_croped, pre_heatmap])
        im_data = torch.from_numpy(im_data)
        im_data = im_data.permute(2, 0, 1)

        visible = np.asarray(visible, dtype=np.float32)
        visible = torch.from_numpy(visible)

        return im_orig, im_data, heatmap_data, visible, obj

    @staticmethod
    def random_pts(pts, std):
        pts = np.asarray(pts, dtype=np.float)
        noise = np.random.normal(0, std, pts.shape)
        return pts + noise

    @staticmethod
    def data_transform(center, scale, im_shape):
        h, w, _ = im_shape

        rw = scale*20
        center_ = np.asarray(center, dtype=np.float)
        center_ += (np.random.uniform(-rw, rw, 2))
        if center_[0] <= rw or center_[0] > w-rw or center_[1] <= rw or center_[1] > h-rw:
            center_ = center

        scale *= (1. + np.random.uniform(-0.1, 0.1))

        return center_, scale


def collate_fn(data):
    im_orig, im_data, heatmap_data, visible, obj = zip(*data)
    im_data = torch.stack(im_data, 0)

    heatmap_data = torch.stack(heatmap_data, 0)
    visible = torch.stack(visible, 0)
    visible = torch.unsqueeze(visible, 2)
    visible = torch.unsqueeze(visible, 3)

    return im_orig, im_data, heatmap_data, visible, obj


def get_loader(root, anno_pk, sub_set=None, inp_size=256, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = MPIIDataset(root, anno_pk, sub_set, inp_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/home/long/work/code/binary-human-pose-estimation_pytorch/data/mpii',
                                'mpii_dataset_train.pk', (-3000, None), inp_size=256, feat_stride=4,
                                training=True, batch_size=3)
        for i, batch in enumerate(train_data):
            im_orig, im_data, heatmap_data, visible, obj = batch
            print len(im_orig)
            print type(im_data), im_data.size()
            # print heatmap_data, visible
            print type(heatmap_data), heatmap_data.size()
            print visible.shape

            cv2.imwrite('origin.jpg', im_orig[0])
            cv2.imwrite('croped.jpg', im_data.numpy()[0])
            # cv2.imshow('orgin', im_orig[0])
            # cv2.imshow('croped', im_data.numpy()[0])
            for i in range(len(heatmap_data.numpy()[0])):
                cv2.imwrite('ht_{}.jpg'.format(i), heatmap_data.numpy()[0][i] * 255)
                # cv2.waitKey(0)
            assert False
    main()


