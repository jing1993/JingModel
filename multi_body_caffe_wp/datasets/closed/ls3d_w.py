import cPickle
import os

import cv2
import numpy as np
import torch
import torch.utils.data as data
from torch.utils.serialization import load_lua

import utils.heatmaps as heatmap_utils
import utils.pose as pose_utils
from tnn.datasets.dataloader import sDataLoader


class LS3D(data.Dataset):
    def __init__(self, root, sub_set=None, inp_size=256, feat_stride=4, training=True):
        self.root = root
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.training = training

        self.heatmap_size = self.inp_size // self.feat_stride

        self.cache_name = os.path.join(root, 'filenames.pk')
        self.filenames = self.load_dataset(re_load=False)

        if sub_set is not None:
            self.filenames = self.filenames[sub_set[0]:sub_set[1]]

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, i):
        im_name = self.filenames[i]
        t7_name = os.path.splitext(im_name)[0] + '.t7'

        im_orig = cv2.imread(im_name)
        im = cv2.cvtColor(im_orig, cv2.COLOR_BGR2RGB)
        pts = load_lua(t7_name).numpy()

        center, scale, bbox_size = self.bounding_box(pts)

        obj = {'center': center, 'scale': scale, 'points': pts, 'headSize': bbox_size, 'image': os.path.split(im_name)[-1]}

        # transform
        center, scale = self.data_transform(center, scale, im_orig.shape) \
            if self.training else (center, scale)
        # center, scale = obj['center'], obj['scale']

        src_size = [200*scale] * 2
        dst_size = self.inp_size
        im_croped = pose_utils.crop(im, center, src_size, dst_size)
        if im_croped is None:
            return self.__getitem__(i)
        im_croped = im_croped.astype(np.float32) / 255.

        im_data = torch.from_numpy(im_croped)
        im_data = im_data.permute(2, 0, 1)

        # heatmap_data, visible = None, None
        # if self.training:
        dst_pts = pose_utils.transform(pts, center, src_size, self.heatmap_size, invert=False)
        visible = np.ones([len(pts)], dtype=np.float32)
        heatmaps = []
        for i, pt in enumerate(dst_pts):
            x, y = pt
            if visible[i] <= 0 or x < 0 or y < 0 or x >= self.heatmap_size[0] or y >= self.heatmap_size[1]:
                visible[i] = 0
                heatmaps.append(np.zeros(self.heatmap_size, dtype=np.float))
                continue
            heatmaps.append(heatmap_utils.make_joint_heatmap(self.heatmap_size[0], self.heatmap_size[1],
                                                             x, y, kernel_size=5, kernel_sigma=2))

        heatmaps = np.asarray(heatmaps, dtype=np.float32)
        heatmap_data = torch.from_numpy(heatmaps)

        visible = torch.from_numpy(visible)

        return im_orig, im_data, heatmap_data, visible, obj

    def load_dataset(self, re_load=False):
        if os.path.isfile(self.cache_name) and not re_load:
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        filenames = []
        for root, dirs, files in os.walk(self.root):
            for fname in files:
                name, ext = os.path.splitext(fname)
                if ext == '.jpg':
                    filenames.append(os.path.join(root, fname))

        with open(self.cache_name, 'w') as f:
            cPickle.dump(filenames, f)

        return filenames

    def bounding_box(self, pts):
        mins = np.min(pts, axis=0)
        maxs = np.max(pts, axis=0)
        x1, y1 = mins
        x2, y2 = maxs
        w = x2 - x1
        h = y2 - y1

        center = np.array([(x1 + x2) / 2., (y1 + y2) / 2.], dtype=np.float)
        center[1] -= h * 0.12

        scale = (w + h) / 195.
        bbox_size = np.sqrt(w * h)

        return center, scale, bbox_size

    @staticmethod
    def data_transform(center, scale, im_shape):
        h, w, _ = im_shape

        rw = scale*20
        center_ = np.asarray(center, dtype=np.float)
        center_ += (np.random.uniform(-rw, rw, 2))
        if center_[0] <= rw or center_[0] > w-rw or center_[1] <= rw or center_[1] > h-rw:
            center_ = center

        scale *= (1. + np.random.uniform(-0.1, 0.1))

        return center_, scale


def collate_fn(data):
    im_orig, im_data, heatmap_data, visible, obj = zip(*data)
    im_data = torch.stack(im_data, 0)

    if heatmap_data[0] is not None:
        heatmap_data = torch.stack(heatmap_data, 0)
        visible = torch.stack(visible, 0)
        visible = torch.unsqueeze(visible, 2)
        visible = torch.unsqueeze(visible, 3)
        # visible = np.asarray(visible, dtype=np.int)
    else:
        heatmap_data, visible = None, None

    return im_orig, im_data, heatmap_data, visible, obj


def get_loader(root, sub_set=None, inp_size=256, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = LS3D(root, sub_set, inp_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/media/longc/LData/data/face/LS3D-W/new_dataset',
                                (-3000, None), inp_size=256, feat_stride=4,
                                training=True, batch_size=3)
        for i, batch in enumerate(train_data):
            im_orig, im_data, heatmap_data, visible, obj = batch
            print len(im_orig)
            print type(im_data), im_data.size()
            # print heatmap_data, visible
            print type(heatmap_data), heatmap_data.size()
            print visible.size()

            # cv2.imwrite('origin.jpg', im_orig[0])
            # cv2.imwrite('croped.jpg', im_data.numpy()[0])
            cv2.imshow('orgin', im_orig[0])
            cv2.imshow('croped', im_data.numpy()[0].transpose([1, 2, 0]))
            cv2.imshow('ht', heatmap_data.numpy()[0][0] * 255)

            # for i in range(len(heatmap_data.numpy()[0])):
                # cv2.imwrite('ht_{}.jpg'.format(i), heatmap_data.numpy()[0][i] * 255)

            cv2.waitKey(0)
            # assert False
    main()


