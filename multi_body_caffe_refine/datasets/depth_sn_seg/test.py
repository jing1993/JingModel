import os
import sys
os.chdir('../../')
sys.path.insert(0, '.')
import numpy as np

from datasets.depth_sn_seg.gta_depth import GTADepth
from datasets.depth_sn_seg.unity_hand import UnityHand
# from datasets.depth_sn_seg.rhd_hand import RHDHand
from datasets.depth_sn_seg.rhd_human import RHDHand
from datasets.depth_sn_seg.nyu_depth_labeled_rlt import NYUDepthLabeledRLT
from datasets.depth_sn_seg.nyu_depth_rlt import NYUDepthRLT
from datasets.depth_sn_seg.cityscapes import CityScapes


def test():
    num_test = 10
    thresh1 = 0.009
    thresh2 = 0.008

    # dataset = GTADepth('data/GTA/indoor', (368, 368),
    #                    feat_stride=2, max_pts=10000, thresh1=0.006, thresh2=0.006, subset=None, training=True)

    # dataset = UnityHand('/media/data/synthetic/hands_v0.6', (368, 368),
    #                    feat_stride=2, max_pts=10000, thresh1=thresh1, thresh2=thresh2, train_set=True, training=True)

    dataset = RHDHand('/data/RHD_all/training', (256, 256),
                       feat_stride=2, max_pts=10000, thresh1=0.015, thresh2=0.01, training=True)

    # dataset = NYUDepthRLT('data/nyu_depth_v2', 'train_data.h5', (368, 368),
    #                       feat_stride=2, max_pts=10000, thresh1=0.012, thresh2=0.012, training=True)

    # dataset = NYUDepthLabeledRLT('data/nyu_depth_v2', 'train', (368, 368),
    #                                feat_stride=2, max_pts=10000, thresh1=0.012, thresh2=0.012, training=True)

    # dataset = CityScapes('data/CityScapes', 'train', (368, 368),
    #                      feat_stride=2, max_pts=10000, thresh1=0.03, thresh2=0.03, training=True)

    total = 0
    n_gts = np.zeros(3, dtype=np.float)
    n_dgts = np.zeros(3, dtype=np.float)
    for i in range(num_test):
        rtns = dataset[i]
        pts, num_pts = rtns[-2:]
        print(num_pts)

        pts = pts[:num_pts].numpy()
        total += num_pts * 3
        n_gts[0] += np.sum(pts[:, 6:9] == 0)
        n_gts[1] += np.sum(pts[:, 6:9] > 0)
        n_gts[2] += np.sum(pts[:, 6:9] < 0)

        n_dgts[0] += np.sum(pts[:, 9:12] == 0)
        n_dgts[1] += np.sum(pts[:, 9:12] > 0)
        n_dgts[2] += np.sum(pts[:, 9:12] < 0)

    print(n_gts/total, total/3./num_test)
    print(n_dgts/total, total/3./num_test)

test()

