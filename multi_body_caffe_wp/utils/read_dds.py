from struct import pack, unpack, calcsize
import numpy as np
import os
from scipy.misc import imsave
import h5py


fields = (
    ('size', 0), ('flags', 1), ('height', 2),
    ('width', 3), ('pitchOrLinearSize', 4), ('depth', 5),
    ('mipmapCount', 6), ('pf_size', 18), ('pf_flags', 19),
    ('pf_fourcc', 20), ('pf_rgbBitCount', 21), ('pf_rBitMask', 22),
    ('pf_gBitMask', 23), ('pf_bBitMask', 24), ('pf_aBitMask', 25),
    ('caps1', 26), ('caps2', 27))


def read_DX10(data, width, height):
    fmt = 'fBccc' * width * height
    ldata = calcsize(fmt)
    assert ldata == len(data)

    udata = unpack(fmt, data)
    depth = np.asarray(udata[0::5], dtype=np.float32).reshape([height, width])
    label = np.asarray(udata[1::5], dtype=np.int).reshape([height, width])

    return depth, label


def read_dds(filename):
    with open(filename, 'rb') as fd:
        data = fd.read()

    # read header
    fmt = 'I' * 31
    fmt_size = calcsize(fmt)
    pf_size = calcsize('I' * 8)
    header, data = data[4:4 + fmt_size], data[4 + fmt_size:]
    assert len(header) == fmt_size

    # depack
    header = unpack(fmt, header)
    meta = dict()
    for name, index in fields:
        meta[name] = header[index]

    depth, label = read_DX10(data[20:], meta['width'], meta['height'])

    return depth, label


def mkdir(path):
    if not os.path.exists(path):
        os.mkdir(path)


if __name__ == '__main__':
    import sys

    if len(sys.argv) == 1:
        print 'Usage: python ddsfile.py <path> ...'
        sys.exit(0)
    root = sys.argv[1]
    # root = '.'

    filenames = os.listdir(root)
    parent = os.path.split(root)[0]
    depth_dir = os.path.join(parent, 'out_depth')
    label_dir = os.path.join(parent, 'out_label')

    mkdir(depth_dir)
    mkdir(label_dir)

    person_labels = [1, 2, ]
    for i, filename in enumerate(filenames):
        name, ext = os.path.splitext(filename)
        if ext != '.dds':
            continue
        print '=== Loading {}/{}'.format(i, len(filenames)), filename
        filename = os.path.join(root, filename)
        depth, label = read_dds(filename)

        depth = depth.astype(np.float32)
        label_int8 = np.zeros_like(label, dtype=np.uint8)
        for v in person_labels:
            label_int8[label == v] = 255

        # save
        # np.save(os.path.join(depth_dir, '{}_depth.npy'.format(name)), depth)
        h5f = h5py.File(os.path.join(depth_dir, '{}_depth.h5'.format(name)), 'w')
        h5f.create_dataset('depth', data=depth, compression='gzip')

        imsave(os.path.join(label_dir, '{}_label.png'.format(name)), label_int8)
