import torch.utils.data as data
import numpy as np


class JointDataset(data.Dataset):
    def __init__(self, datasets, scales=None):
        self.datasets = datasets
        self.scales = [1] * len(datasets) if scales is None else scales
        self.lens = [int(len(self.datasets[i]) * self.scales[i]) for i in range(len(datasets))]
        print([len(d) for d in self.datasets])

    def __len__(self):
        return sum(self.lens)

    def __getitem__(self, i):
        set_i = 0
        while set_i < len(self.datasets) and i >= self.lens[set_i]:
            i -= self.lens[set_i]
            set_i += 1

        if self.scales[set_i] > 1:
            real_len = len(self.datasets[set_i])
            i = i % real_len

            scale = self.scales[set_i] - int(self.scales[set_i])
            s = 1 if scale < 1e-3 else 1. / scale
        else:
            s = 1. / self.scales[set_i]

        i = int(np.round(i * s + np.random.uniform(0, s))) % len(self.datasets[set_i])

        return self.datasets[set_i][i]


if __name__ == '__main__':
    class TestData(data.Dataset):
        def __init__(self, size=1000):
            self.size = size
            self.hits = set()
            self.counts = np.zeros(self.size, dtype=np.int)

        def __len__(self):
            return self.size

        def __getitem__(self, item):
            self.hits.add(item)
            self.counts[item] += 1
            if len(self.hits) == self.size:
                print('TestData hits = {}'.format(len(self.hits)))
                self.hits.clear()
            return item


    def test():
        dataset = JointDataset([TestData(100), TestData(10)], scales=[0.5, 5])

        epoch = 2000
        for k in range(epoch):
            for i in range(len(dataset)):
                v = dataset[i]
                # if i % 10 == 0:
                #     print(k, i, v)
        for _dataset in dataset.datasets:
            print(_dataset.counts / float(_dataset.counts.sum()) * 100)
    test()

