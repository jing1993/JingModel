import numpy as np
import tnn.utils.bbox as bbox_utils
import cv2


def imcv2_recolor(im, a=.1, noise_scale=0.015, blur_ksize=-1, blur_sigma=-1, mask=None):
    # t = [np.random.uniform()]
    # t += [np.random.uniform()]
    # t += [np.random.uniform()]
    # t = np.array(t) * 2. - 1.
    t = np.random.uniform(-1, 1, 3)
    t = np.reshape(t, [1, 1, 3])

    # random amplify each channel
    im = im.astype(np.float)
    im *= (1 + t * a)
    mx = 255. * (1 + a)
    up = np.random.uniform(-1, 1)
    im = np.power(im / mx, 1. + up * .1)

    if blur_ksize > 0 and blur_sigma > 0:
        ksize = np.abs(np.random.normal(loc=0, scale=blur_ksize, size=2).astype(int))
        xy = np.abs(np.random.normal(loc=0, scale=blur_sigma, size=2).astype(int))
        im = cv2.GaussianBlur(im, ksize=(2 * ksize[0] + 1, 2 * ksize[1] + 1), sigmaX=xy[0], sigmaY=xy[1])
        if mask is not None:
            mask = cv2.GaussianBlur(mask, ksize=(2 * ksize[0] + 1, 2 * ksize[1] + 1), sigmaX=xy[0], sigmaY=xy[1])

    noise = np.random.randn(*im.shape) * noise_scale
    im = np.clip(im + noise, 0, 1)
    im = np.array(im * 255., np.uint8)

    if mask is not None:
        return im, mask
    return im


def imcv2_affine_trans(coors, im, flip=None, dst_shape=None, rotate=None, min_scale=1., max_scale=1.5, max_offset=None, replicate_edge=True):
    # Scale and translate
    h, w = im.shape[:2] if dst_shape is None else dst_shape[:2]
    scale = np.random.uniform(min_scale, max_scale)

    degree = np.random.uniform(-rotate, rotate) if rotate is not None else None

    max_offx = max_offset if max_offset is not None else max(1. - dst_shape[0]/(scale*w), 0.02)
    max_offy = max_offset if max_offset is not None else max(1. - dst_shape[1]/(scale*h), 0.02)
    offx = int(np.random.uniform(-1, 1) * scale * w * max_offx)
    offy = int(np.random.uniform(-1, 1) * scale * h * max_offy)

    # max_offx = (scale - 1.) * w
    # max_offy = (scale - 1.) * h
    # offx = int(np.random.uniform() * max_offx)
    # offy = int(np.random.uniform() * max_offy)

    flip_ = np.random.uniform() > 0.5 if flip is None else flip

    if im is not None:
        coors, im = apply_affine_for_im(coors, im, scale, [offx, offy, degree], flip_, dst_shape, replicate_edge)

    return coors, im, [scale, [offx, offy, degree], flip_, dst_shape, replicate_edge]

def rotatepoint(p, R):
    point = np.zeros((3, 1))
    point[0] = p[0]
    point[1] = p[1]
    point[2] = 1

    new_point = R.dot(point)

    p[0] = new_point[0]

    p[1] = new_point[1]
    return p

def apply_affine_for_im(coors, im, scale, offs, flip, dst_shape=None, replicate_edge=True, interpolation=cv2.INTER_LINEAR, pad_val=0):
    offx, offy, degree = offs
    # ori_h, ori_w = im.shape[:2] if dst_shape is None else dst_shape[:2]

    # resize
    im = cv2.resize(im, (0, 0), fx=scale, fy=scale, interpolation=interpolation)
    for p in range(len(coors)):
        for j in range(len(coors[p])):
            coors[p, j, :2] *= scale

    # rotate
    if degree is not None:
        imh, imw = im.shape[:2]
        degree_ratio = np.abs(degree / 180. * np.pi)
        dst_h = int(imh * np.cos(degree_ratio) + imw * np.sin(degree_ratio))
        dst_w = int(imw * np.cos(degree_ratio) + imh * np.sin(degree_ratio))

        retval = cv2.getRotationMatrix2D((imw // 2, imh // 2), degree, 1)

        for p in range(len(coors)):
            for j in range(len(coors[p])):
                x = coors[p, j, 0]
                y = coors[p, j, 1]
                coors[p, j, :2] = rotatepoint([x, y], retval)

        if im.ndim == 3:
            if replicate_edge:
                im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_REPLICATE)
            else:
                constant_values = np.random.randint(0, 256, size=3)
                im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_CONSTANT, borderValue=constant_values)
        else:
            im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_CONSTANT, borderValue=pad_val)


    im_h, im_w = im.shape[0:2]
    # crop
    dst_h, dst_w = dst_shape[0:2]
    x1, y1, x2, y2 = offx, offy, offx + dst_w, offy + dst_h
    padx1 = max(0, -x1)
    padx2 = max(0, x2 - im_w)
    pady1 = max(0, -y1)
    pady2 = max(0, y2 - im_h)
    x1, x2 = x1 + padx1, x2 + padx1
    y1, y2 = y1 + pady1, y2 + pady1

    for p in range(len(coors)):
        for j in range(len(coors[p])):
            coors[p, j, 0] = coors[p, j, 0] + padx1 - x1
            coors[p, j, 1] = coors[p, j, 1] + pady1 - y1


    if im.ndim == 3:
        if replicate_edge:
            im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2), (0, 0)), mode='edge')
        else:
            constant_values = np.random.randint(0, 256)
            im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2), (0, 0)), mode='constant', constant_values=constant_values)
    else:
        im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2)), mode='constant', constant_values=pad_val)
    im = im_pad[y1:y2, x1:x2]



    if flip:
        im = cv2.flip(im, 1)
        w = im.shape[1]

        for p in range(len(coors)):
            for j in range(len(coors[p])):
                coors[p, j, 0] = w - coors[p, j, 0]


    return coors, im

def apply_affine(im, scale, offs, flip, dst_shape=None, replicate_edge=True, interpolation=cv2.INTER_LINEAR, pad_val=0):
    offx, offy, degree = offs
    # ori_h, ori_w = im.shape[:2] if dst_shape is None else dst_shape[:2]

    # resize
    im = cv2.resize(im, (0, 0), fx=scale, fy=scale, interpolation=interpolation)

    # rotate
    if degree is not None:
        imh, imw = im.shape[:2]
        degree_ratio = np.abs(degree / 180. * np.pi)
        dst_h = int(imh * np.cos(degree_ratio) + imw * np.sin(degree_ratio))
        dst_w = int(imw * np.cos(degree_ratio) + imh * np.sin(degree_ratio))

        retval = cv2.getRotationMatrix2D((imw // 2, imh // 2), degree, 1)
        if im.ndim == 3:
            if replicate_edge:
                im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_REPLICATE)
            else:
                constant_values = np.random.randint(0, 256, size=3)
                im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_CONSTANT, borderValue=constant_values)
        else:
            im = cv2.warpAffine(im, retval, (dst_w, dst_h), borderMode=cv2.BORDER_CONSTANT, borderValue=pad_val)

    im_h, im_w = im.shape[0:2]
    # crop
    dst_h, dst_w = dst_shape[0:2]
    x1, y1, x2, y2 = offx, offy, offx + dst_w, offy + dst_h
    padx1 = max(0, -x1)
    padx2 = max(0, x2 - im_w)
    pady1 = max(0, -y1)
    pady2 = max(0, y2 - im_h)
    x1, x2 = x1 + padx1, x2 + padx1
    y1, y2 = y1 + pady1, y2 + pady1
    if im.ndim == 3:
        if replicate_edge:
            im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2), (0, 0)), mode='edge')
        else:
            constant_values = np.random.randint(0, 256)
            im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2), (0, 0)), mode='constant', constant_values=constant_values)
    else:
        im_pad = np.pad(im, ((pady1, pady2), (padx1, padx2)), mode='constant', constant_values=pad_val)
    im = im_pad[y1:y2, x1:x2]

    if flip:
        im = cv2.flip(im, 1)

    return im


def offset_boxes(boxes, scale, offs, flip, dst_shape, is_pts=False):
    if len(boxes) == 0:
        return boxes

    boxes = np.asarray(boxes, dtype=np.float)
    expand = False
    if boxes.ndim == 1:
        expand = True
        boxes = np.expand_dims(boxes, 0)

    boxes *= scale
    boxes[:, 0::2] -= offs[0]
    boxes[:, 1::2] -= offs[1]

    if flip:
        boxes[:, 0::2] = dst_shape[1] - boxes[:, 0::2]
        if not is_pts:
            for i in range(boxes.shape[-1] // 4):
                tmp = boxes[:, i].copy()
                boxes[:, i] = boxes[:, i + 2]
                boxes[:, i + 2] = tmp

    if expand:
        boxes = boxes[0]
    return boxes


def _factor_closest(num, factor, is_ceil=True):
    num = np.ceil(float(num) / factor) if is_ceil else np.floor(float(num) / factor)
    num = int(num) * factor
    return num


def crop_with_factor(im, dest_size, factor=32, is_ceil=True, pad_val=0, basedon='min'):
    im_shape = im.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])

    im_base = im_size_min
    if basedon == 'min':
        im_base = im_size_min
    elif basedon == 'max':
        im_base = im_size_max
    elif basedon == 'w':
        im_base = im.shape[1]
    elif basedon == 'h':
        im_base = im.shape[0]

    im_scale = float(dest_size) / im_base
    im = cv2.resize(im, None, fx=im_scale, fy=im_scale)

    h, w = im.shape[:2]
    new_h = _factor_closest(h, factor=factor, is_ceil=is_ceil)
    new_w = _factor_closest(w, factor=factor, is_ceil=is_ceil)

    if im.ndim == 3:
        c = im.shape[-1]
        im_croped = np.zeros([new_h, new_w, c], dtype=im.dtype)
    else:
        im_croped = np.zeros([new_h, new_w], dtype=im.dtype)

    im_croped[...] = pad_val
    im_croped[0:h, 0:w] = im

    return im_croped, im_scale, im.shape


def getTransform(center, src_size, dst_size):
    # h = 200 * scale
    w, h = src_size
    dst_w, dst_h = dst_size

    t = np.eye(3)

    # scale
    t[0, 0] = float(dst_w) / w
    t[1, 1] = float(dst_h) / h

    # translate
    t[0, 2] = dst_w * (-float(center[0]) / w + 0.5)
    t[1, 2] = dst_h * (-float(center[1]) / h + 0.5)

    return t


def transform(pts, center, src_size, dst_size, invert):
    """Transform the coordinates from the original image space to the cropped one"""
    pts = np.asarray(pts, dtype=np.float)
    expanded = False
    if pts.ndim == 1:
        expanded = True
        pts = np.expand_dims(pts, 0)

    pt_new = np.ones([len(pts), 3], dtype=np.float)
    pt_new[:, 0] = pts[:, 0]
    pt_new[:, 1] = pts[:, 1]

    t = getTransform(center, src_size, dst_size)
    if invert:
        t = np.linalg.inv(t)
    t = t.transpose()

    new_point = np.matmul(pt_new, t).astype(np.int)[:, 0:2]
    if expanded:
        new_point = new_point[0]
    return new_point


def crop(img, center, src_size, dst_size, min_size=10):
    """Crop based on the image center & scale, img: RGB uint8, center: (cx, cy)"""

    # to match the origin lua code, we set index from 1
    # l1 = transform((1, 1), center, scale, res, invert=True)
    # l2 = transform((res, res), center, scale, res, invert=True)
    pts = transform([[0, 0], dst_size], center, src_size, dst_size, invert=True)
    l1, l2 = bbox_utils.int_box(pts)
    if (l2[1] - l1[1]) < min_size or (l2[0] - l1[0]) < min_size:
        return None

    if img.ndim < 3:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

    # img = img.astype(np.float) / 255.
    newImg = np.zeros((l2[1] - l1[1], l2[0] - l1[0], 3), dtype=np.uint8)

    height, width = img.shape[:2]

    newX = (max(0, -l1[0]), min(l2[0], width) - l1[0])
    newY = (max(0, -l1[1]), min(l2[1], height) - l1[1])
    oldX = (max(0, l1[0]), min(l2[0], width))
    oldY = (max(0, l1[1]), min(l2[1], height))

    newImg[newY[0]:newY[1], newX[0]:newX[1], :] = img[oldY[0]:oldY[1], oldX[0]:oldX[1], :]

    newImg = cv2.resize(newImg, tuple(dst_size))
    return newImg