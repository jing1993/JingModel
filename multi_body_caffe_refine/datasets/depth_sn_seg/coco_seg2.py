import os
import cv2
import numpy as np
import torch
import torch.utils.data as data
import h5py
from scipy.misc import imread

import tnn.utils.im_transform as im_transform
import utils.depth as depth_utils
import datasets.data_utils as data_utils


class CocoSeg(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=4, max_pts=6000, save_and_factor=-1, training=True):

        assert name in ('train', 'test', 'val'), name
        assert os.path.isdir(root), root

        self.root = root
        self.img_type = name + '2014'

        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.max_pts = max_pts
        self.training = training
        self.save_and_factor = save_and_factor
        self.mask_root = os.path.join(root, 'mask2014')
        self.seg_root = os.path.join(root, 'mask2014_semantic')
        self.dwt_root = os.path.join(root, 'mask2014_dwt')
        self.dwtmask_root = os.path.join(root, 'mask2014_dwtmask')

        seg_list = os.listdir(self.seg_root)
        self.seg_names = [v for v in seg_list if name in v]

    def __len__(self):
        return len(self.seg_names)

    def read_images(self, i):
        img_idx = self.seg_names[i][-16:-4]

        img_type = self.img_type

        img = cv2.imread(os.path.join(self.root, 'images', img_type, 'COCO_' + img_type + '_' + img_idx + '.jpg'))
        assert img is not None, 'no image found in: {}'.format(
            os.path.join(self.root, 'images', img_type, 'COCO_' + img_type + '_' + img_idx + '.jpg'))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        seg_file = os.path.join(self.seg_root, '{}_mask_semantic_{}.png'.format(img_type, img_idx))
        mask_file = os.path.join(self.mask_root, '{}_mask_miss_{}.png'.format(img_type, img_idx))
        dwt_file = os.path.join(self.dwt_root, '{}_mask_dwt_{}.png'.format(img_type, img_idx))
        dwtmask_file = os.path.join(self.dwtmask_root, '{}_mask_dwtmask_{}.png'.format(img_type, img_idx))

        seg = cv2.imread(seg_file, 0)
        seg_mask = cv2.imread(mask_file, 0)
        dwt_label = imread(dwt_file).astype(np.uint8)
        h5f = h5py.File(dwtmask_file, mode='r')
        dwt_mask = np.asarray(h5f['dwtmask'], dtype=np.float32)

        return img, seg, seg_mask, dwt_label, dwt_mask

    def __getitem__(self, i):

        im, seg, seg_mask, dwt_label, dwt_mask = self.read_images(i)
        seg = (seg > 0).astype(np.float32)
        seg_mask = (seg_mask > 0).astype(np.float32)
        dwt_mask *= seg_mask
        # dwt_mask[dwt_label <= 10] *= 2
        additions = (seg, dwt_label, dwt_mask)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 0.8
        max_scale = target_scale * 1.2
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 35,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': False
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        seg_mask = im_transform.apply_affine(seg_mask, *trans_params, pad_val=1)
        seg, dwt_label, dwt_mask = additions
        # image recolor
        im, seg = im_transform.imcv2_recolor(im, a=0.05, noise_scale=0.015, blur_ksize=2, blur_sigma=2, mask=seg)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # =========== end data augmentation ==============

        seg = cv2.resize(seg, tuple(self.out_size))
        seg_mask = cv2.resize(seg_mask, tuple(self.out_size))
        seg_mask[seg_mask > 0.99] = 1
        seg_mask[seg_mask <= 0.99] = 0
        dwt_label = cv2.resize(dwt_label, tuple(self.out_size), interpolation=cv2.INTER_NEAREST)
        dwt_mask = cv2.resize(dwt_mask, tuple(self.out_size))

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        im_data = torch.from_numpy(im_data)
        seg_data = torch.from_numpy(seg.astype(np.float32))
        seg_mask = torch.from_numpy(seg_mask)
        dwt_label = torch.from_numpy(dwt_label.astype(np.int64))
        dwt_mask = torch.from_numpy(dwt_mask)

        # fake data
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        num_pts = 1
        ow, oh = self.out_size
        pts[0] = depth_utils.fake_pts(self.out_size)
        pts = torch.from_numpy(pts)

        depth_data = torch.zeros(oh, ow)
        sn_data = torch.zeros(3, oh, ow)
        sn_mask = torch.zeros(1, oh, ow)

        return im_data, depth_data, sn_data, seg_data, dwt_label, seg_mask, sn_mask, dwt_mask, pts, num_pts
        #
        # gts = torch.stack((depth_data, seg_data, seg_mask), 0) # 3xHxW
        # sn_gts = torch.cat((sn_data, sn_mask), 0)   # 4xHxW
        # gts = torch.cat((gts, sn_gts), 0) # 7xHxW
        # return im_data, gts, pts, num_pts
