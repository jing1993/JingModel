import os
import cv2
import numpy as np
import torch
import torch.utils.data as data
import h5py
import json
from scipy.misc import imread

import tnn.utils.im_transform as im_transform
import tnn.utils.bbox as bbox_utils
import utils.depth as depth_utils
import datasets.data_utils as data_utils
from tnn.utils.cython_bbox import bbox_ious
from utils.mot import read_mot_results


class MOTCLS(data.Dataset):
    def __init__(self, root, name, seq_names=None, inp_size=256, feat_stride=4, max_bboxes=6000, training=True):

        assert name in ('train', 'test'), name
        assert os.path.isdir(root), root

        self.root = os.path.join(root, name)
        self.seq_names = [p for p in os.listdir(self.root) if os.path.isdir(os.path.join(self.root, p))] if seq_names is None else seq_names

        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.max_bboxes = max_bboxes
        self.training = training

        self.im_names, self.dets, self.gts = self.load_seqs(self.seq_names)
        self.additional_bboxes = dict([(seq, dict()) for seq in self.seq_names])

    def __len__(self):
        return len(self.im_names)

    def load_seqs(self, seq_names):
        im_names = []
        dets = dict()
        gts = dict()
        for seq_name in seq_names:
            im_root = os.path.join(self.root, seq_name, 'img1')
            im_names.extend(
                sorted([os.path.join(im_root, name) for name in os.listdir(im_root) if
                        os.path.splitext(name)[-1] == '.jpg'])
            )

            det_file = os.path.join(self.root, seq_name, 'det', 'det.txt')
            dets[seq_name] = read_mot_results(det_file, is_gt=False)

            gt_file = os.path.join(self.root, seq_name, 'gt', 'gt.txt')
            gts[seq_name] = read_mot_results(gt_file, is_gt=True)

        return im_names, dets, gts

    def load_bboxes(self, bbox_dir):
        total_bboxes = 0
        for seq_name in self.seq_names:
            bbox_file = os.path.join(bbox_dir, '{}.txt'.format(seq_name))
            bboxes = read_mot_results(bbox_file, is_gt=False)
            for k, v in bboxes.items():
                # self.additional_bboxes[seq_name].setdefault(k, [])
                # self.additional_bboxes[seq_name][k].extend(v)
                self.additional_bboxes[seq_name][k] = v
                total_bboxes += len(v)
        print('load {} bboxes from {}'.format(total_bboxes, bbox_dir))

    def read_images(self, i):
        im_name = self.im_names[i]
        p, frame = os.path.split(im_name)
        frame = int(os.path.splitext(frame)[0])
        seq_name = os.path.split(os.path.split(p)[0])[-1]

        gts = self.gts[seq_name].get(frame, [])
        gts, _ = zip(*gts) if len(gts) > 0 else (np.empty([0, 5]), np.empty([0, 1]))

        dets = self.dets[seq_name].get(frame, [])
        dets, _ = zip(*dets) if len(dets) > 0 else (np.empty([0, 5]), np.empty([0, 1]))

        bboxes = self.additional_bboxes[seq_name].get(frame, [])
        bboxes, _ = zip(*bboxes) if len(bboxes) > 0 else (np.empty([0, 5]), np.empty([0, 1]))

        gts = np.asarray(gts)[:, 0:4]
        dets = np.asarray(dets)[:, 0:4]
        bboxes = np.asarray(bboxes)[:, 0:4]
        dets = np.concatenate((dets, bboxes), 0)

        img = cv2.imread(im_name)
        assert img is not None, 'no image found in: {}'.format(im_name)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        return img, dets, gts

    def __getitem__(self, i):

        im, dets, gts = self.read_images(i)

        # =========== data augmentation ==============

        target_size = self.inp_size[0]
        if min(im.shape[0:2]) > 720:
            target_size = 640
        target_scale = float(target_size) / min(im.shape[0:2])

        min_scale = target_scale * 0.7
        max_scale = target_scale * 1.2
        dst_shape = (self.inp_size[1], self.inp_size[0])
        aug_kwargs = {
            'dst_shape': dst_shape,
            'rotate': None,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': False
        }
        im, additions, trans_params = data_utils.data_augmentation(im, **aug_kwargs)
        # image recolor
        im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.010, blur_ksize=1, blur_sigma=1)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # offset gt bboxes
        gts = np.asarray(gts, dtype=np.int)
        gt_bboxes = gts[:, :4]
        gt_bboxes[:, 2:4] += gt_bboxes[:, 0:2]    # x1, y1, x2, y2
        gt_bboxes = im_transform.offset_boxes(gt_bboxes, *trans_params[:4], is_pts=False)
        gt_bboxes = bbox_utils.clip_boxes(gt_bboxes, dst_shape)

        s = (gt_bboxes[:, 2] - gt_bboxes[:, 0]) * (gt_bboxes[:, 3] - gt_bboxes[:, 1])
        gt_bboxes = gt_bboxes[(s > 200)]

        dets = np.asarray(dets, dtype=np.int)
        det_bboxes = dets[:, :4]
        det_bboxes[:, 2:4] += det_bboxes[:, 0:2]  # x1, y1, x2, y2
        det_bboxes = im_transform.offset_boxes(det_bboxes, *trans_params[:4], is_pts=False)
        det_bboxes = bbox_utils.clip_boxes(det_bboxes, dst_shape)

        # =========== end data augmentation ==============

        gt_bboxes = gt_bboxes.astype(np.float)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        im_data = torch.from_numpy(im_data)

        # sampling bboxes for cls
        all_bboxes = np.concatenate((gt_bboxes, det_bboxes), axis=0)
        sampled_bboxes = np.zeros([self.max_bboxes, 4], dtype=np.float)

        n_gts = min(self.max_bboxes, len(gt_bboxes))
        sampled_bboxes[:n_gts] = gt_bboxes[:n_gts]
        n_sampled = n_gts
        if len(all_bboxes) > 0:
            idxs = np.random.choice(len(all_bboxes), int((self.max_bboxes - n_sampled) * 0.98))
            tmp_bboxes = all_bboxes[idxs].copy()
            cx = np.mean(tmp_bboxes[:, 0::2], axis=1)
            cy = np.mean(tmp_bboxes[:, 1::2], axis=1)
            hs = (tmp_bboxes[:, 3] - tmp_bboxes[:, 1]) * 0.5
            ws = (tmp_bboxes[:, 2] - tmp_bboxes[:, 0]) * 0.5

            offset_x = np.random.standard_normal(len(tmp_bboxes)) * ws * 0.5
            offset_y = np.random.standard_normal(len(tmp_bboxes)) * hs * 0.5

            hs += np.random.standard_normal(len(hs)) * hs * 0.08
            ws += np.random.standard_normal(len(ws)) * ws * 0.08
            cx = cx + offset_x
            cy = cy + offset_y

            tmp_bboxes = np.stack((cx - ws, cy - hs, cx + ws, cy + hs), 1)

            tmp_bboxes = bbox_utils.clip_boxes(tmp_bboxes, dst_shape)
            s = (tmp_bboxes[:, 2] - tmp_bboxes[:, 0]) * (tmp_bboxes[:, 3] - tmp_bboxes[:, 1])
            tmp_bboxes = tmp_bboxes[s > 200]

            sampled_bboxes[n_sampled:n_sampled + len(tmp_bboxes)] = tmp_bboxes
            n_sampled += len(tmp_bboxes)

        n_sampling = len(sampled_bboxes) - n_sampled
        hs = dst_shape[0] / np.random.uniform(3, 6, n_sampling)
        ws = hs * np.random.uniform(0.2, 1, n_sampling)
        for i, ibbox in enumerate(range(n_sampled, len(sampled_bboxes))):
            w = ws[i]
            h = hs[i]
            x = np.random.uniform(1, dst_shape[1] - w - 1)
            y = np.random.uniform(1, dst_shape[0] - h - 1)
            box = np.array([x, y, x + w, y + h], dtype=np.float)
            sampled_bboxes[ibbox] = box

        # rois
        bbox_labels = np.zeros([self.max_bboxes, 1], dtype=np.float32)
        bbox_mask = np.zeros([self.max_bboxes, 1], dtype=np.float32)
        if len(gt_bboxes) > 0:
            ious = bbox_ious(
                np.ascontiguousarray(sampled_bboxes, dtype=np.float),
                np.ascontiguousarray(gt_bboxes, dtype=np.float),
            )
            max_ious = np.max(ious, 1)
        else:
            max_ious = np.zeros([self.max_bboxes])
        bbox_labels[max_ious > 0.55] = 1
        bbox_mask[(max_ious < 0.5) | (max_ious > 0.55)] = 1

        pos_inds = np.where(max_ious > 0.55)[0]
        neg_inds = np.where(max_ious < 0.5)[0]
        if len(pos_inds) > len(neg_inds) + 10:
            bbox_mask[np.random.choice(pos_inds, len(pos_inds) - len(neg_inds) - 10, replace=False)] = 0
        elif len(neg_inds) > len(pos_inds) + 10:
            bbox_mask[np.random.choice(neg_inds, len(neg_inds) - len(pos_inds) - 10, replace=False)] = 0

        rois = torch.from_numpy(sampled_bboxes.astype(np.float32))
        roi_labels = torch.from_numpy(bbox_labels)
        roi_mask = torch.from_numpy(bbox_mask)

        return im_data, rois, roi_labels, roi_mask
