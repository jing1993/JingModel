import imp
import os
import sys
import numpy as np
import torch
from torch.autograd import Variable

from datasets.depth_sn_seg.ade20k_seg import ADE20KSeg


from tnn.datasets.coco_v2 import get_data
from datasets.depth_sn_seg.gta_depth import GTADepth
from datasets.depth_sn_seg.nyu_depth_labeled_rlt import NYUDepthLabeledRLT
from datasets.depth_sn_seg.nyu_depth_rlt import NYUDepthRLT
from datasets.depth_sn_seg.voc_seg import VOCSeg
from datasets.depth_sn_seg.rhd_human import RHDHand

from datasets.joint_dataset import JointDataset
from tnn.datasets.dataloader import sDataLoader
from tnn.network.trainer import Trainer
from tnn.utils.path import mkdir
from tnn.network import net_utils

# load training model and params from arg

#exp_model = sys.argv[1]
exp_model = 'depth_small38.py'
if 'experiments' not in exp_model:
    exp_model = os.path.join('network', exp_model)
print(exp_model)

exp = imp.load_source('exp', exp_model)
rtn = exp.get_training_params()
params, model = rtn[0:2]
if len(rtn) >= 3:
    sbp = rtn[2]
else:
    sbp = None
# hyper-parameters
# ====================================

data_dir = '/data/coco_other/coco/images'
mask_dir = '/data/coco_other/coco/'
json_path = '/data/coco_other/coco/COCO.json'

train_h5f = 'train_data.h5'

max_pts = 12000

inp_size = 368
n_threads = 12
# weight_decay = 1e-4
weight_decay = 1e-5

params.save_dir = 'models/training/{}'.format(params.exp_name)

params.print_freq = 30
params.tensorboard_freq = 100
params.tensorboard_hostname = None

# vis
params.do_vis = True
params.vis_freq = 200
params.vis_params = {
    'server': 'http://45.76.67.249',
    # 'server': 'http://127.0.0.1',
    'port': 8097,
    'env': params.exp_name
}
params.do_dist = False
params.do_cum = False
params.do_mean_weights = True
params.do_net_graph = False
# ====================================

mkdir(params.save_dir)


thresh_gta = 0.006
"""
gta_data = GTADepth('/data/new_GTA/13', inp_size,
                    feat_stride=model.feat_stride, max_pts=max_pts, thresh1=thresh_gta, thresh2=thresh_gta, subset=None, training=True)

train_data = sDataLoader(JointDataset([gta_data], scales=[1]),
                         batch_size=params.batch_size, shuffle=True, num_workers=n_threads)
"""
train_dataset = []

for i in range(1, 16):
    gta_data_0 = GTADepth('/data/new_GTA/{}'.format(i), inp_size,
                    feat_stride=model.feat_stride, max_pts=max_pts, thresh1=thresh_gta, thresh2=thresh_gta, subset=None, training=True)
    train_dataset.append(gta_data_0)

coco_data = get_data(json_path, data_dir, mask_dir, inp_size, 8, 'inception',
                        shuffle=True, training=True)
train_dataset.append(coco_data)

train_data = sDataLoader(JointDataset(train_dataset, scales=np.full((16), 1)),
                         batch_size=params.batch_size, shuffle=True, num_workers=n_threads)

print('train dataset len: {}'.format(len(train_data.dataset)))



valid_data = None
if params.val_nbatch > 0:
    thresh_gta = 0.006
    
    valid_dataset = []
    for i in range(1, 16):
        gta_data_0 = GTADepth('/data/new_GTA/{}'.format(i), inp_size,
                    feat_stride=model.feat_stride, max_pts=max_pts, thresh1=thresh_gta, thresh2=thresh_gta, subset=None, training=False)
        valid_dataset.append(gta_data_0)
    
    coco_val = get_data(json_path, data_dir, mask_dir, inp_size, 8, 'inception',
                        shuffle=True, training=False)
    valid_dataset.append(coco_val)
    
    valid_data = sDataLoader(
        JointDataset(valid_dataset, scales=np.full((16), 1)),
        batch_size=params.batch_size, shuffle=True, num_workers=n_threads)

    print('val dataset len: {}'.format(len(valid_data.dataset)))

if params.optimizer is None:
    print('init optimizer')
    # trainable_vars = []
    # feat_vars = []
    # for name, var in model.named_parameters():
    #     if not var.requires_grad:
    #         continue
    #     if 'feature_extractor' in name:
    #         feat_vars.append(var)
    #     else:
    #         trainable_vars.append(var)
    #
    # params.optimizer = torch.optim.RMSprop([
    #     {'params': feat_vars, 'lr': params.init_lr},
    #     {'params': trainable_vars}
    # ], lr=params.init_lr, weight_decay=weight_decay)
    params.optimizer = torch.optim.RMSprop([var for var in model.parameters() if var.requires_grad], lr=params.init_lr,
                                           weight_decay=weight_decay)


def batch_processor(state, batch):
    gpus = state.params.gpus
    im_data, depth_data, sn_data, seg_data, dwt_label, seg_mask, sn_mask, dwt_mask, pts, num_pts = batch

    volatile = not state.model.training
    im_var = Variable(im_data, volatile=volatile).cuda(device_id=gpus[0])
    depth_var = Variable(depth_data).cuda(device_id=gpus[0])
    sn_var = Variable(sn_data).cuda(device_id=gpus[0])
    seg_var = Variable(seg_data).cuda(device_id=gpus[0])
    mask_var = Variable(seg_mask).cuda(device_id=gpus[0])
    # snmask_var = Variable(sn_mask).cuda(device_id=gpus[0])
    pts_var = Variable(pts).cuda(device_id=gpus[0])
    num_pts_var = Variable(num_pts).cuda(device_id=gpus[0])

    inputs = [im_var]
    gts = [depth_var, sn_var, seg_var, mask_var, pts_var, num_pts_var]
    saved_for_eval = None

    return inputs, gts, saved_for_eval


# params.max_epoch = 1
trainer = Trainer(model, params, batch_processor if sbp is None else sbp, train_data, valid_data, use_auto_lr=True)
trainer.train()

#
# net_utils.set_trainable(model, requires_grad=True)
# params.init_lr = 1e-4
# params.max_epoch = 140
# params.ckpt = None
# params.zero_epoch = False
# params.ignore_opt_state = True
# params.optimizer = torch.optim.RMSprop([var for var in model.parameters() if var.requires_grad], lr=params.init_lr, weight_decay=weight_decay)
# trainer = Trainer(model, params, batch_processor if sbp is None else sbp, train_data, valid_data, use_auto_lr=False)
# trainer.train()
