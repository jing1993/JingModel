import torchvision.models as models

import datasets.closed.coco2 as coco

model = models.resnet18(pretrained=True)

kp = coco.CocoKeypoints(root='/data/coco/train2014',
    annFile='/ssddata/coco/annotations/person_keypoints_train2014.json', num_examples=10)

print(len(kp))
img, anns = kp[0]
print(type(img))


hm = coco.keypoint_anns_to_heatmaps(anns, img.size, img.size, 5, 1)

print(hm.shape)