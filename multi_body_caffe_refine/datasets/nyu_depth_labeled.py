import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class NYUDepthLabeled(data.Dataset):
    def __init__(self, root, h5name, inp_size=256, feat_stride=None, out_size=None, training=True):
        self.root = root
        self.h5name = h5name
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training

        h5f = h5py.File(os.path.join(self.root, self.h5name), 'r')
        rgb_data = h5f['images']
        self.len = len(rgb_data)
        h5f.close()

    def __len__(self):
        return self.len

    def __getitem__(self, i):
        h5f = h5py.File(os.path.join(self.root, self.h5name), 'r')
        rgb_data = h5f['images']
        gts = h5f['depths']

        im = rgb_data[i]
        depth = gts[i]
        h5f.close()

        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        im = cv2.resize(im, (320, 240))
        depth = cv2.resize(depth, (320, 240))
        mask = cv2.resize(mask, (320, 240))

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=True)
            scale = trans_params[0]
            # im = im_transform.imcv2_recolor(im)
            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)
            depth = depth / scale
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        depth_data = depth.astype(np.float32)

        mask2 = np.zeros_like(depth_data, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)
        return im_data, depth_data, mask_data


def collate_fn(data):
    im_data, depth_data, mask_data = zip(*data)
    im_data = torch.stack(im_data, 0)
    depth_data = torch.stack(depth_data, 0)
    mask_data = torch.stack(mask_data, 0)

    return im_data, depth_data, mask_data


def get_loader(root, h5name, inp_size=256, feat_stride=None, out_size=None, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = NYUDepthLabeled(root, h5name, inp_size, feat_stride, out_size, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader
