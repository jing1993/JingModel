"""MSCOCO Dataloader
   Thanks to @tensorboy @shuangliu
"""

try:
    import ujson as json
except ImportError:
    import json
from tnn.datasets.dataloader import sDataLoader
from COCO_segmentation_pipeline import Cocosegmentations
from GTA_segmentation_pipeline import GTAsegmentations
from GTA_surfaceNorm_pipeline import GTAsurfaceNorms
from GTA_intrinsic_pipeline import GTAintrinsics
from ADE20K_segmentation_pipeline import ADE20Ksegmentations
from VOC2012_segmentation_pipeline import VOC2012segmentations
from torchvision.transforms import ToTensor
from torch.utils.data import ConcatDataset
import os

def combined_segmentation_loader(inp_size, feat_stride, datanames = ['gta_segmentation', 'gta_indoor', 'gta_outdoor', 'coco', 'ADE20K', 'VOC2012'], batch_size = 20, training = True, shuffle = True, num_workers = 16):
    ''' Combined dataloader for segmentation training.
    param inp_size: input size of your model. This will be the final size of images after ImageAugmentation.
    param feat_stride: scale factor between the input size and output size of your model. e.g. if input size is 368 and output size is 46, then feat_stride is 8. This is used to resize the ground truths
    param datanames: list of datasets you want to use. Default value indicates all the available dataloader. But make sure you have the coresponding dataset in your machine.
    param batch_size: batch size
    param training: True for training, False for validation
    param shuffle: set to True to have the data reshuffled at every epoch
    param num_workers: how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process
    '''
    datasets = []
    for dataname in datanames:
        if dataname == 'gta_segmentation':
            data = os.listdir('/data/GTA/segmentation/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAsegmentations(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/segmentation/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        elif dataname == 'gta_indoor':
            data = os.listdir('/data/GTA/indoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAsegmentations(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/indoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        elif dataname == 'gta_outdoor':
            data = os.listdir('/data/GTA/outdoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAsegmentations(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/outdoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        elif dataname == 'coco':
            seg_list = os.listdir('/data/coco/mask2014_semantic')
            data = [seg for seg in seg_list if 'train' in seg] if training else [seg for seg in seg_list if 'val' in seg]
            datasets.append(Cocosegmentations(inp_size = inp_size, feat_stride = feat_stride, root='/data/coco', index_list=range(len(data)),
                                          data=data, transform=ToTensor()))

        elif dataname == 'ADE20K':
            data_path = '/data/ADE20K/ADE20K_seg'
            root = os.path.join(data_path, 'training') if training else os.path.join(data_path, 'validation')
            data = os.listdir(root+'/images')
            num_samples = len(data)
            datasets.append(ADE20Ksegmentations(inp_size = inp_size, feat_stride = feat_stride, root=root, index_list=range(num_samples), data=data, transform=ToTensor()))

        elif dataname == 'VOC2012':
            data = os.listdir('/data/VOC2012/VOC2012_seg/images')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(VOC2012segmentations(inp_size = inp_size, feat_stride = feat_stride, root='/data/VOC2012/VOC2012_seg', index_list=train_indexes if training else val_indexes, data=data, transform=ToTensor()))

        else:
            assert False, 'cannot find dataset: {}'.format(dataname)

    combined_data = ConcatDataset(datasets)
    data_loader = sDataLoader(combined_data, batch_size = batch_size, shuffle = shuffle, num_workers = num_workers)
    return data_loader


def combined_surfaceNorm_loader(inp_size, feat_stride, datanames = ['gta_indoor', 'gta_outdoor'], batch_size = 20, training = True, shuffle = True, num_workers = 16):
    ''' Combined dataloader for surfaceNorm training.
    Please refer to combined_segmentation_loader for param settings
    '''
    datasets = []
    for dataname in datanames:
        if dataname == 'gta_indoor':
            data = os.listdir('/data/GTA/indoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAsurfaceNorms(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/indoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        elif dataname == 'gta_outdoor':
            data = os.listdir('/data/GTA/outdoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAsurfaceNorms(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/outdoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        else:
            assert False, 'cannot find dataset: {}'.format(dataname)

    combined_data = ConcatDataset(datasets)
    data_loader = sDataLoader(combined_data, batch_size = batch_size, shuffle = shuffle, num_workers = num_workers)
    return data_loader

def combined_intrinsic_loader(inp_size, feat_stride, datanames = ['gta_indoor', 'gta_outdoor'], batch_size = 20, training = True, shuffle = True, num_workers = 16):
    ''' Combined dataloader for intrinsic color training.
    Please refer to combined_segmentation_loader for param settings
    '''
    datasets = []
    for dataname in datanames:
        if dataname == 'gta_indoor':
            data = os.listdir('/data/GTA/indoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAintrinsics(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/indoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        elif dataname == 'gta_outdoor':
            data = os.listdir('/data/GTA/outdoor/origin')
            num_samples = len(data)
            train_indexes = range(0, 9*num_samples/10)
            val_indexes = range(9*num_samples/10, num_samples)
            datasets.append(GTAintrinsics(inp_size = inp_size, feat_stride = feat_stride, root='/data/GTA/outdoor/', index_list=train_indexes if training else val_indexes,
                                      data=data, transform=ToTensor()))
        else:
            assert False, 'cannot find dataset: {}'.format(dataname)

    combined_data = ConcatDataset(datasets)
    data_loader = sDataLoader(combined_data, batch_size = batch_size, shuffle = shuffle, num_workers = num_workers)
    return data_loader
                

