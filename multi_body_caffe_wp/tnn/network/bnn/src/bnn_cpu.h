void BinarySpatialConvolution_forward_cpu(
	THFloatTensor *input,
	THFloatTensor *output,
	THIntTensor *weight,
	THIntTensor *columns,
	THFloatTensor *alphas,
	THIntTensor *columns_binary,
    THFloatTensor *running_mean,
    THFloatTensor * gamma_sign,
	int kW, int kH,
	int dW, int dH,
	int padW, int padH );