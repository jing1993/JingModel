import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader
from tnn.utils.im_transform import crop_with_factor

from pycocotools.coco import COCO
from pycocotools import mask as cocomask


BBOX_THRESHOLD = 2500


class CocoSegCrop(data.Dataset):
    def __init__(self, root, name, date=2014, inp_size=256, feat_stride=4, save_and_factor=-1, training=True):

        assert name in ('train', 'test', 'val'), name
        assert os.path.isdir(root), root

        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.save_and_factor = save_and_factor

        dataType = '{}{}'.format(name, date)
        annFile = '%s/annotations/instances_%s.json' % (self.root, dataType)
        self.annFile = COCO(annFile)
        self.catIds = self.annFile.getCatIds('person')
        self.imageIds = self.annFile.getImgIds(catIds=self.catIds)

        self.annotations = []

        for ids in self.imageIds:
            annIds = self.annFile.getAnnIds(imgIds=ids, catIds=self.catIds)
            anns = self.annFile.loadAnns(annIds)

            for ann in anns:
                if ann['iscrowd'] == 0:
                    bbox = [int(i) for i in ann['bbox']]
                    if bbox[2] * bbox[3] >= BBOX_THRESHOLD:
                        single_ann = {}
                        single_ann['image_id'] = anns[0]['image_id']
                        single_ann['bbox'] = bbox
                        single_ann['segmentation'] = ann['segmentation']
                        self.annotations.append(single_ann)

        self.image_path_prefix = '%s/%s/COCO_%s_' % (self.root, dataType, dataType)

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, i):
        single_ann = self.annotations[i]

        img_path = self.image_path_prefix + '%012d.jpg' % single_ann['image_id']
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        (h, w, _) = img.shape

        single_mask = single_ann['segmentation']
        bbox = single_ann['bbox']
        bbox = [int(i) for i in bbox]

        canvas = np.zeros((h, w), dtype=np.uint8)
        rle = cocomask.frPyObjects(single_mask, h, w)
        mask = cocomask.decode(rle)
        if mask.ndim == 3:
            mask = mask.sum(axis=2)
            mask = (mask > 0).astype(np.uint8)
        canvas = np.maximum(canvas, mask)

        im = img[bbox[1]:bbox[1] + bbox[3], bbox[0]:bbox[0] + bbox[2], :]
        seg_mask = canvas[bbox[1]:bbox[1] + bbox[3], bbox[0]:bbox[0] + bbox[2]]
        mask = np.ones_like(seg_mask, dtype=np.float32)

        # fx = max(self.inp_size[0] / float(w), self.inp_size[1] / float(h)) + 0.03
        # im = cv2.resize(im, None, fx=fx, fy=fx)
        # seg_mask = cv2.resize(seg_mask, None, fx=fx, fy=fx)
        # mask = cv2.resize(mask, None, fx=fx, fy=fx)

        # data augmentation
        if self.save_and_factor < 0:
            if self.training:
                im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                                   rotate=20, max_scale=1.2)
                scale = trans_params[0]
                im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.018, blur_ksize=3, blur_sigma=3)
                mask = im_transform.apply_affine(mask, *trans_params)
                seg_mask = im_transform.apply_affine(seg_mask, *trans_params)
            else:
                im_shape = im.shape
                dh = im_shape[0] - self.inp_size[1]
                dw = im_shape[1] - self.inp_size[0]
                (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
                (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
                im = im[i0:i1, j0:j1].astype(np.float32)
                seg_mask = seg_mask[i0:i1, j0:j1]
                mask = mask[i0:i1, j0:j1]

            mask = cv2.resize(mask, tuple(self.out_size))
            seg_mask = cv2.resize(seg_mask, tuple(self.out_size))
        else:
            im, _, _ = crop_with_factor(im, self.inp_size[0], factor=self.save_and_factor, pad_val=0, basedon='h')
            seg_mask, _, _ = crop_with_factor(seg_mask, self.inp_size[0], factor=self.save_and_factor, basedon='h')
            mask, im_scale, real_shape = crop_with_factor(mask, self.inp_size[0], factor=self.save_and_factor, basedon='h')

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        mask2 = np.zeros_like(seg_mask, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        label2 = np.zeros_like(seg_mask, dtype=np.float32)
        label2[seg_mask > 0] = 1
        seg_mask = label2

        # mask = ins_map * mask
        im_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(mask)
        seg_data = torch.from_numpy(seg_mask)

        return im_data, seg_data, mask_data


def get_loader(root, name, date=2014, inp_size=256, feat_stride=4, save_and_factor=-1, training=True,
               batch_size=32, shuffle=True, num_workers=3):
    dataset = CocoSegCrop(root, name, date, inp_size, feat_stride, save_and_factor, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
