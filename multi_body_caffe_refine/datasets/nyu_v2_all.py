import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader, TimeSeqDataset

import utils.depth as depth_utils


class NYU_v2(TimeSeqDataset):
    def __init__(self, root, name, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, thresh=0.1, training=True, random_seed=None):
        self.root = root
        self.name = name
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training
        self.max_pts = max_pts
        self.thresh = thresh

        self.im_root = os.path.join(root, name, 'images')
        self.gt_root = os.path.join(root, name, 'gts')
        self.sn_root = os.path.join(root, 'normals_gt', 'normals')
        self.sn_mask_root = os.path.join(root, 'normals_gt', 'masks')
        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.jpg']

        self.random_seed = random_seed

    def __len__(self):
        return len(self.im_names)

    def seq_len(self, i, rnd=0):
        return 20

    def get_item(self, vid, fid, clear, rnd=0):
        item = self.__getitem__(vid, rnd)
        return list(item) + [clear]

    def __getitem__(self, i, rnd=None):
        im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, im_name)
        gt_file = os.path.join(self.gt_root, '{}.h5'.format(os.path.splitext(im_name)[0]))

        # image
        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        # depth
        h5f = h5py.File(gt_file, 'r')
        depth = np.asarray(h5f['depth'])
        label = np.asarray(h5f['label'])
        h5f.close()

        # sn
        sn_name = '{}.png'.format(os.path.splitext(im_name)[0])
        sn = cv2.imread(os.path.join(self.sn_root, sn_name))
        sn_mask = cv2.imread(os.path.join(self.sn_mask_root, sn_name), 0)

        # crop
        m = 10
        im = im[m:-m, m:-m]
        depth = depth[m:-m, m:-m]
        label = label[m:-m, m:-m]
        sn = sn[m:-m, m:-m]
        sn_mask = sn_mask[m:-m, m:-m]

        sn_mask = sn_mask.astype(np.float32)
        sn_mask[sn_mask > 0] = 1

        # depth mask
        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        # data augmentation
        if self.training:
            flip = rnd % 2 == 0 if rnd is not None else None
            im, trans_params = im_transform.imcv2_affine_trans(im, flip, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=15, min_scale=0.4, max_scale=1.1)
            im = im_transform.imcv2_recolor(im, noise_scale=0.018, blur_ksize=2, blur_sigma=2)

            trans_params[-1] = False
            label = im_transform.apply_affine(label, *trans_params, interpolation=cv2.INTER_NEAREST)
            sn = im_transform.apply_affine(sn, *trans_params)
            sn_mask = im_transform.apply_affine(sn_mask, *trans_params)
            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)

            scale = trans_params[0]
            depth = depth / scale
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        label = cv2.resize(label, tuple(self.out_size), interpolation=cv2.INTER_NEAREST)
        sn = cv2.resize(sn, tuple(self.out_size))
        sn_mask = cv2.resize(sn_mask, tuple(self.out_size))

        mask[mask > 0.9] = 1
        mask[mask <= 0.9] = 0

        sn_mask[sn_mask > 0.9] = 1
        sn_mask[mask <= 0.9] = 0
        sn_mask = sn_mask[np.newaxis, :, :]

        # normalize depth
        depth_t = depth[mask > 0]
        if len(depth_t) > 0:
            dmin, dmax = depth_t.min(), depth_t.max()
            depth = (depth - dmin) / (dmax - dmin + 1e-5)
        depth_data = depth.astype(np.float32)

        if self.random_seed is not None:
            np.random.seed(self.random_seed)
        pts = np.zeros([self.max_pts, 9], dtype=np.int64)
        sampled_pts = depth_utils.sample_pts(depth_data, mask, max_pts=self.max_pts, dis_scale=60, thresh=self.thresh)
        if sampled_pts is None:
            print('[W]: {} sampled_pts is None'.format(i))
            # return self.__getitem__(i)
            oh, ow = depth_data.shape[0:2]
            sampled_pts = np.asarray((ow//2, oh//2, ow//2, oh//2, 0, ow//2, oh//2, 0, 0), dtype=np.int64)

        num_pts = len(sampled_pts)
        pts[:num_pts, :] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)

        # get surface normal from depth
        # sn = depth_utils.depth_to_normal(depth).astype(np.float32).transpose([2, 0, 1])
        sn = sn.astype(np.float32).transpose([2, 0, 1]) / 127.5 - 1
        sn_data = torch.from_numpy(sn)
        sn_mask_data = torch.from_numpy(sn_mask)
        label_data = torch.from_numpy(label.astype(np.int64))

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)

        return im_data, depth_data, sn_data, label_data, mask_data, sn_mask, pts, num_pts


# def collate_fn(data):
#     im_data, depth_data, mask_data = zip(*data)
#     im_data = torch.stack(im_data, 0)
#     depth_data = torch.stack(depth_data, 0)
#     mask_data = torch.stack(mask_data, 0)
#
#     return im_data, depth_data, mask_data


def get_loader(root, name, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, thresh=0.1, training=True,
               batch_size=128, shuffle=True, num_workers=3, random_seed=None):
    dataset = NYU_v2(root, name, inp_size, feat_stride, out_size, max_pts, thresh, training, random_seed)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/media/longc/LData/data/nyu_depth_v2',
                                'train_data.h5', inp_size=(320, 240), feat_stride=1,
                                training=True, batch_size=2)
        for i, batch in enumerate(train_data):
            im_data, depth_data, mask_data = batch
            print im_data.size()
            print depth_data.size()
            print mask_data.size()

            cv2.imshow('image', np.transpose(im_data[0].numpy(), [1, 2, 0]))
            cv2.imshow('depth', depth_data[0].numpy() / torch.max(depth_data[0]))
            cv2.imshow('mask', mask_data[0].numpy())
            cv2.waitKey(0)

            # assert False
    main()
