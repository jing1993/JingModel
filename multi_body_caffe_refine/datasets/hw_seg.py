import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class HWSeg(data.Dataset):
    def __init__(self, root, inp_size=256, feat_stride=4, subset=None, training=True):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training

        self.im_root = os.path.join(self.root, 'images')
        self.seg_root = os.path.join(self.root, 'seg')
        self.mask_root = os.path.join(self.root, 'mask')

        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.jpg']
        if subset is not None:
            self.im_names = self.im_names[subset[0]:subset[1]]

    def __len__(self):
        return len(self.im_names)

    def __getitem__(self, i):
        im_name = self.im_names[i]  # 20170901_2248.jpg
        im_file = os.path.join(self.im_root, im_name)
        name = os.path.splitext(im_name)[0]

        seg_file = os.path.join(self.seg_root, '{}_seg.jpg'.format(name))
        mask_file = os.path.join(self.mask_root, '{}_mask.jpg'.format(name))
        # 20170901_seg_2248.jpg

        im = cv2.imread(im_file)
        if im.shape[0] < 590 or im.shape[1] < 590:
            print('W0: {}, imshape: {}'.format(im_file, im.shape))
            next_i = (i+1) % self.__len__()
            return self.__getitem__(next_i)

        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        # d5 = h5py.File(depth_file, 'r')
        # depth = np.asarray(d5['depth'])

        seg_mask = cv2.imread(seg_file)[:, :, 0]
        # print seg_mask.shape, seg_mask.max()
        mask = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        mask[mask > 0] = 1
        mask = mask.astype(np.float32)
        # print mask.min(), mask.max()

        # fx = 0.5
        # im = cv2.resize(im, None, fx=fx, fy=fx)
        # seg_mask = cv2.resize(seg_mask, None, fx=fx, fy=fx)
        # mask = cv2.resize(mask, None, fx=fx, fy=fx)

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=20, min_scale=0.4, max_scale=1.2)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.018, blur_ksize=3, blur_sigma=3)
            mask = im_transform.apply_affine(mask, *trans_params)
            seg_mask = im_transform.apply_affine(seg_mask, *trans_params)
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            seg_mask = seg_mask[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        seg_mask = cv2.resize(seg_mask, tuple(self.out_size))

        mask2 = np.zeros_like(seg_mask, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        label2 = np.zeros_like(seg_mask, dtype=np.float32)
        label2[seg_mask > 0] = 1
        seg_mask = label2

        # mask = ins_map * mask
        im_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(mask)
        seg_data = torch.from_numpy(seg_mask)

        return im_data, seg_data, mask_data


def get_loader(root, inp_size=256, feat_stride=4, subset=None, training=True,
               batch_size=32, shuffle=True, num_workers=3):
    dataset = HWSeg(root, inp_size, feat_stride, subset, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader


if __name__ == '__main__':

    import xmljson
    import xml.etree.ElementTree as ET

    def read_annotation(xml_file, im_shape):
        tree = ET.parse(os.path.join(xml_file))
        root = tree.getroot()
        labels = xmljson.parker.data(root)

        n_person = sum((1 for k in labels.keys() if 'PersonRealBox' in k))
        h, w = im_shape[:2]
        mask = np.zeros((h, w), dtype=np.float)

        def decode_pts(content):
            # x, y
            pts = np.asarray([int(x) for x in content.split()], dtype=np.int).reshape(-1, 2)
            return pts

        parts = ['Head', 'Hairs', 'Glasses', 'Hats', 'Jacket', 'Coat', 'RightArm', 'LeftArm', 'Trousers', 'ShortSkirt',
                 'LeftLeg', 'RigthLeg', 'Child']

        real_bboxes = []
        bboxes = []
        for i in range(n_person):
            idx = chr(ord('a') + i % 26) * (i // 26 + 1)
            for part in parts:
                pts = decode_pts(labels.get(idx+part, ''))
                xs, ys = pts[:, 0], pts[:, 1]
                mask[ys, xs] = 1
            bbox = tuple(int(x) for x in labels.get(idx+'PersonRealBox', '').split())
            bbox2 = tuple(int(x) for x in labels.get(idx+'ALLColorBox', '').split())

            real_bboxes.append(bbox)
            bboxes.append(bbox2)

        return mask, real_bboxes, bboxes

    def main():
        root = '/data/huawei/seg_data/origin/2017-6.16/00025_1'
        for filename in os.listdir(root):
            name, ext = os.path.splitext(filename)
            if not ext == '.jpg':
                continue

            im_file = os.path.join(root, filename)
            im = cv2.imread(im_file)

            im_shape = im.shape
            h, w = im_shape[:2]
            seg = np.zeros((h, w), dtype=np.float)

            xmls = ['_20', '_30', '_50']
            for xml in xmls:
                xml_file = os.path.join(root, '{}{}.xml'.format(name, xml))
                if not os.path.isfile(xml_file):
                    continue
                _seg, real_bboxes, bboxes = read_annotation(xml_file, im_shape)
                seg += _seg

                for i, real_bbox in enumerate(real_bboxes):
                    # if len(real_bbox) < 4:
                    #     continue
                    assert len(real_bbox) == 4
                    # cv2.rectangle(im, real_bbox[0:2], real_bbox[2:4], (255, 0, 0), thickness=2)
                    # print i, len(real_bboxes), real_bbox
                    bbox = bboxes[i]
                    if len(bbox) >= 4:
                        cv2.rectangle(im, bbox[0:2], bbox[2:4], (0, 0, 255), thickness=2)

            cv2.imshow('test', im)
            cv2.imshow('seg', seg)
            cv2.waitKey(0)

    main()
