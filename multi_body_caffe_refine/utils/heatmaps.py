from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy.stats as ss
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion


def detect_peaks(heatmap, thr=0.1):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """
    image = heatmap.copy()
    image[image < thr] = 0

    # define an 8-connected neighborhood
    neighborhood = generate_binary_structure(2, 2)

    # apply the local maximum filter; all pixel of maximal value
    # in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood) == image
    # local_max is a mask that contains the peaks we are
    # looking for, but also the background.
    # In order to isolate the peaks we must remove the background from the mask.

    # we create the mask of the background
    background = (image == 0)

    # a little technicality: we must erode the background in order to
    # successfully subtract it form local_max, otherwise a line will
    # appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    # we obtain the final mask, containing only peaks,
    # by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks


def make_2d_gaussian_kernel(size=10, sigma=2):
    """Returns a 2D gaussian kernel as a numpy array. The center is normalized to 1.
        size: size of the kernel
        sigma: variance in std dev
    """
    interval = (2 * sigma + 1.) / (size)
    x = np.linspace(-sigma - interval / 2., sigma + interval / 2., size + 1)
    kern1d = np.diff(ss.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    # Normalize so that the peak is 1
    kernel = kernel_raw / kernel_raw.max()
    return kernel


def make_2d_gaussian_kernel2(width, height, sigma=2):
    """Returns a 2D gaussian kernel as a numpy array. The center is normalized to 1.
        size: size of the kernel
        sigma: variance in std dev
    """
    interval = (2 * sigma + 1.) / (width)
    x = np.linspace(-sigma - interval / 2., sigma + interval / 2., width + 1)
    kern1d_x = np.diff(ss.norm.cdf(x))

    interval = (2 * sigma + 1.) / (height)
    y = np.linspace(-sigma - interval / 2., sigma + interval / 2., height + 1)
    kern1d_y = np.diff(ss.norm.cdf(y))

    kernel_raw = np.sqrt(np.outer(kern1d_y, kern1d_x))
    # Normalize so that the peak is 1
    kernel = kernel_raw / kernel_raw.max()
    return kernel


def make_gaussian_kern(size=7, sigma=1):
    x = size // 2
    y = x
    p, q = np.arange(size), np.arange(size)
    gx = np.exp(-(p - x) ** 2.0 / (2 * sigma ** 2))
    gy = np.exp(-(q - y) ** 2.0 / (2 * sigma ** 2))
    g = np.outer(gx, gy)
    g /= np.max(g)
    return g


def make_bbox_heatmap(bbox, dst_size, kernel_sigma=3):
    bbox = np.asarray(bbox, dtype=np.int)
    kw = bbox[2] - bbox[0] + 1
    kh = bbox[3] - bbox[1] + 1
    cx = int(round((bbox[0] + bbox[2]) / 2.))
    cy = int(round((bbox[1] + bbox[3]) / 2.))
    kern = make_2d_gaussian_kernel2(kw, kh, kernel_sigma)

    width, height = dst_size
    full = np.pad(kern, ((height, height), (width, width)), mode='constant', constant_values=0)
    heatmap = full[height + kh // 2 - cy:2 * height + kh // 2 - cy, width + kw // 2 - cx:2 * width + kw // 2 - cx]

    # if heatmap.shape[0] != 64 or heatmap.shape[1] != 64:
    #     print(kern.shape, kw, kh, cx, cy)
    #     print(full.shape, height + kh // 2 - cy, 2 * height + kh // 2 - cy, width + kw // 2 - cx, 2 * width + kw // 2 - cx)
    #     print(heatmap.shape)
    return heatmap


def make_joint_heatmap(width, height, x, y, kernel_size, kernel_sigma=3):
    """Returns a numpy 2D array (height, weight) and a gaussian kernel centered at (y, x) with given kernel size.
    """
    # print('make hm: ', width, height, x, y, kernel_size, kernel_sigma)
    kern = make_gaussian_kern(kernel_size, kernel_sigma)
    full = np.pad(kern, ((height, height), (width, width)), mode='constant', constant_values=0)
    s = kernel_size // 2
    # print([height+s-y, 2*height+s-y, width+s-x, 2*width+s-x])
    return full[height + s - y:2 * height + s - y, width + s - x:2 * width + s - x]


def make_joints_heatmap(width, height, kernel_size, joints):
    """Returns a numpy 2D array with each joint as a gaussian map.

    Args:
        width/height: returned image dimension
        joints: list of (x, y, sigma), where x/y is the center point, kernal sigma is optional with default value 2.
    Returns:
        Numpy array in (height, weight) with each joint as a gaussian centered at given x/y with center point normalized to 1.
    """
    all = []
    for j in joints:
        x, y = j[0], j[1]
        sigma = j[2] if len(j) > 2 else 2
        # size = 1 + 3 * sigma # kernel size - 3-sigma will be close to 0.
        # TODO(ming): why this happen?
        if x > width or y > height or x < 0 or y < 0:
            continue
        all.append(make_joint_heatmap(width, height, x, y, kernel_size, sigma))
    if len(all) == 0:
        return np.zeros((height, width))
    # print([ v.shape for v in all])
    mat = np.stack(all, axis=2)
    return np.max(mat, axis=2)


def non_maximal_suppression(frame, edge_size):
    dx, dy = edge_size
    M, N = frame.shape
    res = np.zeros(frame.shape)
    for (x, y), value in np.ndenumerate(frame):
        lowx, lowy = max(0, x - dx), max(0, y - dy)
        highx, highy = min(M, x + dx + 1), min(N, y + dy + 1)
        window = frame[lowx:highx, lowy:highy]
        localMax = np.max(window)
        if value == localMax:
            res[x, y] = value
    return res


def nms(frame, edge_size):
    dx, dy = edge_size
    M, N = frame.shape
    res = np.zeros(frame.shape)

    for (x, y), value in np.ndenumerate(frame):
        if x - 1 >= 0 and frame[x - 1, y] > value:
            continue
        if x + 1 < M and frame[x + 1, y] > value:
            continue
        if y - 1 >= 0 and frame[x, y - 1] > value:
            continue
        if y + 1 < N and frame[x, y + 1] > value:
            continue
        res[x, y] = value
    return res
