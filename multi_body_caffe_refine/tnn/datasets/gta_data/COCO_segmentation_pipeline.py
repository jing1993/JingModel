# coding=utf-8
import json
import os
import os.path
import random
import struct
import sys
from math import sqrt

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import torchvision.datasets as dset
import torchvision.transforms as transforms
from numpy import ma
from PIL import Image
from pycocotools.coco import COCO
from torchvision.transforms import CenterCrop, Compose, Scale, ToTensor

import torch
from ImageAugmentation_segmentation import ImageAugmentation
from torch.utils.data import DataLoader, Dataset
from preprocessing import vgg_preprocess


params_transform = dict()

# === aug_scale ===
params_transform['scale_min'] = 0.5
params_transform['scale_max'] = 1.1
params_transform['target_dist'] = 0.6
# === aug_rotate ===
params_transform['max_rotate_degree'] = 40

# === aug_croppad +++
params_transform['center_perterb_max'] = 40

# === aug_flip ===
params_transform['flip_prob'] = 0.5

class Cocosegmentations(Dataset):
    def __init__(self, inp_size, feat_stride, root, index_list, data, transform=None,
                 target_transform=None):

        
        print 'Initializing COCO data...'
        self.data = data
        self.numSample = len(index_list)
        self.index_list = index_list
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.inp_size = inp_size
        self.feat_stride = feat_stride
        params_transform['crop_size_x'] = inp_size
        params_transform['crop_size_y'] = inp_size
        print 'len: {}'.format(self.numSample)

    def __getitem__(self, index):
        idx = self.index_list[index]
        img_idx = self.data[idx][-16:-4]
        img_type = 'train2014' if 'train' in self.data[idx] else 'val2014'
        img = cv2.imread(os.path.join(self.root, 'images', img_type, 'COCO_'+img_type+'_'+img_idx+'.jpg'))
        assert img is not None, 'no image found in: {}'.format(os.path.join(self.root, 'images', img_type, 'COCO_'+img_type+'_'+img_idx+'.jpg'))
        
        mask = cv2.imread('/data/coco/mask2014/'+img_type+'_mask_miss_' + img_idx + '.png', 0)
        seg = cv2.imread('/data/coco/mask2014_semantic/'+img_type+'_mask_semantic_' + img_idx + '.png', 0)

        img, seg, mask = ImageAugmentation(img, seg, mask, params_transform)
        seg = cv2.resize(seg, (self.inp_size/self.feat_stride, self.inp_size/self.feat_stride))
        mask = cv2.resize(mask, (self.inp_size/self.feat_stride, self.inp_size/self.feat_stride))

        img = img.astype(np.float32)/255.
        img = torch.from_numpy(vgg_preprocess(img))

        seg = seg.astype(np.float32)/255.
        seg = np.expand_dims(seg, 0)

        mask = mask.astype(np.float32)/255.
        mask = np.expand_dims(mask, 0)

        return img, seg, mask

    def __len__(self):
        return self.numSample
