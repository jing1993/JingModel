import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
import tnn.utils.im_transform as im_transform

import utils.depth as depth_utils
from datasets import data_utils


class NYUDepthLabeledRLT(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=None, out_size=None, max_pts=6000,
                 thresh1=0.1, thresh2=0.01, training=True, random_seed=None):
        self.root = root
        self.name = name
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training
        self.max_pts = max_pts
        self.thresh1 = thresh1
        self.thresh2 = thresh2
        self.feat_stride = feat_stride

        self.im_root = os.path.join(root, name, 'images')
        self.gt_root = os.path.join(root, name, 'gts')
        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.jpg']

        self.random_seed = random_seed

    def __len__(self):
        return len(self.im_names)

    def read_images(self, i):
        im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, im_name)
        gt_file = os.path.join(self.gt_root, '{}.h5'.format(os.path.splitext(im_name)[0]))

        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        h5f = h5py.File(gt_file, 'r')
        depth = np.asarray(h5f['depth'])
        label = np.asarray(h5f['label'])
        h5f.close()

        # crop
        m = 10
        im = im[m:-m, m:-m]
        depth = depth[m:-m, m:-m]

        # seg
        seg = (label == 331).astype(np.float32)   # person
        seg_mask = np.ones_like(seg, dtype=np.float32)
        depth_mask = (depth > 0).astype(np.float32)

        return im, depth, seg, seg_mask, depth_mask

    def __getitem__(self, i):
        im, depth, seg, seg_mask, depth_mask = self.read_images(i)
        additions = (depth, seg, seg_mask, depth_mask)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 1.0
        max_scale = target_scale * 1.3
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 35,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': False
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        depth, seg, seg_mask, depth_mask = additions
        # image recolor
        im, seg = im_transform.imcv2_recolor(im, a=0.05, noise_scale=0.01, blur_ksize=1, blur_sigma=1,
                                                   mask=seg)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # scale depth
        scale = trans_params[0]
        depth = depth / scale
        # =========== end data augmentation ==============

        # resize to output
        additions = (depth, seg, seg_mask, depth_mask)
        additions = [cv2.resize(v, tuple(self.out_size)) for v in additions]
        depth, seg, seg_mask, depth_mask = additions

        seg_mask[seg_mask > 0.9] = 1
        seg_mask[seg_mask <= 0.9] = 0
        depth_mask[depth_mask > 0.9] = 1
        depth_mask[depth_mask <= 0.1] = 0

        # normalize depth
        depth_t = depth[depth_mask > 0]
        if len(depth_t) > 0:
            dmin, dmax = depth_t.min(), depth_t.max()
            depth = (depth - dmin) / (dmax - dmin + 1e-5)
        depth_n = depth.astype(np.float32)

        # get surface normal from depth
        sn = depth_utils.depth_to_normal(depth_n, im_order=False).astype(np.float32)
        # sn_mask = np.zeros_like(depth_mask)
        sn_mask = depth_mask.copy()
        sn_mask = sn_mask[np.newaxis, :, :]

        # to tensor
        im_data = torch.from_numpy(im_data)
        depth_data = torch.from_numpy(depth_n)
        seg_data = torch.from_numpy(seg)
        seg_mask = torch.from_numpy(seg_mask)
        sn_data = torch.from_numpy(sn)
        sn_mask = torch.from_numpy(sn_mask)

        # fake dwt data
        dwt_label = np.zeros_like(seg, dtype=np.int64)
        dwt_mask = np.zeros_like(seg, dtype=np.float32)
        dwt_label = torch.from_numpy(dwt_label)
        dwt_mask = torch.from_numpy(dwt_mask)

        # =============== sample points =============
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        sampled_pts = depth_utils.sample_pts(depth_n, depth_mask, max_pts=self.max_pts, dis_scale=60./self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts = len(sampled_pts)
        pts[:num_pts, :] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)

        return im_data, depth_data, sn_data, seg_data, dwt_label, seg_mask, sn_mask, dwt_mask, pts, num_pts
        #
        # gts = torch.stack((depth_data, seg_data, seg_mask), 0) # 3xHxW
        # sn_gts = torch.cat((sn_data, sn_mask), 0)   # 4xHxW
        # gts = torch.cat((gts, sn_gts), 0) # 7xHxW
        # return im_data, gts, pts, num_pts