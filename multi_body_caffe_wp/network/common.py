import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models


def binary_cross_entropy_with_logits(input, target, weight=None):
    r"""Function that measures Binary Cross Entropy between target and output
    logits:

    See :class:`~torch.nn.BCEWithLogitsLoss` for details.

    Args:
        input: Variable of arbitrary shape
        target: Variable of the same shape as input
        weight (Variable, optional): a manual rescaling weight
                if provided it's repeated to match input tensor shape
        size_average (bool, optional): By default, the losses are averaged
                over observations for each minibatch. However, if the field
                sizeAverage is set to False, the losses are instead summed
                for each minibatch.
    """
    if not target.is_same_size(input):
        raise ValueError("Target size ({}) must be the same as input size ({})".format(target.size(), input.size()))

    max_val = (-input).clamp(min=0)
    loss = input - input * target + max_val + ((-max_val).exp() + (-input - max_val).exp()).log()

    if weight is not None:
        loss = loss * weight

    return loss


class DilationLayer(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, padding='same_padding', dilation=1, bn=False):
        super(DilationLayer, self).__init__()
        if padding == 'same_padding':
            padding = int((kernel_size - 1) / 2 * dilation)
        self.Dconv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                               padding=padding, dilation=dilation)
        self.Drelu = nn.ReLU(inplace=True)
        self.Dbn = nn.BatchNorm2d(out_channels) if bn else None

    def forward(self, x):
        x = self.Dconv(x)
        if self.Dbn is not None:
            x = self.Dbn(x)
        x = self.Drelu(x)
        return x


class StageBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(StageBlock, self).__init__()
        self.Dconv_1 = DilationLayer(in_channels, out_channels=64)
        self.Dconv_2 = DilationLayer(in_channels=64, out_channels=64)
        self.Dconv_3 = DilationLayer(in_channels=64, out_channels=64, dilation=2)
        self.Dconv_4 = DilationLayer(in_channels=64, out_channels=32, dilation=4)
        self.Dconv_5 = DilationLayer(in_channels=32, out_channels=32, dilation=8)
        self.Mconv_6 = nn.Conv2d(in_channels=256, out_channels=128, kernel_size=1, padding=0)
        self.Mrelu_6 = nn.ReLU(inplace=True)
        self.Mconv_7 = nn.Conv2d(in_channels=128, out_channels=out_channels, kernel_size=1, padding=0)

    def forward(self, x):
        x_1 = self.Dconv_1(x)
        x_2 = self.Dconv_2(x_1)
        x_3 = self.Dconv_3(x_2)
        x_4 = self.Dconv_4(x_3)
        x_5 = self.Dconv_5(x_4)
        x_cat = torch.cat([x_1, x_2, x_3, x_4, x_5], 1)
        x_out = self.Mconv_6(x_cat)
        x_out = self.Mrelu_6(x_out)
        x_pred = self.Mconv_7(x_out)
        return x_pred


class Fire(nn.Module):

    def __init__(self, inplanes, squeeze_planes,
                 expand1x1_planes, expand3x3_planes):
        super(Fire, self).__init__()
        self.inplanes = inplanes
        self.squeeze = nn.Conv2d(inplanes, squeeze_planes, kernel_size=1)
        self.squeeze_activation = nn.ReLU(inplace=True)
        self.expand1x1 = nn.Conv2d(squeeze_planes, expand1x1_planes,
                                   kernel_size=1)
        self.expand1x1_activation = nn.ReLU(inplace=True)
        self.expand3x3 = nn.Conv2d(squeeze_planes, expand3x3_planes,
                                   kernel_size=3, padding=1)
        self.expand3x3_activation = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.squeeze_activation(self.squeeze(x))
        return torch.cat([
            self.expand1x1_activation(self.expand1x1(x)),
            self.expand3x3_activation(self.expand3x3(x))
        ], 1)


class NormalConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1):
        super(NormalConv2d, self).__init__()

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride, padding=padding * dilation,
                              bias=False, dilation=dilation)
        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = F.relu(x, inplace=True)

        return x


class SeparableConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, dilation=1):
        super(SeparableConv2d, self).__init__()

        self.conv1 = nn.Conv2d(in_channels, in_channels, kernel_size, 1, padding * dilation, groups=in_channels,
                               bias=False, dilation=dilation)
        self.conv2 = nn.Conv2d(in_channels, out_channels, 1, stride, 0, bias=False)
        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.bn(x)
        x = F.relu(x, inplace=True)

        return x


class ResidualBlock(nn.Module):  # create residual block


    # Residual Block:
    #        -------------> (conv0 if dimension not consistent) ---
    # input --|                                                    |
    #        -----> conv1 -----------------------                 +----> output
    #                |                         |                  |
    #                -----> conv2 -------------C-------------------
    #                         |                |
    #                         -----> conv3 -----
    #

    def __init__(self, conv, in_channels, out_channels, kernel_size, stride=1, same_padding=True):
        super(ResidualBlock, self).__init__()  # super(Child, self).__init__() means to call a bound __init__
        # from the parent class that follows Child in the instance's method resolution order.

        padding = int((kernel_size - 1) / 2) if same_padding else 0  # keep input & output of conv_block with same size

        self.conv1 = NormalConv2d(in_channels, 80, kernel_size, stride, padding, dilation=1)
        self.conv2 = NormalConv2d(80, 40, kernel_size, 1, padding, dilation=2)
        self.conv3 = NormalConv2d(40, 20, kernel_size, 1, padding, dilation=4)
        self.conv4 = NormalConv2d(20, 20, kernel_size, 1, padding, dilation=8)
        self.conv_all = SeparableConv2d(160, out_channels, 3, 1, 1)

    def forward(self, x):
        outputs = []
        outputs.append(self.conv1(x))
        outputs.append(self.conv2(outputs[-1]))
        outputs.append(self.conv3(outputs[-1]))
        outputs.append(self.conv4(outputs[-1]))
        output = torch.cat(outputs, 1)
        output = self.conv_all(output)
        return output


class FeatExtractorSqueezeNet(nn.Module):
    n_feats = [64, 128, 256]

    def __init__(self, pretrained=True):

        super(FeatExtractorSqueezeNet, self).__init__()
        print("loading layers from squeezenet1_1...")
        sq = models.squeezenet1_1(pretrained=pretrained)
        self.conv = sq.features[0]
        self.relu = sq.features[1]
        self.maxpool2d_1 = sq.features[2]
        self.fire_1 = sq.features[3]
        self.fire_2 = sq.features[4]
        self.maxpool2d_2 = sq.features[5]
        self.fire_3 = sq.features[6]
        # embed()
        self.conv.padding = (1, 1)

    def forward(self, x):
        features_2 = self.conv(x)  # features_2.size() = (batch_size, 64, 184, 184)
        x = self.relu(features_2)
        x = self.maxpool2d_1(x)
        x = self.fire_1(x)
        features_4 = self.fire_2(x)  # features_4.size() = (batch_size, 128, 92, 92)
        x = self.maxpool2d_2(features_4)
        x = self.fire_3(x)  # x.size() = (batch_size, 256, 46, 46)
        return features_2, features_4, x


class FeatExtractorInception(nn.Module):

    n_feats = [64, 192, 288]

    def __init__(self, pretrained=True, transform_input=True):
        super(FeatExtractorInception, self).__init__()

        self.transform_input = transform_input

        print("loading layers from inception_v3...")
        v3 = models.inception_v3(pretrained=pretrained)

        self.Conv2d_1a_3x3 = v3.Conv2d_1a_3x3
        self.Conv2d_2a_3x3 = v3.Conv2d_2a_3x3
        self.Conv2d_2b_3x3 = v3.Conv2d_2b_3x3
        self.Conv2d_3b_1x1 = v3.Conv2d_3b_1x1
        self.Conv2d_4a_3x3 = v3.Conv2d_4a_3x3
        self.Mixed_5b = v3.Mixed_5b
        self.Mixed_5c = v3.Mixed_5c
        # add padding in original conv layers
        self.Conv2d_1a_3x3.conv.padding = (1, 1)
        self.Conv2d_2a_3x3.conv.padding = (1, 1)
        self.Conv2d_2b_3x3.conv.padding = (1, 1)
        self.Conv2d_4a_3x3.conv.padding = (1, 1)

    def forward(self, x):
        if self.transform_input:
            x = x.clone()
            x[:, 0, :, :] = x[:, 0, :, :] * (0.229 / 0.5) + (0.485 - 0.5) / 0.5
            x[:, 1, :, :] = x[:, 1, :, :] * (0.224 / 0.5) + (0.456 - 0.5) / 0.5
            x[:, 2, :, :] = x[:, 2, :, :] * (0.225 / 0.5) + (0.406 - 0.5) / 0.5
        x = self.Conv2d_1a_3x3(x)
        x = self.Conv2d_2a_3x3(x)
        x1 = self.Conv2d_2b_3x3(x)

        x = F.max_pool2d(x1, kernel_size=3, stride=2, ceil_mode=True)
        x = self.Conv2d_3b_1x1(x)
        x2 = self.Conv2d_4a_3x3(x)

        x = F.max_pool2d(x2, kernel_size=3, stride=2, ceil_mode=True)
        x = self.Mixed_5b(x)  # 46 x 46 x 256
        x3 = self.Mixed_5c(x)

        # 46 x 46 x 288
        return x1, x2, x3


class FeatExtractorInceptionx16(nn.Module):

    n_feats = [64, 192, 288, 768]

    def __init__(self, pretrained=True, transform_input=True):
        super(FeatExtractorInceptionx16, self).__init__()

        self.transform_input = transform_input

        print("loading layers from inception_v3...")
        v3 = models.inception_v3(pretrained=pretrained)

        self.Conv2d_1a_3x3 = v3.Conv2d_1a_3x3
        self.Conv2d_2a_3x3 = v3.Conv2d_2a_3x3
        self.Conv2d_2b_3x3 = v3.Conv2d_2b_3x3

        self.Conv2d_3b_1x1 = v3.Conv2d_3b_1x1
        self.Conv2d_4a_3x3 = v3.Conv2d_4a_3x3

        self.Mixed_5b = v3.Mixed_5b
        self.Mixed_5c = v3.Mixed_5c

        self.Mixed_5d = v3.Mixed_5d
        self.Mixed_6a = v3.Mixed_6a
        self.Mixed_6b = v3.Mixed_6b

        # add padding in original conv layers
        self.Conv2d_1a_3x3.conv.padding = (1, 1)
        self.Conv2d_2a_3x3.conv.padding = (1, 1)
        self.Conv2d_2b_3x3.conv.padding = (1, 1)
        self.Conv2d_4a_3x3.conv.padding = (1, 1)

        self.Mixed_6a.branch3x3.conv.padding = (1, 1)
        self.Mixed_6a.branch3x3.conv.padding = (1, 1)
        self.Mixed_6a.branch3x3.conv.padding = (1, 1)
        self.Mixed_6a.branch3x3dbl_3.conv.padding = (1, 1)

    @staticmethod
    def inception_b_forward(model, x):
        branch3x3 = model.branch3x3(x)

        branch3x3dbl = model.branch3x3dbl_1(x)
        branch3x3dbl = model.branch3x3dbl_2(branch3x3dbl)
        branch3x3dbl = model.branch3x3dbl_3(branch3x3dbl)

        branch_pool = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)

        outputs = [branch3x3, branch3x3dbl, branch_pool]
        return torch.cat(outputs, 1)

    def forward(self, x):
        if self.transform_input:
            x = x.clone()
            x[:, 0, :, :] = x[:, 0, :, :] * (0.229 / 0.5) + (0.485 - 0.5) / 0.5
            x[:, 1, :, :] = x[:, 1, :, :] * (0.224 / 0.5) + (0.456 - 0.5) / 0.5
            x[:, 2, :, :] = x[:, 2, :, :] * (0.225 / 0.5) + (0.406 - 0.5) / 0.5
        x = self.Conv2d_1a_3x3(x)
        x = self.Conv2d_2a_3x3(x)
        x1 = self.Conv2d_2b_3x3(x)

        x = F.max_pool2d(x1, kernel_size=3, stride=2, ceil_mode=True)
        x = self.Conv2d_3b_1x1(x)
        x2 = self.Conv2d_4a_3x3(x)

        x = F.max_pool2d(x2, kernel_size=3, stride=2, ceil_mode=True)
        x = self.Mixed_5b(x)  # 46 x 46 x 256
        x3 = self.Mixed_5c(x)   # 46 x 46 x 288

        x = self.Mixed_5d(x3)
        x = self.inception_b_forward(self.Mixed_6a, x)
        x4 = self.Mixed_6b(x)

        return x1, x2, x3, x4


class FeatExtractorResNetFull(nn.Module):
    n_feats = [64, 256, 512, 1024]

    def __init__(self, pretrained=True, resnet='resnet50'):

        super(FeatExtractorResNetFull, self).__init__()

        print("loading layers from {}...".format(resnet))
        if resnet == 'resnet50':
            model = models.resnet50(pretrained=pretrained)
        elif resnet == 'resnet101':
            model = models.resnet101(pretrained)
        else:
            assert False, resnet

        self.conv1 = model.conv1
        self.bn1 = model.bn1
        self.relu = model.relu
        self.maxpool = model.maxpool

        self.layer1 = model.layer1
        self.layer2 = model.layer2
        self.layer3 = model.layer3
        # self.layer4 = model.layer4

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x1 = self.relu(x)   # /2

        x = self.maxpool(x1)
        x2 = self.layer1(x)  # /4

        x3 = self.layer2(x2)    # /8
        x4 = self.layer3(x3)    # /16
        # x5 = self.layer4(x4)    # /32

        return x1, x2, x3, x4
