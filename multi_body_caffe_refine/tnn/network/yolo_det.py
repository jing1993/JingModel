import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict
from multiprocessing import Pool
import numpy as np
from base_model import BaseModel

import net_utils
from tnn.utils.cython_bbox import bbox_ious, bbox_intersections, bbox_overlaps, anchor_intersections
from tnn.utils.cython_yolo import yolo_to_bbox
from tnn.utils.nms_wrapper import nms_detections
from tnn.utils.bbox import clip_boxes


def _process_batch(data):
    bbox_pred_np, iou_pred_np, gt_boxes, gt_classes, cfgs = data
    H, W = cfgs['out_shape']
    n_classes = cfgs['n_classes']
    feat_stride = cfgs['feat_stride']
    inp_H, inp_W = H * feat_stride, W * feat_stride
    # net output
    hw, num_anchors, _ = bbox_pred_np.shape

    object_scale = np.sqrt(5.)
    noobject_scale = 1.
    class_scale = 1.
    coord_scale = 1.
    iou_thresh = 0.6

    # gt
    _classes = np.zeros([hw, num_anchors, n_classes], dtype=np.float)
    _class_mask = np.zeros([hw, num_anchors, 1], dtype=np.float)

    _ious = np.zeros([hw, num_anchors, 1], dtype=np.float)
    _iou_mask = np.zeros([hw, num_anchors, 1], dtype=np.float)

    _boxes = np.zeros([hw, num_anchors, 4], dtype=np.float)
    _boxes[:, :, 0:2] = 0.5
    _boxes[:, :, 2:4] = 1.0
    _box_mask = np.zeros([hw, num_anchors, 1], dtype=np.float)

    if len(gt_boxes) == 0:
        _iou_mask[...] = noobject_scale
        return _boxes, _ious, _classes, _box_mask, _iou_mask, _class_mask

    # scale pred_bbox
    anchors = np.ascontiguousarray(cfgs['anchors'], dtype=np.float)
    bbox_pred_np = np.expand_dims(bbox_pred_np, 0)
    bbox_np = yolo_to_bbox(
        np.ascontiguousarray(bbox_pred_np, dtype=np.float),
        anchors,
        H, W)
    bbox_np = bbox_np[0]  # bbox_np = (hw, num_anchors, (x1, y1, x2, y2))   range: 0 ~ 1
    bbox_np[:, :, 0::2] *= float(inp_W)  # rescale x
    bbox_np[:, :, 1::2] *= float(inp_H)  # rescale y

    # gt_boxes_b = np.asarray(gt_boxes[b], dtype=np.float)
    gt_boxes_b = np.asarray(gt_boxes, dtype=np.float)

    # for each cell, compare predicted_bbox and gt_bbox
    bbox_np_b = np.reshape(bbox_np, [-1, 4])
    ious = bbox_ious(
        np.ascontiguousarray(bbox_np_b, dtype=np.float),
        np.ascontiguousarray(gt_boxes_b, dtype=np.float)
    )
    best_ious = np.max(ious, axis=1).reshape(_iou_mask.shape)
    # _iou_mask[best_ious <= 0.6] = noobject_scale
    iou_penalty = 0 - iou_pred_np[best_ious < 0.6]
    _iou_mask[best_ious <= 0.6] = noobject_scale * iou_penalty

    # locate the cell of each gt_boxe
    cell_w = float(feat_stride)
    cell_h = float(feat_stride)
    cx = (gt_boxes_b[:, 0] + gt_boxes_b[:, 2]) * 0.5 / cell_w
    cy = (gt_boxes_b[:, 1] + gt_boxes_b[:, 3]) * 0.5 / cell_h
    cell_inds = np.floor(cy) * W + np.floor(cx)
    cell_inds = cell_inds.astype(np.int)

    target_boxes = np.empty(gt_boxes_b.shape, dtype=np.float)
    target_boxes[:, 0] = cx - np.floor(cx)  # cx
    target_boxes[:, 1] = cy - np.floor(cy)  # cy
    target_boxes[:, 2] = (gt_boxes_b[:, 2] - gt_boxes_b[:, 0]) / float(feat_stride)  # tw
    target_boxes[:, 3] = (gt_boxes_b[:, 3] - gt_boxes_b[:, 1]) / float(feat_stride)  # th

    # for each gt boxes, match the best anchor
    gt_boxes_resize = np.copy(gt_boxes_b)
    gt_boxes_resize[:, 0::2] /= float(feat_stride)
    gt_boxes_resize[:, 1::2] /= float(feat_stride)
    anchor_ious = anchor_intersections(
        anchors,
        np.ascontiguousarray(gt_boxes_resize, dtype=np.float)
    )
    anchor_inds = np.argmax(anchor_ious, axis=0)

    ious_reshaped = np.reshape(ious, [hw, num_anchors, len(cell_inds)])
    for i, cell_ind in enumerate(cell_inds):
        if cell_ind >= hw or cell_ind < 0:
            print cell_ind
            continue
        a = anchor_inds[i]

        _iou_mask[cell_ind, a, :] = object_scale
        _ious[cell_ind, a, :] = ious_reshaped[cell_ind, a, i]

        _box_mask[cell_ind, a, :] = coord_scale
        target_boxes[i, 2:4] /= anchors[a]
        _boxes[cell_ind, a, :] = target_boxes[i]

        _class_mask[cell_ind, a, :] = class_scale
        _classes[cell_ind, a, gt_classes[i]] = 1.

    # _boxes[:, :, 2:4] = np.maximum(_boxes[:, :, 2:4], 0.001)
    # _boxes[:, :, 2:4] = np.log(_boxes[:, :, 2:4])

    return _boxes, _ious, _classes, _box_mask, _iou_mask, _class_mask


def _build_target(bbox_pred_np, iou_pred_np, gt_boxes, gt_classes, cfg):
    """
    :param bbox_pred_np: shape: (bsize, h x w, num_anchors, 4) : (sig(tx), sig(ty), exp(tw), exp(th))
    """

    bsize = bbox_pred_np.shape[0]

    pool = Pool(20)
    targets = pool.map(_process_batch,
                          ((bbox_pred_np[b], iou_pred_np[b], gt_boxes[b], gt_classes[b], cfg) for b in range(bsize)))

    _boxes = np.stack(tuple((row[0] for row in targets)))
    _ious = np.stack(tuple((row[1] for row in targets)))
    _classes = np.stack(tuple((row[2] for row in targets)))
    _box_mask = np.stack(tuple((row[3] for row in targets)))
    _iou_mask = np.stack(tuple((row[4] for row in targets)))
    _class_mask = np.stack(tuple((row[5] for row in targets)))
    pool.close()
    pool.join()
    return _boxes, _ious, _classes, _box_mask, _iou_mask, _class_mask


class YOLODetNet(BaseModel):
    def __init__(self, in_channels, anchors, n_classes, feat_stride):
        """
        
        :param in_channels: 
        :param anchors: like [(1.08, 1.19), (3.42, 4.41), (6.63, 11.38), (9.42, 5.11), (16.62, 10.52)], based on feat_stride
        :param n_classes: 
        """
        super(YOLODetNet, self).__init__()

        self.anchors = np.asarray(anchors, dtype=np.float)
        self.n_anchors = len(self.anchors)
        self.n_classes = n_classes
        self.feat_stride = feat_stride

        out_channels = self.n_anchors * (n_classes + 5)
        self.conv = nn.Conv2d(in_channels, out_channels, 1)

    def forward(self, feature):
        pred = self.conv(feature)

        # for detection
        # bsize, c, h, w -> bsize, h, w, c -> bsize, h x w, num_anchors, 5+num_classes
        bsize, _, h, w = pred.size()
        # assert bsize == 1, 'detection only support one image per batch'
        pred_reshaped = pred.permute(0, 2, 3, 1).contiguous().view(bsize, -1, self.n_anchors, self.n_classes + 5)

        # tx, ty, tw, th, to -> sig(tx), sig(ty), exp(tw), exp(th), sig(to)
        xy_pred = F.sigmoid(pred_reshaped[:, :, :, 0:2])
        wh_pred = torch.exp(pred_reshaped[:, :, :, 2:4])
        bbox_pred = torch.cat([xy_pred, wh_pred], 3)
        iou_pred = F.sigmoid(pred_reshaped[:, :, :, 4:5])

        score_pred = pred_reshaped[:, :, :, 5:].contiguous()
        prob_pred = F.softmax(score_pred.view(-1, score_pred.size()[-1])).view_as(score_pred)

        return bbox_pred, iou_pred, prob_pred

    @staticmethod
    def build_loss(bbox_pred, iou_pred, prob_pred, cfg, gt_boxes, gt_classes):
        """
        cfg = {'out_shape': (h, w), 'n_classes': 1,
               'anchors': anchors, 'n_anchors': len(anchors), 'feat_stride': stride, 'gpus':list}
        """
        bbox_pred_np = bbox_pred.data.cpu().numpy()
        iou_pred_np = iou_pred.data.cpu().numpy()

        _boxes, _ious, _classes, _box_mask, _iou_mask, _class_mask = _build_target(bbox_pred_np, iou_pred_np, gt_boxes, gt_classes, cfg)

        gpus = cfg['gpus']
        is_cuda = gpus is not None
        _boxes = net_utils.np_to_variable(_boxes, is_cuda, device_id=gpus[0])
        _ious = net_utils.np_to_variable(_ious, is_cuda, device_id=gpus[0])
        _classes = net_utils.np_to_variable(_classes, is_cuda, device_id=gpus[0])
        box_mask = net_utils.np_to_variable(_box_mask, is_cuda, dtype=torch.FloatTensor, device_id=gpus[0])
        iou_mask = net_utils.np_to_variable(_iou_mask, is_cuda, dtype=torch.FloatTensor, device_id=gpus[0])
        class_mask = net_utils.np_to_variable(_class_mask, is_cuda, dtype=torch.FloatTensor, device_id=gpus[0])

        num_boxes = sum((len(boxes) for boxes in gt_boxes)) + 1e-5

        # _boxes[:, :, :, 2:4] = torch.log(_boxes[:, :, :, 2:4])
        box_mask = box_mask.expand_as(_boxes)

        bbox_loss = nn.MSELoss(size_average=False)(bbox_pred * box_mask, _boxes * box_mask) / num_boxes
        iou_loss = nn.MSELoss(size_average=False)(iou_pred * iou_mask, _ious * iou_mask) / num_boxes

        if cfg['n_classes'] > 1:
            class_mask = class_mask.expand_as(prob_pred)
            cls_loss = nn.MSELoss(size_average=False)(prob_pred * class_mask, _classes * class_mask) / num_boxes
        else:
            cls_loss = 0

        loss = bbox_loss + iou_loss + cls_loss

        saved_for_log = OrderedDict()
        saved_for_log['loss'] = loss.data[0]
        saved_for_log['bbox_loss'] = bbox_loss.data[0]
        saved_for_log['iou_loss'] = iou_loss.data[0]

        if cfg['n_classes'] > 1:
            saved_for_log['cls_loss'] = cls_loss.data[0]

        return loss, saved_for_log

    def postprocess(self, bbox_pred, iou_pred, prob_pred, inp_shape, thresh=0.05):
        """
        bbox_pred: (bsize, HxW, num_anchors, 4) ndarray of float (sig(tx), sig(ty), exp(tw), exp(th))
        iou_pred: (bsize, HxW, num_anchors, 1)
        prob_pred: (bsize, HxW, num_anchors, num_classes)
        """

        num_anchors, num_classes = prob_pred.shape[2:4]
        anchors = self.anchors
        stride = self.feat_stride
        H = inp_shape[0] // stride
        W = inp_shape[1] // stride
        # H, W = out_shape[:2]
        assert bbox_pred.shape[0] == 1, 'postprocess only support one image per batch'

        bbox_pred = yolo_to_bbox(
            np.ascontiguousarray(bbox_pred, dtype=np.float),
            np.ascontiguousarray(anchors, dtype=np.float),
            H, W)
        bbox_pred = np.reshape(bbox_pred, [-1, 4])
        bbox_pred[:, 0::2] *= float(inp_shape[1])
        bbox_pred[:, 1::2] *= float(inp_shape[0])
        bbox_pred = bbox_pred.astype(np.int)

        iou_pred = np.reshape(iou_pred, [-1])
        prob_pred = np.reshape(prob_pred, [-1, num_classes])

        cls_inds = np.argmax(prob_pred, axis=1)
        prob_pred = prob_pred[(np.arange(prob_pred.shape[0]), cls_inds)]
        if num_classes > 1:
            scores = iou_pred * prob_pred
        else:
            scores = iou_pred
        # scores = iou_pred

        # threshold
        keep = np.where(scores >= thresh)
        bbox_pred = bbox_pred[keep]
        scores = scores[keep]
        cls_inds = cls_inds[keep]
        # print scores.shape

        # NMS
        keep = np.zeros(len(bbox_pred), dtype=np.int)
        for i in range(num_classes):
            inds = np.where(cls_inds == i)[0]
            if len(inds) == 0:
                continue
            c_bboxes = bbox_pred[inds]
            c_scores = scores[inds]
            c_keep = nms_detections(c_bboxes, c_scores, 0.3)
            keep[inds[c_keep]] = 1

        keep = np.where(keep > 0)
        # keep = nms_detections(bbox_pred, scores, 0.3)
        bbox_pred = bbox_pred[keep]
        scores = scores[keep]
        cls_inds = cls_inds[keep]

        # clip
        bbox_pred = clip_boxes(bbox_pred, inp_shape)

        return bbox_pred, scores, cls_inds
