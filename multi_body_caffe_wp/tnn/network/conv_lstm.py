import torch
import torch.nn as nn
from torch.autograd import Variable


class ConvLSTMCell(nn.Module):
    def __init__(self, input_channels, hidden_channels, kernel_size=None, conv_block=None):
        super(ConvLSTMCell, self).__init__()

        assert kernel_size is not None or conv_block is not None

        self.input_channels = input_channels
        self.hidden_channels = hidden_channels

        if conv_block is None:
            self.padding = (kernel_size - 1) / 2
            self.conv = nn.Conv2d(self.input_channels + self.hidden_channels, 4 * self.hidden_channels, kernel_size, 1,
                                  self.padding)
        else:
            self.conv = conv_block

    def forward(self, input, h, c):

        combined = torch.cat((input, h), 1)
        A = self.conv(combined)
        (ai, af, ao, ag) = torch.split(A, A.size()[1] // 4, dim=1)
        i = torch.sigmoid(ai)
        f = torch.sigmoid(af)
        o = torch.sigmoid(ao)
        g = torch.tanh(ag)

        new_c = f * c + i * g
        new_h = o * torch.tanh(new_c)
        return new_h, new_c

    @staticmethod
    def init_hidden(batch_size, hidden_c, shape, device_id=0):
        h, c = Variable(torch.zeros(batch_size, hidden_c, shape[0], shape[1])), \
               Variable(torch.zeros(batch_size, hidden_c, shape[0], shape[1]))
        if device_id is not None:
            h = h.cuda(device_id)
            c = c.cuda(device_id)

        return h, c


class ConvLSTM(nn.Module):
    def __init__(self, input_channels, hidden_channels, kernel_size=None, conv_block=None):
        super(ConvLSTM, self).__init__()
        self.input_channels = input_channels
        self.hidden_channels = hidden_channels
        self.cell = ConvLSTMCell(input_channels, hidden_channels, kernel_size, conv_block)
        self.state = None
        self.state_training = None
        self.pre_training = True

    def forward(self, input, clear):
        if self.training and not self.pre_training:
            self.state = self.state_training
            self.pre_training = True
        elif not self.training and self.pre_training:
            self.state_training = self.state
            self.state = None
            self.pre_training = False

        bsize, _, h, w = input.size()
        if self.state is None:
            device_id = None if not input.is_cuda else input.get_device()
            state = ConvLSTMCell.init_hidden(bsize, self.hidden_channels, (h, w), device_id=device_id)
        else:
            hx, cx = self.state
            clear = clear.data
            for i in range(bsize):
                if clear[i] != 0:
                    hx[i].zero_()
                    cx[i].zero_()
            state = (Variable(hx), Variable(cx))

        hx, cx = state
        new_h, new_c = self.cell(input, hx, cx)
        self.state = (new_h.data, new_c.data)

        return new_h
