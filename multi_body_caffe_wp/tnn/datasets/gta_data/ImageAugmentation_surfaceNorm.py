# coding=utf-8
import random
import cv2
import numpy as np


def aug_scale(img, sn, mask, params_transform):

    dice = random.random()
    scale = (params_transform['scale_max'] - params_transform['scale_min']) * dice + params_transform['scale_min']

    img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
    sn = cv2.resize(sn, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_NEAREST)
    mask = cv2.resize(mask, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_NEAREST)
    if img.shape[:2] != mask.shape:
        print 'scale size, ', mask.shape, img.shape
    return img, sn, mask


def aug_croppad(img, sn, mask, params_transform):
    dice_x = random.random()
    dice_y = random.random()
    random_color = (np.random.random(3)*255).astype(np.uint8)
    crop_x = int(params_transform['crop_size_x'])
    crop_y = int(params_transform['crop_size_y'])
    x_offset = int((dice_x - 0.5) * 2 *
                   params_transform['center_perterb_max'])
    y_offset = int((dice_y - 0.5) * 2 *
                   params_transform['center_perterb_max'])

    center = np.array([img.shape[1]/2, img.shape[0]/2]) + np.array([x_offset, y_offset])
    center = center.astype(int)

    # pad up and down

    pad_v = np.ones((crop_y, mask.shape[1], 3), dtype=np.uint8) * 0
    pad_v_sn = np.zeros((crop_y, sn.shape[1], 3), dtype=np.uint8) * 255
    pad_v_mask = np.zeros((crop_y, mask.shape[1]), dtype=np.uint8) * 255

    img = np.concatenate((pad_v, img, pad_v), axis=0)
    sn = np.concatenate((pad_v_sn, sn, pad_v_sn), axis=0)
    mask = np.concatenate((pad_v_mask, mask, pad_v_mask), axis=0)

    # pad right and left
    pad_h = np.ones([img.shape[0], crop_x, 3], dtype = np.uint8) * 0
    pad_h_sn = np.zeros((sn.shape[0], crop_x, 3), dtype=np.uint8) * 255
    pad_h_mask = np.zeros((mask.shape[0], crop_x), dtype=np.uint8) * 255

    img = np.concatenate((pad_h, img, pad_h), axis=1)
    sn = np.concatenate((pad_h_sn, sn, pad_h_sn), axis=1)
    mask = np.concatenate((pad_h_mask, mask, pad_h_mask), axis=1)

    img = img[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x, :]
    sn = sn[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x]
    mask = mask[center[1] + crop_y / 2:center[1] + crop_y / 2 + crop_y, center[0] + crop_x / 2:center[0] + crop_x / 2 + crop_x]


    return img, sn, mask


def aug_flip(img, sn, mask, params_transform):
    dice = random.random()
    doflip = dice <= params_transform['flip_prob']

    if doflip:
        img = cv2.flip(img, 1)
        sn = cv2.flip(sn, 1)
        mask = cv2.flip(mask, 1)

        mask_sky = np.sum(sn, 2) == 0
        sn[:, :, 2] = 255 - sn[:, :, 2]
        sn[mask_sky, :] = 0
        sn[mask == 0] = 0
    
    return img, sn, mask

def rotate_bound(image, angle, bordervalue):
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH), flags=cv2.INTER_NEAREST, borderMode=cv2.BORDER_CONSTANT,
                          borderValue=bordervalue)


def aug_rotate(img, sn, mask, params_transform, type="random", input=0, fillType="nearest", constant=0):
    dice = random.random()
    degree = (dice - 0.5) * 2 * params_transform['max_rotate_degree']  # degree [-40,40]
    radius = degree*np.pi/180.
    rotate_matrix = np.asarray([[np.cos(radius), 0, np.sin(radius)], [0, 1., 0], [-np.sin(radius), 0, np.cos(radius)]])
    random_color = (np.random.random(3)*255).astype(int)
    img_rot = rotate_bound(img, np.copy(degree), (0))
    sn_rot = rotate_bound(sn, np.copy(degree), (0))
    mask_sky = np.sum(sn_rot, 2) == 0
    sn_rot = ((sn_rot /127.5 - 1).dot(rotate_matrix) * 127.5 + 127.5).astype(np.uint8)
    mask_rot = rotate_bound(mask, np.copy(degree), (0))
    if mask_rot.shape != img_rot.shape[:2]:
        print 'rotate size, ', mask_rot.shape, img_rot.shape
    sn_rot[mask_sky, :] = 0
    return img_rot, sn_rot, mask_rot

def aug_blur(img, k_std = 1, xy_std = 1):
    ksize = np.abs(np.random.normal(loc = 0, scale = k_std, size = 2).astype(int))
    xy = np.abs(np.random.normal(loc = 0, scale = xy_std, size = 2).astype(int))
    return  cv2.GaussianBlur(img, ksize = (2*ksize[0]+1, 2*ksize[1]+1), sigmaX = xy[0], sigmaY = xy[1])

def aug_noise(img, std = 5):
    noise_matrix = np.random.normal(loc = 0, scale = std, size = img.shape)
    img = img+noise_matrix
    img[img<0] = 0
    img[img>255] = 255
    return img.astype(np.uint8)

def aug_tint(img, scale = 0.1):
    tint_scale = 1+scale*np.random.random([1,1,3])-scale/2
    img = img*tint_scale
    img[img<0] = 0
    img[img>255] = 255
    return img.astype(np.uint8)

def ImageAugmentation(img, sn, mask, params):
    if img.shape[:2] != mask.shape:
        print 'origin size, ', mask.shape, img.shape
    img, sn, mask = aug_scale(img, sn, mask, params)
    img, sn, mask = aug_rotate(img, sn, mask, params)
    img, sn, mask = aug_croppad(img, sn, mask, params)
    img, sn, mask = aug_flip(img, sn, mask, params)
    img = aug_blur(img)
    img = aug_tint(img)
    img = aug_noise(img)
    return img, sn, mask


if __name__ == '__main__':
    def debug(img, sn, mask, status):
        cv2.imwrite('img_{}.png'.format(status), img)
        cv2.imwrite('sn_{}.png'.format(status), sn)
        cv2.imwrite('mask_{}.png'.format(status), mask)

    params_transform = dict()
    params_transform['scale_min'] = 0.5
    params_transform['scale_max'] = 1.1
    params_transform['max_rotate_degree'] = 40
    params_transform['center_perterb_max'] = 40
    params_transform['crop_size_x'] = 368
    params_transform['crop_size_y'] = 368
    params_transform['flip_prob'] = 1.0\
    
    index = np.random.randint(10000)
    image = cv2.imread('/data/GTA/outdoor/origin/frame{}_origin.png'.format(index))
    sn = cv2.imread('/data/GTA/outdoor/surfaceNorm/frame{}_surfaceNorm.png'.format(index))
    mask = np.zeros([sn.shape[0], sn.shape[1]])
    mask[np.sum(sn, 2) != 0] = 255.
    image, sn, mask = ImageAugmentation(image, sn, mask, params_transform)

    cv2.imwrite('img.png', image)
    cv2.imwrite('sn.png', sn)
    cv2.imwrite('mask.png', mask)
