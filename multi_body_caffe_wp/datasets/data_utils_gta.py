import cv2
import numpy as np
import torch
from tnn.utils import im_transform_gta
import utils.depth as depth_utils


def data_augmentation(coors, im, additions=(), **kwargs):
    """
    Apply data augmentation to image and some additional labels
    :param im: np.ndarray, [h, w, 3]
    :param additions: tuple or list of np.ndarray with size [h, w], [h, w, 3]
    :param training: is training
    :param kwargs: kwargs for imcv2_affine_trans
    :return: new_im, new_additions, trans_params
    """

    new_coors, new_im, trans_params = im_transform_gta.imcv2_affine_trans(coors, im, **kwargs)

    new_additions = []
    for v in additions:
        new_additions.append(im_transform_gta.apply_affine(v, *trans_params))

    return new_coors, new_im, new_additions, trans_params


def resize_2d(tensor, scale):
    return torch.from_numpy(cv2.resize(tensor.numpy(), None, fx=scale, fy=scale))


def resize_3d(tensor, scale):
    _im = tensor.numpy().transpose(1, 2, 0)
    _im = cv2.resize(_im, None, fx=scale, fy=scale)
    if _im.ndim == 2:
        _im = _im[:, :, np.newaxis]
    return torch.from_numpy(_im.transpose(2, 0, 1))


def resize_tensor(tensor, scale):
    if tensor.dim() == 2:
        return resize_2d(tensor, scale)
    elif tensor.dim() == 3:
        return resize_3d(tensor, scale)
    else:
        assert False, tensor.dim()


def mask_generation(seg, mask = None):
    """
    generate mask based on segmentation. This can be used for synthetic data which has no mask information, like GTA data.
    """
    if mask is None:
        mask = np.ones_like(seg) * 255

    _, contours, hierarchy = cv2.findContours(seg, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    for i in range(len(contours)):
        con = contours[i]
        mean_x = con[:, 0, 0].mean()
        mean_y = con[:, 0, 1].mean()
        if hierarchy[0][i][-1] == -1:
            area = cv2.contourArea(con)
            arc = cv2.arcLength(con, True)
            hull = cv2.convexHull(con)
            hull_area = cv2.contourArea(hull)
            hull_arc = cv2.arcLength(hull, True)
            area_ratio = area / hull_area if area != 0 else 1
            arc_ratio = arc / hull_arc if arc != 0 else 1
            if area <= 1000 or (area <= 5000 and area_ratio > 0.92 and arc_ratio < 1.1) or (area_ratio > 0.97):
                mask = cv2.drawContours(mask, [con], 0, 0, -1)

    mask[seg == 0] = 255
    return mask
