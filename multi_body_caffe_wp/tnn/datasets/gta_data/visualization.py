import numpy as np
import cv2


def visualization(batch, output, epoch, step, task='segmentation'):
    '''visualize training output for training process eyeballing
    '''

    if task is None:
        return

    means = [0.485, 0.456, 0.406]
    stds = [0.229, 0.224, 0.225]


    im_data, heatmaps_data, heat_mask, pafs, paf_mask, seg_data, seg_mask = batch
    #inp, depth_var, sn_var, seg_var, mask_var, snmask_var, pts_var, num_pts_var, heatmaps_var, pafs_var, _ = batch
    #depth_var, sn_var, seg_var, mask_var, snmask_var, pts_var, num_pts_var, heatmaps_var, pafs_var = gts
    #mask = mask[0].numpy()[0] * 255
    #pred_PAF, pred_heatmap, seg_pred = output.data.cpu()

    heatmaps_data = heatmaps_data[0].numpy()
    heatmaps_gt = heatmaps_data[0]
    for i in range(len(heatmaps_data) - 1):
        heatmaps_gt = np.maximum(heatmaps_gt, heatmaps_data[i])
    heatmaps_gt = (heatmaps_gt * 255).astype(np.uint8)

    heatmaps_pred = output[1].data.cpu().numpy()[0]
    ht_pred = heatmaps_pred[0]
    for i in range(len(heatmaps_pred) - 1):
        ht_pred = np.maximum(ht_pred, heatmaps_pred[i])
    ht_pred = (ht_pred * 255).astype(np.uint8)


    seg_gt = (seg_data[0][0].numpy()*255).astype(np.uint8)
    seg_pred = (output[2].data.cpu().numpy()[0][0]*255).astype(np.uint8)
    #print seg_pred.shape

    image = np.transpose(im_data[0].numpy(), (1, 2, 0))[:, :, ::-1]
    for i in range(3):
        image[:, :, i] = (image[:, :, i] * stds[i] + means[i]) * 255


    cv2.imwrite('/data/wpcaffe/epoch{}_step{}_input_img.png'.format(epoch, step), image)
    cv2.imwrite('/data/wpcaffe/epoch{}_step{}_seg_gt.png'.format(epoch, step), seg_gt)
    cv2.imwrite('/data/wpcaffe/epoch{}_step{}_seg_pred.png'.format(epoch, step), seg_pred)
    cv2.imwrite('/data/wpcaffe/epoch{}_step{}_ht_gt.png'.format(epoch, step), heatmaps_gt)
    cv2.imwrite('/data/wpcaffe/epoch{}_step{}_ht_pred.png'.format(epoch, step), ht_pred)
    

