#!/usr/bin/env bash

CUDA_PATH=/usr/local/cuda/

cd utils
python build.py build_ext --inplace
cd ../

cd network/bnn/src
echo "Compiling bnn layer kernels by nvcc..."
nvcc -c -o bnn_cuda_kernel.cu.o bnn_cuda_kernel.cu -x cu -Xcompiler -fPIC -arch=sm_52

cd ../
python build.py

cd ../real_conv
python build.py
