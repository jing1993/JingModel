import vispy
vispy.use('pyqt5')

import numpy as np
import sys
import os
import cv2
from utils import plyfile

from vispy import scene, app
from vispy.scene import cameras


cmap_is_image = True
# init view before import torch to avoid a strange bug.
# canvas = scene.SceneCanvas(title='Depth Cloud', keys={'q': app.quit}, bgcolor='w')
# view = canvas.central_widget.add_view()


def get_cmap(cimage, shape):
    if cimage.shape[0:2] != shape[0:2]:
        cimage = cv2.resize(cimage, (shape[1], shape[0]))

    if cimage.ndim == 2 or cimage.shape[2] == 1:
        cimage = cv2.applyColorMap((cimage * 255).astype(np.uint8), cv2.COLORMAP_JET) / 255.

    return cimage


def get_xy(depth_shape):
    h, w = depth_shape[0:2]
    y, x = np.mgrid[0:h, 0:w]
    x = np.round(x - w / 2.).astype(np.int)
    y = np.round(y - h / 2.).astype(np.int)

    return x, y


def depth_to_points(depth_map, cmap, min_size=320., scale=None):
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # resize
    fx = float(min_size) / np.min(depth_map.shape[0:2])
    cmap = cv2.resize(cmap, None, fx=fx, fy=fx)
    depth_map = cv2.resize(depth_map, None, fx=fx, fy=fx)

    # d = (depth_map - depth_map.min()) / (depth_map.max() - depth_map.min() + 1e-5)
    d = depth_map
    d = d * min_size * 1
    # d = depth_map * (30. / 288) * image.shape[0]
    # d = (d - d.min() + 1)

    # get x, y
    x, y = get_xy(d.shape)

    # depth -> z
    h, w = d.shape[0:2]
    # z = d * np.cos(np.abs(x) / (w / 2.) * 30 / 180. * np.pi) * np.cos(np.abs(y) / (h / 2.) * 20 / 180. * np.pi)
    z = d

    if scale is not None:
        x = x * scale
        y = y * scale
        z = z * scale

    # get color map
    # cmap = get_cmap(z, image if cmap_is_image else None)
    cmap = get_cmap(cmap, depth_map.shape)

    return x, y, z, cmap


def plot_depth(image, depth_map, depth_normalized, min_size=640.):
    canvas = scene.SceneCanvas(title='Depth Cloud', keys={'q': app.quit}, bgcolor='w')
    view = canvas.central_widget.add_view()
    view.camera = cameras.ArcballCamera()
    # view.camera = cameras.TurntableCamera()
    view.camera.fov = 100
    view.camera.distance = 400

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) / 255.
    cmap = image if cmap_is_image else depth_normalized

    x, y, z, cmap = depth_to_points(depth_map, cmap, min_size)

    p1 = scene.visuals.GridMesh(x, z, -y, cmap, shading=None)
    view.add(p1)

    @canvas.events.mouse_double_click.connect
    def mouse_double_click(event):
        print("Changing Colormap")
        global cmap_is_image
        cmap_is_image = not cmap_is_image

        cmap_ = get_cmap(image if cmap_is_image else depth_normalized, cmap.shape)
        # print cmap.shape, cimage.shape, z.shape
        p1.set_data(x, z, -y, cmap_)
        canvas.update()
    canvas.show(run=True)


def save_ply(filename, image, depth_map, depth_normalized, min_size=640.):
    cmap = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) / 255. if cmap_is_image else depth_normalized
    x, y, z, cmap = depth_to_points(depth_map, cmap, min_size)

    x = x.reshape(-1)
    y = y.reshape(-1)
    z = z.reshape(-1)
    cmap = cmap.reshape(-1, 3)
    cmap_uint8 = (cmap * 255).astype(np.uint8)
    ply_data = np.empty(len(x), dtype=[
        ('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'), ('blue', 'u1')
    ])
    ply_data['x'] = x
    ply_data['y'] = z
    ply_data['z'] = -y
    ply_data['red'] = cmap_uint8[:, 0]
    ply_data['green'] = cmap_uint8[:, 1]
    ply_data['blue'] = cmap_uint8[:, 2]
    el = plyfile.PlyElement.describe(ply_data, name='vertex')
    plyfile.PlyData([el], text=True).write(filename)

    print('save ply to {}'.format(filename))