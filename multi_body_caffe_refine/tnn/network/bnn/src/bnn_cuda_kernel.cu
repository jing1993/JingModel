#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "common.h"
#include "bnn_cuda_kernel.h"

#define BLOCK_SIZE 16

/*
 * Fallows the CUDA tutorial: http://www.nvidia.com/docs/IO/116711/sc11-cuda-c-basics.pdf
 * http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory
 * A is shape (m,n), B is shape (n,k) and C is shape (m,k)
 */
__global__ void gemm( float* A, float* B, float* C, int m, int n, int k, float* alphas )
{
	/* Block row and column */
	int	blockRow	= blockIdx.y;
	int	blockCol	= blockIdx.x;

	/* Thread row and column within Csub */
	int	row	= threadIdx.y;
	int	col	= threadIdx.x;

	int startLocation = BLOCK_SIZE * k * blockRow + BLOCK_SIZE * blockCol;

	/* Each thread block computes one sub-matrix Csub of C */
	float* Csub = &C[BLOCK_SIZE * k * blockRow + BLOCK_SIZE * blockCol];

	/* Shared memory used to store Asub and Bsub respectively */
	__shared__ float	As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float	Bs[BLOCK_SIZE][BLOCK_SIZE];

	/*
	 * Each thread computes one element of Csub
	 * by accumulating results into Cvalue
	 * block_size = 16 -> 256 threads, one per Csub element
	 */
	float Cvalue = 0.0;

	int	c	= blockIdx.x * blockDim.x + threadIdx.x;        /* row value using x-index of current thread */
	int	r	= blockIdx.y * blockDim.y + threadIdx.y;        /* column value using y-index of current thread */

	/*
	 * Loop over all the sub-matrices of A and B that are
	 * required to compute Csub
	 * Multiply each pair of sub-matrices together
	 * and accumulate the results
	 */
	for ( int i = 0; i < 1 + ( (n - 1) / BLOCK_SIZE); ++i )
	{
		/* Get sub-matrix Asub of A */
		float* Asub = &A[BLOCK_SIZE * blockRow * n + BLOCK_SIZE * i];

		/* Get sub-matrix Bsub of B */
		float* Bsub = &B[BLOCK_SIZE * k * i + BLOCK_SIZE * blockCol];

		/*
		 * Load Asub and Bsub from device memory to shared memory
		 * Each thread loads one element of each sub-matrix
		 */

		/* Load zero if outside of the range */
		if ( (BLOCK_SIZE * i + col) < n && r < m )
			As[row][col] = Asub[row * n + col];
		else
			As[row][col] = 0;
		if ( (BLOCK_SIZE * i + row) < n && c < k )
			Bs[row][col] = Bsub[row * k + col];
		else
			Bs[row][col] = 0;

		/*
		 * Synchronize to make sure the sub-matrices are loaded
		 * before starting the computation
		 */
		__syncthreads();

		/* Multiply Asub and Bsub together */
#pragma unroll 4
		for ( int j = 0; j < BLOCK_SIZE; ++j )
		{
			Cvalue += As[row][j] * Bs[j][col];
		}

		/*
		 * Synchronize to make sure that the preceding
		 * computation is done before loading two new
		 * sub-matrices of A and B in the next iteration
		 */
		__syncthreads();
	}

	/*
	 * Write Csub to device memory
	 * Each thread writes one element
	 */
	if ( col + blockCol * BLOCK_SIZE < k && row + blockRow * BLOCK_SIZE < m )
		Csub[row * k + col] = Cvalue * alphas[(startLocation + row * k + col) / k];
}


/*
 * Based on the above one, the multiplication is replaced with binary operation popcount(xor)
 * A is shape (m,n/32), B is shape (n/32,k) and C is shape (m,k)
 */
__global__ void binary_gemm( unsigned int* A, unsigned int* B, float* C, int m, int n, int k, float *alphas )
{
	/* Block row and column */
	int	blockRow	= blockIdx.y;
	int	blockCol	= blockIdx.x;

	/* Thread row and column within Csub */
	int	row	= threadIdx.y;
	int	col	= threadIdx.x;

	int startLocation = BLOCK_SIZE * k * blockRow + BLOCK_SIZE * blockCol;

	/* Each thread block computes one sub-matrix Csub of C */
	float* Csub = &C[BLOCK_SIZE * k * blockRow + BLOCK_SIZE * blockCol];

	/* Shared memory used to store Asub and Bsub respectively */
	__shared__ unsigned int As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ unsigned int Bs[BLOCK_SIZE][BLOCK_SIZE];

	/*
	 * Each thread computes one element of Csub
	 * by accumulating results into Cvalue
	 * block_size = 16 -> 256 threads, one per Csub element
	 */
	unsigned int Cvalue = 0;

	int	c	= blockIdx.x * blockDim.x + threadIdx.x;        /* row value using x-index of current thread */
	int	r	= blockIdx.y * blockDim.y + threadIdx.y;        /* column value using y-index of current thread */

	/*
	 * Loop over all the sub-matrices of A and B that are
	 * required to compute Csub
	 * Multiply each pair of sub-matrices together
	 * and accumulate the results
	 */
	for ( int i = 0; i < 1 + ( (n - 1) / BLOCK_SIZE); ++i )
	{
		/* Get sub-matrix Asub of A */
		unsigned int* Asub = &A[BLOCK_SIZE * blockRow * n + BLOCK_SIZE * i];

		/* Get sub-matrix Bsub of B */
		unsigned int* Bsub = &B[BLOCK_SIZE * k * i + BLOCK_SIZE * blockCol];

		/*
		 * Load Asub and Bsub from device memory to shared memory
		 * Each thread loads one element of each sub-matrix
		 * Load zero if outside of the range
		 */
		if ( (BLOCK_SIZE * i + col) < n && r < m )
			As[row][col] = Asub[row * n + col];
		else
			As[row][col] = 0;
		if ( (BLOCK_SIZE * i + row) < n && c < k )
			Bs[row][col] = Bsub[row * k + col];
		else
			Bs[row][col] = 0;

		/*
		 * Synchronize to make sure the sub-matrices are loaded
		 * before starting the computation
		 */
		__syncthreads();

		/* Multiply Asub and Bsub together (binary case) */
#pragma unroll 4
		for ( int j = 0; j < BLOCK_SIZE; ++j )
			Cvalue += __popc( As[row][j] ^ Bs[j][col] );
/*        for (int j = 0; j < BLOCK_SIZE; ++j) Cvalue += __popc(As[row][j]&Bs[j][col]); */

		/*
		 * Synchronize to make sure that the preceding
		 * computation is done before loading two new
		 * sub-matrices of A and B in the next iteration
		 */
		__syncthreads();
	}

	/*
	 * Write Csub to device memory
	 * Each thread writes one element
	 */
	if ( col + blockCol * BLOCK_SIZE < k && row + blockRow * BLOCK_SIZE < m )
		Csub[row * k + col] = -(2 * (float) Cvalue - 32 * n) * alphas[(startLocation + row * k + col) / k];
/*
 * if(col + blockCol* BLOCK_SIZE< k && row + blockRow* BLOCK_SIZE< m) Csub[row*k+col] = -(2*(float)Cvalue-32*n);
 * if(col + blockCol* BLOCK_SIZE< k && row + blockRow* BLOCK_SIZE< m) Csub[row*k+col] = Cvalue;
 */
}


/*
 * Kernel for fast unfold+copy
 * (borrowed from Caffe: https://github.com/BVLC/caffe/blob/master/src/caffe/layers/conv_layer.cu)
 */
__global__ void im2col_kernel( const int n, const float* data_im, const float * runing_mean, const float * gamma_sign,
			       const int height, const int width,
			       const int ksize_h, const int ksize_w,
			       const int pad_h, const int pad_w,
			       const int stride_h, const int stride_w,
			       const int dilation_h, const int dilation_w,
			       const int height_col, const int width_col, int* data_col )
{
	CUDA_KERNEL_LOOP( index, n )
	{
		int w_out = index % width_col;
		index /= width_col;
		int	h_out		= index % height_col;
		int	channel_in	= index / height_col;

		int	channel_out	= channel_in * ksize_h * ksize_w;
		int	h_in		= h_out * stride_h - pad_h;
		int	w_in		= w_out * stride_w - pad_w;

		data_col	+= (channel_out * height_col + h_out) * width_col + w_out;
		data_im		+= (channel_in * height + h_in) * width + w_in;

		for ( int i = 0; i < ksize_h; ++i )
		{
			for ( int j = 0; j < ksize_w; ++j )
			{
				int	h	= h_in + i * dilation_h;
				int	w	= w_in + j * dilation_w;
//				*data_col = (h >= 0 && w >= 0 && h < height && w < width) ?
//					    data_im[i * dilation_h * width + j * dilation_w] : -1;

				*data_col = (h < 0 || w < 0 || h >= height || w >= width) ?
									  0 : (gamma_sign[channel_in] > 0 ?
										   data_im[i * dilation_h * width + j * dilation_w] > runing_mean[channel_in] :
										   data_im[i * dilation_h * width + j * dilation_w] < runing_mean[channel_in]);

				data_col += height_col * width_col;
			}
		}
	}
}


__global__ void encode_cols_kernel( int *a, unsigned int* b, int m, int n )
{
	int	j	= blockIdx.x * blockDim.x + threadIdx.x;
	int	i	= blockIdx.y * blockDim.y + threadIdx.y;

	int i32 = i * 32;

	if ( j < n && i32 < m )
	{
//		float		num;
		unsigned int	rvalue = 0;
		unsigned int	sign;

#pragma unroll 4
		for ( int k = 0; k < 32; k++ )
		{
//			num	= a[j + n * (i32 + k)];
			sign	= a[j + n * (i32 + k)];
			rvalue	|= (sign << k);
		}
		b[j + n * i] = rvalue;
	}
}


/* for backward: encode or decode binary weight */
__forceinline__ __device__ void decode_val( unsigned int input, float* output )
{
	for ( int i = 0; i < 32; ++i )
	{
		output[i] = (input & (1 << i) ) >> i;
	}
}


__forceinline__ __device__ unsigned int encode_val( float* array )
{
	unsigned int	val = 0;
	unsigned int	sign;

	for ( int i = 0; i < 32; i++ )
	{
		sign	= (array[i] > 0);
		val	|= (sign << i);
	}
	return(val);
}


__global__ void decode_rows_kernel( unsigned int* input, float *output, int size )
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if ( i < size )
		decode_val( input[i], &output[i * 32] );
}


__global__ void encode_rows_kernel( float *input, unsigned int* output, int size )
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if ( i < size )
		output[i] = encode_val( &input[i * 32] );
}


/* torch */
void im2col( const float* data_im, const float * runing_mean, const float * gamma_sign, const int channels,
	     const int height, const int width,
	     const int ksize_h, const int ksize_w, const int pad_h,
	     const int pad_w, const int stride_h, const int stride_w,
	     const int dilation_h, const int dilation_w, int* data_col, cudaStream_t stream )
{
	/*
	 * We are going to launch channels * height_col * width_col kernels, each
	 * kernel responsible for copying a single-channel grid.
	 */
	int height_col = (height + 2 * pad_h - (dilation_h * (ksize_h - 1) + 1) )
			 / stride_h + 1;
	int width_col = (width + 2 * pad_w - (dilation_w * (ksize_w - 1) + 1) )
			/ stride_w + 1;
	int		num_kernels = channels * height_col * width_col;
	cudaError_t	err;

	/* Launch */
	im2col_kernel <<< GET_BLOCKS( num_kernels ), CUDA_NUM_THREADS, 0, stream >>> (
		num_kernels, data_im, runing_mean, gamma_sign, height, width, ksize_h, ksize_w,
		pad_h, pad_w, stride_h, stride_w,
		dilation_h, dilation_w,
		height_col, width_col, data_col
		);

	err = cudaGetLastError();
	if ( cudaSuccess != err )
	{
		fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
		exit( -1 );
	}
}


void encode_cols_ongpu( int *columns, unsigned int* columns_binary, int m, int n, cudaStream_t stream )
{
	/* Encode cols */
	dim3		blockDim_ck( 32, 32, 1 );
	dim3		gridDim_ck( n / 32 + 1, m / 32 + 1, 1 );
	cudaError_t	err;

	encode_cols_kernel <<< gridDim_ck, blockDim_ck, 0, stream >>> (
		columns,
		columns_binary,
		m, n);

	err = cudaGetLastError();
	if ( cudaSuccess != err )
	{
		fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
		exit( -1 );
	}
}


void encode_rows_ongpu( float* input, unsigned int* output, int count, cudaStream_t stream )
{
	encode_rows_kernel <<< GET_BLOCKS( count ), CUDA_NUM_THREADS, 0, stream >>> (input, output, count);

	cudaError_t err;
	err = cudaGetLastError();
	if ( cudaSuccess != err )
	{
		fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
		exit( -1 );
	}
}


void decode_rows_ongpu( unsigned int* input, float* output, int count, cudaStream_t stream )
{
	decode_rows_kernel <<< GET_BLOCKS( count ), CUDA_NUM_THREADS, 0, stream >>> (
		input,
		output,
		count);

	cudaError_t err;
	err = cudaGetLastError();
	if ( cudaSuccess != err )
	{
		fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
		exit( -1 );
	}
}


void binary_gemm_ongpu( unsigned int* weight, unsigned int* columns_binary, float* output_n,
			int m, int n, int k, float *alphas, cudaStream_t stream )
{
	dim3		blockDim( BLOCK_SIZE, BLOCK_SIZE, 1 );
	dim3		gridDim( k / BLOCK_SIZE + 1, m / BLOCK_SIZE + 1 );
	cudaError_t	err;

	/* Do here the binary_gemm call - popcount(XOR) is called here */
	binary_gemm <<< gridDim, blockDim, 0, stream >>> (
		weight,
		columns_binary,
		output_n, m, n, k, alphas);

	err = cudaGetLastError();
	if ( cudaSuccess != err )
	{
		fprintf( stderr, "cudaCheckError() failed : %s\n", cudaGetErrorString( err ) );
		exit( -1 );
	}
}


#ifdef __cplusplus
}
#endif
