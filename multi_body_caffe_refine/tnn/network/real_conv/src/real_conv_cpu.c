#include <TH/TH.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "real_gemm.h"

/*
 * https://github.com/pytorch/pytorch/blob/master/torch/lib/THNN/generic/SpatialConvolutionMM.c
 * https://github.com/pytorch/pytorch/blob/master/torch/lib/TH/generic/THBlas.c#L287
 */
typedef float real;
/* typedef int ptrdiff_t; */

void unfolded_copy(
	THFloatTensor *finput,
	THFloatTensor *input,
	int kW,
	int kH,
	int dW,
	int dH,
	int padW,
	int padH,
	int nInputPlane,
	int inputWidth,
	int inputHeight,
	int outputWidth,
	int outputHeight );


void Vector_fill( real *x, const real c, const ptrdiff_t n );


void RealSpatialConvolutionMM_updateOutput_frame(
	THFloatTensor *input,
	THFloatTensor *output,
	THFloatTensor *weight,
	THFloatTensor *bias,
	THFloatTensor *finput,
	int kW,
	int kH,
	int dW,
	int dH,
	int padW,
	int padH,
	long nInputPlane,
	long inputWidth,
	long inputHeight,
	long nOutputPlane,
	long outputWidth,
	long outputHeight )
{
	long		i;
	THFloatTensor	*output2d;

	unfolded_copy( finput, input, kW, kH, dW, dH, padW, padH,
		       nInputPlane, inputWidth, inputHeight,
		       outputWidth, outputHeight );
//    printf("%ld, %ld\n", nOutputPlane, outputHeight*outputWidth);
//    printf("output: %ld, %ld\n", output->size[0], output->size[1]);
	output2d = THFloatTensor_newWithStorage2d( output->storage, output->storageOffset,
						   nOutputPlane, -1,
						   outputHeight * outputWidth, -1 );
	if ( bias )
	{
		for ( i = 0; i < nOutputPlane; i++ )
			Vector_fill( output->storage->data + output->storageOffset + output->stride[0] * i,
				     THFloatTensor_get1d( bias, i ), outputHeight * outputWidth );
	} else {
		THFloatTensor_zero( output );
	}

//    THFloatTensor_addmm(output2d, 1, output2d, 1, weight, finput);
	addmm( output2d, 1, output2d, 1, weight, finput );
	THFloatTensor_free( output2d );
}


void RealSpatialConvolutionMM_updateOutput(
	THFloatTensor *input,
	THFloatTensor *output,
	THFloatTensor *weight,
/*           THFloatTensor *bias, */
	THFloatTensor *finput,
	int kW, int kH,
	int dW, int dH,
	int padW, int padH )
{
	THFloatTensor *bias = NULL;

	int freeWeight = 0;

	if ( weight->nDimension == 4 )
	{
		long	s1	= weight->size[0];
		long	s2	= weight->size[1] * weight->size[2] * weight->size[3];
		weight = THFloatTensor_newWithStorage2d( weight->storage, weight->storageOffset,
							 s1, -1, s2, -1 );
		freeWeight = 1;
	}

	input = THFloatTensor_newContiguous( input );
	int	ndim	= input->nDimension;
	int	dimf	= 0;
	int	dimh	= 1;
	int	dimw	= 2;

	if ( ndim == 4 )
	{
		dimf++;
		dimh++;
		dimw++;
	}

	long	nInputPlane	= input->size[dimf];
	long	inputHeight	= input->size[dimh];
	long	inputWidth	= input->size[dimw];
	long	nOutputPlane	= weight->size[0];
	long	outputHeight	= (inputHeight + 2 * padH - kH) / dH + 1;
	long	outputWidth	= (inputWidth + 2 * padW - kW) / dW + 1;

	if ( input->nDimension == 3 )
	{
		THFloatTensor_resize2d( finput, kW * kH * nInputPlane, outputHeight * outputWidth );
		THFloatTensor_resize3d( output, nOutputPlane, outputHeight, outputWidth );

		RealSpatialConvolutionMM_updateOutput_frame
			( input, output, weight, bias, finput,
			kW, kH, dW, dH, padW, padH,
			nInputPlane, inputWidth, inputHeight,
			nOutputPlane, outputWidth, outputHeight );
	}else  {
		long	T = input->size[0];
		long	t;

		THFloatTensor_resize3d( finput, T, kW * kH * nInputPlane, outputHeight * outputWidth );
		THFloatTensor_resize4d( output, T, nOutputPlane, outputHeight, outputWidth );

#pragma omp parallel for private(t)
		for ( t = 0; t < T; t++ )
		{
			THFloatTensor	*input_t	= THFloatTensor_newSelect( input, 0, t );
			THFloatTensor	*output_t	= THFloatTensor_newSelect( output, 0, t );
			THFloatTensor	*finput_t	= THFloatTensor_newSelect( finput, 0, t );

			RealSpatialConvolutionMM_updateOutput_frame
				( input_t, output_t, weight, bias, finput_t,
				kW, kH, dW, dH, padW, padH,
				nInputPlane, inputWidth, inputHeight,
				nOutputPlane, outputWidth, outputHeight );

			THFloatTensor_free( input_t );
			THFloatTensor_free( output_t );
			THFloatTensor_free( finput_t );
		}
	}

	THFloatTensor_free( input );
	if ( freeWeight )
		THFloatTensor_free( weight );
}


void unfolded_copy(
	THFloatTensor *finput,
	THFloatTensor *input,
	int kW,
	int kH,
	int dW,
	int dH,
	int padW,
	int padH,
	int nInputPlane,
	int inputWidth,
	int inputHeight,
	int outputWidth,
	int outputHeight )
{
	long	k;
	real	*input_data	= THFloatTensor_data( input );
	real	*finput_data	= THFloatTensor_data( finput );

#pragma omp parallel for private(k)
	for ( k = 0; k < nInputPlane * kH * kW; k++ )
	{
		size_t		nip	= k / (kH * kW);
		size_t		rest	= k % (kH * kW);
		size_t		kh	= rest / kW;
		size_t		kw	= rest % kW;
		size_t		x, y;
		long long	ix, iy;
		real		*dst	= finput_data + nip * (kH * kW * outputHeight * outputWidth) + kh * (kW * outputHeight * outputWidth) + kw * (outputHeight * outputWidth);
		real		*src	= input_data + nip * (inputHeight * inputWidth);
		if ( padW > 0 || padH > 0 )
		{
			size_t lpad, rpad;
			for ( y = 0; y < outputHeight; y++ )
			{
				iy = (long long) (y * dH - padH + kh);
				if ( iy < 0 || iy >= inputHeight )
				{
					memset( dst + y * outputWidth, 0, sizeof(real) * outputWidth );
				} else {
					if ( dW == 1 )
					{
						ix	= (long long) (0 - padW + kw);
						lpad	= fmaxf( 0, (int) (padW - kw) );
						rpad	= fmaxf( 0, (int) (padW - (kW - kw - 1) ) );
						if ( outputWidth - rpad - lpad <= 0 )
						{
							memset( dst + (size_t) (y * outputWidth), 0, sizeof(real) * outputWidth );
						} else {
							if ( lpad > 0 )
								memset( dst + y * outputWidth, 0, sizeof(real) * lpad );
							memcpy( dst + (size_t) (y * outputWidth + lpad), src + (size_t) (iy * inputWidth + ix + lpad), sizeof(real) * (outputWidth - rpad - lpad) );
							if ( rpad > 0 )
								memset( dst + y * outputWidth + outputWidth - rpad, 0, sizeof(real) * rpad );
						}
					}else  {
						for ( x = 0; x < outputWidth; x++ )
						{
							ix = (long long) (x * dW - padW + kw);
							if ( ix < 0 || ix >= inputWidth )
								memset( dst + (size_t) (y * outputWidth + x), 0, sizeof(real) * 1 );
							else
								memcpy( dst + (size_t) (y * outputWidth + x), src + (size_t) (iy * inputWidth + ix), sizeof(real) * (1) );
						}
					}
				}
			}
		} else {
			for ( y = 0; y < outputHeight; y++ )
			{
				iy	= (long long) (y * dH + kh);
				ix	= (long long) (0 + kw);
				if ( dW == 1 )
					memcpy( dst + (size_t) (y * outputWidth), src + (size_t) (iy * inputWidth + ix), sizeof(real) * outputWidth );
				else{
					for ( x = 0; x < outputWidth; x++ )
						memcpy( dst + (size_t) (y * outputWidth + x), src + (size_t) (iy * inputWidth + ix + x * dW), sizeof(real) * (1) );
				}
			}
		}
	}
}


void Vector_fill( real *x, const real c, const ptrdiff_t n )
{
	ptrdiff_t i = 0;

	for (; i < n - 4; i += 4 )
	{
		x[i]		= c;
		x[i + 1]	= c;
		x[i + 2]	= c;
		x[i + 3]	= c;
	}

	for (; i < n; i++ )
		x[i] = c;
}