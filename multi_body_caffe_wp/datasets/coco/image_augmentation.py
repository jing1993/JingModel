import cv2
import numpy as np


def resize(im_orig, anns, mask_orig, orig_size, dest_size):
    if orig_size[0] < orig_size[1]:
        image = cv2.resize(im_orig, (orig_size[1] * dest_size[1] / orig_size[0], dest_size[0]))
        mask = cv2.resize(mask_orig, (orig_size[1] * dest_size[1] / orig_size[0], dest_size[0]))
        for i in range(len(anns)):
            anns_np = np.asarray(anns[i][2], dtype=int)
            anns_np = anns_np * dest_size[0] / orig_size[0]
            anns[i][2] = anns_np.tolist()
    else:
        image = cv2.resize(im_orig, (dest_size[1], orig_size[0] * dest_size[1] / orig_size[1]))
        mask = cv2.resize(mask_orig, (dest_size[1], orig_size[0] * dest_size[1] / orig_size[1]))
        for i in range(len(anns)):
            anns_np = np.asarray(anns[i][2], dtype=int)
            anns_np = anns_np * dest_size[1] / orig_size[1]
            anns[i][2] = anns_np.tolist()
    return image, anns, mask


def randomcrop(image, anns, mask):
    if image.shape[0] < image.shape[1]:
        rand_num = np.random.randint(image.shape[1] - image.shape[0])
        image = image[:, rand_num: rand_num + image.shape[0], :]
        mask = mask[:, rand_num: rand_num + mask.shape[0]]
        for i in range(len(anns)):
            anns_np = np.asarray(anns[i][2], dtype=int)
            for j in range(18):
                anns_np[j][0] -= rand_num
                anns[i][2][j][0] = anns_np[j][0]
                if anns_np[j][0] >= image.shape[0] or anns_np[j][0] < 0:
                    anns[i][1][j] = 0
    elif image.shape[0] > image.shape[1]:
        rand_num = np.random.randint(image.shape[0] - image.shape[1])
        image = image[rand_num: rand_num + image.shape[1], :, :]
        mask = mask[rand_num: rand_num + mask.shape[1], :]
        for i in range(len(anns)):
            anns_np = np.asarray(anns[i][2], dtype=int)
            for j in range(18):
                anns_np[j][1] -= rand_num
                anns[i][2][j][1] = anns_np[j][1]
                if anns_np[j][1] >= image.shape[1] or anns_np[j][1] < 0:
                    anns[i][1][j] = 0
    return image, anns, mask


def randomflip(image, anns, mask):
    if np.random.rand() > 0.5:
        image = cv2.flip(image, 1)
        mask = cv2.flip(mask, 1)
        for i in range(len(anns)):
            anns_vis_np = np.array(anns[i][1], dtype=int)
            anns_coor_np = np.asarray(anns[i][2], dtype=int)
            anns_vis_np = anns_vis_np[[0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15, 17]]
            anns_coor_np = anns_coor_np[[0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15, 17]]
            for j in range(18):
                anns_coor_np[j][0] = image.shape[0] - anns_coor_np[j][0] - 1
            anns[i][1] = anns_vis_np.tolist()
            anns[i][2] = anns_coor_np.tolist()

    return image, anns, mask


def randomrotate(image, anns, mask):
    angle = np.random.randint(-30, 31)
    angle_r = float(angle) * np.pi / 180
    mid_np = np.asarray([image.shape[1] / 2, image.shape[0] / 2])
    tranmax = np.asarray([[np.cos(angle_r), -np.sin(angle_r)], [np.sin(angle_r), np.cos(angle_r)]])
    M = cv2.getRotationMatrix2D((image.shape[1] / 2, image.shape[0] / 2), angle, 1)
    image = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
    mask = cv2.warpAffine(mask, M, (image.shape[1], image.shape[0]))
    for i in range(len(anns)):
        anns_np = np.asarray(anns[i][2])
        anns_np = np.matmul(anns_np - mid_np, tranmax) + mid_np
        anns[i][2] = anns_np.astype(int).tolist()
    return image, anns, mask


def randomcolor(image):
    rand_color = np.random.uniform(0.9, 1.1, 3)
    image = image * rand_color
    return image


def feat(mask, feat_stride):
    mask = cv2.resize(mask, (mask.shape[1] / feat_stride, mask.shape[0] / feat_stride))
    return mask


def img_trans(image, anns, mask, orig_size, dest_size, feat_stride):
    for i in range(len(anns)):
        anns[i][1].append(anns[i][1][5] * anns[i][1][6])
        anns[i][2].append(((np.asarray(anns[i][2][5]) + np.asarray(anns[i][2][6])) / 2).tolist())
        anns[i][0] += 1 if anns[i][1][-1] != 0 else 0
    image, anns, mask = randomrotate(image, anns, mask)
    image, anns, mask = resize(image, anns, mask, orig_size, dest_size)
    # image, annotations = rescale(image, annotations, scale)
    image, anns, mask = randomcrop(image, anns, mask)
    image, anns, mask = randomflip(image, anns, mask)

    image = randomcolor(image)
    mask = feat(mask, feat_stride)
    return image, anns, mask
