import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader, TimeSeqDataset

import utils.depth as depth_utils


class NYUDepthLabeledDIW(TimeSeqDataset):
    def __init__(self, root, pts_h5, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, thresh=0.1, training=True, random_seed=None):
        self.root = root
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training
        self.max_pts = max_pts
        self.thresh = thresh

        self.im_files = [fname for fname in os.listdir(self.root) if os.path.splitext(fname)[-1] == '.png']
        self.im_files = sorted(self.im_files, key=lambda name: int(os.path.splitext(name)[0]))

        pts_h5f = h5py.File(pts_h5, 'r')
        pts = np.asarray(pts_h5f['data'], dtype=np.int64)
        self.pts = pts.reshape(-1, 5, pts.shape[-1]).transpose(0, 2, 1)
        pts_h5f.close()

        self.random_seed = random_seed

    def __len__(self):
        return len(self.im_files)

    def seq_len(self, i):
        return 1

    def get_item(self, vid, fid, clear):
        im_data, depth_data, mask_data, pts, num_pts = self.__getitem__(vid)

        return im_data, depth_data, mask_data, pts, num_pts, clear

    def __getitem__(self, i):
        im_name = os.path.join(self.root, self.im_files[i])
        depth_name = im_name[:-4] + '_depth.h5'

        dh5 = h5py.File(depth_name, 'r')
        depth = np.asarray(dh5['depth'])
        dh5.close()

        im = cv2.imread(im_name)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        # im = cv2.resize(im, (320, 240))
        # depth = cv2.resize(depth, (320, 240))
        # mask = cv2.resize(mask, (320, 240))

        crop = 10
        fx = 0.5
        # im = im[crop:-crop, crop:-crop, :]
        im = cv2.resize(im, (320, 240))
        depth = cv2.resize(depth, (320, 240))
        mask = cv2.resize(mask, (320, 240))

        # fx = 0.50
        # im = cv2.resize(im, None, fx=fx, fy=fx)
        # depth = cv2.resize(depth, None, fx=fx, fy=fx)
        # mask = cv2.resize(mask, None, fx=fx, fy=fx)

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=15, max_scale=1.5)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, noise_scale=0.015)
            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)
            depth = depth / scale
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))

        # normalize depth
        depth_t = depth[mask > 0]
        dmin, dmax = depth_t.min(), depth_t.max()
        depth = (depth - dmin) / (dmax - dmin + 1e-5)
        depth_data = depth.astype(np.float32)

        mask2 = np.zeros_like(depth_data, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        sampled_pts = self.pts[i]
        num_pts = 0
        for pt in sampled_pts:
            if np.sum(pt) == 0:
                break
            num_pts += 1
        sampled_pts = sampled_pts[:num_pts]
        sampled_pts = np.vstack((sampled_pts[:, 1], sampled_pts[:, 0], sampled_pts[:, 3], sampled_pts[:, 2], sampled_pts[:, 4])).transpose()
        sampled_pts[:, 0:4] = (sampled_pts[:, 0:4] - 1)

        pts = np.zeros([self.max_pts, 5], dtype=np.int64)
        pts[:num_pts, 0:5] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)

        return im_data, depth_data, mask_data, pts, num_pts


def get_loader(root, pts_h5, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, thresh=0.1, training=True,
               batch_size=128, shuffle=True, num_workers=3, random_seed=None):
    dataset = NYUDepthLabeledDIW(root, pts_h5, inp_size, feat_stride, out_size, max_pts, thresh, training, random_seed)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/media/longc/LData/data/nyu_depth_v2',
                                'train_data.h5', inp_size=(320, 240), feat_stride=1,
                                training=True, batch_size=2)
        for i, batch in enumerate(train_data):
            im_data, depth_data, mask_data = batch
            print im_data.size()
            print depth_data.size()
            print mask_data.size()

            cv2.imshow('image', np.transpose(im_data[0].numpy(), [1, 2, 0]))
            cv2.imshow('depth', depth_data[0].numpy() / torch.max(depth_data[0]))
            cv2.imshow('mask', mask_data[0].numpy())
            cv2.waitKey(0)

            # assert False
    main()
