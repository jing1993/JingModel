import cPickle
import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class NYUDepth(data.Dataset):
    def __init__(self, root, h5name, inp_size=256, out_size=None, feat_stride=4, training=True):
        self.root = root
        self.h5name = h5name
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        # self.out_size = self.inp_size // self.feat_stride
        self.out_size = (148, 110) if out_size is None else out_size

        self.training = training

        h5f = h5py.File(os.path.join(self.root, self.h5name), 'r')
        rgb_data = h5f['imgRgbs']

        self.cache_name = os.path.join(self.root, '{}_cache.pk'.format(h5name))
        self.inds = self.load_inds(rgb_data, re_load=True)
        h5f.close()

    def load_inds(self, datasets, re_load=True):
        if not re_load and os.path.isfile(self.cache_name):
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        inds = []
        for k, dataset in datasets.items():
            inds.extend([(k, i) for i in range(len(dataset))])

        with open(self.cache_name, 'w') as f:
            cPickle.dump(inds, f)
        return inds

    def __len__(self):
        return len(self.inds)

    def __getitem__(self, i):
        h5f = h5py.File(os.path.join(self.root, self.h5name), 'r')
        rgb_data = h5f['imgRgbs']
        gts = h5f['imgDepthProjs']

        k, ind = self.inds[i]
        im = rgb_data[k][ind]
        depth = gts[k][ind]
        h5f.close()

        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        im = cv2.resize(im, (320, 240))
        depth = cv2.resize(depth, (320, 240))
        mask = cv2.resize(mask, (320, 240))
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               max_scale=1.3, rotate=True)
            # print self.inp_size, im.shape
            scale = trans_params[0]
            # im = im_transform.imcv2_recolor(im, a=0.1)

            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)
            depth = depth / scale
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]
        im = im.astype(np.float32) - np.array((123.68, 116.779, 103.939), dtype=np.float32)

        # im_data = cv2.resize(im, tuple(self.inp_size))
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32)

        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        depth_data = depth.astype(np.float32)

        mask2 = np.zeros_like(depth_data, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)
        return im_data, depth_data, mask_data


def collate_fn(data):
    im_data, depth_data, mask_data = zip(*data)
    im_data = torch.stack(im_data, 0)
    depth_data = torch.stack(depth_data, 0)
    mask_data = torch.stack(mask_data, 0)

    return im_data, depth_data, mask_data


def get_loader(root, h5name, inp_size=256, out_size=None, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = NYUDepth(root, h5name, inp_size, out_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        from utils.depth import plot_result
        train_data = get_loader('/media/longc/LData/data/nyu_depth_v2',
                                'train_data.h5', inp_size=(320, 240), feat_stride=1,
                                training=False, batch_size=2)
        for i, batch in enumerate(train_data):
            im_data, depth_data, mask_data = batch
            print im_data.size()
            print depth_data.size()
            print mask_data.size()
            plot_result(np.transpose(im_data[0].numpy(), [1, 2, 0]),mask_data[0].numpy(), depth_data[0].numpy())

            # cv2.imshow('image', np.transpose(im_data[0].numpy(), [1, 2, 0]))
            # cv2.imshow('depth', depth_data[0].numpy() / torch.max(depth_data[0]))
            # cv2.imshow('mask', mask_data[0].numpy())
            # cv2.waitKey(0)

            # assert False
    main()
