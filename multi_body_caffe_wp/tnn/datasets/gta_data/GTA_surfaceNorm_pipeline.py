# coding=utf-8
import os
import cv2
import numpy as np
import torchvision.transforms as transforms
from torchvision.transforms import ToTensor

import torch
from ImageAugmentation_surfaceNorm import ImageAugmentation
from torch.utils.data import DataLoader, Dataset
from preprocessing import vgg_preprocess

params_transform = dict()

# === aug_scale ===
params_transform['scale_min'] = 0.5
params_transform['scale_max'] = 1.1
params_transform['target_dist'] = 0.6
# === aug_rotate ===
params_transform['max_rotate_degree'] = 40

# === aug_croppad ===
params_transform['center_perterb_max'] = 40

# === aug_flip ===
params_transform['flip_prob'] = 0.5

class GTAsurfaceNorms(Dataset):
    def __init__(self, inp_size, feat_stride, root, index_list, data, transform=None,
                 target_transform=None):

        
        print 'Initializing GTA data from {}...'.format(root)
        self.data = data
        self.numSample = len(index_list)
        self.index_list = index_list
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.inp_size = inp_size
        self.feat_stride = feat_stride
        params_transform['crop_size_x'] = inp_size
        params_transform['crop_size_y'] = inp_size
        print 'len: {}'.format(self.numSample)

    def __getitem__(self, index):
        idx = self.index_list[index]
        img = cv2.imread(os.path.join(self.root, 'origin', self.data[idx]))
        img_idx = self.data[idx][:-11]
        
        sn = cv2.imread(os.path.join(self.root, 'surfaceNorm', img_idx+'_surfaceNorm.png'))
        mask = np.ones([sn.shape[0], sn.shape[1]]) * 255
        mask[np.sum(sn, 2) == 0] = 0
        if img.shape[:2] != mask.shape:
            print 'size wrong for {}'.format(self.root + img_idx)
            print mask.shape, img.shape
        img, sn, mask = ImageAugmentation(img, sn, mask, params_transform)
        sn = cv2.resize(sn, (self.inp_size/self.feat_stride, self.inp_size/self.feat_stride))
        mask = cv2.resize(mask, (self.inp_size/self.feat_stride, self.inp_size/self.feat_stride))

        img = img.astype(np.float32)/255.
        img = torch.from_numpy(vgg_preprocess(img))


        sn = sn.astype(np.float32)/127.5 - 1
        sn = np.transpose(sn, (2, 0, 1))

        mask = mask.astype(np.float32)/255.
        mask = np.expand_dims(mask, 0)

        return img, sn, mask

    def __len__(self):
        return self.numSample
