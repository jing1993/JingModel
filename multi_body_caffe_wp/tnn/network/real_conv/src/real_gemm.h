#ifndef __real_gemm_
#define __real_gemm_

#include <TH/TH.h>
#include <stdio.h>

//#include <cblas.h>

#include "gemmkernel_2x2.h"
#include "gemmkernel_4x4.h"

typedef float real;


void gemm( char transa, char transb, long m, long n, long k, real alpha, real *a, long lda, real *b, long ldb, real beta, real *c, long ldc )
{
	int	transa_ = ( (transa == 't') || (transa == 'T') );       /* n */
	int	transb_ = ( (transb == 't') || (transb == 'T') );       /* n */

	if ( n == 1 )
		ldc = m;

	if ( transa_ )
	{
		if ( m == 1 )
			lda = k;
	}else  {
		if ( k == 1 )
			lda = m;
	}

	if ( transb_ )
	{
		if ( k == 1 )
			ldb = n;
	}else  {
		if ( n == 1 )
			ldb = k;
	}


	long i, j, l;
	if ( !transa_ && !transb_ )
	{
		/* this one */
//		printf( "lda:%ld, ldb:%ld, ldc:%ld\n", lda, ldb, ldc);
//		printf( "m:%ld, n:%ld, k:%ld\n", m, n, k);
		real *a_ = a;
		for ( i = 0; i < m; i++ )
		{
			real *b_ = b;
			for ( j = 0; j < n; j++ )
			{
				real sum = 0;
				for ( l = 0; l < k; l++ )
					sum += a_[l * lda] * b_[l];
				b_ += ldb;
				if ( beta == 0 )
					c[j * ldc + i] = alpha * sum;
				else
					c[j * ldc + i] = beta * c[j * ldc + i] + alpha * sum;
			}
			a_++;
		}
	}else if ( transa_ && !transb_ )
	{
		real *a_ = a;
		for ( i = 0; i < m; i++ )
		{
			real *b_ = b;
			for ( j = 0; j < n; j++ )
			{
				real sum = 0;
				for ( l = 0; l < k; l++ )
					sum += a_[l] * b_[l];
				b_ += ldb;
				if ( beta == 0 )
					c[j * ldc + i] = alpha * sum;
				else
					c[j * ldc + i] = beta * c[j * ldc + i] + alpha * sum;
			}
			a_ += lda;
		}
	}else if ( !transa_ && transb_ )
	{
		real *a_ = a;
		for ( i = 0; i < m; i++ )
		{
			real *b_ = b;
			for ( j = 0; j < n; j++ )
			{
				real sum = 0;
				for ( l = 0; l < k; l++ )
					sum += a_[l * lda] * b_[l * ldb];
				b_++;
				if ( beta == 0 )
					c[j * ldc + i] = alpha * sum;
				else
					c[j * ldc + i] = beta * c[j * ldc + i] + alpha * sum;
			}
			a_++;
		}
	}else  {
		real *a_ = a;
		for ( i = 0; i < m; i++ )
		{
			real *b_ = b;
			for ( j = 0; j < n; j++ )
			{
				real sum = 0;
				for ( l = 0; l < k; l++ )
					sum += a_[l] * b_[l * ldb];
				b_++;
				if ( beta == 0 )
					c[j * ldc + i] = alpha * sum;
				else
					c[j * ldc + i] = beta * c[j * ldc + i] + alpha * sum;
			}
			a_ += lda;
		}
	}
}


void addmm( THFloatTensor *r_, real beta, THFloatTensor *t, real alpha, THFloatTensor *m1, THFloatTensor *m2 )
{
	char		transpose_r = 't', transpose_m1 = 'n', transpose_m2 = 'n';
	THFloatTensor	*r__, *m1_, *m2_;

	if ( (m1->nDimension != 2) || (m2->nDimension != 2) )
		THError( "matrices expected, got %dD, %dD tensors", m1->nDimension, m2->nDimension );

	if ( m1->size[1] != m2->size[0] )
	{
		THDescBuff	bm1	= THFloatTensor_sizeDesc( m1 );
		THDescBuff	bm2	= THFloatTensor_sizeDesc( m2 );
		THError( "size mismatch, m1: %s, m2: %s", bm1.str, bm2.str );
	}

	if ( t->nDimension != 2 )
		THError( "matrix expected, got %dD tensor for t", t->nDimension );

	if ( (t->size[0] != m1->size[0]) || (t->size[1] != m2->size[1]) )
	{
		THDescBuff	bt	= THFloatTensor_sizeDesc( t );
		THDescBuff	bm1	= THFloatTensor_sizeDesc( m1 );
		THDescBuff	bm2	= THFloatTensor_sizeDesc( m2 );
		THError( "size mismatch, t: %s, m1: %s, m2: %s", bt.str, bm1.str, bm2.str );
	}

	if ( t != r_ )
	{
		THFloatTensor_resizeAs( r_, t );
		THFloatTensor_copy( r_, t );
	}

	/* r_ */
	if ( r_->stride[0] == 1 &&
	     r_->stride[1] != 0 )
	{
		transpose_r	= 'n';
		r__		= r_;
	}else if ( r_->stride[1] == 1 &&
		   r_->stride[0] != 0 )
	{
		THFloatTensor *swap = m2;
		m2		= m1;
		m1		= swap;
		transpose_r	= 't';
		r__		= r_;
	}else  {
		transpose_r = 'n';

		THFloatTensor *transp_r_ = THFloatTensor_newTranspose( r_, 0, 1 );
		r__ = THFloatTensor_newClone( transp_r_ );
		THFloatTensor_free( transp_r_ );
		THFloatTensor_transpose( r__, NULL, 0, 1 );
	}

	/* m1 */
	if ( m1->stride[(transpose_r == 'n' ? 0 : 1)] == 1 &&
	     m1->stride[(transpose_r == 'n' ? 1 : 0)] != 0 )
	{
		transpose_m1	= 'n';
		m1_		= m1;
	}else if ( m1->stride[(transpose_r == 'n' ? 1 : 0)] == 1 &&
		   m1->stride[(transpose_r == 'n' ? 0 : 1)] != 0 )
	{
		transpose_m1	= 't';
		m1_		= m1;
	}else  {
		transpose_m1	= (transpose_r == 'n' ? 't' : 'n');
		m1_		= THFloatTensor_newContiguous( m1 );
	}

	/* m2 */
	if ( m2->stride[(transpose_r == 'n' ? 0 : 1)] == 1 &&
	     m2->stride[(transpose_r == 'n' ? 1 : 0)] != 0 )
	{
		transpose_m2	= 'n';
		m2_		= m2;
	}else if ( m2->stride[(transpose_r == 'n' ? 1 : 0)] == 1 &&
		   m2->stride[(transpose_r == 'n' ? 0 : 1)] != 0 )
	{
		transpose_m2	= 't';
		m2_		= m2;
	}else  {
		transpose_m2	= (transpose_r == 'n' ? 't' : 'n');
		m2_		= THFloatTensor_newContiguous( m2 );
	}

	/* do the operation */
//	printf( "m1:%c, m2:%c, r:%c\n", transpose_m1, transpose_m2, transpose_r );
//	printf( "m:%ld, n:%ld, k:%ld\n", r__->size[(transpose_r == 'n' ? 0 : 1)], r__->size[(transpose_r == 'n' ? 1 : 0)], m1_->size[(transpose_r == 'n' ? 1 : 0)] );
//    printf( "lda:%ld, ldb:%ld, ldc:%ld\n", (transpose_m1 == 'n' ? m1_->stride[(transpose_r == 'n' ? 1 : 0)] : m1_->stride[(transpose_r == 'n' ? 0 : 1)]),
//                                (transpose_m2 == 'n' ? m2_->stride[(transpose_r == 'n' ? 1 : 0)] : m2_->stride[(transpose_r == 'n' ? 0 : 1)]),
//                                r__->stride[(transpose_r == 'n' ? 1 : 0)] );

//	cblas_sgemm(CblasColMajor,
//				CblasNoTrans,
//				CblasNoTrans,
//				r__->size[(transpose_r == 'n' ? 0 : 1)],
//				r__->size[(transpose_r == 'n' ? 1 : 0)],
//				m1_->size[(transpose_r == 'n' ? 1 : 0)],
//				alpha,
//				THFloatTensor_data( m1_ ),
//				(transpose_m1 == 'n' ? m1_->stride[(transpose_r == 'n' ? 1 : 0)] : m1_->stride[(transpose_r == 'n' ? 0 : 1)]),
//				THFloatTensor_data( m2_ ),
//				(transpose_m2 == 'n' ? m2_->stride[(transpose_r == 'n' ? 1 : 0)] : m2_->stride[(transpose_r == 'n' ? 0 : 1)]),
//				beta,
//				THFloatTensor_data( r__ ),
//				r__->stride[(transpose_r == 'n' ? 1 : 0)]
//	);

	gemm_blas_4x4(
	    		r__->size[(transpose_r == 'n' ? 0 : 1)],
				r__->size[(transpose_r == 'n' ? 1 : 0)],
				m1_->size[(transpose_r == 'n' ? 1 : 0)],
				alpha,
				THFloatTensor_data( m1_ ),
				(transpose_m1 == 'n' ? m1_->stride[(transpose_r == 'n' ? 1 : 0)] : m1_->stride[(transpose_r == 'n' ? 0 : 1)]),
				THFloatTensor_data( m2_ ),
				(transpose_m2 == 'n' ? m2_->stride[(transpose_r == 'n' ? 1 : 0)] : m2_->stride[(transpose_r == 'n' ? 0 : 1)]),
				THFloatTensor_data( r__ ),
				r__->stride[(transpose_r == 'n' ? 1 : 0)]
	);

//	gemm( transpose_m1,
//	      transpose_m2,
//	      r__->size[(transpose_r == 'n' ? 0 : 1)],
//	      r__->size[(transpose_r == 'n' ? 1 : 0)],
//	      m1_->size[(transpose_r == 'n' ? 1 : 0)],
//	      alpha,
//	      THFloatTensor_data( m1_ ),
//	      (transpose_m1 == 'n' ? m1_->stride[(transpose_r == 'n' ? 1 : 0)] : m1_->stride[(transpose_r == 'n' ? 0 : 1)]),
//	      THFloatTensor_data( m2_ ),
//	      (transpose_m2 == 'n' ? m2_->stride[(transpose_r == 'n' ? 1 : 0)] : m2_->stride[(transpose_r == 'n' ? 0 : 1)]),
//	      beta,
//	      THFloatTensor_data( r__ ),
//	      r__->stride[(transpose_r == 'n' ? 1 : 0)] );

	/* free intermediate variables */
	if ( m1_ != m1 )
		THFloatTensor_free( m1_ );

	if ( m2_ != m2 )
		THFloatTensor_free( m2_ );

	if ( r__ != r_ )
		THFloatTensor_freeCopyTo( r__, r_ );
}


#endif