import numpy as np
import cv2
import tnn.utils.bbox as bbox_utils


def getTransform(center, src_size, dst_size):
    # h = 200 * scale
    w, h = src_size
    dst_w, dst_h = dst_size

    t = np.eye(3)

    # scale
    t[0, 0] = float(dst_w) / w
    t[1, 1] = float(dst_h) / h

    # translate
    t[0, 2] = dst_w * (-float(center[0]) / w + 0.5)
    t[1, 2] = dst_h * (-float(center[1]) / h + 0.5)

    return t


def transform(pts, center, src_size, dst_size, invert):
    """Transform the coordinates from the original image space to the cropped one"""
    pts = np.asarray(pts, dtype=np.float)
    expanded = False
    if pts.ndim == 1:
        expanded = True
        pts = np.expand_dims(pts, 0)

    pt_new = np.ones([len(pts), 3], dtype=np.float)
    pt_new[:, 0] = pts[:, 0]
    pt_new[:, 1] = pts[:, 1]

    t = getTransform(center, src_size, dst_size)
    if invert:
        t = np.linalg.inv(t)
    t = t.transpose()

    new_point = np.matmul(pt_new, t).astype(np.int)[:, 0:2]
    if expanded:
        new_point = new_point[0]
    return new_point


def crop(img, center, src_size, dst_size, min_size=1):
    """Crop based on the image center & scale, img: RGB uint8, center: (cx, cy)"""
    # to match the origin lua code, we set index from 1
    # l1 = transform((1, 1), center, scale, res, invert=True)
    # l2 = transform((res, res), center, scale, res, invert=True)

    if src_size[0] <= 0 or src_size[1] <= 0 or dst_size[0] <= 0 or dst_size[1] <= 0:
        return None

    pts = transform([[0, 0], dst_size], center, src_size, dst_size, invert=True)
    l1, l2 = bbox_utils.int_box(pts)
    crop_h, crop_w = l2[1] - l1[1] + 1, l2[0] - l1[0] + 1

    if crop_h < min_size or crop_w < min_size:
        # print crop_h, crop_w
        # print center, src_size, dst_size, pts
        return None

    if img.ndim == 2:
        newImg = np.zeros((crop_h, crop_w), dtype=np.uint8)
    else:
        newImg = np.zeros((crop_h, crop_w, img.shape[2]), dtype=np.uint8)
    height, width = img.shape[:2]

    oldX = (max(0, l1[0]), min(l2[0] + 1, width))
    oldY = (max(0, l1[1]), min(l2[1] + 1, height))
    if oldX[0] < oldX[1] and oldY[0] < oldY[1]:
        newX = (max(0, -l1[0]), min(l2[0] + 1, width) - l1[0])
        newY = (max(0, -l1[1]), min(l2[1] + 1, height) - l1[1])
        newImg[newY[0]:newY[1], newX[0]:newX[1]] = img[oldY[0]:oldY[1], oldX[0]:oldX[1]]

    newImg = cv2.resize(newImg, tuple(dst_size))
    return newImg


def getPreds_ht(heatmaps):
    if heatmaps.ndim == 3:
        heatmaps = np.expand_dims(heatmaps, 0)

    bsize, c, height, width = heatmaps.shape

    # Get locations of maximum activations
    ht_reshaped = np.reshape(heatmaps, [bsize, c, height * width])
    idx = np.argmax(ht_reshaped, axis=2)
    scores = np.max(ht_reshaped, axis=2)
    preds = np.tile(np.expand_dims(idx, 2), (1, 1, 2)).astype(np.float)
    preds[:, :, 0] = preds[:, :, 0].astype(np.int) % width  # x
    preds[:, :, 1] = preds[:, :, 1] // width    # y

    for i in range(bsize):
        for j in range(c):
            hm = heatmaps[i, j]
            pX, pY = preds[i, j].astype(np.int)
            if 0 < pX < hm.shape[1]-1 and 0 < pY < hm.shape[0]-1:
                diff = np.array([hm[pY, pX+1] - hm[pY, pX-1], hm[pY+1, pX] - hm[pY-1, pX]], dtype=np.float)
                preds[i, j] += np.sign(diff) * 0.25

    # preds -= 0.5
    # preds += 1  # lua index start from 1
    return preds, scores


def getPreds(heatmaps, centers, src_sizes):
    centers = np.asarray(centers, dtype=np.float)
    if centers.ndim == 1:
        centers = np.expand_dims(centers, 0)
    src_sizes = np.asarray(src_sizes, dtype=np.float)
    if src_sizes.ndim == 1:
        src_sizes = np.expand_dims(src_sizes, 0)

    if heatmaps.ndim == 3:
        heatmaps = np.expand_dims(heatmaps, 0)

    bsize, c, height, width = heatmaps.shape

    # Get locations of maximum activations
    preds = getPreds_ht(heatmaps)

    # Get the coordinates in the original space
    preds_orig = np.zeros_like(preds, dtype=np.float)
    for i in range(bsize):
        preds_orig[i, :] = transform(preds[i], centers[i], src_sizes[i], [width, height], invert=True) # -1

    return preds, preds_orig


def plot(image, points):
    matched_parts = (
        (1, 2), (2, 3), (3, 7),
        (4, 5), (5, 6), (4, 7),
        (9, 10), (7, 8),
        (11, 12), (12, 13), (13, 8),
        (8, 14), (14, 15), (15, 16)
    )
    points = points.astype(np.int).reshape([16, 2])
    points_tup = [(p[0], p[1]) for p in points]

    image2show = np.copy(image)
    for i, (p1, p2) in enumerate(matched_parts):
        cv2.line(image2show, points_tup[p1-1], points_tup[p2-1], color=(255, 0, 0), thickness=2)

    return image2show


def eval(predicts, gts, visible, head_sizes, thresh=0.5):
    dist = np.linalg.norm(predicts - gts, ord=2, axis=2) / head_sizes.reshape(-1, 1)
    dist *= visible

    correct = dist <= thresh
    correct_cnt = (np.sum(correct, axis=0) - np.sum(visible < 0.5, axis=0))
    sample_cnt = np.sum(visible > 0.5, axis=0)

    return correct_cnt, sample_cnt


def calc_dists(predicts, gts, head_sizes):
    dists = np.linalg.norm(predicts - gts, ord=2, axis=2) / head_sizes.reshape(-1, 1)
    return dists


def eval_face(dists, fname=None):
    errors = np.mean(dists, axis=0).reshape(-1)

    axes1 = np.linspace(0, 1, 1000)
    axes2 = np.zeros(1000)
    print(errors.shape)
    for i in range(1000):
        axes2[i] = (errors < axes1[i]).sum() / float(errors.shape[0])
    print('AUC: ', np.sum(axes2[:70]) / 70)

    if fname is not None:
        import matplotlib.pyplot as plt
        plt.xlim(0, 7)
        plt.ylim(0, 100)
        plt.yticks(np.arange(0, 110, 10))
        plt.xticks(np.arange(0, 8, 1))
        plt.grid()
        plt.title('NME (%)', fontsize=20)
        plt.xlabel('NME (%)', fontsize=16)
        plt.ylabel('Test Images (%)', fontsize=16)
        plt.plot(axes1 * 100, axes2 * 100, 'b-', label='FAN (Ours)', lw=3)
        plt.legend(loc=4, fontsize=16)

        plt.savefig(fname)


def plot_face(image, preds, scores=None, thr=0):
    im2show = np.copy(image)
    preds = bbox_utils.int_box(preds)
    if scores is None:
        scores = np.ones(len(preds), dtype=np.float)

    def plot_sub(sub_preds, sub_scores):
        prev_pt = None
        for i, pt in enumerate(sub_preds):
            pt = tuple(pt)
            s = sub_scores[i]
            if s >= thr:
                cv2.circle(im2show, pt, 2, color=(255, 0, 0), thickness=2)
                if prev_pt is not None:
                    cv2.line(im2show, prev_pt, pt, color=(255, 255, 255), thickness=1)
                prev_pt = pt
            else:
                prev_pt = None

    plot_sub(preds[0:17], scores[0:17])
    plot_sub(preds[17:22], scores[17:22])
    plot_sub(preds[22:27], scores[22:27])
    plot_sub(preds[27:31], scores[27:31])
    plot_sub(preds[27:31], scores[27:31])
    plot_sub(preds[36:42], scores[36:42])
    plot_sub(preds[42:48], scores[42:48])
    plot_sub(preds[48:60], scores[48:60])
    plot_sub(preds[60:68], scores[60:68])

    return im2show

    # import matplotlib.pyplot as plt
    # plt.imshow(image)
    # plt.plot(preds[0:17, 0], preds[0:17, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[17:22, 0], preds[17:22, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[22:27, 0], preds[22:27, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[27:31, 0], preds[27:31, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[31:36, 0], preds[31:36, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[36:42, 0], preds[36:42, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[42:48, 0], preds[42:48, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[48:60, 0], preds[48:60, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.plot(preds[60:68, 0], preds[60:68, 1], marker='o', markersize=6, linestyle='-', color='w', lw=2)
    # plt.show()


def plt_to_numpy():
    pass