"""CPM Pytorch Implementation"""

import torch
import torch.nn as nn
from torch.nn import init
import torch.nn.functional as F
from torch.autograd import Variable

import torch.utils.model_zoo as model_zoo
from collections import OrderedDict
import numpy as np
from torchvision import models
from tnn.network import net_utils
from tnn.network.base_model import BaseModel


def batch_processor(state, batch):

	gpus = state.params.gpus
	im_data, heatmaps, heat_mask, pafs, paf_mask, seg_data, seg_mask = batch
	volatile = not state.model.training
	im_var = Variable(im_data, volatile=volatile).cuda(device_id=gpus[0])
	heatmaps_var = Variable(heatmaps).cuda(device_id=gpus[0])
	heat_mask_var = Variable(heat_mask).cuda(device_id=gpus[0])
	pafs_var = Variable(pafs).cuda(device_id=gpus[0])
	paf_mask_var = Variable(paf_mask).cuda(device_id=gpus[0])
	seg_var = Variable(seg_data).cuda(device_id=gpus[0])
	seg_mask_var = Variable(seg_mask).cuda(device_id=gpus[0])
	

	gts = (heatmaps_var, heat_mask_var, pafs_var, paf_mask_var, seg_var, seg_mask_var)
	inputs = (im_var, gts[1:])
	saved_for_eval = None

	return inputs, gts, saved_for_eval


def get_training_params():
	from tnn.network.trainer import Trainer
	extractor = 'inception_v3'
	n_stages = 5

	params = Trainer.TrainParams()
	params.exp_name = 'exp_caffe_v1'

	params.ckpt = None
	params.zero_epoch = False

	params.max_epoch = 140
	params.init_lr = 0.00003
	params.use_auto_lr = True
	# params.lr_decay = 0.3
	# params.lr_decay_epoch = {5, 10, 26, 40, 60, 120}

	params.gpus = [0, 1, 2, 3]
	params.batch_size = 20 * len(params.gpus)
	params.val_nbatch = 2
	params.val_nbatch_epoch = 100

	model = Model(stages=n_stages, have_bn= False, have_bias = True)
	#freeze the weights
	"""
	for name, param in model.named_parameters(): #state_dict().iteritems():
		if 'feature_extractor' in name or 'stage_0' in name or 'PAF' in name or 'heatmap' in name:
			param.requires_grad = False
	"""

	global sigmas
	trainable_vars = []
	feat_vars = []
	final_var = []
	for name, var in model.named_parameters():
		if not var.requires_grad:
			continue
		if 'feature_extractor' in name:
			feat_vars.append(var)
		elif 'upconv_final' in name or 'fc' in name:
			final_var.append(var)
		else:
			trainable_vars.append(var)
	params.optimizer = torch.optim.RMSprop([
		{'params': feat_vars, 'lr': params.init_lr},
		{'params': final_var, 'weight_decay': 0},
		{'params': trainable_vars}
	], lr=params.init_lr, weight_decay=1e-5)

	return params, model, batch_processor

class BasicConv2d(nn.Module):

	def __init__(self, in_channels, out_channels, bn, **kwargs):
		super(BasicConv2d, self).__init__()
		self.bn = bn
		self.conv = nn.Conv2d(in_channels, out_channels, **kwargs)
		if self.bn:
			self.bn = nn.BatchNorm2d(out_channels, eps=0.001)

	def forward(self, x):
		x = self.conv(x)
		if self.bn:
			x = self.bn(x)
		return F.relu(x, inplace=True)


class InceptionA(nn.Module):

	def __init__(self, in_channels, pool_features, have_bn, have_bias):
		super(InceptionA, self).__init__()
		self.branch1x1 = BasicConv2d(in_channels, 64, kernel_size=1, bn = have_bn, bias = have_bias)

		self.branch5x5_1 = BasicConv2d(in_channels, 48, kernel_size=1,bn = have_bn,  bias = have_bias)
		self.branch5x5_2 = BasicConv2d(48, 64, kernel_size=5, padding=2, bn = have_bn, bias = have_bias)

		self.branch3x3dbl_1 = BasicConv2d(in_channels, 64, kernel_size=1, bn = have_bn, bias = have_bias)
		self.branch3x3dbl_2 = BasicConv2d(64, 96, kernel_size=3, padding=1, bn = have_bn, bias = have_bias)
		self.branch3x3dbl_3 = BasicConv2d(96, 96, kernel_size=3, padding=1, bn = have_bn, bias = have_bias)

		self.branch_pool = BasicConv2d(
			in_channels, pool_features, kernel_size=1, bn = have_bn, bias = have_bias)

	def forward(self, x):
		branch1x1 = self.branch1x1(x)

		branch5x5 = self.branch5x5_1(x)
		branch5x5 = self.branch5x5_2(branch5x5)

		branch3x3dbl = self.branch3x3dbl_1(x)
		branch3x3dbl = self.branch3x3dbl_2(branch3x3dbl)
		branch3x3dbl = self.branch3x3dbl_3(branch3x3dbl)

		branch_pool = F.avg_pool2d(x, kernel_size=3, stride=1, padding=1)
		branch_pool = self.branch_pool(branch_pool)

		outputs = [branch1x1, branch5x5, branch3x3dbl, branch_pool]
		return torch.cat(outputs, 1)


class dilation_layer(nn.Module):
	def __init__(self, in_channels, out_channels, kernel_size=3, padding='same_padding', dilation=1):
		super(dilation_layer, self).__init__()
		if padding == 'same_padding':
			padding = (kernel_size - 1) / 2 * dilation
		self.Dconv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
							   kernel_size=kernel_size, padding=padding, dilation=dilation)
		self.Drelu = nn.ReLU(inplace=True)

	def forward(self, x):
		x = self.Dconv(x)
		x = self.Drelu(x)
		return x


class stage_block(nn.Module):
	def __init__(self, in_channels, out_channels):
		super(stage_block, self).__init__()
		self.Dconv_1 = dilation_layer(in_channels, out_channels=64)
		self.Dconv_2 = dilation_layer(in_channels=64, out_channels=64)
		self.Dconv_3 = dilation_layer(in_channels=64, out_channels=64, dilation=2)
		self.Dconv_4 = dilation_layer(in_channels=64, out_channels=32, dilation=4)
		self.Dconv_5 = dilation_layer(in_channels=32, out_channels=32, dilation=8)
		self.Mconv_6 = nn.Conv2d(in_channels=256, out_channels=128, kernel_size=1, padding=0)
		self.Mrelu_6 = nn.ReLU(inplace=True)
		self.Mconv_7 = nn.Conv2d(in_channels=128, out_channels=out_channels, kernel_size=1, padding=0)

	def forward(self, x):
		x_1 = self.Dconv_1(x)
		x_2 = self.Dconv_2(x_1)
		x_3 = self.Dconv_3(x_2)
		x_4 = self.Dconv_4(x_3)
		x_5 = self.Dconv_5(x_4)
		x_cat = torch.cat([x_1, x_2, x_3, x_4, x_5], 1)
		x_out = self.Mconv_6(x_cat)
		x_out = self.Mrelu_6(x_out)
		x_before = x_out
		x_pred = self.Mconv_7(x_out)
		return [x_pred, x_before]


class feature_extractor(nn.Module):
	def __init__(self, have_bn, have_bias):
		super(feature_extractor, self).__init__()
		print "loading layers from inception_v3..."
		'''
		annotations in inception_v3
		self.Conv2d_1a_3x3 = BasicConv2d(3, 32, kernel_size=3, stride=2)
		self.Conv2d_2a_3x3 = BasicConv2d(32, 32, kernel_size=3)
		self.Conv2d_2b_3x3 = BasicConv2d(32, 64, kernel_size=3, padding=1)
		self.Conv2d_3b_1x1 = BasicConv2d(64, 80, kernel_size=1)
		self.Conv2d_4a_3x3 = BasicConv2d(80, 192, kernel_size=3)
		self.Mixed_5b = InceptionA(192, pool_features=32)
		self.Mixed_5c = InceptionA(256, pool_features=64)
		'''
		self.conv1_3x3_s2 = BasicConv2d(
			3, 32, kernel_size=3, stride=2, padding=1, bn = have_bn, bias = have_bias)
			
		self.conv2_3x3_s1 = BasicConv2d(
			32, 32, kernel_size=3, stride=1, padding=1, bn = have_bn, bias = have_bias)
			
		self.conv3_3x3_s1 = BasicConv2d(
			32, 64, kernel_size=3, stride=1, padding=1, bn = have_bn, bias = have_bias)
			
		self.conv4_3x3_reduce = BasicConv2d(
			64, 80, kernel_size=1, stride=1, padding=1, bn = have_bn, bias = have_bias)
		self.conv4_3x3 = BasicConv2d(80, 192, kernel_size=3, bn = have_bn,  bias = have_bias)
		
		self.inception_a1 = InceptionA(192, pool_features=32, have_bn = have_bn, have_bias =have_bias)
		self.inception_a2 = InceptionA(256, pool_features=64, have_bn = have_bn, have_bias= have_bias)

	def forward(self, x):
		x = self.conv1_3x3_s2(x)
		features_2 = x
		x = self.conv2_3x3_s1(x)
		x = self.conv3_3x3_s1(x)
		x = F.max_pool2d(x, kernel_size=3, stride=2, ceil_mode=True)
		features_4 = x
		x = self.conv4_3x3_reduce(x)
		x = self.conv4_3x3(x)
		x = F.max_pool2d(x, kernel_size=3, stride=2, ceil_mode=True)
		x = self.inception_a1(x)
		x = self.inception_a2(x)
		# 46 x 46 x 288
		return [x, features_2, features_4]


class NormalConv2d(nn.Module):
	def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation = 1):
		super(NormalConv2d, self).__init__()

		
		# self.conv = BinaryConv2d(in_channels, out_channels, kernel_size, stride, padding)
		self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride = stride, padding = padding*dilation, bias=False, dilation = dilation)
		self.bn = nn.BatchNorm2d(out_channels)
		# self.conv = RealConv2d(in_channels, out_channels, kernel_size, stride, padding)

	def forward(self, x):
		#x = self.bn(x)
		# x = BinaryActiveFunction()(x)
		x = self.conv(x)
		x = self.bn(x)
		x = F.relu(x, inplace=True)

		return x

class BRBlock(nn.Module):
	def __init__(self, in_channels):
		super(BRBlock, self).__init__()

		self.conv1 = nn.Conv2d(in_channels, in_channels, 3, 1, 1)
		self.relu = nn.ReLU(inplace = True)
		self.conv2 = nn.Conv2d(in_channels, in_channels, 3, 1, 1)

	def forward(self, x):
		x_1 = self.conv1(x)
		x_1 = self.relu(x_1)
		x_1 = self.conv2(x_1)
		return x_1+x


class Model(BaseModel):
	feat_stride = 2
	def __init__(self, stages=5, have_bn = True, have_bias = False):
		super(Model, self).__init__()
		self.stages = stages
		self.feature_extractor = feature_extractor(have_bn = have_bn, have_bias=have_bias)
		self.stage_0 = nn.Sequential(nn.Conv2d(in_channels=288, out_channels=256, kernel_size=3, padding=1),
									 nn.ReLU(inplace=True),
									 nn.Conv2d(in_channels=256, out_channels=128, kernel_size=3, padding=1),
									 nn.ReLU(inplace=True))

		for i in range(stages):
			setattr(self, 'PAF_stage{}'.format(i + 2), stage_block(in_channels=128, out_channels=38) if i == 0 
				else stage_block(in_channels=185, out_channels=38))
			setattr(self, 'heatmap_stage{}'.format(i + 2), stage_block(in_channels=128, out_channels=19) if i == 0 
				else stage_block(in_channels=185, out_channels=19))

			setattr(self, 'seg_stages_{}'.format(i+2), nn.Sequential(stage_block(in_channels = 275, out_channels = 64) if i == 0 else
				stage_block(in_channels = 339, out_channels = 64)))

			setattr(self, "conv1_"+str(i+2), NormalConv2d(128, 64, 3, 1, 1))
			setattr(self, "conv2_"+str(i+2), NormalConv2d(96, 32, 3, 1, 1))
			setattr(self, "conv3_"+str(i+2), nn.Conv2d(32, 1, 1))
			setattr(self, "BR_"+str(i+2), BRBlock(1))

		#self.init_weight()

	def forward(self, x, gts=None):
		saved_for_loss = []
		x_in, features_2, features_4 = self.feature_extractor(x)

		x_in_0 = self.stage_0(x_in)
		x_in = x_in_0
		x_in_for_seg = None
		for i in range(self.stages):
			
			x_PAF_pred, _ =  getattr(self, 'PAF_stage{}'.format(i+2))(x_in)
			x_heatmap_pred, x_before = getattr(self, 'heatmap_stage{}'.format(i+2))(x_in)

			x_for_seg = torch.cat((x_in_0, x_before, x_heatmap_pred), 1)

			if i != 0:
				x_for_seg = torch.cat((x_in_0, x_before, x_heatmap_pred, x_in_for_seg), 1)

			x_seg_pred, _ = getattr(self, 'seg_stages_{}'.format(i+2))(x_for_seg)
			x_in_for_seg = x_seg_pred


			x_up = torch.cat((nn.Upsample(scale_factor = 2)(x_seg_pred), features_4), 1)
			x_up = getattr(self, "conv1_"+str(i+2))(x_up)
			x_up = torch.cat((nn.Upsample(scale_factor = 2)(x_up), features_2), 1)
			x_up = getattr(self, "conv2_"+str(i+2))(x_up)

			x_seg = getattr(self, "conv3_"+str(i+2))(x_up)
			x_seg = getattr(self, "BR_"+str(i+2))(x_seg)
			x_seg = F.sigmoid(x_seg)

			saved_for_loss.append(x_PAF_pred)
			saved_for_loss.append(x_heatmap_pred)
			saved_for_loss.append(x_seg)
			
			if i != self.stages-1:
				x_in = torch.cat((x_PAF_pred, x_heatmap_pred, x_in_0), 1)
			

		return (x_PAF_pred, x_heatmap_pred, x_seg), saved_for_loss


	def init_weight(self):
		for m in self.modules():
			if m in self.feature_extractor.modules():
				continue
			if isinstance(m, nn.Conv2d):
				nn.init.normal(m.weight, std=0.01)
				if m.bias is not None:
					nn.init.constant(m.bias.data, 0.0)
			elif isinstance(m, nn.Linear):
				nn.init.normal(m.weight, std=0.01)
				if m.bias is not None:
					nn.init.constant(m.bias.data, 0.0)
		#self.depth_to_sn.reset_parameters()
		# self.loss_fc13.data.copy_(torch.ones(3))


	@staticmethod
	def get_IoU(pred, seg_mask, thr=0.5):
		tPerd = (pred > thr).astype(int)
		tMask = (seg_mask > thr).astype(int)
		intersection = np.multiply(tPerd, tMask)

		union = np.maximum(tPerd, tMask)
		iou = np.sum(intersection, dtype=np.float32) / np.sum(union, dtype=np.float32)
		return iou

	@staticmethod
	def build_loss(saved_for_loss, heatmaps_var, heat_mask_var, pafs_var, paf_mask_var, seg_var, seg_mask_var):

		criterion1 = nn.MSELoss(size_average=False).cuda()
		criterion2 = nn.BCEWithLogitsLoss(size_average=False).cuda()
		loss = 0
		saved_for_log = OrderedDict()
		for j in range(5):
			pred1 = saved_for_loss[3*j] * paf_mask_var
			gt1 = pafs_var * paf_mask_var

			pred2 = saved_for_loss[3*j+1] * heat_mask_var
			gt2 = heatmaps_var * heat_mask_var
			
			pred3 = saved_for_loss[3*j+2] * seg_mask_var
			gt3 = seg_var * seg_mask_var


			paf_loss = criterion1(pred1, gt1) / 100
			ht_loss = criterion1(pred2, gt2) / 100
			seg_loss = criterion2(pred3, gt3) / 10000
			

			loss += paf_loss + ht_loss + seg_loss

			
			saved_for_log['paf_loss_{}'.format(j)] = paf_loss.data[0]
			saved_for_log['ht_loss_{}'.format(j)] = ht_loss.data[0]
			saved_for_log['seg_loss_{}'.format(j)] = seg_loss.data[0]
		
		saved_for_log['loss'] = loss.data[0]

		return loss, saved_for_log
