#include <stdio.h>
#include "real_gemm_common.h"


int gemm_blas_4x4(BLASLONG bm, BLASLONG bn, BLASLONG bk, FLOAT alpha, FLOAT *ba, BLASLONG lda, FLOAT *bb, BLASLONG ldb, FLOAT *C, BLASLONG ldc) {

//    printf( "lda:%ld, ldb:%ld, ldc:%ld\n", lda, ldb, ldc);
//    printf( "m:%ld, n:%ld, k:%ld\n", bm, bn, bk);

    BLASLONG i, j, k;
    FLOAT *C0, *C1, *C2, *C3;
    FLOAT *ptrba, *ptrbb;
    FLOAT *a_;
    FLOAT res0, res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12, res13, res14, res15;
    FLOAT load0, load1, load2, load3, load4, load5, load6, load7, load8, load9, load10, load11, load12, load13, load14,
            load15, load16, load17, load18, load19, load20, load21, load22, load23, load24, load25, load26, load27, load28,
            load29, load30, load31;

    for (j = 0; j < bn / 4; j += 1) {
        C0 = C;
        C1 = C0 + ldc;
        C2 = C1 + ldc;
        C3 = C2 + ldc;

        a_ = ba;

        for (i = 0; i < bm / 4; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            res2 = 0;
            res3 = 0;
            res4 = 0;
            res5 = 0;
            res6 = 0;
            res7 = 0;
            res8 = 0;
            res9 = 0;
            res10 = 0;
            res11 = 0;
            res12 = 0;
            res13 = 0;
            res14 = 0;
            res15 = 0;
            for (k = 0; k < bk / 4; k += 1) {

                // b + 0
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + load0 * load1;

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + (load2 * load1);

                load4 = ptrba[lda * 0 + 2];
                res2 = res2 + (load4 * load1);

                load6 = ptrba[lda * 0 + 3];
                res3 = res3 + (load6 * load1);

                // b + 1
                load3 = ptrbb[0 + 1 * ldb];
                res4 = res4 + (load0 * load3);
                res5 = res5 + (load2 * load3);
                res6 = res6 + (load4 * load3);
                res7 = res7 + (load6 * load3);

                // b + 2
                load5 = ptrbb[0 + 2 * ldb];
                res8 = res8 + (load0 * load5);
                res9 = res9 + (load2 * load5);
                res10 = res10 + (load4 * load5);
                res11 = res11 + (load6 * load5);

                // b + 3
                load7 = ptrbb[0 + 3 * ldb];
                res12 = res12 + (load0 * load7);
                res13 = res13 + (load2 * load7);
                res14 = res14 + (load4 * load7);
                res15 = res15 + (load6 * load7);

                // k + 1
                load8 = ptrba[lda * 1 + 0];
                load9 = ptrbb[1 + 0];
                res0 = res0 + (load8 * load9);
                load10 = ptrba[lda * 1 + 1];
                res1 = res1 + (load10 * load9);
                load12 = ptrba[lda * 1 + 2];
                res2 = res2 + (load12 * load9);
                load14 = ptrba[lda * 1 + 3];
                res3 = res3 + (load14 * load9);

                // b + 1
                load11 = ptrbb[1 + 1 * ldb];
                res4 = res4 + (load8 * load11);
                res5 = res5 + (load10 * load11);
                res6 = res6 + (load12 * load11);
                res7 = res7 + (load14 * load11);

                // b + 2
                load13 = ptrbb[1 + 2 * ldb];
                res8 = res8 + (load8 * load13);
                res9 = res9 + (load10 * load13);
                res10 = res10 + (load12 * load13);
                res11 = res11 + (load14 * load13);

                // b + 3
                load15 = ptrbb[1 + 3 * ldb];
                res12 = res12 + (load8 * load15);
                res13 = res13 + (load10 * load15);
                res14 = res14 + (load12 * load15);
                res15 = res15 + (load14 * load15);

                // k + 2
                load16 = ptrba[lda * 2 + 0];
                load17 = ptrbb[2 + 0];
                res0 = res0 + (load16 * load17);
                load18 = ptrba[lda * 2 + 1];
                res1 = res1 + (load18 * load17);
                load20 = ptrba[lda * 2 + 2];
                res2 = res2 + (load20 * load17);
                load22 = ptrba[lda * 2 + 3];
                res3 = res3 + (load22 * load17);

                // b + 1
                load19 = ptrbb[2 + 1 * ldb];
                res4 = res4 + (load16 * load19);
                res5 = res5 + (load18 * load19);
                res6 = res6 + (load20 * load19);
                res7 = res7 + (load22 * load19);

                // b + 2
                load21 = ptrbb[2 + 2 * ldb];
                res8 = res8 + (load16 * load21);
                res9 = res9 + (load18 * load21);
                res10 = res10 + (load20 * load21);
                res11 = res11 + (load22 * load21);

                // b + 3
                load23 = ptrbb[2 + 3 * ldb];
                res12 = res12 + (load16 * load23);
                res13 = res13 + (load18 * load23);
                res14 = res14 + (load20 * load23);
                res15 = res15 + (load22 * load23);


                // k + 3
                load24 = ptrba[lda * 3 + 0];
                load25 = ptrbb[3 + 0];
                res0 = res0 + (load24 * load25);
                load26 = ptrba[lda * 3 + 1];
                res1 = res1 + (load26 * load25);
                load28 = ptrba[lda * 3 + 2];
                res2 = res2 + (load28 * load25);
                load30 = ptrba[lda * 3 + 3];
                res3 = res3 + (load30 * load25);

                // b + 1
                load27 = ptrbb[3 + 1 * ldb];
                res4 = res4 + (load24 * load27);
                res5 = res5 + (load26 * load27);
                res6 = res6 + (load28 * load27);
                res7 = res7 + (load30 * load27);

                // b + 2
                load29 = ptrbb[3 + 2 * ldb];
                res8 = res8 + (load24 * load29);
                res9 = res9 + (load26 * load29);
                res10 = res10 + (load28 * load29);
                res11 = res11 + (load30 * load29);

                // b + 3
                load31 = ptrbb[3 + 3 * ldb];
                res12 = res12 + (load24 * load31);
                res13 = res13 + (load26 * load31);
                res14 = res14 + (load28 * load31);
                res15 = res15 + (load30 * load31);

                ptrba = ptrba + 4 * lda;
                ptrbb = ptrbb + 4;
            }
            for (k = 0; k < (bk & 3); k += 1) {

                // b + 0
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + (load0 * load1);

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + (load2 * load1);

                load4 = ptrba[lda * 0 + 2];
                res2 = res2 + (load4 * load1);

                load6 = ptrba[lda * 0 + 3];
                res3 = res3 + (load6 * load1);

                // b + 1
                load3 = ptrbb[0 + 1 * ldb];
                res4 = res4 + (load0 * load3);
                res5 = res5 + (load2 * load3);
                res6 = res6 + (load4 * load3);
                res7 = res7 + (load6 * load3);

                // b + 2
                load5 = ptrbb[0 + 2 * ldb];
                res8 = res8 + (load0 * load5);
                res9 = res9 + (load2 * load5);
                res10 = res10 + (load4 * load5);
                res11 = res11 + (load6 * load5);

                // b + 3
                load7 = ptrbb[0 + 3 * ldb];
                res12 = res12 + (load0 * load7);
                res13 = res13 + (load2 * load7);
                res14 = res14 + (load4 * load7);
                res15 = res15 + (load6 * load7);

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }

            C0[0] = C0[0] + res0 * alpha;
            C0[1] = C0[1] + res1 * alpha;
            C0[2] = C0[2] + res2 * alpha;
            C0[3] = C0[3] + res3 * alpha;

            C1[0] = C1[0] + res4 * alpha;
            C1[1] = C1[1] + res5 * alpha;
            C1[2] = C1[2] + res6 * alpha;
            C1[3] = C1[3] + res7 * alpha;

            C2[0] = C2[0] + res8 * alpha;
            C2[1] = C2[1] + res9 * alpha;
            C2[2] = C2[2] + res10 * alpha;
            C2[3] = C2[3] + res11 * alpha;

            C3[0] = C3[0] + res12 * alpha;
            C3[1] = C3[1] + res13 * alpha;
            C3[2] = C3[2] + res14 * alpha;
            C3[3] = C3[3] + res15 * alpha;

            C0 = C0 + 4;
            C1 = C1 + 4;
            C2 = C2 + 4;
            C3 = C3 + 4;
            a_ = a_ + 4;
        }

        for (i = 0; i < (bm & 3); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            res2 = 0;
            res3 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0 * ldb];
                res0 = res0 + (load0 * load1);

                load2 = ptrbb[0 + 1 * ldb];
                res1 = res1 + (load0 * load2);

                load3 = ptrbb[0 + 2 * ldb];
                res2 = res2 + (load0 * load3);

                load4 = ptrbb[0 + 3 * ldb];
                res3 = res2 + (load0 * load4);

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }
            C0[0] = C0[0] + res0 * alpha;
            C1[0] = C1[0] + res1 * alpha;
            C2[0] = C2[0] + res2 * alpha;
            C3[0] = C3[0] + res3 * alpha;

            C0 = C0 + 1;
            C1 = C1 + 1;
            C2 = C2 + 1;
            C3 = C3 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 2);
        bb = bb + k;
        i = (ldc << 2);
        C = C + i;
    }
    for (j = 0; j < (bn & 3); j += 1) {
        C0 = C;
        a_ = ba;
        for (i = 0; i < bm / 2; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + (load0 * load1);

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + (load2 * load1);
                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }

            C0[0] = C0[0] + res0 * alpha;
            C0[1] = C0[1] + res1 * alpha;
            C0 = C0 + 2;
            a_ = a_ + 2;
        }

        for (i = 0; i < (bm & 1); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + (load0 * load1);
                ptrba = ptrba + 1;
                ptrbb = ptrbb + 1;
            }
            C0[0] = C0[0] + res0 * alpha;
            C0 = C0 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 0);
        bb = bb + k;
        C = C + ldc;
    }
    return 0;
}
