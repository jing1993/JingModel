import numpy as np


def gaussian(location, size, sigma):
    x = np.arange(size[0])
    y = np.arange(size[1])
    gx = np.exp(-(location[0] - x) ** 2 / (2 * sigma ** 2))
    gy = np.exp(-(location[1] - y) ** 2 / (2 * sigma ** 2))
    mp = np.outer(gy, gx)
    return mp


def heatmap(locations, size):
    maps = []
    for location in locations:
        if location[0] is not 0:
            mp = gaussian(location[1], size, sigma=3)
        else:
            mp = np.zeros(size, dtype=np.float32)
        maps.append(mp)
    return np.max(maps, 0)


def heatmap_generator(anns, input_size, feat_stride):
    heatmap_size = input_size / feat_stride
    heatmaps = []
    for joint_id in range(18):
        joint_locations = []
        for person in anns:
            joint_locations.append([person[1][joint_id], np.asarray(person[2][joint_id]) / feat_stride])
        joint_map = heatmap(joint_locations, heatmap_size)
        heatmaps.append(joint_map)
    # background_map = 1-np.max(heatmaps, 0)
    # heatmaps.append(background_map)
    return heatmaps
