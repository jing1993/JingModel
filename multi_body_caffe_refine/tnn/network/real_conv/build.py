import os
import torch
from torch.utils.ffi import create_extension


sources = ['src/real_conv_cpu.c']
headers = ['src/real_conv_cpu.h']
defines = []
extra_objects = []
extra_compile_args = ['-fopenmp', '-lopenblas']
# include_dirs = ['OpenBLAS/include']
# library_dirs = ['OpenBLAS/lib']

with_cuda = False

this_file = os.path.dirname(os.path.realpath(__file__))
print(this_file)

# include_dirs = [os.path.join(this_file, '../../', fname) for fname in include_dirs]
# library_dirs = [os.path.join(this_file, '../../', fname) for fname in library_dirs]

sources = [os.path.join(this_file, fname) for fname in sources]
headers = [os.path.join(this_file, fname) for fname in headers]
extra_objects = [os.path.join(this_file, fname) for fname in extra_objects]

ffi = create_extension(
    '_ext.real_conv',
    headers=headers,
    sources=sources,
    define_macros=defines,
    relative_to=__file__,
    with_cuda=with_cuda,
    extra_objects=extra_objects,
    extra_compile_args=extra_compile_args,
    # include_dirs=include_dirs,
    # library_dirs=library_dirs
)

if __name__ == '__main__':
    ffi.build()
