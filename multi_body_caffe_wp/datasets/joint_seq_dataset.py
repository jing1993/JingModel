from tnn.datasets.dataloader import TimeSeqDataset
import numpy as np


class JointSeqDataset(TimeSeqDataset):
    def __init__(self, datasets, scales=None):
        self.datasets = datasets
        self.scales = [1] * len(datasets) if scales is None else scales
        self.lens = [int(len(self.datasets[i]) * self.scales[i]) for i in range(len(datasets))]
        print([len(d) for d in self.datasets])

    def __len__(self):
        return sum(self.lens)

    def _real_idx(self, i, rnd):
        set_i = 0
        while set_i < len(self.datasets) and i >= self.lens[set_i]:
            i -= self.lens[set_i]
            set_i += 1

        # assert self.scales[set_i] >= 1, 'TimeSeqDataset only supports scales >= 1'
        # real_len = len(self.datasets[set_i])
        # i = i % real_len
        # return set_i, i

        if self.scales[set_i] > 1:
            real_len = len(self.datasets[set_i])
            i = i % real_len
            s = (self.lens[set_i] % real_len) / float(real_len)
            s = 1 if s < 1e-3 else int(np.ceil(1. / self.scales[set_i]))
        else:
            s = int(np.ceil(1. / self.scales[set_i]))
        # i = min(i * s + np.random.randint(0, s), len(self.datasets[set_i]) - 1)
        i = min(i * s + rnd % s, len(self.datasets[set_i]) - 1)

        return set_i, i

    def seq_len(self, i, rnd=0):
        set_i, i = self._real_idx(i, rnd)

        return self.datasets[set_i].seq_len(i)

    def get_item(self, vid, fid, clear, rnd=0):
        set_i, i = self._real_idx(vid, rnd)

        return self.datasets[set_i].get_item(i, fid, clear)
