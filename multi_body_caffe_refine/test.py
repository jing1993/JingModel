import os
import collections
import cv2

import numpy as np
import torch
import torch.utils.data as data


try:
    import ujson as json
except ImportError:
    import json



for batch in range(1, 16):
    root = '/data/new_GTA/{}'.format(batch)

    keypoint_root = os.path.join(root, 'keypoint', 'kpts_after.json')
    keypoint = os.path.join(root, 'keypoint', 'kpts.json')

    with open(keypoint_root) as data_file:
        kpts_after = json.load(data_file)

    with open(keypoint) as data_file:
        kpts = json.load(data_file)

    num_kpts_after = len(kpts_after)
    num_kpts = len(kpts)
    print batch, ', ', num_kpts, ', ', num_kpts_after, ', ', float(num_kpts_after)/num_kpts
    