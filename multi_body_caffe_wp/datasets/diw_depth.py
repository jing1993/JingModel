import os
import cPickle
import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
from scipy.misc import imread
import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class DIWDataset(data.Dataset):
    def __init__(self, root, csv_file, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, training=True):
        self.root = root
        self.csv_file = csv_file
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.max_pts = max_pts
        self.training = training
        self.im_root = root
        self.cache_name = os.path.join(self.root, '{}_cache.pk'.format(csv_file))
        self.items = self.load_inds(re_load=False)

    def load_inds(self, re_load=False):
        # [filename, (x1, y1, x2, y2, rel)]
        if not re_load and os.path.isfile(self.cache_name):
            with open(self.cache_name, 'r') as f:
                items = cPickle.load(f)
                return items

        input_csv = os.path.join(self.root, self.csv_file)
        n_images = 0
        items = []
        with open(input_csv, 'r') as f:
            while True:
                im_name = f.readline().strip()
                # print im_name
                if not im_name:
                    break
                coords = f.readline()
                y1, x1, y2, x2, rel, h, w = coords.strip().split(',')
                if int(h) < 224 or int(w) < 224:
                    continue
                rel = {'=': 0, '<': -1, '>': 1}[rel]
                data = [float(x) - 1 for x in (x1, y1, x2, y2,)]
                data.append(rel)
                items.append((im_name, tuple(data)))
                n_images += 1

        with open(self.cache_name, 'w') as f:
            cPickle.dump(items, f)

        return items

    def __len__(self):
        return len(self.items)

    def __getitem__(self, i):
        im_file, ori_pts = self.items[i]
        im_file = os.path.join(self.im_root, im_file)
        ori_im = imread(im_file)
        if ori_im.ndim == 2:
            ori_im = cv2.cvtColor(ori_im, cv2.COLOR_GRAY2RGB)
        max_pts = 1

        # crop
        im = ori_im.copy()
        imh, imw = im.shape[:2]
        x1, y1, x2, y2, rel = ori_pts
        bx1, by1 = min(x1, x2), min(y1, y2)
        bx2, by2 = max(x1, x2), max(y1, y2)
        nx1 = int(bx1 - np.random.randint(min(50, bx1), bx1 + 1))
        nx2 = int(bx2 + np.random.randint(min(50, imw - bx2 - 1), imw - bx2))
        ny1 = int(by1 - np.random.randint(min(50, by1), by1 + 1))
        ny2 = int(by2 + np.random.randint(min(50, imh - by2 - 1), imh - by2))

        im_crop = im[ny1:ny2+1, nx1:nx2+1].copy()
        x1 = x1 - nx1
        x2 = x2 - nx1
        y1 = y1 - ny1
        y2 = y2 - ny1
        # print im_crop.shape
        scale = min(self.inp_size[0] / float(im_crop.shape[1]), self.inp_size[1] / float(im_crop.shape[0]))
        im_resize = cv2.resize(im_crop, None, fx=scale, fy=scale)
        pts = np.asarray([x1, y1, x2, y2], dtype=np.float)
        pts = np.append(pts, [rel]).reshape([1, 5])
        im = np.zeros([self.inp_size[1], self.inp_size[0], 3], dtype=im_resize.dtype)
        im[0:im_resize.shape[0], 0:im_resize.shape[1], :] = im_resize
        #
        # if self.training:
        #     im, trans_params = im_transform.imcv2_affine_trans(im, im_shape=[self.inp_size[1], self.inp_size[0]],
        #                                                        rotate=False)
        #     pts[:, 0:4] = im_transform.offset_boxes(pts[:, 0:4], *trans_params)
        # else:
        #     im_shape = im.shape
        #     dh = im_shape[0] - self.inp_size[1]
        #     dw = im_shape[1] - self.inp_size[0]
        #     (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
        #     (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
        #     im = im[i0:i1, j0:j1]
        #     pts[:, 0:4:2] -= j0
        #     pts[:, 1:4:2] -= i0
        # print pts
        # keep = (pts[:, 0] >= 0) & (pts[:, 1] >= 0) & (pts[:, 2] >= 0) & (pts[:, 3] >= 0) & \
        #        (pts[:, 0] < self.inp_size[0]) & (pts[:, 1] < self.inp_size[1]) & \
        #        (pts[:, 2] < self.inp_size[0]) & (pts[:, 3] < self.inp_size[1])
        # pts = pts[keep]
        #
        # ow, oh = self.out_size[:2]
        # n_pts = len(pts)
        # if n_pts != 1:
        #     print('[W]: DIW: {} sampled_pts is None'.format(i))
        #     # return self.__getitem__(i)
        #     pts = np.asarray((ow//2, oh//2, ow//2, oh//2, 0), dtype=np.int64)
        #     n_pts = 1
        # else:
        #     pts[:, 0:4:2] *= (self.out_size[0] / float(self.inp_size[0]))
        #     pts[:, 1:4:2] *= (self.out_size[1] / float(self.inp_size[1]))

        ow, oh = self.out_size[:2]
        pts[:, 0:4:2] *= (self.out_size[0] / float(self.inp_size[0])) * scale
        pts[:, 1:4:2] *= (self.out_size[1] / float(self.inp_size[1])) * scale
        pts = np.floor(pts).astype(np.int64)
        keep = (pts[:, 0] >= 0) & (pts[:, 1] >= 0) & (pts[:, 2] >= 0) & (pts[:, 3] >= 0) & \
               (pts[:, 0] < self.out_size[0]) & (pts[:, 1] < self.out_size[1]) & \
               (pts[:, 2] < self.out_size[0]) & (pts[:, 3] < self.out_size[1])
        pts2 = pts[keep]
        if len(pts2) == 0:
            print('[W]: DIW {} sampled_pts is None'.format(i))
            print 'Zero PTS!!!!!!!!!!!!!!'
            print x1, y1, x2, y2
            print nx1, nx2, ny1, ny2
            print im_crop.shape, scale
            print ori_pts, ori_im.shape
            print pts, self.out_size

            pts2 = np.asarray((ow // 2, oh // 2, ow // 2, oh // 2, 0), dtype=np.int64)
        pts = pts2
        depth = np.ones([oh, ow], dtype=np.float32)
        depth = np.ones([oh, ow], dtype=np.float32)
        mask = np.ones_like(depth, dtype=np.float32)

        new_pts = np.zeros([self.max_pts, 5], dtype=np.int64)
        new_pts[:1, :] = pts
        pts = new_pts

        pts = torch.from_numpy(pts).type(torch.LongTensor)
        n_pts = 1

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        im_data = torch.from_numpy(im_data)

        depth_data = torch.from_numpy(depth)
        mask_data = torch.from_numpy(mask)

        return im_data, depth_data, mask_data, pts, n_pts


def get_loader(root, csv_file, inp_size=256, feat_stride=None, out_size=None, max_pts=6000, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = DIWDataset(root, csv_file, inp_size, feat_stride, out_size, max_pts, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
