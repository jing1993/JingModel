# Filter Pruning
Filter pruning, as described in [Filter Pruning For Efficient Convnets](), is
implemented in `filter_pruning.py`.

## Usage
You can use this class to run pruning experiments on your own models.

1. Create a `PruneFilters` object with your desired options

2. Generate a list of layers to prune.

    Pytorch's graph are dynamic so its hard to know a priori what the
    computational graph looks like. In order to simplify things, the user
    needs to specify how the layers are connected.

    To prune a single layer you need to extract references to the layer to
    prune and its "upper" layer so that it's input channels can be adjusted
    accordingly. For an example of this refer to the function
    `extract_layers` in the `ying_prune.py` module in the [rtpose
    project](https://gitlab.com/aifi-ml/rtpose).

3. Get the layers' sensitivity to pruning.

    For each layer in your list, you can use the `sensitivity` method in
    order to prune and run your custom evaluation on the model. This will
    give you baseline accuracies after pruning.

4. Prune and retrain.

    Once you have decided which layers to prune you can use the
    `prune_and_retrain` function in order to prune the selected layers and
    retrain the network.


## Examples

For a complete example of how to use prune your network please refer to
the `ying_prune.py` mentioned above.
