import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader, TimeSeqDataset
import utils.depth as depth_utils


class GTADepthDIW(TimeSeqDataset):
    def __init__(self, root, inp_size=256, feat_stride=4, sample_images=3000, max_pts=6000, thresh=0.1, re_load=False):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.thresh = thresh
        self.max_pts = max_pts
        self.n_images = sample_images

        self.im_root = os.path.join(self.root, 'origin')
        self.depth_root = os.path.join(self.root, 'depth')
        self.label_root = os.path.join(self.root, 'label')

        self.cache_file = os.path.join(self.root, 'gta_valset.pk')
        # [N], [N, 5]
        self.im_names, self.pts, self.num_pts = self.load_cache(self.cache_file, re_load)

    def load_cache(self, cache_file, re_load=False):
        if os.path.isfile(cache_file) and not re_load:
            print('loading cache...')
            with open(cache_file, 'r') as f:
                im_names, pts, num_pts = cPickle.load(f)
                return im_names, pts, num_pts

        im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.png']
        n_images = min(len(im_names), self.n_images)
        idxs = np.random.choice(len(im_names), size=n_images, replace=False)

        # load pts
        sel_ims = []
        pts = []
        num_pts = []
        for i in idxs:
            im_name = im_names[i]
            print(im_name)
            rtn = self.load_image(im_name)
            if rtn is None:
                continue
            im_data, depth_data, mask = rtn

            # sample points
            ipts = np.zeros([self.max_pts, 5], dtype=np.int64)
            # sampled_pts = depth_utils.sample_from_two_mask(depth_data, mask, mask, max_pts=self.max_pts, thresh=self.thresh)
            sampled_pts = depth_utils.sample_pts(depth_data, mask, self.max_pts, 60, self.thresh)
            if sampled_pts is None:
                continue
            ipts[0:len(sampled_pts), :] = sampled_pts
            sel_ims.append(im_name)
            pts.append(ipts)
            num_pts.append(len(sampled_pts))

        with open(cache_file, 'w') as f:
            cPickle.dump((sel_ims, pts, num_pts), f)
            return sel_ims, pts, num_pts

    def load_image(self, im_name):
        im_file = os.path.join(self.im_root, im_name)
        name = im_name.split('_')[0]
        depth_file = os.path.join(self.depth_root, '{}_depth.h5'.format(name))

        im = cv2.imread(im_file)
        if im.shape[0] < 600 or im.shape[1] < 600:
            print('W0: {}, imshape: {}'.format(name, im.shape))
            return None

        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        d5 = h5py.File(depth_file, 'r')
        depth = np.asarray(d5['depth'])
        mask = np.ones_like(depth, dtype=np.float32)

        fx = 0.5
        im = cv2.resize(im, None, fx=fx, fy=fx)
        depth = cv2.resize(depth, None, fx=fx, fy=fx)
        # for gta
        depth = np.exp(-20 * depth) * 4000

        # crop
        im_shape = im.shape
        dh = im_shape[0] - self.inp_size[1]
        dw = im_shape[1] - self.inp_size[0]
        (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
        (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
        im = im[i0:i1, j0:j1].astype(np.float32)
        depth = depth[i0:i1, j0:j1]
        mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))

        mask[mask > 0.9] = 1
        mask[mask <= 0.9] = 0

        # normalize depth
        depth_t = depth[mask > 0]
        dmin, dmax = depth_t.min(), depth_t.max()
        depth = (depth - dmin) / (dmax - dmin + 1e-5)
        depth_data = depth.astype(np.float32)

        return im_data, depth_data, mask

    def seq_len(self, i, rnd=0):
        return 20

    def get_item(self, vid, fid, clear, rnd=0):
        item = self.__getitem__(vid, rnd)
        return list(item) + [clear]

    def __len__(self):
        return len(self.im_names)

    def __getitem__(self, i, rnd=None):
        im_name = self.im_names[i]
        # print(im_name)
        rtn = self.load_image(im_name)
        if rtn is None:
            return self.__getitem__((i + 1) % self.__len__())

        im_data, depth_data, mask = rtn
        pts = torch.from_numpy(self.pts[i]).type(torch.LongTensor)
        num_pts = self.num_pts[i]

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)

        return im_data, depth_data, mask_data, pts, num_pts


def get_loader(root, inp_size=256, feat_stride=4, sample_ims=3000, max_pts=6000, thresh=0.1, re_load=False,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = GTADepthDIW(root, inp_size, feat_stride, sample_ims, max_pts, thresh, re_load)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
