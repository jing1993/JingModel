import cv2
import numpy as np


def rtpose_preprocess(image):
    image = image.astype(np.float32)
    image = image / 256. - 0.5
    image = image.transpose((2, 0, 1)).astype(np.float32)

    return image


def vgg_preprocess(image):
    '''Use Imagenet to normalize input images
    '''
    means = [0.485, 0.456, 0.406]
    stds = [0.229, 0.224, 0.225]

    #BGR2RGB
    preprocessed_img = image.copy()[:, :, ::-1]

    #normalized_value = (RGB_value - ImageNet_mean)/ImageNet_std
    for i in range(3):
        preprocessed_img[:, :, i] = preprocessed_img[:, :, i] - means[i]
        preprocessed_img[:, :, i] = preprocessed_img[:, :, i] / stds[i]

    #H x W x 3 -> 3 x H x W
    preprocessed_img = preprocessed_img.transpose((2, 0, 1)).astype(np.float32)

    return preprocessed_img


def inception_preprocess(image):
    image = image.astype(np.float32)
    image = image / 128. - 0.5
    image = image.transpose((2, 0, 1)).astype(np.float32)

    return image


def ssd_preprocess(image):
    image = image.astype(np.float32)
    rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    rgb_image -= (104.0, 117.0, 123.0)

    x = rgb_image.astype(np.float32)

    x = x[:, :, ::-1].copy()

    x = x.transpose((2, 0, 1))

    return x
