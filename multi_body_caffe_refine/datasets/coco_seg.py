try:
    import cPickle as pickle
except ImportError:
    import pickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader
from tnn.utils.im_transform import crop_with_factor

from pycocotools.coco import COCO
from pycocotools import mask as cocomask


class CocoSeg(data.Dataset):
    def __init__(self, root, name, date=2014, inp_size=256, feat_stride=4, save_and_factor=-1, training=True):

        assert name in ('train', 'test', 'val'), name
        assert os.path.isdir(root), root

        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.save_and_factor = save_and_factor
        self.mask_root = os.path.join(root, 'mask2014')
        self.seg_root = os.path.join(root, 'mask2014_semantic')

        self.dataType = '{}{}'.format(name, date)
        self.cache_file = os.path.join(root, 'cocoseg_{}_cache.pk'.format(self.dataType))

        re_load = False
        if os.path.isfile(self.cache_file) and not re_load:
            with open(self.cache_file, 'r') as f:
                self.imageIds = pickle.load(f)
        else:
            annFile = '%s/annotations/instances_%s.json' % (self.root, self.dataType)
            cocofile = COCO(annFile)
            self.catIds = cocofile.getCatIds('person')
            self.imageIds = cocofile.getImgIds(catIds=self.catIds)
            with open(self.cache_file, 'w') as f:
                pickle.dump(self.imageIds, f)

        self.image_path_prefix = '%s/%s/COCO_%s_' % (self.root, self.dataType, self.dataType)

    def __len__(self):
        return len(self.imageIds)

    def __getitem__(self, i):
        # single_ann = self.annotations[i]
        im_id = self.imageIds[i]
        img_path = self.image_path_prefix + '%012d.jpg' % im_id
        im = cv2.imread(img_path, cv2.IMREAD_COLOR)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        h, w = im.shape[:2]

        seg_file = os.path.join(self.seg_root, '{}_mask_semantic_{:012d}.png'.format(self.dataType, im_id))
        seg_mask = cv2.imread(seg_file, 0)

        mask_file = os.path.join(self.mask_root, '{}_mask_miss_{:012d}.png'.format(self.dataType, im_id))
        mask = cv2.imread(mask_file, 0)

        # data augmentation
        if self.save_and_factor < 0:
            if self.training:
                im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                                   rotate=25, min_scale=0.5, max_scale=1.1, pad_edge=False)
                scale = trans_params[0]
                im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.018, blur_ksize=2, blur_sigma=2)
                mask = im_transform.apply_affine(mask, *trans_params)
                seg_mask = im_transform.apply_affine(seg_mask, *trans_params)
            else:
                im_shape = im.shape
                dh = im_shape[0] - self.inp_size[1]
                dw = im_shape[1] - self.inp_size[0]
                (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
                (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
                im = im[i0:i1, j0:j1].astype(np.float32)
                seg_mask = seg_mask[i0:i1, j0:j1]
                mask = mask[i0:i1, j0:j1]

            mask = cv2.resize(mask, tuple(self.out_size))
            seg_mask = cv2.resize(seg_mask, tuple(self.out_size))
        else:
            im, _, _ = crop_with_factor(im, self.inp_size[0], factor=self.save_and_factor, pad_val=0)
            seg_mask, _, _ = crop_with_factor(seg_mask, self.inp_size[0], factor=self.save_and_factor, pad_val=0)
            mask, im_scale, real_shape = crop_with_factor(mask, self.inp_size[0], factor=self.save_and_factor, pad_val=0)

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        mask2 = np.zeros_like(seg_mask, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        label2 = np.zeros_like(seg_mask, dtype=np.float32)
        label2[seg_mask > 0] = 1
        seg_mask = label2

        # mask = ins_map * mask
        im_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(mask)
        seg_data = torch.from_numpy(seg_mask)

        return im_data, seg_data, mask_data


def get_loader(root, name, date=2014, inp_size=256, feat_stride=4, save_and_factor=-1, training=True,
               batch_size=32, shuffle=True, num_workers=3):
    dataset = CocoSeg(root, name, date, inp_size, feat_stride, save_and_factor, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
