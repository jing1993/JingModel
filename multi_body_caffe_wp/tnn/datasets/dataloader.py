import os
import torch.multiprocessing as multiprocessing
import sys
import threading
import numpy as np

import torch
from torch.utils.data import Dataset
from torch.utils.data.dataloader import DataLoader, DataLoaderIter, ExceptionWrapper, _pin_memory_loop, default_collate

if sys.version_info[0] == 2:
    import Queue as queue
    string_classes = basestring
else:
    import queue
    string_classes = (str, bytes)


_use_shared_memory = False
"""Whether to use shared memory in default_collate"""


class TimeSeqDataset(Dataset):
    def seq_len(self, i, rnd):
        """
        Get length of a video seq
        :param i: video index
        :return: len(self.videos[i])
        """
        raise NotImplementedError

    def get_item(self, vid, fid, clear, rnd):
        raise NotImplementedError


def _worker_loop_with_history(dataset, index_queue, data_queue, collate_fn):
    global _use_shared_memory
    _use_shared_memory = True

    torch.set_num_threads(1)
    while True:
        r = index_queue.get()
        if r is None:
            data_queue.put(None)
            break
        idx, batch_indices = r
        try:
            samples = collate_fn([dataset.get_item(*i) for i in batch_indices])
        except Exception:
            data_queue.put((idx, ExceptionWrapper(sys.exc_info())))
        else:
            data_queue.put((idx, samples))


class TimeSeqDataLoaderIter(DataLoaderIter):

    def __init__(self, loader):
        self.dataset = loader.dataset
        self.batch_size = loader.batch_size
        self.collate_fn = loader.collate_fn
        self.sampler = loader.sampler
        self.num_workers = loader.num_workers
        self.pin_memory = loader.pin_memory
        self.drop_last = loader.drop_last
        self.done_event = threading.Event()

        self.samples_remaining = len(self.sampler)
        self.sample_iter = iter(self.sampler)

        assert isinstance(loader.dataset, TimeSeqDataset)
        assert self.samples_remaining >= self.batch_size

        # history
        self.max_step = loader.max_step
        self.max_inc_step = loader.max_inc_step

        self.last_vids = np.zeros(self.batch_size, dtype=np.int) - 1
        self.last_fids = np.zeros(self.batch_size, dtype=np.int) - 1
        self.rnds = np.random.randint(0, 100000, size=self.batch_size)
        self.inc_steps = np.ones(self.batch_size, dtype=np.int)
        self.step_cnts = np.zeros(self.batch_size, dtype=np.int)
        self.max_steps = np.random.randint(1, self.max_step+1, self.batch_size)

        if self.num_workers > 0:
            self.index_queue = multiprocessing.SimpleQueue()
            self.data_queue = multiprocessing.SimpleQueue()
            self.batches_outstanding = 0
            self.shutdown = False
            self.send_idx = 0
            self.rcvd_idx = 0
            self.reorder_dict = {}

            self.workers = [
                multiprocessing.Process(
                    target=_worker_loop_with_history,
                    args=(self.dataset, self.index_queue, self.data_queue, self.collate_fn))
                for _ in range(self.num_workers)]

            for w in self.workers:
                w.daemon = True  # ensure that the worker exits on process exit
                w.start()

            if self.pin_memory:
                in_data = self.data_queue
                self.data_queue = queue.Queue()
                self.pin_thread = threading.Thread(
                    target=_pin_memory_loop,
                    args=(in_data, self.data_queue, self.done_event))
                self.pin_thread.daemon = True
                self.pin_thread.start()

            # prime the prefetch loop
            for _ in range(2 * self.num_workers):
                self._put_indices()

    def _next_indices(self):
        """
        :return: a batch of (vid, frame_id, clear)
        """
        batch = []
        # print self.step_cnts
        for i, vid in enumerate(self.last_vids):
            clear = False
            if vid < 0 or self.last_fids[i] < 0 or self.last_fids[i] >= self.dataset.seq_len(vid, self.rnds[i]) or self.step_cnts[i] >= self.max_steps[i]:
                if self.samples_remaining > 0:
                    vid = next(self.sample_iter)
                else:
                    assert vid >= 0
                clear = True
                self.rnds[i] = np.random.randint(0, 100000)
                self.last_fids[i] = np.random.choice(self.dataset.seq_len(vid, self.rnds[i]))
                self.last_vids[i] = vid
                self.inc_steps[i] = np.random.randint(0, 2) * 2 - 1
                self.step_cnts[i] = 0
                self.max_steps[i] = np.random.randint(1, self.max_step+1)
                self.samples_remaining -= 1

            fid = self.last_fids[i]
            rnds = self.rnds[i]

            batch.append((vid, fid, clear, rnds))
            self.last_fids[i] += self.inc_steps[i] * np.random.randint(1, self.max_inc_step + 1)
            self.step_cnts[i] += 1

        return batch

    def _put_indices(self):
        assert self.batches_outstanding < 2 * self.num_workers
        if self.samples_remaining > 0:
            if self.samples_remaining < self.batch_size and self.drop_last:
                self._next_indices()
            else:
                self.index_queue.put((self.send_idx, self._next_indices()))
                self.batches_outstanding += 1
                self.send_idx += 1


class sDataLoader(DataLoader):
    def get_stream(self):
        while True:
            for data in DataLoaderIter(self):
                yield data


class TimeSeqDataLoader(DataLoader):

    def __init__(self, dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0,
                 collate_fn=default_collate, pin_memory=False, drop_last=False, max_inc_step=1, max_step=np.inf):

        super(TimeSeqDataLoader, self).__init__(dataset, batch_size, shuffle, sampler=sampler, num_workers=num_workers,
                 collate_fn=collate_fn, pin_memory=pin_memory, drop_last=drop_last)

        self.max_step = max_step
        self.max_inc_step = max_inc_step

    def __iter__(self):
        return TimeSeqDataLoaderIter(self)

    def __len__(self):
        if self.drop_last:
            return len(self.sampler) // self.batch_size * self.max_step
        else:
            return (len(self.sampler) + self.batch_size - 1) // self.batch_size * self.max_step

    def get_stream(self):
        while True:
            for data in TimeSeqDataLoaderIter(self):
                yield data
