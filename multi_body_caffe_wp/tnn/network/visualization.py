'''
__author__     = Alberto Sadde, Long Chen, Andy Tang
__copyright__  = Copyright 2017, AiFi Inc.
__license__    =
__version__    = 0.01
__maintainer__ = Alberto Sadde
__email__      = alberto@aifi.io
__status__     = Experimental
'''

import numpy as np
import torch
import torch.nn as nn

from tnn.utils.param_counter import get_model_size, param_counter
from tnn.utils.vis_async import AsyncVisdom as Visdom


class Visualize(object):
    def __init__(self, model, **kwargs):
        """
        Visualization object. Stores pointer to model and creates a visdom
        object.

        Before running this code: Initialize local visdom server by running
        "python -m visdom.server" in terminal To view the visdom server in
        browser, go to the address "http://localhost:8097"

        More information on visdom is available at:
        https://github.com/facebookresearch/visdom

        :param model: pytorch model encompassing the entire model
        """
        self.model = model
        self.vis = Visdom(**kwargs)

        self.info_win = '_info_win'
        self.mean_weight_win = '_mean_weight_win'

    def _get_stats(self):
        """
        Returns statistics on the model using its stored state dictionary.
        """
        means = []
        medians = []
        maxs = []
        mins = []
        stds = []
        for key, value in self.model.state_dict().iteritems():
            curr = value
            np_value = curr.cpu().numpy()
            mean = torch.mean(curr)
            median = np.median(np_value)
            amax = torch.max(curr)
            amin = torch.min(curr)
            std = np.std(np_value)

            means.append(mean)
            medians.append(median)
            maxs.append(amax)
            mins.append(amin)
            stds.append(std)

        mean = np.mean(np.asarray(means))
        median = np.median(np.asarray(medians))
        amax = max(maxs)
        amin = min(mins)
        std = np.std(np.asarray(stds))
        print("Global stats -- median = {} -- mean = {}"
              "max = {} -- min = {} -- std = {}".format(median, mean, amax,
                                                        amin, std))
        return mean, median, amax, amin, std

    def plot_info(self, learning_rate=None):
        """ Plots a text bullet comprised of the key information of the model
        onto the visdom server's board
        """
        if learning_rate is None:
            learning_rate = -1

        counts = param_counter(self.model)
        size = get_model_size(self.model)
        mean, median, amax, amin, std = self._get_stats()

        msg = """ <h>General information</h>
                      <p>Learning Rate        = {}</p>
                      <p># of Layers          = {}</p>
                      <p># of parameters      = {}</p>
                      <p># of Trainabe Params = {}</p>
                      <p>Size of model        = {} MBs </p>
                      <p>Mean of weights      = {}</p>
                      <p>Median of weights    = {}</p>
                      <p>Max of weights       = {}</p>
                      <p>Min of weights       = {}</p>
                      <p>Standard dev         = {}</p>
                  """.format(learning_rate, counts[0], counts[1], counts[2], size, mean,
                             median, amax, amin, std)

        self.info_win = self.vis.text(msg, win=self.info_win)

    def plot_layer_weights(self, thresh=-1, binNum=150, start=1,
                           end=9999, filter_zeros=False):
        """
        Plots distribution of weights as a visdom histogram
        Args:
            threshold: value used to compare absolute value of weights with
            in order to calculate ratio -leave at -1 for the threshold to be
            the standard deviation of the weights
            binNum:			number of bins on the histogram plot
            start: the number of the first convolutional layer you wish to
            visualize
            end: the number of the final convolutional layer you wish to
            visualize + 1
            filter_zeros:	NOT IMPLEMENTED
        """
        count = 1
        for idx, layer in enumerate(self.model.modules()):
            if isinstance(layer, nn.Conv2d) and count >= start and count < end:
                weights = layer.weight.data.cpu().numpy().flatten()
                if thresh == -1:
                    threshold = weights.std()
                weight_abs = np.abs(weights)
                weights_outside = np.extract(np.greater(threshold, weight_abs), weights)
                ratio = float(weights_outside.shape[0]) / float(weights.shape[0]) * 100
                weights_plot = weights
                if filter_zeros:
                    weights_plot = np.asarray([])
                    for weight in weights:
                        if weight == 0.0:
                            weights_plot.append(weight)
                weights_cumul = np.copy(weights)
                options = dict(title='Conv-{} -- weight-count: {}'.format(count, weights.shape[0]), numbins=binNum)
                self.vis.histogram(weights_plot, opts=options)
                count += 1

    def plot_mean_weights(self):
        """
        Plots mean weights per layer as visdom bar graph
        """
        count = 1
        labels = []
        means = []
        for layer in self.model.modules():
            if isinstance(layer, nn.Conv2d):
                curr_mean = torch.mean(torch.abs(layer.weight.data))
                labels.append(count)
                means.append(curr_mean)
                count += 1

        options = dict(title="Mean absolute weight value in each layer",
                       columnnames=labels, xtype='linear')
        self.mean_weight_win = self.vis.bar(means, opts=options,
                                            win=self.mean_weight_win)

    def plot_cum_hist(self, bins=60):
        """
        Plots cumulative histogram of weights via visdom
        TODO: create x-axis lable

        Args:
            bins:		number of bins in the cumulative
        """
        count = 1
        for layer in self.model.modules():
            if isinstance(layer, nn.Conv2d):
                weights = layer.weight.data.cpu().numpy().flatten()
                weight_abs = np.abs(weights)
                amax = np.amax(weight_abs)
                amin = np.amin(weight_abs)
                range_per_bin = float(amax - amin) / float(bins - 1)
                freqs = np.zeros(bins)
                col_labels = np.zeros(bins)
                for i in range(1, freqs.shape[0]):
                    col_labels[i] = col_labels[i - 1] + range_per_bin
                for i in range(0, weights.shape[0]):
                    freqs[int(weight_abs[i] / range_per_bin)] += 1
                for i in range(1, freqs.shape[0]):
                    freqs[i] = freqs[i] + freqs[i - 1]
                options = dict(title="Cumulative Frequencies of Conv{}".format(count))
                self.vis.bar(freqs, opts=options)
                count += 1

    def full_plot(self, plot_stats=True, plot_dist=True, plot_cumu=True,
                  plot_mw=True, lr=None):
        """
        """
        if plot_stats:
            self.plot_info(lr)
        if plot_dist:
            self.plot_layer_weights()
        if plot_cumu:
            self.plot_cum_hist()
        if plot_mw:
            self.plot_mean_weights()
