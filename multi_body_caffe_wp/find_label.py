import os
import cv2
import numpy as np

"""
myset = set()
for i in range(3, 16):
	print i
	cur_instance = '/data/new_GTA/{}'.format(i) + '/instance/'
	cur_stencil = '/data/new_GTA/{}'.format(i) + '/stencil/'
	for filename in os.listdir(cur_instance):
		if filename[-4:] != '.png':
			print filename
			continue
		name = filename.split('_')[0]

		img = cv2.imread(cur_instance + filename)

		img[img != 0] = 1

		img_stencil = cv2.imread(cur_stencil + name + '_stencil.png')

		label_0 = np.unique(img_stencil*img)

		for num in label_0:
			if num == 8:
				print filename
			myset.add(num)
	print myset

print 'done'
print myset
"""

img = cv2.imread('/data/new_GTA/3/stencil/frame2492101_stencil.png')
img[img != 8] = 0
img[img == 8] = 255
cv2.imwrite('/data/test.png', img)

