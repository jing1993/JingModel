from tnn.utils.path import mkdir
import os
import collections
import cv2

import numpy as np

thred1 = 1000
thred2 = 40
thred3 = 3000


for batch in range(11, 12):
	print batch
	root = '/data/new_GTA/{}'.format(batch)
	if batch != 11:
		mkdir('/data/new_GTA/{}/mask_for_seg'.format(batch))

	im_root = os.path.join(root, 'origin')
	instance_root = os.path.join(root, 'instance')
	instancevis_root = os.path.join(root, 'instance_vis')

	im_names = [filename for filename in os.listdir(im_root) if os.path.splitext(filename)[-1] == '.png']

	for index in range(len(im_names)):
		#print index
		im_name = im_names[index]
		im_file = os.path.join(im_root, im_name)
		im = cv2.imread(im_file)

		name = im_name.split('_')[0]

		instance_file = os.path.join(instance_root, '{}_instance.png'.format(name))
		instance_vis = os.path.join(instancevis_root, '{}_instance_vis.png'.format(name))
		instance = cv2.imread(instance_file, cv2.IMREAD_GRAYSCALE)
		instance_v = cv2.imread(instance_vis)

		rows, cols = instance.shape

		instance[instance > 100] -= 128
		unique_numbers = np.unique(instance)

		mask = np.ones_like(instance)


		for num in unique_numbers:
			if num == 0:
				continue
			total = (instance == num).sum()

			indices = np.argwhere(instance == num)

			r, c = np.mean(indices, axis=0)

			if total > thred3:
				continue

			if total < thred1 or r < thred2 or r > rows - thred2 or c < thred2 or c > cols - thred2:
				mask[instance == num] = 0
		"""
		cv2.imwrite('/data/mask/{}_mask.png'.format(index), mask*255)
		cv2.imwrite('/data/mask/{}_vis.png'.format(index), instance_v)
		cv2.imwrite('/data/mask/{}_im.png'.format(index), im)
		"""
		cv2.imwrite('/data/mask/{}_mask.png'.format(name), mask)

print 'finish'
