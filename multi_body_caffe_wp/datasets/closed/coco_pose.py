import torch
import numpy as np
import cv2
import os
import cPickle
import torch.utils.data as data
import datasets.coco.image_augmentation as Aug
import datasets.coco.heatmap_generator as heat_gen
import datasets.coco.PAFmap_generator as PAF_gen
from tnn.datasets.dataloader import sDataLoader


class COCODataset(data.Dataset):
    def __init__(self, root, anno_pk, data_type, inp_size=256, feat_stride=4, training=True):
        self.root = root
        self.inp_size = np.array([inp_size] * 2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.training = training

        self.im_root = os.path.join(self.root, 'images', data_type)
        self.mask_root = os.path.join(self.root, 'mask', data_type)
        self.heatmap_size = self.inp_size // self.feat_stride

        with open(os.path.join(root, anno_pk), 'r') as f:
            self.annotations = cPickle.load(f)

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, i):
        obj = self.annotations[i]

        im_name = os.path.join(self.im_root, obj['name'])
        mask_name = os.path.join(self.mask_root, 'mask_' + obj['name'])
        im_orig = cv2.imread(im_name)
        mask_orig = cv2.imread(mask_name, 0)

        # data augmentation
        anns = obj['anns']
        orig_size = np.asarray(obj['size'], dtype=int)
        dest_size = self.inp_size
        im_mod, anns_mod, mask_mod = Aug.img_trans(im_orig, anns, mask_orig, orig_size, dest_size, self.feat_stride)
        im_mod = np.asarray(im_mod, dtype=np.float32)
        im_data = torch.from_numpy(im_mod / 255. - 0.5)
        im_data = im_data.permute(2, 0, 1)
        mask_data = torch.unsqueeze(torch.from_numpy(mask_mod.astype(np.float32)) / 255, 0)

        PAF = PAF_gen.PAFmap_generator(anns_mod, self.inp_size, self.feat_stride)
        PAF_data = torch.from_numpy(PAF)
        heatmaps = heat_gen.heatmap_generator(anns_mod, self.inp_size, self.feat_stride)
        heatmaps = np.asarray(heatmaps, dtype=np.float32)
        heatmap_data = torch.from_numpy(heatmaps)
        return im_data, heatmap_data, PAF_data, mask_data


def collate_fn(data):
    im_data, heatmap_data, PAF_data, mask_data = zip(*data)
    im_data = torch.stack(im_data, 0)
    heatmap_data = torch.stack(heatmap_data, 0)
    PAF_data = torch.stack(PAF_data, 0)
    mask_data = torch.stack(mask_data, 0)

    return im_data, heatmap_data, PAF_data, mask_data


def get_loader(root, anno_pk, sub_set=None, inp_size=256, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = COCODataset(root, anno_pk, sub_set, inp_size, feat_stride, training)
    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader
