import cPickle
import os

import cv2
import numpy as np
import torch
import torch.utils.data as data

from tnn.datasets.dataloader import sDataLoader


class Make3D(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=4, training=True):
        self.root = root
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training

        self.im_root = os.path.join(self.root, '{}Img'.format(name))
        self.im_names = [v for v in os.listdir(self.im_root) if os.path.splitext(v)[-1] == '.jpg']

        with open(os.path.join(root, '{}Depth.pk'.format(name)), 'r') as f:
            self.annotations = cPickle.load(f)

        assert len(self.im_names) == len(self.annotations)

    def __len__(self):
        return len(self.im_names)

    def __getitem__(self, i):
        fname = self.im_names[i]
        name = os.path.splitext(fname)[0]

        im = cv2.imread(os.path.join(self.im_root, fname))
        im_data = cv2.resize(im, tuple(self.inp_size))
        im_data = np.transpose(im_data, [2, 0, 1]).astype(np.float32) / 255.

        depth = self.annotations[name]
        depth_data = cv2.resize(depth, tuple(self.out_size))
        depth_data = depth_data.astype(np.float32)

        mask = np.zeros_like(depth_data, dtype=np.float32)
        mask[np.logical_and(depth_data < 70, depth_data > 0)] = 1

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)
        return im_data, depth_data, mask_data


def collate_fn(data):
    im_data, depth_data, mask_data = zip(*data)
    im_data = torch.stack(im_data, 0)
    depth_data = torch.stack(depth_data, 0)
    mask_data = torch.stack(mask_data, 0)

    return im_data, depth_data, mask_data


def get_loader(root, name, inp_size=256, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = Make3D(root, name, inp_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/media/longc/LData/data/make3d',
                                'Train400', inp_size=(344, 460), feat_stride=1,
                                training=True, batch_size=6)
        for i, batch in enumerate(train_data):
            im_data, depth_data, mask_data = batch
            print im_data.size()
            print depth_data.size()
            print mask_data.size()

            cv2.imshow('image', np.transpose(im_data[0].numpy(), [1, 2, 0]))
            cv2.imshow('depth', depth_data[0].numpy() / torch.max(depth_data[0]))
            cv2.waitKey(0)

            # assert False
    main()
