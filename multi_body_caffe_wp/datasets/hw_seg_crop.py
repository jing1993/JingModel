import json
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader

import utils.pose as pose_utils


class HWSegCrop(data.Dataset):
    def __init__(self, root, inp_size=256, feat_stride=4, subset=None, training=True):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training

        self.im_root = os.path.join(self.root, 'images')
        self.seg_root = os.path.join(self.root, 'seg')
        self.mask_root = os.path.join(self.root, 'mask')
        self.bboxes_root = os.path.join(self.root, 'bboxes')

        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.jpg']
        if subset is not None:
            self.im_names = self.im_names[subset[0]:subset[1]]

        self.all_bboxes = self.load_anns()

    def load_anns(self):
        all_bboxes = []
        for im_name in self.im_names:
            name = os.path.splitext(im_name)[0]
            bbox_file = os.path.join(self.bboxes_root, '{}_bboxes.json'.format(name))
            with open(bbox_file, 'r') as f:
                bboxes = json.load(f)
                for realbbox, bbox in bboxes:
                    if len(bbox) < 4:
                        continue
                    all_bboxes.append((realbbox, bbox, im_name))
        return all_bboxes

    def __len__(self):
        return len(self.all_bboxes)

    def __getitem__(self, i):
        realbbox, bbox, im_name = self.all_bboxes[i]
        # im_name = self.im_names[i]  # 20170901_2248.jpg
        im_file = os.path.join(self.im_root, im_name)
        name = os.path.splitext(im_name)[0]

        seg_file = os.path.join(self.seg_root, '{}_seg.jpg'.format(name))
        mask_file = os.path.join(self.mask_root, '{}_mask.jpg'.format(name))
        # 20170901_seg_2248.jpg

        im = cv2.imread(im_file)
        if im.shape[0] < 590 or im.shape[1] < 590:
            print('W0: {}, imshape: {}'.format(im_file, im.shape))
            next_i = (i+1) % self.__len__()
            return self.__getitem__(next_i)

        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        seg_mask = cv2.imread(seg_file)[:, :, 0]
        mask = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        mask[mask > 0] = 1
        mask = mask.astype(np.float32)

        # crop
        cx, cy = np.mean(bbox[0::2]), np.mean(bbox[1::2])
        h, w = bbox[3] - bbox[1], bbox[2] - bbox[0]
        crop_size = max(h, w) * np.random.uniform(1.6, 2.4)
        im = pose_utils.crop(im, (cx, cy), (crop_size, crop_size), self.inp_size, min_size=0)
        if im is None:
            return self.__getitem__((i + 1) % self.__len__())
        seg_mask = pose_utils.crop(seg_mask, (cx, cy), (crop_size, crop_size), self.inp_size, min_size=0)
        mask = pose_utils.crop(mask, (cx, cy), (crop_size, crop_size), self.inp_size, min_size=0)

        # fx = 0.5
        # im = cv2.resize(im, None, fx=fx, fy=fx)
        # seg_mask = cv2.resize(seg_mask, None, fx=fx, fy=fx)
        # mask = cv2.resize(mask, None, fx=fx, fy=fx)

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=20, min_scale=0.6, max_scale=1.4, replicate_edge=False)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.01, blur_ksize=1, blur_sigma=1)
            mask = im_transform.apply_affine(mask, *trans_params)
            seg_mask = im_transform.apply_affine(seg_mask, *trans_params)
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            seg_mask = seg_mask[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        seg_mask = cv2.resize(seg_mask, tuple(self.out_size))

        mask2 = np.zeros_like(seg_mask, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        label2 = np.zeros_like(seg_mask, dtype=np.float32)
        label2[seg_mask > 0] = 1
        seg_mask = label2

        # mask = ins_map * mask
        im_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(mask)
        seg_data = torch.from_numpy(seg_mask)

        return im_data, seg_data, mask_data


def get_loader(root, inp_size=256, feat_stride=4, subset=None, training=True,
               batch_size=32, shuffle=True, num_workers=3):
    dataset = HWSegCrop(root, inp_size, feat_stride, subset, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader


if __name__ == '__main__':

    def main():
        hwcrop_data = HWSegCrop('../data/hw_seg', (288,288), feat_stride=1, subset=(0, -200), training=True)
        print(hwcrop_data[0])

    main()
