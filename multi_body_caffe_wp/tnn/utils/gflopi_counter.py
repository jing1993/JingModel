import torch
import numpy as np
from torch.autograd import Variable
from IPython import embed


def gflopi_counter(model, inp_size = (3, 368, 368)):
    """
    This function computes number of floating point operations per image.
    It takes only convolutions into account.
    """

    inp_data = np.zeros(inp_size)
    inp_var = Variable(torch.FloatTensor(inp_data).unsqueeze(0))


    gflopi_counter.total_operations = 0
    def hook(self, input, output):
        out = output.size()
        out = out[1] * out[2] * out[3]
        ker = self.kernel_size[0] * self.kernel_size[1]
        inp = input[0].size()[1]
        gflopi_counter.total_operations += ker * inp * out

    hooks = []
    seen = set()

    def visit(mod):
        if mod not in seen:
            seen.add(mod)
            if type(mod).__name__ == "Conv2d":
                hooks.append(mod.register_forward_hook(hook))

            for m in mod.modules():
                visit(m)

    visit(model)
    model(inp_var)

    for hk in hooks:
        hk.remove()

    print "Computed GFLOPI: ", float(gflopi_counter.total_operations) / 1024 / 1024 / 1024
