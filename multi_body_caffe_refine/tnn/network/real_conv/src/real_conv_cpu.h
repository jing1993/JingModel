void RealSpatialConvolutionMM_updateOutput(
           THFloatTensor *input,
           THFloatTensor *output,
           THFloatTensor *weight,
//           THFloatTensor *bias,
           THFloatTensor *finput,
           int kW, int kH,
           int dW, int dH,
           int padW, int padH);