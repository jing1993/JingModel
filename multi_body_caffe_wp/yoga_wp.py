import os

import cv2
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable

import post_processing as rtpose_utils
from tnn.network.tester import Tester

from network.depth_small38 import Model
from tnn.network import net_utils
from tnn.utils.path import mkdir
from tnn.utils.timer import Timer
# Set Testing parameters
params = Tester.TestParams()
#thre_params = {'thre1':0.3, 'thre2':0.05, 'thre3':0.5}
# ===================================================#
# ========change the following parameters============#
# ===================================================#
data_dir = '/data/coco/'
json_path = '/data/coco/COCO.json'

extractor = 'inception_v3'  # select the feature_extractor
# get test data
testdata_dir = 'huawei'
testresult_dir = 'testresult_wp'
params.gpus = [0]

gpus = [0]

params.save_dir = '/data/'
exp_name = 'exp_caffe'
save_dir = 'models/training/{}'.format(exp_name)

params.ckpt = os.path.join(save_dir, 'ckpt_1.h5')
#params.ckpt = 'ckpt_92.h5'  # checkpoint file to load, no need to change this

inp_size = 368
#inp_size = 736

# ===================================================#
# ===================================================#
# ===================================================#

model = Model(stages=5, have_bn= False, have_bias = True)
net_utils.load_net(params.ckpt, model)


#grabcut algorithm
def post_process(img, seg, mode = 'final'):
    bg_threshold = 0.1
    fg_threshold = 0.9
    pr_threshold = 0.5
    threshold = 0.3

    raw_mask = np.zeros_like(seg)
    if mode == 'raw':
        raw_mask[seg < threshold] = 0
        raw_mask[seg >= threshold] = 1
        return raw_mask

    raw_mask[seg < pr_threshold] = 2
    raw_mask[seg >= pr_threshold] = 3
    raw_mask[seg < bg_threshold] = 0
    raw_mask[seg > fg_threshold] = 1
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)

    #cv2.imwrite('image.png', img)
    #cv2.imwrite('seg.png', raw_mask*85)
    refined_mask, bgdModel, fgdModel = cv2.grabCut(img,np.uint8(raw_mask),None,bgdModel,fgdModel,1,cv2.GC_INIT_WITH_MASK)
    if mode == 'refined':
        refined_mask[refined_mask == 2] = 0 
        refined_mask[refined_mask == 3] = 1
        return refined_mask
    elif mode == 'final':
        refined_mask[(raw_mask == 3) & (refined_mask == 3)] = 1
        refined_mask[refined_mask != 1] = 0
        return refined_mask
    else:
        assert False, 'invalid mode.'


def _factor_closest(num, factor, is_ceil=True):
    num = np.ceil(float(num) / factor) if is_ceil else np.floor(float(num) / factor)
    num = int(num) * factor
    return num

def crop_with_factor(im, dest_size=None, factor=32, is_ceil=True):
    im_shape = im.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])
    # im_scale = 1.
    # if max_size is not None and im_size_min > max_size:
    im_scale = float(dest_size) / im_size_min
    im = cv2.resize(im, None, fx=im_scale, fy=im_scale)

    h, w, c = im.shape
    new_h = _factor_closest(h, factor=factor, is_ceil=is_ceil)
    new_w = _factor_closest(w, factor=factor, is_ceil=is_ceil)
    im_croped = np.zeros([new_h, new_w, c], dtype=im.dtype)
    im_croped[0:h, 0:w, :] = im

    return im_croped, im_scale, im.shape

def run_test(model, testdata_dir, testresult_dir, params):

    #model = nn.DataParallel(model, device_ids=params.gpus)
    
    model = model.cuda(device_id=params.gpus[0])
    model.eval()


    if not os.path.isdir(params.save_dir):
        os.mkdir(params.save_dir)

    if not os.path.isdir(testdata_dir):
        assert False, 'Please download test data from https://drive.google.com/drive/u/0/folders/0B5L6mkx2k-2lQS1JcHV4SjdHdGc.'

    if not os.path.isdir(testresult_dir):
        os.mkdir(testresult_dir)

    img_list = os.listdir(testdata_dir)

    for img_name in img_list:
        print 'Processing image: ' + img_name
        frame = cv2.imread(os.path.join(testdata_dir, img_name))
        scale = 368./frame.shape[0]
        # padding
        im_croped, im_scale, real_shape = crop_with_factor(frame, 368, factor=8, is_ceil=True)

        im_data = im_croped.astype(np.float32) / 256. - 0.5

        im_data = im_data.transpose([2, 0, 1]).astype(np.float32)

        im_data = np.expand_dims(im_data,axis=0)
        
        im_tensor = Variable(torch.from_numpy(im_data), volatile = True).cuda()

        predicted_outputs,_ = model(im_tensor)

        output1, output2 = predicted_outputs[0], predicted_outputs[1]

        heatmap = output2.cpu().data.numpy().transpose(0, 2, 3, 1)[0]

        paf = output1.cpu().data.numpy().transpose(0, 2, 3, 1)[0]   
        

        heatmap = cv2.resize(heatmap, None, fx=8, fy=8, interpolation=cv2.INTER_CUBIC)
        heatmap = heatmap[0:real_shape[0], 0:real_shape[1], :]
        heatmap = cv2.resize(heatmap, (frame.shape[1], frame.shape[0]), interpolation=cv2.INTER_CUBIC)

        paf = cv2.resize(paf, None, fx=8, fy=8, interpolation=cv2.INTER_CUBIC)
        paf = paf[0:real_shape[0], 0:real_shape[1], :]
        paf = cv2.resize(paf, (frame.shape[1], frame.shape[0]), interpolation=cv2.INTER_CUBIC)
        
        canvas, to_plot, candidate, subset = rtpose_utils.paf_to_pose(np.copy(frame), heatmap, paf)


        cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_4associations.png'),to_plot)

        """                        
        seg_pred = output[2][0][0].data.cpu().numpy()
        #seg_pred = cv2.resize(seg_pred, dsize = (origin_w, origin_h))
        
        h, w = im_pad.shape[:2]
        seg_pred = cv2.resize(seg_pred, dsize=(w, h))

        seg = seg_pred[:real_shape[0], :real_shape[1]]
        im_croped = im_pad[:real_shape[0], :real_shape[1]]


        seg_raw = post_process(im_croped, seg, mode = 'raw')
        seg_refined = post_process(im_croped, seg, mode = 'refined')

        alpha = (im_croped.astype(np.float) * np.expand_dims(seg, -1))
        raw_output = (im_croped.astype(np.float) * np.expand_dims(seg_raw, -1))
        refined_output = (im_croped.astype(np.float) * np.expand_dims(seg_refined, -1))

        compiled = np.concatenate([alpha, raw_output, refined_output], axis=1)

        #cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_compare.png'), compiled)
        """
    print 'completed...'


run_test(model, testdata_dir, testresult_dir, params)