import os
import collections
import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform_gta as im_transform
import utils.depth as depth_utils
import datasets.data_utils_gta as data_utils
import data_filter as data_filter

try:
    import ujson as json
except ImportError:
    import json


from tnn.datasets.coco_data.heatmap import putGaussianMaps
from tnn.datasets.coco_data.paf import putVecMaps

params_transform = dict()

params_transform['crop_size_x'] = 368
params_transform['crop_size_y'] = 368
params_transform['stride'] = 8

params_transform['sigma'] = 7.0

label_list = [9, 17, 25, 33, 41, 57, 129, 137, 145, 153, 161, 169, 177, 185]
joint_list = [71, 48, 49, 50, 24, 25, 26, 9, 10, 11, 2, 3, 4, 81, 77]

class GTADepth(data.Dataset):
    def __init__(self, root, inp_size=256, feat_stride=4, max_pts=6000, thresh1=0.1, thresh2=0.01, subset=None, training=True):
        self.root = root
        # self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride[-1] if isinstance(feat_stride, collections.Iterable) else feat_stride
        self.all_scales = self.feat_stride / np.asarray(feat_stride, dtype=np.float) if isinstance(feat_stride, collections.Iterable) else (1,)
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.thresh1 = thresh1
        self.thresh2 = thresh2
        self.max_pts = max_pts

        self.im_root = os.path.join(self.root, 'origin')
        self.mask_root = os.path.join(self.root, 'mask_for_seg')
        self.depth_root = os.path.join(self.root, 'depth')
        self.label_root = os.path.join(self.root, 'stencil')
        self.snormal_root = os.path.join(self.root, 'surfaceNorm')
        self.keypoint_root = os.path.join(self.root, 'keypoint', 'kpts_after.json')

        with open(self.keypoint_root) as data_file:
            self.kpts_data = json.load(data_file)
        

        if not os.path.isdir(self.snormal_root):
            self.snormal_root = None

        self.im_names = [filename for filename in os.listdir(self.im_root) if os.path.splitext(filename)[-1] == '.png']
        #print self.im_names
        length = len(self.im_names)

        if self.training:
            self.im_names = self.im_names[0:int(length*0.9)]
        else:
            self.im_names = self.im_names[int(length*0.9):]
        if subset is not None:
            self.im_names = self.im_names[subset[0]:subset[1]]

    def __len__(self):
        return len(self.im_names)

    def read_images(self, i):
        im_name = self.im_names[i]
        im_file = os.path.join(self.im_root, im_name)
        name = im_name.split('_')[0]
        coors = None
        if name in self.kpts_data.keys():
            coors = self.kpts_data[name]
            """
            next_i = (i + 1) % self.__len__()
            return self.__getitem__(next_i)
            """
        
        
        mask_file = os.path.join(self.mask_root, '{}_mask.png'.format(name))

        depth_file = os.path.join(self.depth_root, '{}_depth.h5'.format(name))
        label_file = os.path.join(self.label_root, '{}_stencil.png'.format(name))


        im = cv2.imread(im_file)
        if im.shape[0] < 600 or im.shape[1] < 600:
            print('W0: {}, imshape: {}'.format(name, im.shape))
            next_i = (i + 1) % self.__len__()
            return self.__getitem__(next_i)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        d5 = h5py.File(depth_file, 'r')
        gta_depth = np.asarray(d5['depth'])
        seg_label = cv2.imread(label_file, cv2.IMREAD_GRAYSCALE)

        for num in label_list:
            seg_label[seg_label == num] = 1
        seg_label[seg_label != 1] = 0
        seg_label = seg_label * 255
        #seg_label = cv2.imread(label_file)
        #cv2.imwrite('/data/test/{}_im.png'.format(i), im)
        #cv2.imwrite('/data/test/{}_seg.png'.format(i), seg_label * 255)
        mask_for_seg = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
        

        if self.snormal_root is not None:
            snormal_file = os.path.join(self.snormal_root, '{}_normal.png'.format(name))
            sn = cv2.imread(snormal_file)
        else:
            sn = np.zeros_like(im)

        seg_mask = data_utils.mask_generation(seg_label)

        seg_label = seg_label.astype(np.float32)
        seg_label[seg_label > 0] = 1
        seg_mask = seg_mask.astype(np.float32)
        seg_mask[seg_mask > 0] = 1

        sn_mask = np.ones_like(gta_depth, dtype=np.float32)

        mask_sky = np.zeros_like(gta_depth, dtype=np.float32)
        mask_sky[gta_depth > 0] = 1

        return im, gta_depth, seg_label, sn, seg_mask, sn_mask, mask_sky, coors, mask_for_seg

    def get_heatmaps_pafs(self, coors, seg):
        stride = params_transform['stride']
        crop_size_y = params_transform['crop_size_y']
        crop_size_x = params_transform['crop_size_x']
        grid_y = crop_size_y / stride
        grid_x = crop_size_x / stride
        heatmaps = np.zeros((grid_y, grid_x, 19))
        pafs = np.zeros((grid_y, grid_x, 38))
        seg_for_background = cv2.resize(seg, (grid_y, grid_x))

        for i in range(15):
            for j in range(len(coors)):
                center = coors[j, i, :2]
                if center[0] >= 0 and center[0] <= crop_size_x and center[1] >= 0 and center[1] <= crop_size_y:
                    gaussian_map = heatmaps[:, :, i+1]
                    heatmaps[:, :, i+1] = putGaussianMaps(center, gaussian_map, params_transform=params_transform)

        
        """
        mid_1 = [0, 0, 1, 2, 4, 5, 0, 7, 8, 0, 10, 11, 0, 0]
        mid_2 = [1, 4, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        """

        mid_1 = [0, 7, 8, 0, 10, 11, 0, 1, 2, -1, 0, 4, 5]
        mid_2 = [7, 8, 9, 10, 11, 12, 1, 2, 3, -1, 4, 5, 6]

        thre = 1
        for i in range(13):
            if i == 9:
                continue
            # limb
            count = np.zeros((grid_y, grid_x), dtype=np.uint32)
            for j in range(len(coors)):
                centerA = coors[j, mid_1[i], :2]
                centerB = coors[j, mid_2[i], :2]
                if centerA[0] >= 0 and centerA[0] <= crop_size_x and centerA[1] >= 0 and centerA[1] <= crop_size_y  \
                            and centerB[0] >= 0 and centerB[0] <= crop_size_x and centerB[1] >= 0 and centerB[1] <= crop_size_y:
                    vec_map = pafs[:, :, 2 * i:2 * i + 2]
                    pafs[:, :, 2 * i:2 * i + 2], count = putVecMaps(centerA=centerA,
                                                                    centerB=centerB,
                                                                    accumulate_vec_map=vec_map,
                                                                    count=count, params_transform=params_transform)
        
        #heatmaps[:, :, -1] = np.maximum(1 - np.max(heatmaps[:, :, :15], axis=2), 0.)
        heatmaps[:, :, -1] = seg_for_background
        return pafs.transpose((2, 0, 1)), heatmaps.transpose((2, 0, 1))


    def __getitem__(self, i):
        #print i
        # read images
        im, gta_depth, seg_label, sn, mask, sn_mask, mask_sky, coors, mask_for_seg = self.read_images(i)
        
        if coors == None:
            next_i = (i + 1) % self.__len__()
            return self.__getitem__(next_i)

        #print 'done'

        """
        for joint in range(98):
            im_copy = im.copy()
            for person in coors.keys():
                x = coors[person][joint * 3]
                y = coors[person][joint * 3 + 1]
                if x >= 0 and y >= 0:
                    cv2.circle(im_copy, (int(x), int(y)), 4, (0,0,255), -1)
            cv2.imwrite('/data/test/{}_kp_{}.png'.format(i, joint), im_copy)
        """
        kpts = []
        for person in coors:
            cur_person = coors[person]
            cur_joints = np.reshape(cur_person, (98, 3))
            cur = np.zeros((15, 3))
            for joint in range(len(joint_list)):
                cur[joint] = cur_joints[joint_list[joint]]
                if cur[joint][0] < 0 or cur[joint][1] < 0:
                    cur[joint][2] = -1
                else:
                    cur[joint][2] = 0
            kpts.append(cur)
        kpts = np.asarray(kpts, dtype=np.float32)


        # for gta
        depth = np.exp(-20. * gta_depth) * 4000
        abs_depth = 0.1 / (np.exp(1 * (gta_depth + 1e-5)) - 1)
        """
        maxval = np.max(abs_depth)
        minval = np.min(abs_depth)
        depth_test = (abs_depth - minval)/(maxval - minval) * 255
        cv2.imwrite('/data/test/{}_depth.png'.format(i), depth_test.astype(np.uint8))
        """

        additions = (depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn, mask_for_seg)

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 1.0
        max_scale = target_scale * 1.3
        aug_kwargs = {
            'dst_shape': (self.inp_size[1], self.inp_size[0]),
            'rotate': 25,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': True
        }

        kpts, im, additions, trans_params = data_utils.data_augmentation(kpts, im, additions, **aug_kwargs)
        depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn, mask_for_seg = additions

        #print mask_for_seg.shape = 368*368


        pafs, heatmaps = self.get_heatmaps_pafs(kpts, seg_label)

        # image recolor
        im, seg_label = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.015, blur_ksize=2, blur_sigma=2,
                                                   mask=seg_label)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # scale depth
        scale = trans_params[0]
        depth = depth / scale
        abs_depth = abs_depth / scale
        # degree = trans_params[1][-1]
        # radius = degree * np.pi / 180.
        # rotate_matrix = np.asarray(
        #     [[np.cos(radius), 0, np.sin(radius)], [0, 1., 0], [-np.sin(radius), 0, np.cos(radius)]])
        # sn = ((sn / 127.5 - 1).dot(rotate_matrix) * 127.5 + 127.5).astype(np.uint8)

        # =========== end data augmentation ==============

        # resize to output
        additions = (depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn, mask_for_seg)
        additions = [cv2.resize(v, tuple(self.out_size)) for v in additions]
        depth, abs_depth, seg_label, mask, sn_mask, mask_sky, sn, mask_for_seg = additions

        size_for_ht = params_transform['crop_size_x']/params_transform['stride']
        mask_miss = cv2.resize(mask_for_seg, (size_for_ht, size_for_ht))
        mask_miss = np.expand_dims(mask_miss, axis=2)

        mask_for_seg = np.expand_dims(mask_for_seg, axis = 2)
        seg_label_new = np.expand_dims(seg_label, axis = 2)

        #print mask_for_ht.shape
        mask_for_ht = np.repeat(mask_miss, 19, axis=2)
        mask_for_ht[:, :, 0] = 0
        mask_for_ht[:, :, -3:-1] = 0

        mask_for_paf = np.repeat(mask_miss, 38, axis = 2)
        mask_for_paf[:, :, 18:20] = 0
        mask_for_paf[:, :, -12:] = 0
        """
        print mask_for_seg.shape
        print mask_for_paf.shape
        print mask_for_ht.shape
        """
        #print mask_for_seg.shape = 184*184

        mask[mask > 0.9] = 1
        mask[mask <= 0.9] = 0
        mask_sky[mask_sky > 0.9] = 1
        mask_sky[mask_sky <= 0.1] = 0

        # normalize depth
        depth_t = depth[mask > 0]
        if depth_t.size > 0:
            dmin, dmax = depth_t.min(), depth_t.max()
            depth_n = (depth - dmin) / (dmax - dmin + 1e-5)
            depth_n = depth_n.astype(np.float32)
        else:
            depth_n = depth

        # get surface normal from depth
        # sn = depth_utils.depth_to_normal(depth_n, im_order=False).astype(np.float32)
        sn = depth_utils.depth_to_normal(abs_depth, z_scale=0.008, im_order=False).astype(np.float32)
        # sn = sn.astype(np.float32).transpose([2, 0, 1]) / 127.5 - 1

        sn_mask = sn_mask * mask_sky
        sn_mask[0, :] = 0
        sn_mask[-1, :] = 0
        sn_mask[:, 0] = 0
        sn_mask[:, -1] = 0
        sn_mask[np.sum(np.abs(sn), axis=0) < 0.001] = 0
        sn_mask = sn_mask[np.newaxis, :, :]

        # to tensor
        mask_for_seg = torch.from_numpy(mask_for_seg.transpose((2, 0, 1)).astype(np.float32))
        mask_for_ht = torch.from_numpy(mask_for_ht.transpose((2, 0, 1)).astype(np.float32))
        mask_for_paf = torch.from_numpy(mask_for_paf.transpose((2, 0, 1)).astype(np.float32))
        pafs_data = torch.from_numpy(pafs).type(torch.FloatTensor)
        heatmaps_data = torch.from_numpy(heatmaps).type(torch.FloatTensor)
        im_data = torch.from_numpy(im_data)
        depth_data = torch.from_numpy(depth_n)
        seg_data = torch.from_numpy(seg_label_new.transpose((2, 0, 1)).astype(np.float32))
        seg_mask = torch.from_numpy(mask)
        sn_data = torch.from_numpy(sn)
        sn_mask = torch.from_numpy(sn_mask)

        # fake dwt data
        dwt_label = np.zeros_like(seg_label, dtype=np.int64)
        dwt_mask = np.zeros_like(seg_label, dtype=np.float32)
        dwt_label = torch.from_numpy(dwt_label)
        dwt_mask = torch.from_numpy(dwt_mask)

        # =============== sample points =============
        pts = np.zeros([self.max_pts, len(depth_utils.fake_pts())], dtype=np.int64)
        num_pts = 0

        # part one: regular
        max_pts = int(self.max_pts * 0.5)
        sampled_pts = depth_utils.sample_pts(depth_n, mask, max_pts=max_pts, dis_scale=50./self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts += len(sampled_pts)
        pts[:num_pts, :] = sampled_pts

        # part two: regular sampling on human
        max_pts = int((self.max_pts - num_pts) * 0.4)
        sampled_pts = depth_utils.sample_pts(depth_n, seg_label, max_pts=max_pts, dis_scale=10./self.feat_stride,
                                             thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts:num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part three: human and floor
        floor_mask = np.zeros_like(mask, dtype=np.float32)
        h = floor_mask.shape[0]
        floor_mask[0:int(h * 0.3), :] = 1
        floor_mask[h - int(h * 0.2):, :] = 1
        floor_mask = floor_mask * mask
        max_pts = int((self.max_pts - num_pts) * 0.1)
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, floor_mask, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part four: human and background
        max_pts = int((self.max_pts - num_pts) * 0.3)
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, mask, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # part five: human and human
        max_pts = self.max_pts - num_pts
        sampled_pts = depth_utils.sample_from_two_mask(depth_n, seg_label, seg_label, max_pts=max_pts,
                                                       thresh1=self.thresh1, thresh2=self.thresh2)
        num_pts2 = len(sampled_pts)
        pts[num_pts: num_pts + num_pts2, :] = sampled_pts
        num_pts += num_pts2

        # random permutation
        rinds = np.random.permutation(num_pts)
        pts[:num_pts, :] = pts[rinds]
        pts = torch.from_numpy(pts).type(torch.LongTensor)
        # =============== end sample pts ===============
        im_test = np.transpose(im_data.numpy(), [1, 2, 0])
  

        return im_data, heatmaps_data, mask_for_ht, pafs_data, mask_for_paf, seg_data, mask_for_seg
        #
        # gts = torch.stack((depth_data, seg_data, seg_mask), 0) # 3xHxW
        # sn_gts = torch.cat((sn_data, sn_mask), 0)   # 4xHxW
        # gts = torch.cat((gts, sn_gts), 0) # 7xHxW
        # return im_data, gts, pts, num_pts

