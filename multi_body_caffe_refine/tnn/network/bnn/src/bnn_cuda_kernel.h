#ifndef _BNN_CUDA_KERNEL
#define _BNN_CUDA_KERNEL

#ifdef __cplusplus
extern "C" {
#endif


void encode_cols_ongpu(int *columns, unsigned int* columns_binary, int m, int n, cudaStream_t stream);
void binary_gemm_ongpu(unsigned int* weight, unsigned int* columns_binary, float* output_n,
                        int m, int n, int k, float *alphas, cudaStream_t stream);

void encode_rows_ongpu(float* input, unsigned int* output, int count, cudaStream_t stream);
void decode_rows_ongpu(unsigned int* input, float* output, int count, cudaStream_t stream);

void im2col( const float* data_im, const float * runing_mean, const float * gamma_sign, const int channels,
             const int height, const int width,
             const int ksize_h, const int ksize_w, const int pad_h,
             const int pad_w, const int stride_h, const int stride_w,
             const int dilation_h, const int dilation_w, int* data_col, cudaStream_t stream );

#ifdef __cplusplus
}
#endif

#endif
