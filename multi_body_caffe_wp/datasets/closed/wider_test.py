import cPickle
import os

import cv2
import numpy as np
import torch
import torch.utils.data as data

from tnn.datasets.dataloader import sDataLoader


def _factor_closest(num, factor, is_ceil=True):
    num = np.ceil(float(num) / factor) if is_ceil else np.floor(float(num) / factor)
    num = int(num) * factor
    return num


def crop_with_factor(im, max_size=None, factor=32):
    im_shape = im.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])
    im_scale = 1.
    if max_size is not None and im_size_min > max_size:
        im_scale = float(max_size) / im_size_min
        im = cv2.resize(im, None, fx=im_scale, fy=im_scale)

    h, w, c = im.shape
    new_h = _factor_closest(h, factor=factor, is_ceil=True)
    new_w = _factor_closest(w, factor=factor, is_ceil=True)
    im_croped = np.zeros([new_h, new_w, c], dtype=np.uint8)
    im_croped[0:h, 0:w, :] = im

    return im_croped, im_scale


class WiderFaceTest(data.Dataset):
    def __init__(self, root, sub_set, inp_size=256, factor=32):
        self.root = root
        self.inp_size = inp_size
        self.factor = factor

        self.im_root = os.path.join(self.root, 'WIDER_{}'.format(sub_set), 'images')
        self.anno_file = os.path.join(self.root, 'wider_face_split', 'wider_face_{}_bbx_gt.txt'.format(sub_set))

        self.cache_name = os.path.join(self.root, 'wider_{}_cache.pk'.format(sub_set))
        self.annos = self.load_dataset(re_load=True)

    def __len__(self):
        return len(self.annos)

    def __getitem__(self, i):
        im_name, objs = self.annos[i]
        im_name = os.path.join(self.im_root, im_name)

        im_orig = cv2.imread(im_name)
        im_croped, im_scale = crop_with_factor(im_orig, self.inp_size, factor=self.factor)

        gt_boxes = []
        for obj in objs:
            x1, y1, w, h = obj[:4]
            x2 = x1 + w
            y2 = y1 + h
            gt_boxes.append([x1, y1, x2, y2])
        gt_boxes = np.asarray(gt_boxes, dtype=np.float)
        gt_classes = np.zeros(len(gt_boxes), dtype=np.int)
        # gt_boxes *= im_scale

        im_croped = im_croped / 255.

        im_data = torch.from_numpy(im_croped).type(torch.FloatTensor).permute(2, 0, 1)

        return im_orig, im_data, gt_boxes, gt_classes, im_scale

    def load_dataset(self, re_load=False):
        if os.path.isfile(self.cache_name) and not re_load:
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        annos = []
        with open(self.anno_file, 'r') as f:
            line = f.readline()
            while len(line) > 0:
                if line.find('/') >= 0:
                    im_fname = line.strip()
                    num = int(f.readline())
                    objs = []
                    for i in range(num):
                        obj = tuple([float(x) for x in f.readline().strip().split(' ')])
                        objs.append(obj)
                    annos.append((im_fname, objs))
                line = f.readline()

        with open(self.cache_name, 'w') as f:
            cPickle.dump(annos, f)

        return annos


def collate_fn(data):
    im_orig, im_data, gt_boxes, gt_classes, im_scale = zip(*data)
    im_data = torch.stack(im_data, 0)

    return im_orig, im_data, gt_boxes, gt_classes, im_scale


def get_loader(root, sub_set='train', inp_size=512, factor=32, shuffle=True, num_workers=3):
    dataset = WiderFaceTest(root, sub_set, inp_size, factor)

    data_loader = sDataLoader(dataset, 1, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/data/WIDER', 'val', inp_size=1024)
        for i, batch in enumerate(train_data):
            im_orig, im_data, gt_boxes, gt_classes, im_scale = batch
            print im_scale
            print len(im_orig), im_orig[0].shape
            print type(im_data), im_data.size()

            im = np.asarray(im_data[0].permute(1, 2, 0).numpy() * 255, dtype=np.uint8).copy()
            print im.shape
            for bbox in gt_boxes[0]:
                # print bbox
                bbox = tuple([int(round(x)) for x in bbox])
                cv2.rectangle(im, bbox[:2], bbox[2:4], (255, 0, 0), thickness=2)

            cv2.imshow('orgin', im_orig[0])
            cv2.imshow('croped', im)

            cv2.waitKey(0)

    main()


