import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import TimeSeqDataset, TimeSeqDataLoader
import utils.depth as depth_utils


class ScenenetTimeDepth(TimeSeqDataset):
    def __init__(self, root, subset, inp_size=256, feat_stride=4, max_pts=6000, thresh=0.1, training=True):
        self.root = root
        self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.thresh = thresh
        self.max_pts = max_pts

        self.cache_name = os.path.join(self.root, '{}_video_cache.pk'.format(self.subset))
        self.videos = self.load_videos(re_load=False)

    def load_videos(self, re_load=True):
        if not re_load and os.path.isfile(self.cache_name):
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        videos = {}
        for root, dirs, files in os.walk(os.path.join(self.root, self.subset)):
            if 'photo' in dirs:
                video_dir = os.path.join(root, 'photo')
                im_files = [fname for fname in os.listdir(video_dir) if os.path.splitext(fname)[-1] == '.jpg']
                im_files = sorted(im_files, key=lambda name: int(os.path.splitext(name)[0]))
                videos[root] = im_files

        with open(self.cache_name, 'w') as f:
            cPickle.dump(videos, f)
        return videos

    def __len__(self):
        return len(self.videos)

    def seq_len(self, i, rnd=0):
        key = self.videos.keys()[i]
        return len(self.videos[key])

    def get_item(self, vid, fid, clear, rnd=0):
        head = self.videos.keys()[vid]
        im_name = self.videos[head][fid]
        im_name = os.path.splitext(im_name)[0]
        im_file = os.path.join(head, 'photo', '{}.jpg'.format(im_name))
        depth_file = os.path.join(head, 'depth', '{}.png'.format(im_name))
        ins_file = os.path.join(head, 'instance', '{}.png'.format(im_name))
        # print im_file, depth_file, ins_file
        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        # depth = cv2.imread(depth_file)[:, :, 0]
        depth = imread(depth_file, mode='F')
        ins_map = imread(ins_file,  mode='F')

        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        # filter out bright circle
        # bright = np.mean(im.astype(np.float), axis=2) / 255.
        # mask[bright > 0.9] = 0

        # data augmentation
        if self.training:
            flip = rnd % 2 == 0 if rnd is not None else None
            im, trans_params = im_transform.imcv2_affine_trans(im, flip, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=True, max_scale=1.2)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, noise_scale=0.02)
            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)
            ins_map = im_transform.apply_affine(ins_map, *trans_params)
            depth = depth / scale

            # im_shape = im.shape
            # dh = im_shape[0] - self.inp_size[1]
            # dw = im_shape[1] - self.inp_size[0]
            # (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            # (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            # im = im[i0:i1, j0:j1]
            # im = im_transform.imcv2_recolor(im)
            # depth = depth[i0:i1, j0:j1]
            # ins_map = ins_map[i0:i1, j0:j1]
            # mask = mask[i0:i1, j0:j1]

        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            ins_map = ins_map[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        ins_map = cv2.resize(ins_map, tuple(self.out_size))
        depth_data = depth.astype(np.float32)

        mask2 = np.zeros_like(depth_data, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        pts = np.zeros([self.max_pts, 7], dtype=np.int64)
        sampled_pts = depth_utils.sample_pts(depth_data, mask, ins_map, max_pts=self.max_pts, dis_scale=50,
                                             thresh=self.thresh)
        if sampled_pts is None:
            print('[W]: {} sampled_pts is None'.format(im_file))
            # return self.__getitem__(i)
            oh, ow = depth_data.shape[0:2]
            sampled_pts = np.asarray((ow//2, oh//2, ow//2, oh//2, 0, 0, 0), dtype=np.int64)

        num_pts = len(sampled_pts)
        pts[:num_pts, :] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)
        mask = ins_map * mask
        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask)

        return im_data, depth_data, mask_data, pts, num_pts, clear


def get_loader(root, subset, inp_size=256, feat_stride=4, max_pts=6000, thresh=0.1, training=True,
               batch_size=128, shuffle=True, num_workers=3, max_inc_step=2, max_step=50):
    dataset = ScenenetTimeDepth(root, subset, inp_size, feat_stride, max_pts, thresh, training)

    data_loader = TimeSeqDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, max_inc_step=max_inc_step, max_step=max_step)

    return data_loader
