import time

import cv2
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.bbox as bbox_utils
import tnn.utils.im_transform as im_utils
import utils.heatmaps as heatmap_utils
import utils.pose as pose_utils
from face300w_lp import Face300W_LP
from tnn.datasets.dataloader import sDataLoader
from wider import WiderFace


class WiderFace_300W(data.Dataset):
    def __init__(self, wider_dir, wider_sub_set, face_300w_dir, face_300w_subset=None, inp_size=256, feat_stride=4, training=True):
        self.inp_size = np.array([inp_size] * 2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.training = training

        self.heatmap_size = self.inp_size // self.feat_stride

        self.wider = WiderFace(wider_dir, wider_sub_set, inp_size, feat_stride, training)
        self.face300 = Face300W_LP(face_300w_dir, face_300w_subset, inp_size, feat_stride, training)

        self.len_wider = len(self.wider)
        self.len_face300 = len(self.face300)

    def __len__(self):
        return self.len_wider * 2

    def pts_to_center(self, pts):
        mins = np.min(pts, axis=0)
        maxs = np.max(pts, axis=0)
        x1, y1 = mins
        x2, y2 = maxs
        w = x2 - x1
        h = y2 - y1

        center = np.array([(x1 + x2) / 2., (y1 + y2) / 2.], dtype=np.float)
        center[1] -= h * 0.12

        scale = (w + h) / np.random.uniform(190, 195)
        bbox_size = np.sqrt(w * h)

        return center, scale, bbox_size

    def pts_to_bbox(self, pts):
        mins = np.min(pts, axis=0)
        maxs = np.max(pts, axis=0)
        x1, y1 = mins
        x2, y2 = maxs
        w = x2 - x1
        h = y2 - y1

        # expand
        y1 -= h * 0.40
        y2 += h * 0.05
        x1 -= w * 0.05
        x2 += w * 0.05

        return np.array([x1, y1, x2, y2])

    def make_sample(self, ims, pts):
        pts = np.asarray(pts, dtype=np.float)

        shapes = [im.shape for im in ims]
        shapes = np.asarray(shapes, dtype=np.int)
        max_h = np.max(shapes[:, 0])
        max_w = np.max(shapes[:, 1])
        grid = int(np.ceil(np.sqrt(len(ims))))

        im_orig = np.zeros([max_h * grid, max_w * grid, 3], dtype=np.uint8)
        for k, im in enumerate(ims):
            i = k // grid
            j = k % grid

            # crop face
            center, scale, bbox_size = self.pts_to_center(pts[k])
            src_size = [200 * scale] * 2
            dst_size = np.round([min(max_h, max_w, 200 * scale)] * 2).astype(np.int)
            im_croped = pose_utils.crop(im, center, src_size, dst_size)
            pts[k] = pose_utils.transform(pts[k], center, src_size, dst_size, invert=False)

            # attach face
            h, w = im_croped.shape[:2]
            max_offx = max_w - w
            max_offy = max_h - h
            offx = int(np.random.uniform() * max_offx) + max_w * j
            offy = int(np.random.uniform() * max_offy) + max_h * i
            im_orig[offy: (offy + h), offx: (offx + w)] = im_croped
            pts[k, :, 0::2] += offx
            pts[k, :, 1::2] += offy

        sample_shape = im_orig.shape
        dst_shape = [min(sample_shape[:2])] * 2
        sample, trans_params = im_utils.imcv2_affine_trans(im_orig, dst_shape=dst_shape, max_scale=1.2)

        pt_shape = pts.shape
        pts_reshaped = im_utils.offset_boxes(pts.reshape([-1, 2]), *trans_params)
        pts = pts_reshaped.reshape(pt_shape)

        return im_orig, sample, pts

    def __getitem__(self, ind):
        if ind < self.len_wider:
            heatmap = np.zeros([68, self.heatmap_size[1], self.heatmap_size[0]], dtype=np.float32)
            ht_mask = np.zeros([1, 1, 1], dtype=np.float32)
            heatmap_data = torch.from_numpy(heatmap)
            ht_mask_data = torch.from_numpy(ht_mask)

            im_orig, im_data, gt_boxes, gt_classes = self.wider[ind]
            return im_orig, im_data, gt_boxes, gt_classes, heatmap_data, ht_mask_data

        np.random.seed(int(time.time()) % 347 + ind)
        n_faces = np.random.randint(1, 17)
        inds = np.random.choice(self.len_face300, n_faces, replace=False)
        ims = []
        pts = []
        for i in inds:
            obj = self.face300[i][-1]
            im_name = obj['full_image']
            pts.append(obj['points'])
            im = cv2.imread(im_name)
            ims.append(im)

        im_orig, im, pts = self.make_sample(ims, pts)

        # resize
        x_scale = self.inp_size[0] / float(im.shape[1])
        y_scale = self.inp_size[1] / float(im.shape[0])
        im = cv2.resize(im, None, fx=x_scale, fy=y_scale)
        pts[..., 0::2] *= x_scale
        pts[..., 1::2] *= y_scale

        gt_boxes = []
        hts = []
        # im2show = im
        for pt in pts:
            gt_boxes.append(self.pts_to_bbox(pt))

            # im2show = pose_utils.plot_face(im2show, pt)
            # bbox = tuple(int(round(x)) for x in gt_boxes[-1])
            # cv2.rectangle(im2show, bbox[0:2], bbox[2:4], (255, 0, 0), 2)

            pt = np.round(pt / float(self.feat_stride)).astype(np.int)
            heatmaps = []
            for i, (x, y) in enumerate(pt):
                if x < 0 or y < 0 or x >= self.heatmap_size[0] or y >= self.heatmap_size[1]:
                    heatmaps.append(np.zeros(self.heatmap_size, dtype=np.float))
                    continue
                heatmaps.append(heatmap_utils.make_joint_heatmap(self.heatmap_size[0], self.heatmap_size[1],
                                                                 x, y, kernel_size=2, kernel_sigma=1))

            heatmaps = np.asarray(heatmaps, dtype=np.float32)
            hts.append(heatmaps)
        hts = sum(hts)

        # ht2 = np.sum(hts, axis=0)
        # ht2 = cv2.resize(ht2, None, fx=4, fy=4)
        # cv2.imshow('ht', ht2)
        # cv2.imshow('sample', im2show)
        # cv2.imwrite('/data/face/videos/train_sample_{}.jpg'.format(ind), im2show)
        # cv2.waitKey(0)

        heatmap = np.asarray(hts, dtype=np.float32)

        gt_boxes = np.asarray(gt_boxes, dtype=np.float)
        orig_areas = np.abs(gt_boxes[:, 2] - gt_boxes[:, 0]) * np.abs(gt_boxes[:, 3] - gt_boxes[:, 1])  + 1

        gt_boxes = bbox_utils.clip_boxes(gt_boxes, im.shape)
        gt_classes = np.zeros(len(gt_boxes), dtype=np.int)

        areas = (gt_boxes[:, 2] - gt_boxes[:, 0]) * (gt_boxes[:, 3] - gt_boxes[:, 1])
        keep = (orig_areas > 36) & ((areas / orig_areas) > 0.7)
        gt_boxes = gt_boxes[keep]
        gt_classes = gt_classes[keep]

        if self.training:
            im = im_utils.imcv2_recolor(im)
        else:
            im = im / 255.

        im_data = torch.from_numpy(im).type(torch.FloatTensor).permute(2, 0, 1)

        ht_mask = np.ones([1, 1, 1], dtype=np.float32)
        heatmap_data = torch.from_numpy(heatmap)
        ht_mask_data = torch.from_numpy(ht_mask)

        return im_orig, im_data, gt_boxes, gt_classes, heatmap_data, ht_mask_data


def collate_fn(data):
    im_orig, im_data, gt_boxes, gt_classes, heatmap_data, ht_mask_data = zip(*data)

    im_data = torch.stack(im_data, 0)
    heatmap_data = torch.stack(heatmap_data, 0)
    ht_mask_data = torch.stack(ht_mask_data, 0)

    return im_orig, im_data, gt_boxes, gt_classes, heatmap_data, ht_mask_data


def get_loader(wider_dir, wider_sub_set, face_300w_dir, face_300w_subset=None, inp_size=512, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = WiderFace_300W(wider_dir, wider_sub_set, face_300w_dir, face_300w_subset, inp_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/data/WIDER', 'train', '/data/face/300W_LP', None, inp_size=384, feat_stride=4,
                                training=True, batch_size=3)
        for i, batch in enumerate(train_data):
            im_orig, im_data, gt_boxes, gt_classes, heatmap_data, ht_mask_data = batch
            print len(im_orig)
            print type(im_data), im_data.size()

            im = np.asarray(im_data[0].permute(1, 2, 0).numpy() * 255, dtype=np.uint8).copy()
            print im.shape
            for bbox in gt_boxes[0]:
                print bbox
                bbox = tuple([int(round(x)) for x in bbox])
                cv2.rectangle(im, bbox[:2], bbox[2:4], (255, 0, 0), thickness=2)
            cv2.imshow('test', im)
            cv2.imwrite('/data/face/videos/train_wider_{}.jpg'.format(i), im)
            cv2.waitKey(0)

    main()


