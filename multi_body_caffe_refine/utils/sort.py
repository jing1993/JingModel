"""
    SORT: A Simple, Online and Realtime Tracker
    Copyright (C) 2016 Alex Bewley alex@dynamicdetection.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import print_function

from numba import jit
import cv2
import os.path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage import io
from sklearn.utils.linear_assignment_ import linear_assignment
import glob
import time
import argparse
from filterpy.kalman import KalmanFilter

from tnn.utils.cython_bbox import bbox_ious, bbox_overlaps
from utils import mot


Debug_ = True


def _Print(*args, **kwargs):
    if Debug_:
        print(*args, **kwargs)


@jit
def iou(bb_test, bb_gt):
    """
    Computes IUO between two bboxes in the form [x1,y1,x2,y2]
    """
    xx1 = np.maximum(bb_test[0], bb_gt[0])
    yy1 = np.maximum(bb_test[1], bb_gt[1])
    xx2 = np.minimum(bb_test[2], bb_gt[2])
    yy2 = np.minimum(bb_test[3], bb_gt[3])
    w = np.maximum(0., xx2 - xx1)
    h = np.maximum(0., yy2 - yy1)
    wh = w * h
    s1 = (bb_test[2] - bb_test[0]) * (bb_test[3] - bb_test[1])
    s2 = (bb_gt[2] - bb_gt[0]) * (bb_gt[3] - bb_gt[1])

    o = wh / (s1 + s2 - wh)
    o1 = wh / s1
    o2 = wh / s2
    return o, o1, o2


def convert_bbox_to_z(bbox):
    """
    Takes a bounding box in the form [x1,y1,x2,y2] and returns z in the form
      [x,y,s,r] where x,y is the centre of the box and s is the scale/area and r is
      the aspect ratio
    """
    depth = bbox[4]
    w = bbox[2] - bbox[0]
    h = bbox[3] - bbox[1]
    x = bbox[0] + w / 2.
    y = bbox[1] + h / 2.
    s = w * h  # scale is just area
    r = w / float(h)
    return np.array([x, y, s, r, depth]).reshape((5, 1))


def convert_x_to_bbox(x, score=None):
    """
    Takes a bounding box in the centre form [x,y,s,r] and returns it in the form
      [x1,y1,x2,y2] where x1,y1 is the top left and x2,y2 is the bottom right
    """
    w = np.sqrt(x[2] * x[3])
    h = x[2] / w
    if score is None:
        return np.array([x[0] - w / 2., x[1] - h / 2., x[0] + w / 2., x[1] + h / 2., x[4]]).reshape((1, 5))
    else:
        return np.array([x[0] - w / 2., x[1] - h / 2., x[0] + w / 2., x[1] + h / 2., x[4], score]).reshape((1, 6))


class KalmanBoxTracker(object):
    """
    This class represents the internel state of individual tracked objects observed as bbox.
    """
    count = 0

    def __init__(self, bbox):
        """
        Initialises a tracker using initial bounding box.
        """
        # define constant velocity model
        self.kf = KalmanFilter(dim_x=9, dim_z=5)
        self.kf.F = np.array([
            [1, 0, 0, 0, 0, 1, 0, 0, 0],   # x
            [0, 1, 0, 0, 0, 0, 1, 0, 0],   # y
            [0, 0, 1, 0, 0, 0, 0, 1, 0],   # s
            [0, 0, 0, 1, 0, 0, 0, 0, 0],   # r
            [0, 0, 0, 0, 1, 0, 0, 0, 1],   # z
            [0, 0, 0, 0, 0, 1, 0, 0, 0],   # dx
            [0, 0, 0, 0, 0, 0, 1, 0, 0],   # dy
            [0, 0, 0, 0, 0, 0, 0, 1, 0],   # ds
            [0, 0, 0, 0, 0, 0, 0, 0, 1],   # dz
        ])
        self.kf.H = np.array([
            [1, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0],
        ])

        self.kf.R[2:, 2:] *= 10.
        self.kf.R[4, 4] *= 0.1
        self.kf.P[5:, 5:] *= 1000.  # give high uncertainty to the unobservable initial velocities
        # self.kf.P[-1, -1] *= 0.1
        self.kf.P *= 10.
        self.kf.Q[-2, -2] *= 0.01
        self.kf.Q[5:, 5:] *= 0.01

        self.kf.x[:5] = convert_bbox_to_z(bbox)
        self.time_since_update = 0
        self.time_by_tracking = 0

        KalmanBoxTracker.count += 1
        self.id = KalmanBoxTracker.count
        self.history = []
        self.hits = 0
        self.hit_streak = 0
        self.age = 0
        self.score = 0.
        self.occluded = -1

        self.history.append(convert_x_to_bbox(self.kf.x, score=self.score))

    def update(self, bbox, by_tracking=False):
        """
        Updates the state vector with observed bbox.
        """

        self.time_since_update = 0
        if not by_tracking:
            self.hits += 1
            self.hit_streak += 1
            self.time_by_tracking = 0
        else:
            self.time_by_tracking += 1

        self.id = np.abs(self.id)
        self.kf.update(convert_bbox_to_z(bbox))
        # self.history = []
        self.history.append(convert_x_to_bbox(self.kf.x, score=self.score))

    def predict(self):
        """
        Advances the state vector and returns the predicted bounding box estimate.
        """
        if (self.kf.x[7] + self.kf.x[2]) <= 0:
            self.kf.x[7] *= 0.0
        self.kf.predict()
        self.age += 1
        if self.time_since_update > 0:
            self.hit_streak = 0
        self.time_since_update += 1
        self.history.append(convert_x_to_bbox(self.kf.x, score=self.score))
        return self.history[-1]

    def get_state(self):
        """
        Returns the current bounding box estimate.
        """
        return convert_x_to_bbox(self.kf.x, score=self.score)

    def get_depth(self):
        return self.kf.x[4]


@jit
def linear_assignment2(dist):
    trkids = np.argmin(dist, axis=1)
    dets = dict()
    det_dists = dict()
    for i, trkid in enumerate(trkids):
        dets.setdefault(trkid, list())
        det_dists.setdefault(trkid, list())
        dets[trkid].append(i)
        det_dists[trkid].append(dist[i, trkid])

    matched = []
    for trkid, row in det_dists.items():
        det_ind = np.argmin(row)
        matched.append([dets[trkid][det_ind], trkid])
    return np.asarray(matched, dtype=np.int)


def associate_detections_to_trackers(detections, trackers, iou_threshold=0.3):
    """
    Assigns detections to tracked object (both represented as bounding boxes)

    Returns 3 lists of matches, unmatched_detections and unmatched_trackers
    """
    if len(trackers) == 0:
        return np.empty((0, 2), dtype=int), np.arange(len(detections)), np.empty((0, 5), dtype=int)
    iou_matrix = np.zeros((len(detections), len(trackers)), dtype=np.float32)
    odet_matrix = np.zeros((len(detections), len(trackers)), dtype=np.float32)
    otrk_matrix = np.zeros((len(detections), len(trackers)), dtype=np.float32)
    dist_matrix = np.zeros((len(detections), len(trackers)), dtype=np.float32)

    _Print('\n\n===============associate=================')
    for d, det in enumerate(detections):
        for t, trk in enumerate(trackers):
            trk_id = int(trk[-1])
            dist = np.linalg.norm(trk[:5] - det[:5])
            if trk_id < 0:
                dist += 100.
            elif trk[5] > 0:
                dist += 50  # occluded
            _iou, _odet, _otrk = iou(det, trk)

            iou_matrix[d, t] = _iou
            odet_matrix[d, t] = _odet if trk_id > 0 else 0
            otrk_matrix[d, t] = _otrk
            dist_matrix[d, t] = dist if _iou >= iou_threshold or (_iou > 0.15 and _otrk > 0.7) else 10000

            _Print('(%d, %d, %d), %.1f, (%.2f, %.2f, %.2f), (%.1f, %.1f)' % (d, t, trk_id, dist_matrix[d, t], _iou, _odet, _otrk, trk[4], det[4]))
    matched_indices = linear_assignment2(dist_matrix)
    _Print('matched_indices\n', matched_indices)
    # filter out matched with low IOU

    if len(matched_indices) <= 0:
        matches = np.empty([0, 2])
    else:
        matched_ious = iou_matrix[zip(*matched_indices)]
        matched_otrk = otrk_matrix[zip(*matched_indices)]
        matched_odet = odet_matrix[zip(*matched_indices)]
        _Print('matched_ious:', matched_ious)
        _Print('matched_otrk:', matched_otrk)
        _Print('matched_odet:', matched_odet)
        _Print('seg_scores:', detections[matched_indices[:, 0], 6])
        matched_mask = (matched_ious >= iou_threshold) | ((matched_ious > 0.15) & (matched_otrk > 0.7))
        matches = matched_indices[matched_mask]
        if len(matches) <= 0:
            matches = np.empty([0, 2])
        _Print('matches\n', matches)

    overlapped_detections = set(np.where(odet_matrix > 0.5)[0])
    _Print(overlapped_detections)
    _Print(set(matches[:, 0]))
    unmatched_detections = list(set(range(0, len(detections))) - set(matches[:, 0]) - overlapped_detections)
    unmatched_trackers = list(set(range(0, len(trackers))) - set(matches[:, 1]))

    # # filter out matched with low IOU
    # matched_ious = iou_matrix[zip(*matched_indices)]
    # unmatched_mask = matched_ious < iou_threshold
    #
    # unmatched_detections.extend(matched_indices[unmatched_mask, 0])
    # unmatched_trackers.extend(matched_indices[unmatched_mask, 1])
    # matches = matched_indices[np.logical_not(unmatched_mask)]

    if len(matches) == 0:
        matches = np.empty((0, 2), dtype=int)

    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)


class Sort(object):
    def __init__(self, model, max_age=12, min_hits=2):
        """
        Sets key parameters for SORT
        """
        self.model = model

        self.max_age = max_age
        self.min_hits = min_hits
        self.trackers = []
        self.frame_count = 0
        KalmanBoxTracker.count = 0

    def _tracking(self, trks, depth_mask, seg_mask, im_scale, im=None):
        """

        :param trks: [x1, y1, x2, y2, z, id]
        :param im:
        :return: new_trks, scores [N]
        """
        if len(trks) == 0:
            return trks, np.zeros([0])

        scores = np.zeros(len(trks), dtype=np.float) - 1
        tracked_inds = np.where(trks[:, -1] > 0)[0]
        if len(tracked_inds) == 0:
            return trks, scores

        trks = trks.copy()
        tracked_trks = trks[tracked_inds]

        # sampling
        rois = mot.sampling_rois(tracked_trks[:, 0:4], n_sample=20)
        _rois = rois * im_scale
        zs, cls_scores = mot.extract_depth_cls(self.model, _rois, depth_mask, seg_mask)

        # reshape to [N, n_sample, 4]
        N = len(tracked_trks)
        rois = rois.reshape(N, -1, 4)
        zs = zs.reshape(N, -1)

        # depth score
        zs_dists = np.abs(zs - tracked_trks[:, 4:5])
        zs_max = zs_dists.max(1, keepdims=True)
        zs_min = zs_dists.min(1, keepdims=True)
        zs_scores = (np.maximum(zs_max - zs_min, 1e-3)) / np.maximum(zs_dists + zs_max - 2 * zs_min, 1e-3)

        # cls score
        cls_scores = cls_scores.reshape(N, -1)
        cls_scores[cls_scores < 0.5] = 1e-5

        # predictions
        tracked_scores = cls_scores * zs_scores # [N, n_sample]
        scores_sum = tracked_scores.sum(1)
        bboxes_pred = np.sum(rois * tracked_scores[:, :, np.newaxis], axis=1) / scores_sum[:, np.newaxis]
        zs_pred = np.sum(zs * tracked_scores, axis=1) / scores_sum

        trks[tracked_inds, 0:4] = bboxes_pred
        trks[tracked_inds, 4] = zs_pred

        # scores
        tracked_scores = scores_sum / np.maximum(np.sum(cls_scores > 0.3, axis=1), 1)
        scores[tracked_inds] = tracked_scores

        if im is not None:
            im2show = im.copy()
            for _roi in rois.reshape(-1, 4).astype(np.int):
                x1, y1, x2, y2 = _roi
                cv2.rectangle(im2show, (x1, y1), (x2, y2), (255, 0, 0), 1)

            for i, _roi in enumerate(bboxes_pred.reshape(-1, 4).astype(np.int)):
                x1, y1, x2, y2 = _roi
                cv2.rectangle(im2show, (x1, y1), (x2, y2), (0, 0, 255), 2)

            cv2.imshow('rois', im2show)

        return trks, scores

    def update(self, dets, depth_mask, seg_mask, im_scale, im=None):
        self.frame_count += 1
        ret = []

        trks = np.zeros((len(self.trackers), 7))
        # for t, trk in enumerate(trks):
        #     pos = self.trackers[t].history[-1][0]
        #     trk[:5] = pos[:5]
        #     trk[-1] = self.trackers[t].id

        # prediction by kalman filter
        to_del = []
        for t, trk in enumerate(trks):
            prev_pos = self.trackers[t].history[-1][0]
            pos = self.trackers[t].predict()[0]
            if np.any(np.isnan(pos)):
                to_del.append(t)
            trk[:5] = (pos[:5] + prev_pos[:5]) * 0.5
            trk[5] = self.trackers[t].occluded
            trk[-1] = self.trackers[t].id
        for t in reversed(to_del):
            self.trackers.pop(t)
        trks = np.ma.compress_rows(np.ma.masked_invalid(trks))

        # association
        matched, unmatched_dets, unmatched_trks = associate_detections_to_trackers(dets, trks)

        # update matched trackers with assigned detections
        _Print('===============matched===============')
        for d, t in matched:
            det = dets[d]
            tracker = self.trackers[t]
            tracker.update(det)

            _Print(tracker.id, d, det[4:], tracker.time_since_update)

        # tracking by sampling
        new_trks, scores = self._tracking(trks, depth_mask, seg_mask, im_scale, im)
        # update
        _Print('===============tracking===============')
        for t, trk in enumerate(trks):
            new_trk = new_trks[t]
            tracker = self.trackers[t]
            _Print(tracker.id, scores[t], tracker.occluded, trk[4], new_trk[4], tracker.time_since_update)

            if scores[t] > 0.5 and tracker.time_since_update > 0:
                # if tracker.occluded > 0:
                #     new_trk[4] = trk[4]    # DO NOT update depth if occluded
                if tracker.occluded <= 0:
                    tracker.update(new_trk[0:5], by_tracking=True)

        # create and initialise new trackers for unmatched detections
        _Print('===============unmatched_dets===============')
        for i in unmatched_dets:
            seg_score = dets[i, 6]
            if seg_score < 0.6:
                _Print(i, -1, dets[i, 4:])
                continue
            trk = KalmanBoxTracker(dets[i, :])
            self.trackers.append(trk)
            _Print(i, trk.id, dets[i, 4:])

        # all predictions
        preds = np.asarray([trk.get_state()[0] for trk in self.trackers], dtype=np.float32) if len(self.trackers) > 0 else np.empty([0, 5])
        # extract seg_score, z
        bboxes = preds[:, :4].copy()
        bboxes[:] *= im_scale
        zs, cls_scores = mot.extract_depth_cls(self.model, bboxes, depth_mask, seg_mask, seg_threshold=0.3)

        _Print('===============trackers===============')
        i = len(self.trackers)
        heights = preds[:, 3] - preds[:, 1]
        for trk in reversed(self.trackers):
            d = preds[i-1]
            trk.score = cls_scores[i-1]

            overtime = trk.time_since_update > 5 or (trk.occluded and trk.time_since_update > 2)
            if trk.id > 0 and not overtime and trk.time_by_tracking < 30 and trk.score > 0.50 and heights[i-1] > 20:
                _Print(trk.id, trk.time_since_update, trk.hit_streak, trk.hits, trk.age, cls_scores[i - 1], (d[4], zs[i-1][0]))
                ret.append(np.concatenate((d, [trk.id])).reshape(1, -1))
            else:
                trk.id = -np.abs(trk.id)

                if Debug_:
                    ret.append(np.concatenate((d, [trk.id])).reshape(1, -1))
                _Print(trk.id, trk.time_since_update, trk.hit_streak, trk.age, cls_scores[i - 1], (d[4], zs[i-1][0]))

            i -= 1
            # remove dead tracklet
            if trk.time_since_update > 60 or (trk.hits <= 2 and trk.time_since_update > 5):
                self.trackers.pop(i)

        # update occlusion relations
        ids = np.array([trk.id for trk in self.trackers], dtype=np.int)
        tracked_inds = np.where(ids > 0)[0]
        if len(tracked_inds) > 0:
            bboxes = preds[tracked_inds, :4]
            zs = preds[tracked_inds, 5]
            ious = bbox_overlaps(
                np.ascontiguousarray(bboxes, dtype=np.float),
                np.ascontiguousarray(bboxes, dtype=np.float),
            )
            ious[np.arange(0, len(bboxes)), np.arange(0, len(bboxes))] = 0

            max_inds = np.argmax(ious, axis=1)
            max_ious = ious[np.arange(0, len(bboxes)), max_inds]
            for i, ind in enumerate(tracked_inds):
                if max_ious[i] > 0.5:
                    ind2 = tracked_inds[max_inds[i]]
                    z1 = self.trackers[ind].get_depth()
                    z2 = self.trackers[ind2].get_depth()
                    if z1 > z2:
                        self.trackers[ind].occluded = ids[ind2]
                elif max_ious[i] < 0.3:
                    self.trackers[ind].occluded = -1

        if len(ret) > 0:
            return np.concatenate(ret)
        return np.empty((0, 7))


def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='SORT demo')
    parser.add_argument('--display', dest='display', help='Display online tracker output (slow) [False]',
                        action='store_true')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    # all train
    sequences = ['PETS09-S2L1', 'TUD-Campus', 'TUD-Stadtmitte', 'ETH-Bahnhof', 'ETH-Sunnyday', 'ETH-Pedcross2',
                 'KITTI-13', 'KITTI-17', 'ADL-Rundle-6', 'ADL-Rundle-8', 'Venice-2']
    args = parse_args()
    display = True
    phase = 'train'
    total_time = 0.0
    total_frames = 0
    colours = np.random.rand(32, 3)  # used only for display
    if (display):
        if not os.path.exists('mot_benchmark'):
            print(
                '\n\tERROR: mot_benchmark link not found!\n\n    Create a symbolic link to the MOT benchmark\n    (https://motchallenge.net/data/2D_MOT_2015/#download). E.g.:\n\n    $ ln -s /path/to/MOT2015_challenge/2DMOT2015 mot_benchmark\n\n')
            exit()
        plt.ion()
        fig = plt.figure()

    if not os.path.exists('output'):
        os.makedirs('output')

    for seq in sequences:
        mot_tracker = Sort()  # create instance of the SORT tracker
        seq_dets = np.loadtxt('data/%s/det.txt' % (seq), delimiter=',')  # load detections
        with open('output/%s.txt' % (seq), 'w') as out_file:
            print("Processing %s." % (seq))
            for frame in range(int(seq_dets[:, 0].max())):
                frame += 1  # detection and frame numbers begin at 1
                dets = seq_dets[seq_dets[:, 0] == frame, 2:7]
                dets[:, 2:4] += dets[:, 0:2]  # convert to [x1,y1,w,h] to [x1,y1,x2,y2]
                total_frames += 1

                if (display):
                    ax1 = fig.add_subplot(111, aspect='equal')
                    fn = 'mot_benchmark/%s/%s/img1/%06d.jpg' % (phase, seq, frame)
                    im = io.imread(fn)
                    ax1.imshow(im)
                    plt.title(seq + ' Tracked Targets')

                start_time = time.time()
                trackers = mot_tracker.update(dets)
                cycle_time = time.time() - start_time
                total_time += cycle_time

                for d in trackers:
                    print('%d,%d,%.2f,%.2f,%.2f,%.2f,1,-1,-1,-1' % (frame, d[4], d[0], d[1], d[2] - d[0], d[3] - d[1]),
                          file=out_file)
                    if (display):
                        d = d.astype(np.int32)
                        ax1.add_patch(patches.Rectangle((d[0], d[1]), d[2] - d[0], d[3] - d[1], fill=False, lw=3,
                                                        ec=colours[d[4] % 32, :]))
                        ax1.set_adjustable('box-forced')

                if (display):
                    fig.canvas.flush_events()
                    plt.draw()
                    ax1.cla()

    print("Total Tracking took: %.3f for %d frames or %.1f FPS" % (total_time, total_frames, total_frames / total_time))
    if (display):
        print("Note: to get real runtime results run without the option: --display")
