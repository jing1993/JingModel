import os

import cv2
import h5py
import numpy as np
import torch
import torch.utils.data as data
import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader


class RelativePtsDataset(data.Dataset):
    def __init__(self, root, csv_file, inp_size=256, feat_stride=None, out_size=None, training=True):
        self.root = root
        self.csv_file = csv_file
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)

        if out_size is None:
            self.out_size = self.inp_size // feat_stride if feat_stride is not None else None
        else:
            self.out_size = np.array([out_size] * 2, dtype=np.int) \
                if isinstance(out_size, int) else np.asarray(out_size, dtype=np.int)

        self.training = training
        self.im_root = root
        self.cache_name = os.path.join(self.root, '{}_cache.h5'.format(csv_file))
        self.pts, self.n_pts, self.im_files = self.load_inds(re_load=False)

    def load_inds(self, re_load=True):
        if not re_load and os.path.isfile(self.cache_name):
            h5f = h5py.File(self.cache_name, 'r')
            pts = np.array(h5f['pts'])
            n_pts = np.array(h5f['n_pts'])
            im_files = np.array(h5f['im_files'])
            h5f.close()
            return pts, n_pts, im_files

        input_csv = os.path.join(self.root, self.csv_file)
        max_num = 0
        n_images = 0
        with open(input_csv, 'r') as f:
            f.readline()
            while True:
                dummy_info = f.readline()
                if not dummy_info:
                    break
                infos = dummy_info.split(',')
                _, n_point = infos[0], int(infos[2])

                max_num = max(max_num, n_point)
                n_images += 1

                for i in range(n_point):
                    f.readline()
        print('Max n_point: %d' % max_num)

        # second pass: save to hdf5
        pts = np.zeros([n_images, max_num, 5], dtype=np.float)
        n_pts = np.zeros([n_images], dtype=np.int)
        im_files = []
        idx = 0
        with open(input_csv, 'r') as f:
            f.readline()
            while True:
                dummy_info = f.readline()
                if not dummy_info:
                    break
                infos = dummy_info.split(',')
                png, n_point = infos[0], int(infos[2])
                print(png)
                for i in range(n_point):
                    coords = f.readline()
                    y1, x1, y2, x2, rel = coords[:-1].split(',')
                    rel = {'=': 0, '<': -1, '>': 1}[rel]
                    data = np.asarray([x1, y1, x2, y2, rel], dtype=np.float)
                    pts[idx, i, :] = data
                n_pts[idx] = n_point
                im_files.append(png)
                idx += 1

        h5f = h5py.File(self.cache_name, 'w')
        h5f.create_dataset('pts', data=pts)
        h5f.create_dataset('n_pts', data=n_pts)
        h5f.create_dataset('im_files', data=np.asarray(im_files))
        h5f.close()
        return pts, n_pts, im_files

    def __len__(self):
        return len(self.pts)

    def __getitem__(self, i):
        im_file = os.path.join(self.im_root, self.im_files[i])
        pts = self.pts[i]
        n_pts = self.n_pts[i]
        max_pts = len(pts)
        pts = pts[:n_pts]

        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=False)
            pts[:, 0:4] = im_transform.offset_boxes(pts[:, 0:4], *trans_params)
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1]
            pts[:, 0:4:2] -= j0
            pts[:, 1:4:2] -= i0

        keep = (pts[:, 0] >= 0) & (pts[:, 1] >= 0) & (pts[:, 2] >= 0) & (pts[:, 3] >= 0) & \
               (pts[:, 0] < self.inp_size[0]) & (pts[:, 1] < self.inp_size[1]) & \
               (pts[:, 2] < self.inp_size[0]) & (pts[:, 3] < self.inp_size[1])
        pts = pts[keep]
        pts[:, 0:4:2] *= (self.out_size[0] / float(self.inp_size[0]))
        pts[:, 1:4:2] *= (self.out_size[1] / float(self.inp_size[1]))
        n_pts = len(pts)

        new_pts = np.zeros([max_pts, 5], dtype=np.int64)
        new_pts[:n_pts, :] = pts
        pts = torch.from_numpy(new_pts).type(torch.LongTensor)

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        im_data = torch.from_numpy(im_data)

        return im_data, pts, n_pts


def get_loader(root, csv_file, inp_size=256, feat_stride=None, out_size=None, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = RelativePtsDataset(root, csv_file, inp_size, feat_stride, out_size, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
