#include <TH/TH.h>
#include <stdio.h>
#include "bnn_gemmkernel_2x2.h"
#include "bnn_gemmkernel_4x4.h"

void im2col_cpu(const float *data_im, const float *running_mean, const float *gamma_sign, const int channels,
                const int height, const int width,
                const int ksize_h, const int ksize_w, const int pad_h,
                const int pad_w, const int stride_h, const int stride_w,
                const int dilation_h, const int dilation_w, int *data_col);


void encode_cols_cpu(int *columns, unsigned int *columns_binary, int m, int n);


void binary_gemm_cpu(unsigned int *weight, unsigned int *columns_binary, float *output_n,
                     int m, int n, int k, float *alphas);

void binary_gemm_cpu2(long m, long n, long k, float * alphas, unsigned int * a, long lda, unsigned int * b, long ldb, float * c, long ldc);


void real_gemm_cpu(float *weight, float *columns, float *output_n,
                   int m, int n, int k, float *alphas);


/*
 * https://github.com/pytorch/pytorch/blob/master/torch/lib/THNN/generic/SpatialFullConvolution.c#L114
 * https://github.com/pytorch/pytorch/blob/master/torch/lib/TH/generic/THBlas.c#L287
 */
void BinarySpatialConvolution_forward_cpu(
        THFloatTensor *input,
        THFloatTensor *output,
        THIntTensor *weight,
        THIntTensor *columns,
        THFloatTensor *alphas,
        THIntTensor *columns_binary,
        THFloatTensor *running_mean,
        THFloatTensor *gamma_sign,
        int kW, int kH,
        int dW, int dH,
        int padW, int padH) {
    /* Params: */
    int nInputPlane = weight->nDimension == 2 ? weight->size[1] / (kH * kW) * 32 : weight->size[1];
    int nOutputPlane = weight->size[0];

    input = THFloatTensor_newContiguous(input);
    int batch = 1;
    if (input->nDimension == 3) {
        /* Force batch */
        batch = 0;
        THFloatTensor_resize4d(input, 1, input->size[0], input->size[1], input->size[2]);
    }

    long inputWidth = input->size[3];
    long inputHeight = input->size[2];
    long outputWidth = (inputWidth + 2 * padW - kW) / dW + 1;
    long outputHeight = (inputHeight + 2 * padH - kH) / dH + 1;

    /* Batch size + input planes */
    long batchSize = input->size[0];

    /* Resize output */
    THFloatTensor_resize4d(output, batchSize, nOutputPlane, outputHeight, outputWidth);
    /* Resize temporary columns */
    THIntTensor_resize3d( columns, batchSize, kW * kH * nInputPlane, outputHeight * outputWidth );
    THIntTensor_resize3d(columns_binary, batchSize, weight->size[1], outputHeight * outputWidth);

//    /* Resize the weight/columns buffers */
//    if (columns_binary->nDimension != 2 ||
//        columns_binary->size[0] * columns_binary->size[1] < outputHeight * outputWidth * weight->size[1]) {
//        THIntTensor_resize2d(columns_binary, weight->size[1], outputHeight * outputWidth);
//    }


    /* For each elt in batch, do: */
    int elt;
#pragma omp parallel for
    for (elt = 0; elt < batchSize; elt++) {
        /* Helpers */
        THFloatTensor *input_n = THFloatTensor_newSelect(input, 0, elt);
        THFloatTensor *output_n = THFloatTensor_newSelect(output, 0, elt);
        THIntTensor *columns_n = THIntTensor_newSelect(columns, 0, elt);
        THIntTensor *columns_binary_n = THIntTensor_newSelect(columns_binary, 0, elt);

//        /* Matrix mulitply per output: */
//        THFloatTensor_select(input_n, input, 0, elt);
//        THFloatTensor_select(output_n, output, 0, elt);

        /* Extract columns: */
        im2col_cpu(
                THFloatTensor_data(input_n), THFloatTensor_data(running_mean), THFloatTensor_data(gamma_sign),
                nInputPlane, inputHeight, inputWidth, kH, kW, padH, padW, dH, dW,
                1, 1, THIntTensor_data(columns_n)
        );

        /* Encode cols */
        encode_cols_cpu(
                THIntTensor_data(columns_n),
                (unsigned int *) THIntTensor_data(columns_binary_n),
                columns_n->size[0], columns_n->size[1]
        );

        THFloatTensor_zero( output_n );
        THFloatTensor *output2d = THFloatTensor_newWithStorage2d( output_n->storage, output_n->storageOffset,
                                                                  nOutputPlane, -1,
                                                                  outputHeight * outputWidth, -1 );

//        /*
//         * M,N,K are dims of matrix A and B
//         * (see http://docs.nvidia.com/cuda/cublas/#cublas-lt-t-gt-gemm)
//         * row-major to column-major change
//         */
//        int m = weight->size[0];
//        int n = weight->size[1];
//        int k = columns_n->size[1];
//        /*
//         *        printf("%d\n", n);
//         * Do here the binary_gemm call - popcount(XOR) is called here
//         */
//        binary_gemm_cpu(
//                (unsigned int *) THIntTensor_data(weight),
//                (unsigned int *) THIntTensor_data(columns_binary_n),
//                THFloatTensor_data(output2d), m, n, k, THFloatTensor_data(alphas)
//        );

        int m = output2d->size[1];
        int n = output2d->size[0];
        int k = columns_binary_n->size[0];
//        binary_gemm_cpu2(m, n, k, THFloatTensor_data(alphas),
//                         (unsigned int *) THIntTensor_data(columns_binary_n), columns_binary_n->stride[0],
//                         (unsigned int *) THIntTensor_data(weight), weight->stride[0],
//                         (float *)THFloatTensor_data(output2d), output2d->stride[0]);

        binary_gemm_blas(m, n, k, THFloatTensor_data(alphas),
                 (unsigned int *) THIntTensor_data(columns_binary_n), columns_binary_n->stride[0],
                 (unsigned int *) THIntTensor_data(weight), weight->stride[0],
                 (float *)THFloatTensor_data(output2d), output2d->stride[0]);

        /* Free */
        THFloatTensor_free( output2d );
        THFloatTensor_free(input_n);
        THFloatTensor_free(output_n);
        THIntTensor_free(columns_n);
        THIntTensor_free(columns_binary_n);
    }

    if (batch == 0) {
        THFloatTensor_resize3d(output, nOutputPlane, outputHeight, outputWidth);
        THFloatTensor_resize3d(input, nInputPlane, inputHeight, inputWidth);
    }

    THFloatTensor_free(input);
}


void im2col_cpu(const float *data_im, const float *runing_mean, const float *gamma_sign, const int channels,
                const int height, const int width,
                const int ksize_h, const int ksize_w, const int pad_h,
                const int pad_w, const int stride_h, const int stride_w,
                const int dilation_h, const int dilation_w, int *data_col) {
    /*
     * We are going to launch channels * height_col * width_col kernels, each
     * kernel responsible for copying a single-channel grid.
     */
    int height_col = (height + 2 * pad_h - (dilation_h * (ksize_h - 1) + 1))
                     / stride_h + 1;
    int width_col = (width + 2 * pad_w - (dilation_w * (ksize_w - 1) + 1))
                    / stride_w + 1;
    int channel_col = channels * ksize_h * ksize_w;


    int c_out;
//#pragma omp parallel for
    for (c_out = 0; c_out < channel_col; c_out++) {
        int h_out, w_out;
        int w_offset = c_out % ksize_w;
        int h_offset = (c_out / ksize_w) % ksize_h;
        int c_in = c_out / ksize_w / ksize_h;

        for (h_out = 0; h_out < height_col; h_out++) {
            int h_in = h_offset + h_out * stride_h - pad_h;
            for (w_out = 0; w_out < width_col; w_out++) {
                int w_in = w_offset + w_out * stride_w - pad_w;
                int col_index = (c_out * height_col + h_out) * width_col + w_out;
                data_col[col_index] = (h_in < 0 || w_in < 0 || h_in >= height || w_in >= width) ?
                                      0 : (gamma_sign[c_in] > 0 ?
                                           data_im[w_in + width * (h_in + height * c_in)] > runing_mean[c_in] :
                                           data_im[w_in + width * (h_in + height * c_in)] < runing_mean[c_in]);
//			    data_col[col_index] = (h_in < 0 || w_in < 0 || h_in >= height || w_in >= width) ?
//						      0 : data_im[w_in + width * (h_in + height * c_in)] > runing_mean[c_in];
//			    data_col[col_index] = (h_in < 0 || w_in < 0 || h_in >= height || w_in >= width) ?
//						      0 : data_im[w_in + width * (h_in + height * c_in)] > 0;
            }
        }
    }
}


void im2col_cpu2(const float *data_im, const int channels,
                 const int height, const int width,
                 const int ksize_h, const int ksize_w, const int pad_h,
                 const int pad_w, const int stride_h, const int stride_w,
                 const int dilation_h, const int dilation_w, float *data_col) {
    /*
     * We are going to launch channels * height_col * width_col kernels, each
     * kernel responsible for copying a single-channel grid.
     */
    int height_col = (height + 2 * pad_h - (dilation_h * (ksize_h - 1) + 1))
                     / stride_h + 1;
    int width_col = (width + 2 * pad_w - (dilation_w * (ksize_w - 1) + 1))
                    / stride_w + 1;

    int channel_in, h_out, w_out;
    for (channel_in = 0; channel_in < channels; channel_in++) {
        int channel_out = channel_in * ksize_h * ksize_w;
        for (h_out = 0; h_out < height_col; h_out++) {
            int h_in = h_out * stride_h - pad_h;
            for (w_out = 0; w_out < width_col; w_out++) {
                int w_in = w_out * stride_w - pad_w;
                float *data_colp = data_col + (channel_out * height_col + h_out) * width_col + w_out;
                const float *data_imp = data_im + (channel_in * height + h_in) * width + w_in;

                int i, j;
                for (i = 0; i < ksize_h; ++i) {
                    for (j = 0; j < ksize_w; ++j) {
                        int h = h_in + i * dilation_h;
                        int w = w_in + j * dilation_w;
                        *data_colp = (h >= 0 && w >= 0 && h < height && w < width) ?
                                     data_imp[i * dilation_h * width + j * dilation_w] : -1;
                        data_colp += height_col * width_col;
                    }
                }
            }
        }
    }
}


void encode_cols_cpu(int *columns, unsigned int *columns_binary, int m, int n) {
    /* Encode cols */
    int col_bin_m = m / 32;
    int j;
//#pragma omp parallel for
    for (j = 0; j < n; j++) {
        int i, k;
        for (i = 0; i < col_bin_m; i++) {
            int i32 = i * 32;
//			float		num;
            unsigned int rvalue = 0;
            unsigned int sign;

            for (k = 0; k < 32; k++) {
                sign = columns[j + n * (i32 + k)];
//				sign	= (num > 0);
                rvalue |= (sign << k);
            }
            columns_binary[j + n * i] = rvalue;
        }
    }
}


void binary_gemm_cpu(unsigned int *weight, unsigned int *columns_binary, float *output_n,
                     int m, int n, int k, float *alphas) {
//    printf( "m:%d, n:%d, k:%d\n", m, n, k);
    int i;
//#pragma omp parallel for
    for (i = 0; i < m; i++) {
        int j, nn;
        for (j = 0; j < k; j++) {
            register unsigned int Cvalue = 0;
            for (nn = 0; nn < n; nn++) {
                Cvalue += popcount(weight[i * n + nn] ^ columns_binary[nn * k + j]);
            }
            output_n[i * k + j] = (32 * n - 2 * (float) Cvalue) * alphas[i];
        }
    }
}


void binary_gemm_cpu2(long m, long n, long k, float * alphas, unsigned int * a, long lda, unsigned int * b, long ldb, float * c, long ldc) {
//    printf( "lda:%ld, ldb:%ld, ldc:%ld\n", lda, ldb, ldc);
//    printf( "m:%ld, n:%ld, k:%ld\n", m, n, k);
    int i;
    unsigned int * a_ = a;
//#pragma omp parallel for
    for (i = 0; i < m; i++) {
        int j, l;
        unsigned int *b_ = b;
        for (j = 0; j < n; j++) {
            register unsigned int Cvalue = 0;
            for (l = 0; l < k; l++) {
                Cvalue += popcount(b_[l] ^ a_[l * lda + i]);
            }
            b_ += ldb;
            c[j * ldc + i] = (32 * k - 2 * (float) Cvalue) * alphas[j];
        }
//        a_++;
    }
}


void real_gemm_cpu(float *weight, float *columns, float *output_n,
                   int m, int n, int k, float *alphas) {
    int i, j, nn;
    for (i = 0; i < m; i++) {
        for (j = 0; j < k; j++) {
            register float Cvalue = 0;
            for (nn = 0; nn < n; nn++) {
                Cvalue += weight[i * n + nn] * columns[nn * k + j];
            }
            output_n[i * k + j] = Cvalue;
        }
    }
}
