from filterpy.kalman import KalmanFilter
import numpy as np


# def convert_bbox_to_z(bbox):
#     """
#     Takes a bounding box in the form [x,y,w,h] and returns z in the form
#       [x,y,s,r] where x,y is the centre of the box and s is the scale/area and r is
#       the aspect ratio
#     """
#     x, y, w, h = bbox
#     s = w * h  # scale is just area
#     r = w / h
#     return np.array([x, y, s, r]).reshape((4, 1))
#
#
# def convert_x_to_bbox(x, score=None):
#     """
#     Takes a bounding box in the form [x,y,s,r] and returns it in the form
#       [x,y,w,h] where x1,y1 is the top left and x2,y2 is the bottom right
#     """
#     w = np.sqrt(x[2] * x[3])
#     h = x[2] / w
#     if score == None:
#         return np.asarray([x[0], x[1], w, h]).reshape(-1)
#     else:
#         return np.asarray([x[0], x[1], w, h, score]).reshape(-1)


def convert_bbox_to_z(bbox):
    """
    Takes a bounding box in the form [x1,y1,x2,y2] and returns z in the form
      [x,y,s,r] where x,y is the centre of the box and s is the scale/area and r is
      the aspect ratio
    """
    x1, y1, x2, y2 = bbox
    x = (x1 + x2) / 2.
    y = (y1 + y2) / 2.
    w = float(x2 - x1)
    h = float(y2 - y1)
    # x, y, w, h = bbox
    s = w * h  # scale is just area
    r = w / h
    return np.array([x, y, s, r]).reshape((4, 1))


def convert_x_to_bbox(x, score=None):
    """
    Takes a bounding box in the form [x,y,s,r] and returns it in the form
      [x1,y1,x2,y2] where x1,y1 is the top left and x2,y2 is the bottom right
    """
    w = np.sqrt(x[2] * x[3])
    h = x[2] / w
    hw = w / 2.
    hh = h / 2.
    x1, x2 = x[0] - hw, x[0] + hw
    y1, y2 = x[1] - hh, x[1] + hh

    if score == None:
        return np.asarray([x1, y1, x2, y2]).reshape(-1)
    else:
        return np.asarray([x1, y1, x2, y2, score]).reshape(-1)


class KalmanBoxTracker(object):
    """
    This class represents the internel state of individual tracked objects observed as bbox.
    """

    def __init__(self, bbox, velocities=None):
        """
        Initialises a tracker using initial bounding box.
        """
        # define constant velocity model
        self.kf = KalmanFilter(dim_x=7, dim_z=4)
        self.kf.F = np.array(
            [[1, 0, 0, 0, 1, 0, 0],
             [0, 1, 0, 0, 0, 1, 0],
             [0, 0, 1, 0, 0, 0, 1],
             [0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0],
             [0, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 1]])
        self.kf.H = np.array(
            [[1, 0, 0, 0, 0, 0, 0],
             [0, 1, 0, 0, 0, 0, 0],
             [0, 0, 1, 0, 0, 0, 0],
             [0, 0, 0, 1, 0, 0, 0]])

        self.kf.R[2:, 2:] *= 10.
        self.kf.P[4:, 4:] *= 1000.  # give high uncertainty to the unobservable initial velocities
        self.kf.P *= 10.
        self.kf.Q[-1, -1] *= 0.01
        self.kf.Q[4:, 4:] *= 0.01

        self.kf.x[:4] = convert_bbox_to_z(bbox)
        if velocities is not None:
            self.kf.x[4:6] = np.reshape(velocities, [-1, 1])
        self.time_since_update = 0

        self.loc = None
        self.age = 0

    def update(self, bbox):
        """
        Updates the state vector with observed bbox.
        """
        self.time_since_update = 0
        self.kf.update(convert_bbox_to_z(bbox))

    def predict(self):
        """
        Advances the state vector and returns the predicted bounding box estimate.
        """
        if (self.kf.x[6] + self.kf.x[2]) <= 0:
            self.kf.x[6] *= 0.0
        self.kf.predict()
        self.age += 1
        self.time_since_update += 1
        self.loc = convert_x_to_bbox(self.kf.x)
        return self.loc

    @property
    def state(self):
        """
        Returns the current bounding box estimate.
        """
        return convert_x_to_bbox(self.kf.x)

    @property
    def velocity(self):
        return self.kf.x[4:6]
