import torch.nn as nn

def param_fixer(Module, option):
	params = Module.parameters()
	if option == 'fix':
		for param in params:
			param.requires_grad = False
		print "parameters fixed"

	elif option == 'unfix':
		for param in params:
			param.requires_grad = True
		print 'parameters unfixed'
		
	else:
		assert False, 'option can only be \'fix\' or \'unfix\''

