import torch.utils.data
import os
import cv2

from tnn.datasets.dataloader import sDataLoader


class SingleImageFolder(torch.utils.data.Dataset):
    def __init__(self, root, ext='.jpg', need_name=False, preprocess=None):
        self.root = root
        if isinstance(ext, (str, unicode)):
            exts = (ext, )
        else:
            exts = ext
        self.need_name = need_name
        self.preprocess_fun = preprocess

        filenames = [name for name in os.listdir(self.root) if os.path.splitext(name)[-1] in exts]
        self.filenames = sorted(filenames)

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, i):
        im_name = os.path.join(self.root, self.filenames[i])
        im = cv2.imread(im_name)

        if self.preprocess_fun is not None:
            im = self.preprocess_fun(im)

        if not self.need_name:
            return im
        else:
            return im, im_name


def collect_fn(data):
    return data[0] if len(data) == 1 else data


def get_loader(data_dir, ext='.jpg', need_name=False, preprocess=None, batch_size=1, shuffle=False, num_workers=3):
    dataset = SingleImageFolder(data_dir, ext, need_name, preprocess)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collect_fn)
    return data_loader
