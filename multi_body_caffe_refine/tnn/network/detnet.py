import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np

import net_utils


class DetNet(nn.Module):
    def __init__(self, in_channels):
        super(DetNet, self).__init__()

        self.conv = nn.Conv2d(in_channels, 4, 1)

    def forward(self, feature):
        pred = self.conv(feature)

        return pred

    @staticmethod
    def build_loss(pred, label, mask):
        """
        
        :param pred: bsize, 4, h, w
        :param label: bsize, 4, h, w
        :param mask: bsize, 1, h, w
        :return: loss
        """
        pred = pred.permute(0, 2, 3, 1)
        label = label.permute(0, 2, 3, 1)
        mask = mask.permute(0, 2, 3, 1)

        area_x = torch.clamp((pred[:, :, :, 2] - pred[:, :, :, 0]) * (pred[:, :, :, 3] - pred[:, :, :, 1]), min=0.)
        area_g = (label[:, :, :, 2] - label[:, :, :, 0]) * (label[:, :, :, 3] - label[:, :, :, 1])

        w_overlap = torch.clamp(
            torch.min(pred[:, :, :, 3], label[:, :, :, 3]) - torch.max(pred[:, :, :, 1], label[:, :, :, 1]), min=0.
        )
        h_overlap = torch.clamp(
            torch.min(pred[:, :, :, 2], label[:, :, :, 2]) - torch.max(pred[:, :, :, 0], label[:, :, :, 0]), min=0.
        )

        area_overlap = w_overlap * h_overlap
        area_u = area_x + area_g - area_overlap
        iou = area_overlap / torch.clamp(area_u, min=1.)
        iou = torch.clamp(iou, min=1e-6)
        cost = -torch.log(iou)
        loss = torch.mean(cost * mask.type_as(cost)) / (1e-8 + torch.mean(mask))

        return loss

    @staticmethod
    def build_target(gt_bboxes, im_shape):
        h, w = im_shape[:2]
        boxes = np.zeros((h, w, 4), dtype=np.int16)
        mask = np.zeros([h, w, 1], dtype=np.int16)
        for bbox in gt_bboxes:
            box = np.round(np.asarray(bbox)).astype(np.int16)   # x1, y1, x2, y2
            bw = box[2] - box[0]    # w
            bh = box[3] - box[1]    # h

            bw = min(bw, w - box[0])
            bh = min(bh, h - box[1])
            p1 = (box[0], box[1])
            p2 = (box[0] + bw, box[1] + bh)

            bmax = np.array([bh - 1, bw - 1]).reshape(2, 1, 1)

            mesh = np.mgrid[0:bh, 0:bw]
            mesh = np.concatenate([-mesh, bmax - mesh], axis=0)
            mesh = np.rollaxis(mesh, 0, 3).astype(np.int16)

            boxes[p1[1]:p2[1], p1[0]:p2[0]] = mesh
            mask[p1[1]:p2[1], p1[0]:p2[0]] = 1

        return boxes, mask

    @staticmethod
    def invert_bboxes(bboxes):
        """
        
        :param bboxes: bsize, h, w, 4
        :return: 
        """
        h, w = bboxes.shape[1:3]
        coords = np.mgrid[0:h, 0:w]
        coords = np.concatenate([coords, coords], axis=0)
        coords = np.rollaxis(coords, 0, 3).reshape(1, -1, 4)

        batch_size = bboxes.shape[0]
        bbmap = bboxes.reshape(batch_size, -1, 4)
        bbmap = bbmap + coords
        bbmap = np.round(bbmap).astype('int32')

        return bbmap.reshape(batch_size, h, w, 4)