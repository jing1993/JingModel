import cPickle
import os

import cv2
from scipy.misc import imread
import h5py
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.im_transform as im_transform
from tnn.datasets.dataloader import sDataLoader
import utils.depth as depth_utils


class ScenenetDepth(data.Dataset):
    def __init__(self, root, subset, inp_size=256, feat_stride=4, max_pts=6000, thresh=0.1, training=True):
        self.root = root
        self.subset = subset
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.training = training
        self.thresh = thresh
        self.max_pts = max_pts

        self.cache_name = os.path.join(self.root, '{}_cache.pk'.format(self.subset))
        self.im_files = self.load_images(re_load=False)

    def load_images(self, re_load=True):
        if not re_load and os.path.isfile(self.cache_name):
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        im_files = []
        for root, dirs, files in os.walk(os.path.join(self.root, self.subset)):
            for filename in files:
                name, ext = os.path.splitext(filename)
                if ext == '.jpg':
                    im_files.append(os.path.join(root, filename))

        with open(self.cache_name, 'w') as f:
            cPickle.dump(im_files, f)
        return im_files

    def __len__(self):
        return len(self.im_files)

    def __getitem__(self, i):
        im_file = self.im_files[i]
        head, im_name = os.path.split(im_file)
        head = os.path.split(head)[0]
        im_name = os.path.splitext(im_name)[0]
        depth_file = os.path.join(head, 'depth', '{}.png'.format(im_name))
        ins_file = os.path.join(head, 'instance', '{}.png'.format(im_name))

        im = cv2.imread(im_file)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        # depth = cv2.imread(depth_file)[:, :, 0]
        depth = imread(depth_file, mode='F')
        ins_map = imread(ins_file,  mode='F')

        mask = np.zeros_like(depth, dtype=np.float32)
        mask[depth > 0] = 1

        # filter out bright circle
        bright = np.mean(im.astype(np.float), axis=2) / 255.
        mask[bright > 0.95] = 0

        # data augmentation
        if self.training:
            im, trans_params = im_transform.imcv2_affine_trans(im, dst_shape=[self.inp_size[1], self.inp_size[0]],
                                                               rotate=20, max_scale=1.5)
            scale = trans_params[0]
            im = im_transform.imcv2_recolor(im, noise_scale=0.015)
            mask = im_transform.apply_affine(mask, *trans_params)
            depth = im_transform.apply_affine(depth, *trans_params)
            ins_map = im_transform.apply_affine(ins_map, *trans_params)
            depth = depth / scale
        else:
            im_shape = im.shape
            dh = im_shape[0] - self.inp_size[1]
            dw = im_shape[1] - self.inp_size[0]
            (i0, i1) = (dh // 2, self.inp_size[1] + dh // 2)
            (j0, j1) = (dw // 2, self.inp_size[0] + dw // 2)
            im = im[i0:i1, j0:j1].astype(np.float32)
            depth = depth[i0:i1, j0:j1]
            ins_map = ins_map[i0:i1, j0:j1]
            mask = mask[i0:i1, j0:j1]

        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5
        mask = cv2.resize(mask, tuple(self.out_size))
        depth = cv2.resize(depth, tuple(self.out_size))
        ins_map = cv2.resize(ins_map, tuple(self.out_size))
        depth_data = depth.astype(np.float32)

        mask2 = np.zeros_like(depth_data, dtype=np.float32)
        mask2[mask > 0.9] = 1
        mask = mask2

        # normalize depth
        depth_t = depth[mask > 0]
        dmin, dmax = depth_t.min(), depth_t.max()
        depth = (depth - dmin) / (dmax - dmin + 1e-5)

        pts = np.zeros([self.max_pts, 5], dtype=np.int64)
        sampled_pts = depth_utils.sample_pts(depth_data, mask, max_pts=self.max_pts, dis_scale=40, thresh=self.thresh)
        if sampled_pts is None:
            print('[W]: {} sampled_pts is None'.format(i))
            # return self.__getitem__(i)
            oh, ow = depth_data.shape[0:2]
            sampled_pts = np.asarray((ow//2, oh//2, ow//2, oh//2, 0), dtype=np.int64)

        num_pts = len(sampled_pts)
        pts[:num_pts, :] = sampled_pts
        pts = torch.from_numpy(pts).type(torch.LongTensor)

        # get surface normal from depth
        sn = depth_utils.depth_to_normal(depth).astype(np.float32).transpose([2, 0, 1])
        sn_data = torch.from_numpy(sn)

        mask_sn = np.zeros_like(mask)
        # mask_sn = mask

        im_data, depth_data, mask_data = torch.from_numpy(im_data), torch.from_numpy(depth_data), torch.from_numpy(mask_sn)

        return im_data, depth_data, sn_data, mask_data, pts, num_pts


def get_loader(root, subset, inp_size=256, feat_stride=4, max_pts=6000, thresh=0.1, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = ScenenetDepth(root, subset, inp_size, feat_stride, max_pts, thresh, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers)

    return data_loader
