import cv2
import os
import numpy as np


def visualization(IoU_res, Precision_res, Recall_res, IoU_res_boundary, Precision_res_boundary, Recall_res_boundary):
    IoU_mean = []
    Precision_mean = []
    Recall_mean = []
    IoU_boundary_mean = []
    Precision_boundary_mean = []
    Recall_boundary_mean = []
    num_samples = []
    dataset_names = []
    datasets = IoU_res.keys()
    for dataset in datasets:
        if len(IoU_res[dataset]) != 0:
            IoU_mean.append(np.mean(IoU_res[dataset]))
            Precision_mean.append(np.mean(Precision_res[dataset]))
            Recall_mean.append(np.mean(Recall_res[dataset]))
            IoU_boundary_mean.append(np.mean(IoU_res_boundary[dataset]))
            Precision_boundary_mean.append(np.mean(Precision_res_boundary[dataset]))
            Recall_boundary_mean.append(np.mean(Recall_res_boundary[dataset]))
            num_samples.append(len(IoU_res[dataset]))
            dataset_names.append(dataset)

    print('{:12}  {:12}{:12}{:12}{:12}{:12}{:12}'.format('datasets', 'IoU', 'Precision', 'Recall', 'IoU_bound',
                                                         'Prec_bound', 'Recall_bound'))
    print('------------------------------------------------------------------------------------------')
    for i in range(len(dataset_names)):
        print('{:12}  {:.4f}      {:.4f}      {:.4f}      {:.4f}      {:.4f}      {:.4f}'.format(
            dataset_names[i], IoU_mean[i], Precision_mean[i], Recall_mean[i], IoU_boundary_mean[i],
            Precision_boundary_mean[i], Recall_boundary_mean[i]))
    print('------------------------------------------------------------------------------------------')
    IoU_overall = np.sum(np.array(IoU_mean) * np.array(num_samples)) / np.sum(num_samples)
    Precision_overall = np.sum(np.array(Precision_mean) * np.array(num_samples)) / np.sum(num_samples)
    Recall_overall = np.sum(np.array(Recall_mean) * np.array(num_samples)) / np.sum(num_samples)
    IoU_overall_boundary = np.sum(np.array(IoU_boundary_mean) * np.array(num_samples)) / np.sum(num_samples)
    Precision_overall_boundary = np.sum(np.array(Precision_boundary_mean) * np.array(num_samples)) / np.sum(num_samples)
    Recall_overall_boundary = np.sum(np.array(Recall_boundary_mean) * np.array(num_samples)) / np.sum(num_samples)
    print('{:12}  {:.4f}      {:.4f}      {:.4f}      {:.4f}      {:.4f}      {:.4f}'.format(
        'overall', IoU_overall, Precision_overall, Recall_overall, IoU_overall_boundary, Precision_overall_boundary,
        Recall_overall_boundary))


def compute_IoU(gt, pred):
    if np.logical_and(pred != 1, pred != 0).any():
        assert False, 'prediction is not a binary mask.'
    boundary = cv2.dilate(cv2.Laplacian(gt, cv2.CV_64F), kernel=np.ones([5, 5]), iterations=1)
    gt = gt.astype(bool)
    pred = pred.astype(bool)
    boundary = boundary.astype(bool)

    pixel_gt = float(len(np.nonzero(gt)[0]))
    pixel_pred = float(len(np.nonzero(pred)[0]))
    Intersection = float(len(np.nonzero(gt & pred)[0]))
    Union = float(len(np.nonzero(gt | pred)[0]))

    pixel_gt_boundary = float(len(np.nonzero(gt & boundary)[0]))
    pixel_pred_boundary = float(len(np.nonzero(pred & boundary)[0]))
    Intersection_boundary = float(len(np.nonzero(gt & pred & boundary)[0]))
    Union_boundary = float(len(np.nonzero((gt | pred) & boundary)[0]))

    assert Union != 0, 'No union area. Probably ground truth contains no human.'
    return Intersection / Union, Intersection / pixel_pred if pixel_pred != 0 else 0, Intersection / pixel_gt, \
           Intersection_boundary / Union_boundary, Intersection_boundary / pixel_pred_boundary if pixel_pred_boundary != 0 else 0, Intersection_boundary / pixel_gt_boundary


def validation(datasets, res):
    IoU_res = {}
    Precision_res = {}
    Recall_res = {}
    IoU_res_boundary = {}
    Precision_res_boundary = {}
    Recall_res_boundary = {}
    for dataset in datasets:
        print("computing IoU on {}...".format(dataset))
        seg_names = os.listdir('data/segmentation_validation/' + dataset + '/segmentations')
        seg_names.sort()
        preds = res[dataset]
        IoU_res[dataset] = []
        Precision_res[dataset] = []
        Recall_res[dataset] = []
        IoU_res_boundary[dataset] = []
        Precision_res_boundary[dataset] = []
        Recall_res_boundary[dataset] = []

        for i in range(len(seg_names)):
            gt = cv2.imread('data/segmentation_validation/' + dataset + '/segmentations/' + seg_names[i], 0) / 255
            pred = preds[i]
            IoU, Precision, Recall, IoU_boundary, Precision_boundary, Recall_boundary = compute_IoU(gt, pred)
            IoU_res[dataset].append(IoU)
            Precision_res[dataset].append(Precision)
            Recall_res[dataset].append(Recall)
            IoU_res_boundary[dataset].append(IoU_boundary)
            Precision_res_boundary[dataset].append(Precision_boundary)
            Recall_res_boundary[dataset].append(Recall_boundary)

    print('\n')
    visualization(IoU_res, Precision_res, Recall_res, IoU_res_boundary, Precision_res_boundary, Recall_res_boundary)
