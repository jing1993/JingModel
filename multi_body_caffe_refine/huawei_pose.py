import os
import re
import sys
import cv2
import math
import time
import scipy
import argparse
import matplotlib
from torch import np
import pylab as plt
#from joblib import Parallel, delayed
import torch
import torch as T
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from collections import OrderedDict
import post_processing as rtpose_utils
from tnn.network import net_utils
from network.depth_small38 import Model


gpus = [0]
exp_name = 'exp_caffe_v1'
save_dir = 'models/training/{}'.format(exp_name)

ckpt = os.path.join(save_dir, 'ckpt_27.h5')

model = Model(stages=5, have_bn= False, have_bias = True)
net_utils.load_net(ckpt, model)
model = model.cuda(gpus[0])

testdata_dir = 'testdata'
testresult_dir = 'testresult_yogapose'


input_h = 368.0
input_w = 656.0



if __name__ == "__main__":
	if not os.path.isdir(testresult_dir):
		os.mkdir(testresult_dir)

	img_list = os.listdir(testdata_dir)

	for img_name in img_list:
		print 'Processing image: ' + img_name
		frame = cv2.imread(os.path.join(testdata_dir, img_name))

		real_h, real_w = frame.shape[:2]
		ratio_h = real_h / input_h
		ratio_w = real_w / input_w

		ratio = max(ratio_h, ratio_w)

		img_resized = cv2.resize(frame, (int(real_w/ratio), int(real_h/ratio)))

		pad_size = max(368 - int(real_h/ratio), 656 - int(real_w/ratio))

		im_croped = np.pad(img_resized, ([0, pad_size], [0, pad_size], [0, 0]), 'constant')[:368, :656]

		#cv2.imwrite('/data/test/{}.png'.format(img_name), im_croped)
		#scale = 368./frame.shape[0]
		# padding
		#im_croped, im_scale, real_shape = im_transform.crop_with_factor(frame, 368, factor=8, is_ceil=True)

		im_data = im_croped.astype(np.float32) / 256. - 0.5

		im_data = im_data.transpose([2, 0, 1]).astype(np.float32)

		im_data = np.expand_dims(im_data,axis=0)
		
		im_tensor = Variable(torch.from_numpy(im_data), volatile = True).cuda()

		predicted_outputs,_ = model(im_tensor)
		

		PAFs, heatmaps= predicted_outputs[0][0].data.cpu().numpy(), predicted_outputs[1][0].data.cpu().numpy()
		PAFs = np.transpose(PAFs, (1, 2, 0))
		heatmaps = np.transpose(heatmaps, (1, 2, 0))


		canvas, to_plot, candidate, subset = rtpose_utils.paf_to_pose(im_croped, heatmaps, PAFs)
		"""
		heatmap_max = np.max(heatmap[:, :, :18], 2)
		PAF_max = np.max(paf[:, :, :], 2)
		cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_ht.png'),heatmap_max * 255)
		cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_paf.png'),PAF_max*255)
		"""

		cv2.imwrite(os.path.join(testresult_dir, img_name.split('.', 1)[0]+'_4associations.png'),to_plot)

	print 'completed'
