import os
import cv2
import numpy as np
import torch
import torch.utils.data as data
import h5py
import json
from scipy.misc import imread

import tnn.utils.im_transform as im_transform
import tnn.utils.bbox as bbox_utils
import utils.depth as depth_utils
import datasets.data_utils as data_utils
from tnn.utils.cython_bbox import bbox_ious


class CocoCLS(data.Dataset):
    def __init__(self, root, name, inp_size=256, feat_stride=4, max_bboxes=6000, training=True):

        assert name in ('train', 'test', 'val'), name
        assert os.path.isdir(root), root

        self.root = root
        self.img_type = name + '2014'

        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.out_size = self.inp_size // self.feat_stride

        self.max_bboxes = max_bboxes
        self.training = training
        self.mask_root = os.path.join(root, 'mask2014')
        self.seg_root = os.path.join(root, 'mask2014_semantic')
        self.det_root = os.path.join(root, 'person2014_bboxes')

        seg_list = os.listdir(self.seg_root)
        self.seg_names = [v for v in seg_list if name in v]

    def __len__(self):
        return len(self.seg_names)

    def read_images(self, i):
        img_idx = self.seg_names[i][-16:-4]

        img_type = self.img_type

        img = cv2.imread(os.path.join(self.root, 'images', img_type, 'COCO_' + img_type + '_' + img_idx + '.jpg'))
        assert img is not None, 'no image found in: {}'.format(
            os.path.join(self.root, 'images', img_type, 'COCO_' + img_type + '_' + img_idx + '.jpg'))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        mask_file = os.path.join(self.mask_root, '{}_mask_miss_{}.png'.format(img_type, img_idx))
        det_file = os.path.join(self.det_root, '{}_bboxes_{}.json'.format(img_type, img_idx))

        seg_mask = cv2.imread(mask_file, 0)
        with open(det_file, 'r') as f:
            dets = json.load(f)

        return img, seg_mask, dets

    def __getitem__(self, i):

        im, seg_mask, dets = self.read_images(i)
        seg_mask = (seg_mask > 0).astype(np.float32)

        additions = (seg_mask, )

        # =========== data augmentation ==============
        target_scale = float(self.inp_size[0]) / max(im.shape[0:2])
        min_scale = target_scale * 0.7
        max_scale = target_scale * 1.1
        dst_shape = (self.inp_size[1], self.inp_size[0])
        aug_kwargs = {
            'dst_shape': dst_shape,
            'rotate': None,
            'min_scale': min_scale,
            'max_scale': max_scale,
            'replicate_edge': False
        }
        im, additions, trans_params = data_utils.data_augmentation(im, additions, **aug_kwargs)
        additions = additions[0]
        # image recolor
        im = im_transform.imcv2_recolor(im, a=0.1, noise_scale=0.015, blur_ksize=2, blur_sigma=2)
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        # offset bboxes
        dets = np.asarray(dets, dtype=np.int)
        is_crowd = dets[:, 4]
        bboxes = dets[:, :4]
        bboxes[:, 2:4] += bboxes[:, 0:2]    # x1, y1, x2, y2
        bboxes = im_transform.offset_boxes(bboxes, *trans_params[:4], is_pts=False)
        bboxes = bbox_utils.clip_boxes(bboxes, dst_shape)
        s = (bboxes[:, 2] - bboxes[:, 0]) * (bboxes[:, 3] - bboxes[:, 1])
        bboxes = bboxes[(s > 400) & (is_crowd == 0)]

        # =========== end data augmentation ==============

        bboxes = bboxes.astype(np.float)
        seg_mask = cv2.resize(seg_mask, tuple(self.out_size))
        im_data = np.transpose(im, [2, 0, 1]).astype(np.float32) / 255. - 0.5

        im_data = torch.from_numpy(im_data)
        seg_mask = torch.from_numpy(seg_mask)

        # sampling bboxes for cls
        sampled_bboxes = np.zeros([self.max_bboxes, 4], dtype=np.float)
        n_sampled = 0
        if len(bboxes) > 0:
            idxs = np.random.choice(len(bboxes), int(self.max_bboxes * 0.8))
            tmp_bboxes = bboxes[idxs].copy()
            cx = np.mean(tmp_bboxes[:, 0::2], axis=1)
            cy = np.mean(tmp_bboxes[:, 1::2], axis=1)
            hs = (tmp_bboxes[:, 3] - tmp_bboxes[:, 1]) * 0.5
            ws = (tmp_bboxes[:, 2] - tmp_bboxes[:, 0]) * 0.5

            offset_x = np.random.standard_normal(len(tmp_bboxes)) * ws * 0.3
            offset_y = np.random.standard_normal(len(tmp_bboxes)) * hs * 0.3

            hs = np.random.uniform(0.8, 1.2, len(tmp_bboxes)) * hs
            ws = np.random.uniform(0.8, 1.2, len(tmp_bboxes)) * ws
            cx = cx + offset_x
            cy = cy + offset_y

            tmp_bboxes = np.stack((cx - ws, cy - hs, cx + ws, cy + hs), 1)

            tmp_bboxes = bbox_utils.clip_boxes(tmp_bboxes, dst_shape)
            s = (tmp_bboxes[:, 2] - tmp_bboxes[:, 0]) * (tmp_bboxes[:, 3] - tmp_bboxes[:, 1])
            tmp_bboxes = tmp_bboxes[s > 200]

            n_sampled += len(tmp_bboxes)
            sampled_bboxes[:n_sampled] = tmp_bboxes

        n_sampling = len(sampled_bboxes) - n_sampled
        hs = dst_shape[0] / np.random.uniform(3, 6, n_sampling)
        ws = hs * np.random.uniform(0.2, 1, n_sampling)
        for i, ibbox in enumerate(range(n_sampled, len(sampled_bboxes))):
            w = ws[i]
            h = hs[i]
            x = np.random.uniform(1, dst_shape[1] - w - 1)
            y = np.random.uniform(1, dst_shape[0] - h - 1)
            box = np.array([x, y, x + w, y + h], dtype=np.float)
            sampled_bboxes[ibbox] = box

        # rois
        bbox_labels = np.zeros([self.max_bboxes, 1], dtype=np.float32)
        bbox_mask = np.zeros([self.max_bboxes, 1], dtype=np.float32)
        if len(bboxes) > 0:
            ious = bbox_ious(
                np.ascontiguousarray(sampled_bboxes, dtype=np.float),
                np.ascontiguousarray(bboxes, dtype=np.float),
            )
            max_ious = np.max(ious, 1)
        else:
            max_ious = np.zeros([self.max_bboxes])

        bbox_labels[max_ious > 0.55] = 1
        bbox_mask[(max_ious < 0.5) | (max_ious > 0.7)] = 1

        pos_inds = np.where(max_ious > 0.55)[0]
        neg_inds = np.where(max_ious < 0.5)[0]
        if len(pos_inds) > len(neg_inds) + 10:
            bbox_mask[np.random.choice(pos_inds, len(pos_inds) - len(neg_inds) - 10, replace=False)] = 0
        elif len(neg_inds) > len(pos_inds) + 10:
            bbox_mask[np.random.choice(neg_inds, len(neg_inds) - len(pos_inds) - 10, replace=False)] = 0

        rois = torch.from_numpy(sampled_bboxes.astype(np.float32))
        roi_labels = torch.from_numpy(bbox_labels)
        roi_mask = torch.from_numpy(bbox_mask)

        return im_data, rois, roi_labels, roi_mask
