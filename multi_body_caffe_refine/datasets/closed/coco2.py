import math
import os
import os.path

import numpy as np
import torch.utils.data as data
from PIL import Image
from skimage.draw import line_aa
from thirdparty.mscoco.pycocotools.coco import COCO

import utils.heatmaps as heatmaps


class CocoCaptions(data.Dataset):
    def __init__(self, root, annFile, transform=None, target_transform=None):
        self.root = root
        self.coco = COCO(annFile)
        self.ids = self.coco.imgs.keys()
        self.transform = transform
        self.target_transform = target_transform

    def __getitem__(self, index):
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id)
        anns = coco.loadAnns(ann_ids)
        target = [ann['caption'] for ann in anns]

        path = coco.loadImgs(img_id)[0]['file_name']

        img = Image.open(os.path.join(self.root, path)).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.ids)


class CocoDetection(data.Dataset):
    def __init__(self, root, annFile, transform=None, target_transform=None):
        self.root = root
        self.coco = COCO(annFile)
        self.ids = self.coco.imgs.keys()
        self.transform = transform
        self.target_transform = target_transform

    def __getitem__(self, index):
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id)
        target = coco.loadAnns(ann_ids)

        path = coco.loadImgs(img_id)[0]['file_name']

        img = Image.open(os.path.join(self.root, path)).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.ids)


"""
'keypoints':
0  ['nose',
1  'left_eye',
2  'right_eye',
3  'left_ear',
4  'right_ear',
5  'left_shoulder',
6  'right_shoulder',
7  'left_elbow',
8  'right_elbow',
9  'left_wrist',
10  'right_wrist',
11  'left_hip',
12  'right_hip',
13  'left_knee',
14  'right_knee',
15  'left_ankle',
16  'right_ankle'],
17  'neck' is a computed joint
"""


class CocoKeypoints(data.Dataset):
    def __init__(self, root, annFile, num_examples=10, img_resize=256, network_downsampling_rate=8, shuffle=False,
                 transform=None, target_transform=None):
        coco = COCO(annFile)
        self.root = root
        self.coco = COCO(annFile)
        self.transform = transform
        self.target_transform = target_transform
        self.PERSON_CAT_ID = coco.getCatIds(catNms=['person'])
        self.ids = coco.getImgIds(catIds=self.PERSON_CAT_ID)
        self.img_resize = img_resize
        self.network_downsampling_rate = network_downsampling_rate
        if shuffle:
            self.ids = shuffle(self.ids)
        if num_examples > 0:
            self.ids = self.ids[:num_examples]

    def __getitem__(self, index):
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id)
        anns = coco.loadAnns(ann_ids)

        path = coco.loadImgs(img_id)[0]['file_name']
        img = Image.open(os.path.join(self.root, path)).convert('RGB')

        w, h = img.size
        ts = self.img_resize  # target size
        orig_size = img.size
        img = img.resize((ts, ts), Image.BILINEAR)
        output_width = int(1.0 * ts / self.network_downsampling_rate)

        scale_x, scale_y = 1.0 * output_width / w, 1.0 * output_width / h
        for ann in anns:
            kp = ann['keypoints']
            append_neck(kp)
            ann['scaled_keypoints'] = scale_keypoints(kp, scale_x, scale_y)

        part_vec_maps = keypoints_anns_to_part_vectors(anns, (output_width, output_width))
        joints_heat_maps = keypoint_anns_to_heatmaps(anns, (output_width, output_width), 5, 1)

        target = np.concatenate((joints_heat_maps, part_vec_maps), axis=0)

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.ids)


def scale_keypoints(kp, scale_x, scale_y):
    ll = len(kp) // 3
    skp = []
    for i in range(ll):
        x, y, v = kp[i * 3], kp[i * 3 + 1], kp[i * 3 + 2]
        if v > 0:
            x, y = int(x * scale_x), int(y * scale_y)
        skp.extend((x, y, v))
    return skp


def keypoint_anns_to_heatmaps(anns, heatmap_size, kernel_size, sigma):
    """
     anns: coco keypoint annotations for an image.
     heatmap_size: (width, height) of heatmap to generate
     kernel_size: int of the gaussian kernel size
     sigma: sigma of gaussian kernel, the kernel size would be 1+2*sigma pixels in the heatmap

     return: heatmaps of shape (19, heatmap_height, heatmap_width), 19 = background + 17-coco-parts + neck
    """
    kps = 'scaled_keypoints'
    njoints = len(anns[0][kps]) // 3
    joints = [[] for _ in range(njoints)]  # one extra is for neck - middle of two shoulders.
    for ann in anns:
        kp = ann[kps]
        for p in range(njoints):
            x, y, v = kp[3 * p], kp[3 * p + 1], kp[3 * p + 2]
            if v > 0:
                joints[p].append((x, y, sigma))  # sigma 1
    hm = []
    # print('img size: ', img_size, ' hm size ', heatmap_size)
    for parts in joints:
        hm.append(heatmaps.make_joints_heatmap(heatmap_size[0], heatmap_size[1], kernel_size, parts))

    joints = np.stack(hm, axis=0)
    # return joints
    bg = np.max(joints, axis=0)
    bg = (1 - bg) * .2  # scale down background channel so that it doesn't dominate the loss
    return np.insert(joints, 0, bg, axis=0)


lsh, rsh = 5, 6  # index of left and right shoulder


def append_neck(kp):
    lv, rv = kp[3 * lsh + 2], kp[3 * rsh + 2]
    x, y, v = 0, 0, min(lv, rv)
    if v > 0:
        x, y = kp[3 * lsh] + kp[3 * rsh], kp[3 * lsh + 1] + kp[3 * rsh + 1]
        x, y = round(x * .5), round(y * .5)
    kp.extend([x, y, v])
    return kp


# 17 parts
part_segments = [(3, 1), (0, 1), (4, 2), (0, 2), (0, 17),
                 (17, 5), (5, 7), (7, 9),
                 (17, 6), (6, 8), (8, 10),
                 (17, 11), (11, 13), (13, 15),
                 (17, 12), (12, 14), (14, 16)]


def keypoints_anns_to_part_vectors(anns, vec_map_size):
    """
     anns: coco keypoint annotations for an image.
     img_size: (width, height) of image
     vec_map_size: the final vec map size in (width, height)

     return: vector map of shape (num, vec_map_size, vec_map_wsize)
    """
    vmaps = []
    for i in range(len(part_segments)):
        vmap_x = np.zeros([vec_map_size[1], vec_map_size[0]])
        vmap_y = np.zeros([vec_map_size[1], vec_map_size[0]])
        j1, j2 = part_segments[i]
        for ann in anns:
            kp = ann['scaled_keypoints']
            if kp[j1 * 3 + 2] <= 0 or kp[j2 * 3 + 2] <= 0:
                continue
            x1, y1 = kp[j1 * 3], kp[j1 * 3 + 1]
            x2, y2 = kp[j2 * 3], kp[j2 * 3 + 1]
            draw_limb_in_vec_map(vmap_x, vmap_y, x1, y1, x2, y2)
        vmaps.append(vmap_x)
        vmaps.append(vmap_y)
    return np.stack(vmaps, axis=0)


def draw_limb_in_vec_map(vmap_x, vmap_y, x1, y1, x2, y2):
    dx, dy = x2 - x1, y2 - y1
    dist = math.sqrt(dx * dx + dy * dy)
    if dist == 0:
        return
    nx, ny = dx / dist, dy / dist  # normalized diff

    rr, cc, _ = line_aa(y1, x1, y2, x2)
    vmap_x[rr, cc] = nx
    vmap_y[rr, cc] = ny


def draw_parts_in_vec_map(vmap_x, vmap_y, x1, y1, x2, y2):
    """
        vmap: the vec map
    """
    h, w = vmap_x.shape  # height and width

    gap = 1
    min_x = max(min(x1, x2) - gap, 0)
    max_x = min(max(x1, x2) + gap, w)
    min_y = max(min(y1, y2) - gap, 0)
    max_y = min(max(y1, y2) + gap, h)

    dx, dy = x2 - x1, y2 - y1
    dist = math.sqrt(dx * dx + dy * dy)
    if dist == 0:
        return
    nx, ny = dx / dist, dy / dist  # normalized diff

    for y in range(min_y, max_y):
        for x in range(min_x, max_y):
            dx, dy = x - x1, y - y1
            dist = abs(dx * ny - dy * nx)
            if dist <= gap:
                # TODO(ming): handle the case that parts intersects
                vmap_x[y, x] = nx
                vmap_y[y, x] = ny
