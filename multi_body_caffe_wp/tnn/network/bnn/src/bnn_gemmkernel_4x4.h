#include <stdio.h>
#include "gemm_common.h"


int binary_gemm_blas_4x4(BLASLONG bm, BLASLONG bn, BLASLONG bk, FLOAT * alphas, BLASUINT *ba, BLASLONG lda, BLASUINT *bb, BLASLONG ldb, FLOAT *C, BLASLONG ldc) {

//    printf( "lda:%ld, ldb:%ld, ldc:%ld\n", lda, ldb, ldc);
//    printf( "m:%ld, n:%ld, k:%ld\n", bm, bn, bk);

    BLASLONG i, j, k;
    FLOAT *C0, *C1, *C2, *C3;
    FLOAT alpha;
    FLOAT * ptralpha;
    BLASUINT *ptrba, *ptrbb;
    BLASUINT *a_;
    BLASLONG res0, res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12, res13, res14, res15;
    BLASUINT load0, load1, load2, load3, load4, load5, load6, load7, load8, load9, load10, load11, load12, load13, load14,
            load15, load16, load17, load18, load19, load20, load21, load22, load23, load24, load25, load26, load27, load28,
            load29, load30, load31;

    ptralpha = alphas;
    for (j = 0; j < bn / 4; j += 1) {
        C0 = C;
        C1 = C0 + ldc;
        C2 = C1 + ldc;
        C3 = C2 + ldc;

        a_ = ba;

        for (i = 0; i < bm / 4; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            res2 = 0;
            res3 = 0;
            res4 = 0;
            res5 = 0;
            res6 = 0;
            res7 = 0;
            res8 = 0;
            res9 = 0;
            res10 = 0;
            res11 = 0;
            res12 = 0;
            res13 = 0;
            res14 = 0;
            res15 = 0;
            for (k = 0; k < bk / 4; k += 1) {

                // b + 0
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + popcount(load0 ^ load1);

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + popcount(load2 ^ load1);

                load4 = ptrba[lda * 0 + 2];
                res2 = res2 + popcount(load4 ^ load1);

                load6 = ptrba[lda * 0 + 3];
                res3 = res3 + popcount(load6 ^ load1);

                // b + 1
                load3 = ptrbb[0 + 1 * ldb];
                res4 = res4 + popcount(load0 ^ load3);
                res5 = res5 + popcount(load2 ^ load3);
                res6 = res6 + popcount(load4 ^ load3);
                res7 = res7 + popcount(load6 ^ load3);

                // b + 2
                load5 = ptrbb[0 + 2 * ldb];
                res8 = res8 + popcount(load0 ^ load5);
                res9 = res9 + popcount(load2 ^ load5);
                res10 = res10 + popcount(load4 ^ load5);
                res11 = res11 + popcount(load6 ^ load5);

                // b + 3
                load7 = ptrbb[0 + 3 * ldb];
                res12 = res12 + popcount(load0 ^ load7);
                res13 = res13 + popcount(load2 ^ load7);
                res14 = res14 + popcount(load4 ^ load7);
                res15 = res15 + popcount(load6 ^ load7);

                // k + 1
                load8 = ptrba[lda * 1 + 0];
                load9 = ptrbb[1 + 0];
                res0 = res0 + popcount(load8 ^ load9);
                load10 = ptrba[lda * 1 + 1];
                res1 = res1 + popcount(load10 ^ load9);
                load12 = ptrba[lda * 1 + 2];
                res2 = res2 + popcount(load12 ^ load9);
                load14 = ptrba[lda * 1 + 3];
                res3 = res3 + popcount(load14 ^ load9);

                // b + 1
                load11 = ptrbb[1 + 1 * ldb];
                res4 = res4 + popcount(load8 ^ load11);
                res5 = res5 + popcount(load10 ^ load11);
                res6 = res6 + popcount(load12 ^ load11);
                res7 = res7 + popcount(load14 ^ load11);

                // b + 2
                load13 = ptrbb[1 + 2 * ldb];
                res8 = res8 + popcount(load8 ^ load13);
                res9 = res9 + popcount(load10 ^ load13);
                res10 = res10 + popcount(load12 ^ load13);
                res11 = res11 + popcount(load14 ^ load13);

                // b + 3
                load15 = ptrbb[1 + 3 * ldb];
                res12 = res12 + popcount(load8 ^ load15);
                res13 = res13 + popcount(load10 ^ load15);
                res14 = res14 + popcount(load12 ^ load15);
                res15 = res15 + popcount(load14 ^ load15);

                // k + 2
                load16 = ptrba[lda * 2 + 0];
                load17 = ptrbb[2 + 0];
                res0 = res0 + popcount(load16 ^ load17);
                load18 = ptrba[lda * 2 + 1];
                res1 = res1 + popcount(load18 ^ load17);
                load20 = ptrba[lda * 2 + 2];
                res2 = res2 + popcount(load20 ^ load17);
                load22 = ptrba[lda * 2 + 3];
                res3 = res3 + popcount(load22 ^ load17);

                // b + 1
                load19 = ptrbb[2 + 1 * ldb];
                res4 = res4 + popcount(load16 ^ load19);
                res5 = res5 + popcount(load18 ^ load19);
                res6 = res6 + popcount(load20 ^ load19);
                res7 = res7 + popcount(load22 ^ load19);

                // b + 2
                load21 = ptrbb[2 + 2 * ldb];
                res8 = res8 + popcount(load16 ^ load21);
                res9 = res9 + popcount(load18 ^ load21);
                res10 = res10 + popcount(load20 ^ load21);
                res11 = res11 + popcount(load22 ^ load21);

                // b + 3
                load23 = ptrbb[2 + 3 * ldb];
                res12 = res12 + popcount(load16 ^ load23);
                res13 = res13 + popcount(load18 ^ load23);
                res14 = res14 + popcount(load20 ^ load23);
                res15 = res15 + popcount(load22 ^ load23);


                // k + 3
                load24 = ptrba[lda * 3 + 0];
                load25 = ptrbb[3 + 0];
                res0 = res0 + popcount(load24 ^ load25);
                load26 = ptrba[lda * 3 + 1];
                res1 = res1 + popcount(load26 ^ load25);
                load28 = ptrba[lda * 3 + 2];
                res2 = res2 + popcount(load28 ^ load25);
                load30 = ptrba[lda * 3 + 3];
                res3 = res3 + popcount(load30 ^ load25);

                // b + 1
                load27 = ptrbb[3 + 1 * ldb];
                res4 = res4 + popcount(load24 ^ load27);
                res5 = res5 + popcount(load26 ^ load27);
                res6 = res6 + popcount(load28 ^ load27);
                res7 = res7 + popcount(load30 ^ load27);

                // b + 2
                load29 = ptrbb[3 + 2 * ldb];
                res8 = res8 + popcount(load24 ^ load29);
                res9 = res9 + popcount(load26 ^ load29);
                res10 = res10 + popcount(load28 ^ load29);
                res11 = res11 + popcount(load30 ^ load29);

                // b + 3
                load31 = ptrbb[3 + 3 * ldb];
                res12 = res12 + popcount(load24 ^ load31);
                res13 = res13 + popcount(load26 ^ load31);
                res14 = res14 + popcount(load28 ^ load31);
                res15 = res15 + popcount(load30 ^ load31);

                ptrba = ptrba + 4 * lda;
                ptrbb = ptrbb + 4;
            }
            for (k = 0; k < (bk & 3); k += 1) {

                // b + 0
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + popcount(load0 ^ load1);

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + popcount(load2 ^ load1);

                load4 = ptrba[lda * 0 + 2];
                res2 = res2 + popcount(load4 ^ load1);

                load6 = ptrba[lda * 0 + 3];
                res3 = res3 + popcount(load6 ^ load1);

                // b + 1
                load3 = ptrbb[0 + 1 * ldb];
                res4 = res4 + popcount(load0 ^ load3);
                res5 = res5 + popcount(load2 ^ load3);
                res6 = res6 + popcount(load4 ^ load3);
                res7 = res7 + popcount(load6 ^ load3);

                // b + 2
                load5 = ptrbb[0 + 2 * ldb];
                res8 = res8 + popcount(load0 ^ load5);
                res9 = res9 + popcount(load2 ^ load5);
                res10 = res10 + popcount(load4 ^ load5);
                res11 = res11 + popcount(load6 ^ load5);

                // b + 3
                load7 = ptrbb[0 + 3 * ldb];
                res12 = res12 + popcount(load0 ^ load7);
                res13 = res13 + popcount(load2 ^ load7);
                res14 = res14 + popcount(load4 ^ load7);
                res15 = res15 + popcount(load6 ^ load7);

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }

            alpha = ptralpha[0];
            C0[0] = C0[0] + (32 * bk - 2 * (float) res0) * alpha;
            C0[1] = C0[1] + (32 * bk - 2 * (float) res1) * alpha;
            C0[2] = C0[2] + (32 * bk - 2 * (float) res2) * alpha;
            C0[3] = C0[3] + (32 * bk - 2 * (float) res3) * alpha;

            alpha = ptralpha[1];
            C1[0] = C1[0] + (32 * bk - 2 * (float) res4) * alpha;
            C1[1] = C1[1] + (32 * bk - 2 * (float) res5) * alpha;
            C1[2] = C1[2] + (32 * bk - 2 * (float) res6) * alpha;
            C1[3] = C1[3] + (32 * bk - 2 * (float) res7) * alpha;

            alpha = ptralpha[2];
            C2[0] = C2[0] + (32 * bk - 2 * (float) res8) * alpha;
            C2[1] = C2[1] + (32 * bk - 2 * (float) res9) * alpha;
            C2[2] = C2[2] + (32 * bk - 2 * (float) res10) * alpha;
            C2[3] = C2[3] + (32 * bk - 2 * (float) res11) * alpha;

            alpha = ptralpha[3];
            C3[0] = C3[0] + (32 * bk - 2 * (float) res12) * alpha;
            C3[1] = C3[1] + (32 * bk - 2 * (float) res13) * alpha;
            C3[2] = C3[2] + (32 * bk - 2 * (float) res14) * alpha;
            C3[3] = C3[3] + (32 * bk - 2 * (float) res15) * alpha;

            C0 = C0 + 4;
            C1 = C1 + 4;
            C2 = C2 + 4;
            C3 = C3 + 4;
            a_ = a_ + 4;
        }

        for (i = 0; i < (bm & 3); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            res2 = 0;
            res3 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0 * ldb];
                res0 = res0 + popcount(load0 ^ load1);

                load2 = ptrbb[0 + 1 * ldb];
                res1 = res1 + popcount(load0 ^ load2);

                load3 = ptrbb[0 + 2 * ldb];
                res2 = res2 + popcount(load0 ^ load3);

                load4 = ptrbb[0 + 3 * ldb];
                res3 = res2 + popcount(load0 ^ load4);

                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }
            alpha = ptralpha[0];
            C0[0] = C0[0] + (32 * bk - 2 * (float) res0) * alpha;

            alpha = ptralpha[1];
            C1[0] = C1[0] + (32 * bk - 2 * (float) res1) * alpha;

            alpha = ptralpha[2];
            C2[0] = C2[0] + (32 * bk - 2 * (float) res2) * alpha;

            alpha = ptralpha[3];
            C3[0] = C3[0] + (32 * bk - 2 * (float) res3) * alpha;

            C0 = C0 + 1;
            C1 = C1 + 1;
            C2 = C2 + 1;
            C3 = C3 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 2);
        bb = bb + k;
        i = (ldc << 2);
        C = C + i;
        ptralpha = ptralpha + 4;
    }
    for (j = 0; j < (bn & 3); j += 1) {
        C0 = C;
        a_ = ba;
        alpha = ptralpha[0];
        for (i = 0; i < bm / 2; i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            res1 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[lda * 0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + popcount(load0 ^ load1);

                load2 = ptrba[lda * 0 + 1];
                res1 = res1 + popcount(load2 ^ load1);
                ptrba = ptrba + lda;
                ptrbb = ptrbb + 1;
            }

            C0[0] = C0[0] + (32 * bk - 2 * (float) res0) * alpha;
            C0[1] = C0[1] + (32 * bk - 2 * (float) res1) * alpha;
            C0 = C0 + 2;
            a_ = a_ + 2;
        }

        for (i = 0; i < (bm & 1); i += 1) {
            ptrba = a_;
            ptrbb = bb;
            res0 = 0;
            for (k = 0; k < bk; k += 1) {
                load0 = ptrba[0 + 0];
                load1 = ptrbb[0 + 0];
                res0 = res0 + popcount(load0 ^ load1);
                ptrba = ptrba + 1;
                ptrbb = ptrbb + 1;
            }
            C0[0] = C0[0] + (32 * bk - 2 * (float) res0) * alpha;
            C0 = C0 + 1;
            a_ = a_ + 1;
        }
        k = (bk << 0);
        bb = bb + k;
        C = C + ldc;
        ptralpha = ptralpha + 1;
    }
    return 0;
}
