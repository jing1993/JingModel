import torch
import torch.nn as nn
import torch.nn.functional as F

import net_utils


class mCReLU(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, same_padding=False):
        super(mCReLU, self).__init__()
        out1, out2, out3 = out_channels
        self.residual = out1 is not None and out3 is not None
        # momentum = 0.05 if self.training else 0

        if self.residual:
            self.conv0 = net_utils.Conv2d(in_channels, out3, 1, stride, relu=False,
                                          same_padding=same_padding) if in_channels != out3 else None
            self.conv1 = net_utils.Conv2d_BatchNorm(in_channels, out1, 1, same_padding=same_padding)
            self.conv2 = net_utils.Conv2d_CReLU(out1, out2, kernel_size, stride, same_padding=same_padding)
            self.conv3 = net_utils.Conv2d(out2*2, out3, 1, relu=False, same_padding=same_padding)
            self.bn = nn.BatchNorm2d(in_channels)
        else:
            self.conv2 = net_utils.Conv2d_CReLU(
                in_channels, out2, kernel_size, stride, same_padding=same_padding, bias=False)

    def forward(self, x):
        if not self.residual:
            return self.conv2(x)

        x_bn = F.relu(self.bn(x))

        x0 = x if self.conv0 is None else self.conv0(x_bn)

        x1 = self.conv1(x_bn)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)

        x_post = x0 + x3

        return x_post


class Inception(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(Inception, self).__init__()
        # momentum = 0.05 if self.training else 0
        out_1x1, out_3x3, out_5x5, out_pool, out_out = out_channels

        self.conv1 = nn.Sequential(
            net_utils.Conv2d_BatchNorm(in_channels, out_1x1, 1, stride, same_padding=True)
        )

        self.conv2 = nn.Sequential(
            net_utils.Conv2d_BatchNorm(in_channels, out_3x3[0], 1, stride, same_padding=True),
            net_utils.Conv2d_BatchNorm(out_3x3[0], out_3x3[1], 3, 1, same_padding=True)
        )

        self.conv3 = nn.Sequential(
            net_utils.Conv2d_BatchNorm(in_channels, out_5x5[0], 1, stride, same_padding=True),
            net_utils.Conv2d_BatchNorm(out_5x5[0], out_5x5[1], 3, 1, same_padding=True),
            net_utils.Conv2d_BatchNorm(out_5x5[1], out_5x5[2], 3, 1, same_padding=True)
        )

        self.conv_pool = nn.Sequential(
            nn.MaxPool2d(3, stride, padding=1),
            net_utils.Conv2d_BatchNorm(in_channels, out_pool, 1, 1, same_padding=True)
        ) if out_pool > 0 else None

        in_c = out_1x1 + out_3x3[-1] + out_5x5[-1] + out_pool
        self.conv_out = net_utils.Conv2d(in_c, out_out, 1, relu=False, same_padding=True)

        self.conv0 = net_utils.Conv2d(in_channels, out_out, 1, stride, relu=False,
                                      same_padding=True) if in_channels != out_out else None
        self.bn = nn.BatchNorm2d(in_channels)

    def forward(self, x):
        x_bn = F.relu(self.bn(x))

        x1 = self.conv1(x_bn)
        x2 = self.conv2(x_bn)
        x3 = self.conv3(x_bn)
        xs = [x1, x2, x3]
        if self.conv_pool is not None:
            xs.append(self.conv_pool(x_bn))

        x4 = self.conv_out(torch.cat(xs, 1))
        x0 = x if self.conv0 is None else self.conv0(x_bn)
        x_post = x0 + x4

        return x_post


def _make_layers(in_channels, net_cfg):
    layers = []

    if len(net_cfg) > 0 and isinstance(net_cfg[0], list):
        for sub_cfg in net_cfg:
            layer, in_channels = _make_layers(in_channels, sub_cfg)
            layers.append(layer)
    else:
        for item in net_cfg:
            if item == 'M':
                layers.append(nn.MaxPool2d(kernel_size=2, stride=2))
            elif item[0] == 'C':
                out_channels, ksize, stride = item[1:]
                layers.append(mCReLU(in_channels, out_channels, ksize, stride, same_padding=True))
                in_channels = out_channels[2] if out_channels[2] is not None else out_channels[1] * 2
            elif item[0] == 'I':
                out_channels, stride = item[1:]
                layers.append(Inception(in_channels, out_channels, stride))
                in_channels = out_channels[-1]
            else:
                assert False, 'Unknown layer type'

    return nn.Sequential(*layers), in_channels


class PVANetFPN(nn.Module):
    def __init__(self, in_channels=3):
        super(PVANetFPN, self).__init__()

        net_cfgs = [
            # conv1s
            [('C', (None, 16, None), 7, 2), 'M'],
            [('C', (24, 24, 64), 3, 1), ('C', (24, 24, 64), 3, 1), ('C', (24, 24, 64), 3, 1)],
            [('C', (48, 48, 128), 3, 2), ('C', (48, 48, 128), 3, 1), ('C', (48, 48, 128), 3, 1), ('C', (48, 48, 128), 3, 1)],

            [('I', (64, (48, 128), (24, 48, 48), 128, 256), 2),
             ('I', (64, (64, 128), (24, 48, 48), 0, 256), 1),
             ('I', (64, (64, 128), (24, 48, 48), 0, 256), 1),
             ('I', (64, (64, 128), (24, 48, 48), 0, 256), 1)],

            [('I', (64, (96, 192), (32, 64, 64), 128, 384), 2),
             ('I', (64, (96, 192), (32, 64, 64), 0, 384), 1),
             ('I', (64, (96, 192), (32, 64, 64), 0, 384), 1),
             ('I', (64, (96, 192), (32, 64, 64), 0, 384), 1)],
        ]

        self.conv1, conv1_c = _make_layers(in_channels, net_cfgs[0])
        self.conv2, conv2_c = _make_layers(conv1_c, net_cfgs[1])
        self.conv3, conv3_c = _make_layers(conv2_c, net_cfgs[2])

        self.conv4, conv4_c = _make_layers(conv3_c, net_cfgs[3])
        self.conv5, conv5_c = _make_layers(conv4_c, net_cfgs[4])

        # FPN
        self.conv4_5 = net_utils.Conv2d_BatchNorm(conv4_c, conv5_c, 1, same_padding=True)
        self.conv3_5 = net_utils.Conv2d_BatchNorm(conv3_c, conv5_c, 1, same_padding=True)
        self.conv2_5 = net_utils.Conv2d_BatchNorm(conv2_c, conv5_c, 1, same_padding=True)

        self.conv4_up = net_utils.Conv2d_BatchNorm(conv5_c, conv5_c, 1, same_padding=True)
        self.conv3_up = net_utils.Conv2d_BatchNorm(conv5_c, conv5_c, 1, same_padding=True)
        self.conv2_up = net_utils.Conv2d_BatchNorm(conv5_c, conv5_c, 1, same_padding=True)

        self.feat_stride = 4
        self.out_channels = conv5_c

    def forward(self, image):
        conv1 = self.conv1(image)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        conv4 = self.conv4(conv3)
        conv5 = self.conv5(conv4)

        # FPN
        conv4_up = self.conv4_5(conv4) + nn.UpsamplingBilinear2d(scale_factor=2)(conv5)
        conv4_up = self.conv4_up(conv4_up)

        conv3_up = self.conv3_5(conv3) + nn.UpsamplingBilinear2d(scale_factor=2)(conv4_up)
        conv3_up = self.conv3_up(conv3_up)

        conv2_up = self.conv2_5(conv2) + nn.UpsamplingBilinear2d(scale_factor=2)(conv3_up)
        conv2_up = self.conv2_up(conv2_up)

        return conv2_up, conv3_up, conv4_up
