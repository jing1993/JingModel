import cPickle
import os

import cv2
import numpy as np
import torch
import torch.utils.data as data

import tnn.utils.bbox as bbox_utils
import tnn.utils.im_transform as im_utils
from tnn.datasets.dataloader import sDataLoader


class WiderFace(data.Dataset):
    def __init__(self, root, sub_set, inp_size=256, feat_stride=4, training=True):
        self.root = root
        self.inp_size = np.array([inp_size]*2, dtype=np.int) \
            if isinstance(inp_size, int) else np.asarray(inp_size, dtype=np.int)
        self.feat_stride = feat_stride
        self.training = training

        self.heatmap_size = self.inp_size // self.feat_stride

        self.im_root = os.path.join(self.root, 'WIDER_{}'.format(sub_set), 'images')
        self.anno_file = os.path.join(self.root, 'wider_face_split', 'wider_face_{}_bbx_gt.txt'.format(sub_set))

        self.cache_name = os.path.join(self.root, 'wider_{}_cache.pk'.format(sub_set))
        self.annos = self.load_dataset(re_load=True)

    def __len__(self):
        return len(self.annos)

    def __getitem__(self, i):
        im_name, objs = self.annos[i]
        im_name = os.path.join(self.im_root, im_name)

        gt_boxes = []
        for obj in objs:
            x1, y1, w, h = obj[:4]
            x2 = x1 + w
            y2 = y1 + h
            gt_boxes.append([x1, y1, x2, y2])
        gt_boxes = np.asarray(gt_boxes, dtype=np.float)
        gt_classes = np.zeros(len(gt_boxes), dtype=np.int)

        # crop image
        im_orig = cv2.imread(im_name)
        im_shape = im_orig.shape
        dst_shape = [min(im_shape[:2])] * 2
        im, trans_params = im_utils.imcv2_affine_trans(im_orig, dst_shape=dst_shape)
        gt_boxes = im_utils.offset_boxes(gt_boxes, *trans_params)

        # resize
        x_scale = self.inp_size[0] / float(dst_shape[1])
        y_scale = self.inp_size[1] / float(dst_shape[0])
        im = cv2.resize(im, None, fx=x_scale, fy=y_scale)

        gt_boxes[:, 0::2] *= x_scale
        gt_boxes[:, 1::2] *= y_scale
        orig_areas = np.abs(gt_boxes[:, 2] - gt_boxes[:, 0]) * np.abs(gt_boxes[:, 3] - gt_boxes[:, 1]) + 1

        gt_boxes = bbox_utils.clip_boxes(gt_boxes, im.shape)
        areas = (gt_boxes[:, 2] - gt_boxes[:, 0]) * (gt_boxes[:, 3] - gt_boxes[:, 1])
        keep = (orig_areas > 100) & ((areas / orig_areas) > 0.7)
        gt_boxes = gt_boxes[keep]
        gt_classes = gt_classes[keep]

        if self.training:
            im = im_utils.imcv2_recolor(im)
        else:
            im = im / 255.

        im_data = torch.from_numpy(im).type(torch.FloatTensor).permute(2, 0, 1)

        return im_orig, im_data, gt_boxes, gt_classes

    def load_dataset(self, re_load=False):
        if os.path.isfile(self.cache_name) and not re_load:
            with open(self.cache_name, 'r') as f:
                return cPickle.load(f)

        annos = []
        with open(self.anno_file, 'r') as f:
            line = f.readline()
            while len(line) > 0:
                if line.find('/') >= 0:
                    im_fname = line.strip()
                    num = int(f.readline())
                    objs = []
                    for i in range(num):
                        obj = tuple([float(x) for x in f.readline().strip().split(' ')])
                        objs.append(obj)
                    annos.append((im_fname, objs))
                line = f.readline()

        with open(self.cache_name, 'w') as f:
            cPickle.dump(annos, f)

        return annos


def collate_fn(data):
    im_orig, im_data, gt_boxes, gt_classes = zip(*data)
    im_data = torch.stack(im_data, 0)

    return im_orig, im_data, gt_boxes, gt_classes


def get_loader(root, sub_set='train', inp_size=512, feat_stride=4, training=True,
               batch_size=128, shuffle=True, num_workers=3):
    dataset = WiderFace(root, sub_set, inp_size, feat_stride, training)

    data_loader = sDataLoader(dataset, batch_size, shuffle, num_workers=num_workers, collate_fn=collate_fn)

    return data_loader


if __name__ == '__main__':
    def main():
        train_data = get_loader('/data/WIDER',
                                'val', inp_size=512, feat_stride=4,
                                training=True, batch_size=3)
        for i, batch in enumerate(train_data):
            im_orig, im_data, gt_boxes, gt_classes = batch
            print len(im_orig)
            print type(im_data), im_data.size()

            im = np.asarray(im_data[0].permute(1, 2, 0).numpy() * 255, dtype=np.uint8).copy()
            print im.shape
            for bbox in gt_boxes[0]:
                print bbox
                bbox = tuple([int(round(x)) for x in bbox])
                cv2.rectangle(im, bbox[:2], bbox[2:4], (255, 0, 0), thickness=2)

            print im_orig[0].shape
            cv2.imshow('orgin', im_orig[0])
            cv2.imshow('croped', im)

            cv2.waitKey(0)

    main()


