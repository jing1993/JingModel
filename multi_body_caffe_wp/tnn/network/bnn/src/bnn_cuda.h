
void BinarySpatialConvolution_forward(
		THCudaTensor *input,
		THCudaTensor *output,
		THCudaIntTensor *weight,
		THCudaIntTensor *columns,
		THCudaTensor *alphas,
		THCudaIntTensor *columns_binary,
		THCudaTensor *running_mean,
		THCudaTensor * gamma_sign,
		int kW, int kH,
		int dW, int dH,
		int padW, int padH );

void encode_rows(THCudaTensor* input, THCudaIntTensor* output);

void decode_rows(THCudaIntTensor* input, THCudaTensor* output);
